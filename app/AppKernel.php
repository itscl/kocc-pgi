<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new FR3D\LdapBundle\FR3DLdapBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Ensepar\Html2pdfBundle\EnseparHtml2pdfBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),

            new UserBundle\UserBundle(),
            new CultureBundle\CultureBundle(),
            new TechniqueBundle\TechniqueBundle(),
            new ProductionBundle\ProductionBundle(),
            new AgroBundle\AgroBundle(),
            new ReceptionBundle\ReceptionBundle(),
            new ConditionnementBundle\ConditionnementBundle(),
            new VenteBundle\VenteBundle(),
            new GuideUtilisateurBundle\GuideUtilisateurBundle(),
            new DashboardBundle\DashboardBundle(),
            new FicheInterventionBundle\FicheInterventionBundle(),

            /*new Snc\RedisBundle\SncRedisBundle(),*/
            new PointageBundle\PointageBundle(),

            new Lsw\MemcacheBundle\LswMemcacheBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
