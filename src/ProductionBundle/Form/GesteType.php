<?php

namespace ProductionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GesteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cODE')
            ->add('lIBELLE')
            ->add('tYPEGESTE', 'choice', array(
                'choices' => array(
                    'MA' => 'MA',
                    'HO' => 'HO',
                    'MA/HO' => 'MA/HO'),
                'required' => false,
                'expanded' => false
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ProductionBundle\Entity\Geste'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'productionbundle_geste';
    }


}
