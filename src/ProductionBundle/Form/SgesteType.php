<?php

namespace ProductionBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ProductionBundle\Entity\Geste;
use TechniqueBundle\Entity\Lov;

class SgesteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $geste = array();
        $typeinstruction = array();

        $query = $gestes = $this->em->getRepository('ProductionBundle:Geste')->gesteValideList();

        //on cr�e la liste de choix
        foreach($query as $ref){
            $geste[$ref->getId()]=$ref->getLIBELLE();
        }

        $querylov = $gestes = $this->em->getRepository('TechniqueBundle:Lov')->lovLisstValid();

        foreach($querylov as $ref){
            $typeinstruction[$ref->getId()]=$ref->getLIBELLE();
        }

        $builder
            ->add('gESTEID', 'choice', array(
                'choices' => $geste,
            ))
            ->add('cODE')
            ->add('lIBELLE')
            ->add('tYPESGESTE', 'choice', array(
                'choices' => array(
                    'MA' => 'MA',
                    'HO' => 'HO',
                    'MA/HO' => 'MA/HO'),
                'required' => true,
                'expanded' => false
            ))
            ->add('cOMMENTAIRE', 'textarea')
            ->add('tYPEINSTRUCTIONID', 'choice', array(
                'choices' => $typeinstruction
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ProductionBundle\Entity\Sgeste'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'productionbundle_sgeste';
    }


}
