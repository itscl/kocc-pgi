<?php

namespace ProductionBundle\Controller;

use ProductionBundle\Entity\Sgeste;
use ProductionBundle\Form\SgesteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class InterventionController extends Controller
{
    public function indexAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $sgestes = $em->getRepository('ProductionBundle:Sgeste')->sgestesList();

            $entity = new Sgeste();

            $form = $this->createForm(new SgesteType($em), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    $em->persist($entity);
                    $em->flush();
                    return $this->redirect($this->generateUrl('interventionprod'));
                }
            }

            return $this->render('ProductionBundle:Intervention:intervention.html.twig', array(
                'sgeste' => $sgestes,
                'form' => $form->createView()
            ));
        }
    }

    public function modifierAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $sgestes = $em->getRepository('ProductionBundle:Sgeste')->sgestesList();

            $sgeste = $em->getRepository('ProductionBundle:Sgeste')->find($id);

            $edit_form = $this->createForm(new SgesteType($em), $sgeste);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $em->persist($sgeste);
                    $em->flush();
                    return $this->redirect($this->generateUrl('interventionprod'));
                }
            }


            return $this->render('ProductionBundle:Intervention:modifier.html.twig',array(
                'sgestes' => $sgestes,
                'sgeste' => $sgeste,
                'edit_form' =>$edit_form->createView()
            ));


        }
    }

    public function supprimerAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $sgeste = $em->getRepository('ProductionBundle:Sgeste')->find($id);

            $em->remove($sgeste);
            $em->flush();

            return $this->redirect($this->generateUrl('interventionprod'));
        }
    }
}
