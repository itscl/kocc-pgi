<?php

namespace ProductionBundle\Controller;

use ProductionBundle\Entity\Geste;
use ProductionBundle\Form\GesteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GesteController extends Controller
{
    public function indexAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $entity = new Geste();

            $form = $this->createForm(new GesteType(), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    $em->persist($entity);
                    $em->flush();
                    return $this->redirect($this->generateUrl('geste'));
                }
            }

            $gestes = $em->getRepository('ProductionBundle:Geste')->gestesList();

            return $this->render('ProductionBundle:Geste:geste.html.twig', array(
                'geste' => $gestes,
                'form' => $form->createView()
            ));
        }
    }

    public function modifierAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $idgeste = $request->request->get('idgeste');
            $code  = $request->request->get('code');
            $libelle = $request->request->get('libelle');
            $dateinvalide = $request->request->get('dateinvalide');
            $typegeste = $request->request->get('typegeste');

            var_dump($typegeste);
            var_dump($idgeste);
            var_dump($code);
            var_dump($libelle);
            var_dump($dateinvalide);


            if($libelle != null && $typegeste != null)
            {
                $sql = "UPDATE GESTE SET CODE = '$code', LIBELLE = '$libelle', TYPEGESTE = '$typegeste', DATEINVALIDE = '$dateinvalide' WHERE GESTEID = $idgeste";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                echo "Form Submitted Succesfully";
            }

            return $this->render('ProductionBundle:Geste:modifiergeste.html.twig');
        }
    }

    public function supprimerAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('idgeste');

            $geste = $em->getRepository('ProductionBundle:Geste')->find($id);

            $em->remove($geste);
            $em->flush();

            return $this->redirect($this->generateUrl('geste'));
        }
    }
}
