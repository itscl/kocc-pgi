<?php

namespace ProductionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sgeste
 */
class Sgeste
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $gESTEID;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $tYPESGESTE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var int
     */
    private $tYPEINSTRUCTIONID;

    /**
     * @var int
     */
    private $oRIGINSITEID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gESTEID
     *
     * @param integer $gESTEID
     * @return Sgeste
     */
    public function setGESTEID($gESTEID)
    {
        $this->gESTEID = $gESTEID;

        return $this;
    }

    /**
     * Get gESTEID
     *
     * @return integer 
     */
    public function getGESTEID()
    {
        return $this->gESTEID;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return Sgeste
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string 
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return Sgeste
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set tYPESGESTE
     *
     * @param string $tYPESGESTE
     * @return Sgeste
     */
    public function setTYPESGESTE($tYPESGESTE)
    {
        $this->tYPESGESTE = $tYPESGESTE;

        return $this;
    }

    /**
     * Get tYPESGESTE
     *
     * @return string 
     */
    public function getTYPESGESTE()
    {
        return $this->tYPESGESTE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return Sgeste
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return Sgeste
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return Sgeste
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return Sgeste
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string 
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set tYPEINSTRUCTIONID
     *
     * @param integer $tYPEINSTRUCTIONID
     * @return Sgeste
     */
    public function setTYPEINSTRUCTIONID($tYPEINSTRUCTIONID)
    {
        $this->tYPEINSTRUCTIONID = $tYPEINSTRUCTIONID;

        return $this;
    }

    /**
     * Get tYPEINSTRUCTIONID
     *
     * @return integer 
     */
    public function getTYPEINSTRUCTIONID()
    {
        return $this->tYPEINSTRUCTIONID;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return Sgeste
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }
}
