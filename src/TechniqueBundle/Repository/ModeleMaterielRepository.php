<?php

namespace TechniqueBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ModeleMaterielRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ModeleMaterielRepository extends EntityRepository
{
    public function getAllModel()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('mm.id, mm.lIBELLE, mm.tYPEMATERIEL, m.libelle, m.code')
            ->from('TechniqueBundle:ModeleMateriel','mm')
            ->leftJoin('TechniqueBundle:Marque','m', 'WITH', 'mm.mATERIELMARQUEID = m.id')
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();;
        return $query;
    }

    public function getListModel()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('mm')
            ->from('TechniqueBundle:ModeleMateriel','mm')
            ->addOrderBy('mm.lIBELLE')
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();;
        return $query;
    }

    public function getLastId()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('MAX(mm.id) as id')
            ->from('TechniqueBundle:ModeleMateriel','mm')
            ->addOrderBy('mm.lIBELLE')
            ->setMaxResults(1)
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();;
        return $query;
    }
}
