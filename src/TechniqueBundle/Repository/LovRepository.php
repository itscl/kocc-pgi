<?php

namespace TechniqueBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * LovRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LovRepository extends EntityRepository
{
    public function lovLisstValid()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('lov')
            ->from('TechniqueBundle:Lov', 'lov')
            ->andWhere('lov.cATEGORIE =:identified ')
            ->setParameter('identified', 'ITKTYPEINSTRUCTION')
            ->andWhere('lov.dATEINVALIDE IS NULL')
            ->orderBy('lov.lIBELLE')
            ->getQuery()->getResult();

        return $query;
    }

    public function getAllModeApplication()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('lo.id as MODID, lo.lIBELLE as LIBELLE')
            ->from('TechniqueBundle:Lov', 'lo')
            ->Where('lo.dATEINVALIDE IS NULL')
            ->andWhere('lo.cATEGORIE =:mod')
            ->setParameter('mod','MODEAPPLICATIONS' )
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }

    public function getAllModeApp()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('lo.id as MODID, lo.lIBELLE as LIBELLE')
            ->from('TechniqueBundle:Lov', 'lo')
            ->Where('lo.dATEINVALIDE IS NULL')
            ->andWhere('lo.cATEGORIE =:mod')
            ->setParameter('mod','ITKMODEAPPLICATION' )
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }

    public function getAllTMVegetal()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('lo.id as MVID, lo.lIBELLE as LIBELLE')
            ->from('TechniqueBundle:Lov', 'lo')
            ->andWhere('lo.cATEGORIE =:mod')
            ->setParameter('mod','TYPEMVEGETAL' )
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }

    public function getAllUniteIntrant($motcle)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('lo.id as id, lo.lIBELLE as name')
            ->from('TechniqueBundle:Lov', 'lo')
            ->Where('lo.dATEINVALIDE IS NULL')
            ->andWhere('LOWER(lo.lIBELLE)  LIKE :motcle')
            ->andWhere('lo.cATEGORIE =:unite')
            ->setParameter('motcle','%'.$motcle.'%')
            ->setParameter('unite','UNITEPHYTOCULTURE' )
            ->orderBy('lo.lIBELLE', 'ASC')
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }

    public function lovunitemo()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('lov')
            ->from('TechniqueBundle:Lov', 'lov')
            ->andWhere('lov.cATEGORIE =:identified ')
            ->setParameter('identified', 'UNITEMOCULTURE')
            ->andWhere('lov.dATEINVALIDE IS NULL')
            ->orderBy('lov.lIBELLE')
            ->getQuery()->getResult();

        return $query;
    }

    public function lovunitemo1($unite)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('lov')
            ->from('TechniqueBundle:Lov', 'lov')
            ->andWhere('lov.cATEGORIE =:identified ')
            ->setParameter('identified', 'UNITEMOCULTURE')
            ->andWhere('lov.dATEINVALIDE IS NULL')
            ->andWhere('lov.lIBELLE !=:unite')
            ->setParameter('unite', '$unite')
            ->orderBy('lov.lIBELLE')
            ->getQuery()->getResult();

        return $query;
    }
}
