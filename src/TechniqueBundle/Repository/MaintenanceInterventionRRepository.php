<?php

namespace TechniqueBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * MaintenanceInterventionRRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MaintenanceInterventionRRepository extends EntityRepository
{
}
