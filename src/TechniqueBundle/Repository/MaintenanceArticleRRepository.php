<?php

namespace TechniqueBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * MaintenanceArticleRRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MaintenanceArticleRRepository extends EntityRepository
{
}
