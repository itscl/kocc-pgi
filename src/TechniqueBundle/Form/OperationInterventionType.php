<?php

namespace TechniqueBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OperationInterventionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $action = array();

       $query=$this->em->getRepository('TechniqueBundle:Lov')
           ->createQueryBuilder('l')
           ->andWhere('l.cATEGORIE =:categorie')
           ->orderBy('l.lIBELLE')
           ->setParameter('categorie', 'INTERVENTIONMODELE');

        //on cr�e la liste de choix
         foreach($query->getQuery()->getResult() as $ref){
             $action[$ref->getLIBELLE()]=$ref->getLIBELLE();
         }
        $builder
            ->add('dESCRIPTION', 'textarea')
            ->add('aCTION', 'choice', array(
                'choices' => $action,
                'empty_value' => " -- Aucun -- ",
                'required' => true
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TechniqueBundle\Entity\OperationIntervention'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'techniquebundle_operationintervention';
    }


}
