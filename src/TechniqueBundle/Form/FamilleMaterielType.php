<?php

namespace TechniqueBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FamilleMaterielType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lIBELLE')
            ->add('cODE');

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $famillemateriel = $event->getData();
            $form = $event->getForm();
            if($famillemateriel && $famillemateriel->getId() != null) {
                $form->add('dATEINVALIDE', DateType::class, array(
                    'widget' => 'single_text',
                ));
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TechniqueBundle\Entity\FamilleMateriel'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'techniquebundle_famillemateriel';
    }


}
