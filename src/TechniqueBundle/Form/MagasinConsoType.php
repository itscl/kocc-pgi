<?php

namespace TechniqueBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MagasinConsoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $magasin = array();
        $pompiste = array();

        $query=$this->em->getRepository('TechniqueBundle:Magasin')
            ->createQueryBuilder('m')
            ->andWhere('m.dATEINVALIDE IS NULL')
            ->orderBy('m.lIBELLE');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $magasin[$ref->getId()]=$ref->getLIBELLE();
        }

        $query=$this->em->getRepository('TechniqueBundle:Personnel')
            ->createQueryBuilder('p')
            ->andWhere('p.dATEINVALIDE IS NULL')
            ->orderBy('p.pRENOM , p.nOM');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $pompiste[$ref->getId()]=$ref->getPRENOM().' '.$ref->getNOM();
        }

        $builder
           // ->add('dATECONSO', 'text', array('date_widget' => "single_text", 'time_widget' => "single_text"))
            ->add('dATECONSO', 'text', array(
                'required' => true,
                'attr' => array(
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'data-format' => 'dd-mm-yyyy HH:ii',
                ),
            ))
            ->add('pOMPISTEID', 'choice', array(
                'choices' => $pompiste
            ))
            ->add('iNDEXDEBUT')
            ->add('iNDEXFIN')
            ->add('jAUGEDEBUT')
            ->add('jAUGEFIN')
            ->add('cOMMENTAIRE', TextareaType::class, array('required' => false))
            ->add('mAGASINID', 'choice',array(
                'choices' => $magasin
            ));

        $builder->get('dATECONSO')
            ->addModelTransformer(new DateTimeTransformer());

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $magasinconso = $event->getData();
            $form = $event->getForm();
            if($magasinconso && $magasinconso->getId() != null) {
                $form->add('dATEINVALIDE', DateType::class, array(
                    'widget' => 'single_text',
                ));
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TechniqueBundle\Entity\MagasinConso'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'techniquebundle_magasinconso';
    }


}
