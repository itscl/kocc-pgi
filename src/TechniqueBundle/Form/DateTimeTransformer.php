<?php
/**
 * Created by PhpStorm.
 * User: akdaho
 * Date: 21/01/2017
 * Time: 12:22
 */

namespace TechniqueBundle\Form;


use Symfony\Component\Form\DataTransformerInterface;

class DateTimeTransformer implements DataTransformerInterface
{
    /**
     * Transforms an object (DateTime) to a string.
     *
     * @param  DateTime|null $datetime
     * @return string
     */
    public function transform($datetime)
    {
        if (null === $datetime) {
            return '';
        }

        return $datetime->format('d-m-Y H:i:s');
    }

    /**
     * Transforms a string to an object (DateTime).
     *
     * @param  string $datetime
     * @return DateTime|null
     */
    public function reverseTransform($datetime)
    {
        // datetime optional
        if (!$datetime) {
            return;
        }

        return date_create_from_format('d-m-Y H:i:s', $datetime, new \DateTimeZone('UTC'));
    }
}