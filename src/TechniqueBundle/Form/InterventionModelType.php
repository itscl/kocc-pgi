<?php

namespace TechniqueBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InterventionModelType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $action = array();

        $query=$this->em->getRepository('TechniqueBundle:Lov')
            ->createQueryBuilder('l')
            ->andWhere('l.cATEGORIE =:categorie')
            ->orderBy('l.lIBELLE')
            ->setParameter('categorie', 'INTERVENTIONMODELE');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $action[$ref->getLIBELLE()]=$ref->getLIBELLE();
        }
        $builder

            ->add('aCTIONID', 'choice', array(
                'choices' => $action,
                'empty_value' => " -- Aucun -- ",
            ))
            ->add('cOMPTEUR')
            ->add('dESCRIPTION', TextareaType::class)
        ;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $intervention = $event->getData();
            $form = $event->getForm();
            if($intervention && $intervention->getId() != null) {
                $form->add('dATEINVALIDE', DateType::class, array(
                    'widget' => 'single_text',
                ));
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TechniqueBundle\Entity\InterventionModel'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'techniquebundle_interventionmodel';
    }


}
