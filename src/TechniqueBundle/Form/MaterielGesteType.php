<?php

namespace TechniqueBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaterielGesteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $gestes = array();
        $typegeste = array('MA/HO', 'MA');

        $query=$this->em->getRepository('TechniqueBundle:SGeste')
            ->createQueryBuilder('sg')
            ->andWhere('sg.dATEINVALIDE IS NULL')
            ->andWhere('sg.tYPESGESTE IN (:typegeste)')
            ->setParameter('typegeste', $typegeste)
            ->orderBy('sg.lIBELLE');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $gestes[$ref->getId()]=$ref->getLIBELLE();
        }

        $builder
           // ->add('mATERIELID', 'integer');
            ->add('sGESTEID', 'choice', array(
               'choices' => $gestes,
               'required' => false
           ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TechniqueBundle\Entity\MaterielGeste'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'techniquebundle_materielgeste';
    }


}
