<?php

namespace TechniqueBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MagasinType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $typemag = array();
        $ferme = array();

        $query=$this->em->getRepository('TechniqueBundle:Lov')
            ->createQueryBuilder('l')
            ->andWhere('l.cATEGORIE =:categorie')
            ->andWhere('l.dATEINVALIDE IS NULL')
            ->orderBy('l.lIBELLE')
            ->setParameter('categorie', 'MAGASINTYPE');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $typemag[$ref->getLIBELLE()]=$ref->getLIBELLE();
        }

        $query=$this->em->getRepository('CultureBundle:Ferme')
            ->createQueryBuilder('f')
            ->andWhere('f.dATEINVALIDE IS NULL')
            ->orderBy('f.lIBELLE')
        ;

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $ferme[$ref->getId()]=$ref->getlIBELLE();
        }

        $builder
            ->add('cODE')
            ->add('lIBELLE')
            ->add('fERMEID', 'choice', array(
                'choices' => $ferme,
                'required' => false
            ))
            ->add('tYPEMAGASIN', 'choice', array(
                'choices' => $typemag
            ))
            ->add('cAPACITE')
            ->add('qTEMAX')
            ->add('qTEALERTE')
        ;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $magasin = $event->getData();
            $form = $event->getForm();
            if($magasin && $magasin->getId() != null) {
                $form->add('dATEINVALIDE', DateType::class, array(
                    'widget' => 'single_text',
                ));
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TechniqueBundle\Entity\Magasin'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'techniquebundle_magasin';
    }


}
