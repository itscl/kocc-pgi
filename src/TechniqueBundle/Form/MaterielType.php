<?php

namespace TechniqueBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class MaterielType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $modele = array();
        $service = array();
        $sites = array();
        $etat = array();
        $chauffeur = array();
        $carburant = array();
        $article = array();
        $sfamille = array();

        $resModel=$this->em->getRepository('TechniqueBundle:ModeleMateriel')->getListModel();

        //on cr�e la liste de choix
        foreach($resModel as $ref){
            $modele[$ref->getId()]=$ref->getLIBELLE();
        }

        $query=$this->em->getRepository('TechniqueBundle:Service')
            ->createQueryBuilder('s')
            ->orderBy('s.lIBELLE');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $service[$ref->getId()]=$ref->getlIBELLE();
        }

        $query=$this->em->getRepository('CultureBundle:Site')
            ->createQueryBuilder('s')
            ->orderBy('s.lIBELLE');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $sites[$ref->getId()]=$ref->getlIBELLE();
        }

        $query=$this->em->getRepository('TechniqueBundle:Lov')
            ->createQueryBuilder('l')
            ->andWhere('l.cATEGORIE =:categorie')
            ->orderBy('l.lIBELLE')
            ->setParameter('categorie', 'MATERIELETAT');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $etat[$ref->getLIBELLE()]=$ref->getLIBELLE();
        }

        $query=$this->em->getRepository('TechniqueBundle:Personnel')->getChauffeur()
        ;
        //on cr�e la liste de choix
        foreach($query as $ref){
            $chauffeur[$ref['id']]=$ref['nOM'].' '.$ref['pRENOM'];
        }

        $query=$this->em->getRepository('TechniqueBundle:Lov')
            ->createQueryBuilder('l')
            ->andWhere('l.cATEGORIE =:categorie')
            ->orderBy('l.lIBELLE')
            ->setParameter('categorie', 'MATERIELCARBURANT');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $carburant[$ref->getLIBELLE()]=$ref->getLIBELLE();
        }

        $query=$this->em->getRepository('TechniqueBundle:Article')
            ->createQueryBuilder('a')
            ->innerJoin('a.fAMILLEID', 'f')
            ->andWhere('a.dATEINVALIDE IS NULL')
            ->andWhere('f.dATEINVALIDE IS NULL')
            ->andWhere('f.cODE =:code')
            ->setParameter('code', 'CARBURANT');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $article[$ref->getId()]=$ref->getLIBELLE();
        }

        $resSFamille=$this->em->getRepository('TechniqueBundle:SousFamilleMateriel')->getListSFamille();

        //on cr�e la liste de choix
        foreach($resSFamille as $ref){
            $sfamille[$ref->getId()]=$ref->getLibelle();
        }


        $builder
            ->add('cODE')
            ->add('lIBELLE')
            ->add('cOUTHEURE')
            ->add('cOUTHECTARE')
            ->add('mODELEID','choice', array(
                'choices' => $modele,
                'empty_value' => " -- Choisir un modele -- ",
                'required' => true
            ))
            ->add('iMMATRICULATION')
            ->add('eTAT', 'choice', array(
                'choices' => $etat,
                'required' => false
            ))
            ->add('cHAUFFEURID', 'choice', array(
                'choices' => $chauffeur,
                'required' => false
            ))
            ->add('cARBURANT', 'choice', array(
                'choices' => $carburant
            ))
            ->add('nOSERIE')
            ->add('tOUR')
            ->add('vOLUME')
            ->add('pOIDS')
            ->add('cONSMIN')
            ->add('cONSMAX')
            ->add('tRACTION', 'choice', array(
                'choices' => array(
                    '1' => 'Materiel de traction',
                    '2' => 'Materiel d\'attelage',
                    '3' => 'Materiel autonome',),
                'required' => true,
                'expanded' => false
            ))
            ->add('cOMMENTAIRE')
            ->add('dEBITCHANTIERHAJ')
            ->add('dEBITCHANTIERHHA')
            ->add('uPDATEDATE')
            ->add('uPDATEUSER')
            ->add('uNITECONSO')
            ->add('aRTICLEID', 'choice', array(
                'choices' => $article,
                'required' => false,
            ))
            ->add('cODEIMMO')
            ->add('sERVICEID', 'choice', array(
                'choices' => $service,
                'required' => false
            ))
            ->add('cAPACITERESERVOIR')
            ->add('sITEID', 'choice', array(
                'choices' => $sites,
                'required' => false
            ))
            ->add('dATEMISESERVICE', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd-mm-yyyy'
                ]
            ))
           ->add('sOUSFAMILLEID', 'choice', array(
               'choices' => $sfamille,
               'empty_value' => " -- Choisir une sous famille -- ",
           ))
        ;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $materiel = $event->getData();
            $form = $event->getForm();
            if($materiel && $materiel->getId() != null) {
                $form->add('dATEINVALIDE', DateType::class, array(
                    'widget' => 'single_text',
                ));
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TechniqueBundle\Entity\Materiel'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'techniquebundle_materiel';
    }


}
