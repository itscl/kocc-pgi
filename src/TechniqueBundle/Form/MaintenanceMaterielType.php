<?php

namespace TechniqueBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaintenanceMaterielType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mATERIELID', 'hidden')
            ->add('cOMPTEUR','integer', array(
                'required' => true,
            ))
           // ->add('dATEMAINTENANCE')
            ->add('dATEMAINTENANCE', 'date', array(
               'widget' => 'single_text',
               'required' => true,
               'data' => new \DateTime(),
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd-mm-yyyy'
                ]
            ))
            ->add('cPTPROCHAIN', 'integer', array(
                'required' => true,
            ))
            ->add('nOMMECANICIEN', 'text', array(
                'required' => true,
            ))
            ->add('dATEENTREE', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'required' => true,
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd-mm-yyyy'
                ]
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TechniqueBundle\Entity\MaintenanceMateriel'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'techniquebundle_maintenancemateriel';
    }


}
