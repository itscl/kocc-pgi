<?php

namespace TechniqueBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaterielConsoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $magasins =array();
        $materiel = array();
        $chauffeur = array();

        $query=$this->em->getRepository('TechniqueBundle:MagasinConso')->getAllMagConso()
            ;
        //on cr�e la liste de choix
        foreach($query as $ref){
            $id = $ref['id'];
            $dateconso = $ref['dATECONSO'];

            $libelle = '('.$ref['cODE'].')'.' '.$dateconso->format('d-m-Y h:i:s').' '.'('.$ref['id'].')';

            $magasins[$id]=$libelle;
        }
       // exit;
        $query=$this->em->getRepository('TechniqueBundle:Materiel')
            ->createQueryBuilder('m')
            ->orderBy('m.lIBELLE');

        //var_dump($query1->getQuery()->getResult()); exit;

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $materiel[$ref->getId()]=$ref->getlIBELLE();
        }

        $query=$this->em->getRepository('TechniqueBundle:Materiel')
            ->createQueryBuilder('m')
            ->orderBy('m.cODE');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $materiel[$ref->getId()]=$ref->getCODE().'-'.$ref->getlIBELLE();
        }

        $query=$this->em->getRepository('TechniqueBundle:Personnel')
            ->createQueryBuilder('p')
            ->orderBy('p.nOM, p.pRENOM');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $chauffeur[$ref->getId()]=$ref->getNOM().' '.$ref->getPRENOM();
        }

        $builder
            ->add('mAGASINCONSOID', 'choice', array(
                'choices' => $magasins ,'required' => false
            ))
            ->add('dATECONSO', 'hidden')
            ->add('qTE')
            ->add('cTR')
            ->add('pLEIN', 'checkbox', array(
                'required' => false,
            ))
            ->add('cHAUFFEURID', 'choice', array(
                'choices' => $chauffeur,  'required' => false
            ))
            ->add('cOMMENTAIRE', TextareaType::class, array('required' => false))
            ->add('mATERIELCONSOPID', 'hidden')
            ->add('mATERIELID', 'choice', array(
                 'choices' => $materiel,  'required' => false
            ))        ;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $materielconso = $event->getData();
            $form = $event->getForm();
            if($materielconso && $materielconso->getId() != null) {
                $form->add('dATECONSO', 'datetime', array('widget' => 'single_text','input' =>'datetime',
                    'format' => 'yyyy-MM-dd H:mm'))
                ;
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TechniqueBundle\Entity\MaterielConso'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'techniquebundle_materielconso';
    }
}
