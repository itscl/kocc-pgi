<?php

namespace TechniqueBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SousFamilleMaterielType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $famille = array();

        $result=$this->em->getRepository('TechniqueBundle:FamilleMateriel')->getAllFamilleMateriel();

        //var_dump($result); exit;
        foreach($result as $ref){
            $famille[$ref->getId()]=$ref->getlIBELLE();
        }

        $builder
            ->add('cODE')
            ->add('lIBELLE')
            ->add('fAMILLEMATERIELID', 'choice', array(
                'choices' => $famille,
                'empty_value' => " -- Aucun -- ",
            ))
        ;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $sousfamillemateriel = $event->getData();
            $form = $event->getForm();
            if($sousfamillemateriel && $sousfamillemateriel->getId() != null) {
                $form->add('dATEINVALIDE', DateType::class, array(
                    'widget' => 'single_text',
                ));
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TechniqueBundle\Entity\SousFamilleMateriel'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'techniquebundle_sousfamillemateriel';
    }


}
