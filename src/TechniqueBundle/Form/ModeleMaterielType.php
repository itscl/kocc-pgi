<?php

namespace TechniqueBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModeleMaterielType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $marque = array();
        $typemateriel = array();

        $query1=$this->em->getRepository('TechniqueBundle:Marque')
            ->createQueryBuilder('m')
            ->orderBy('m.libelle');

        //on cr�e la liste de choix
        foreach($query1->getQuery()->getResult() as $ref){
            $marque[$ref->getId()]=$ref->getLibelle();
        }

        $query=$this->em->getRepository('TechniqueBundle:Lov')
            ->createQueryBuilder('l')
            ->andWhere('l.cATEGORIE =:categorie')
            ->orderBy('l.lIBELLE')
            ->setParameter('categorie', 'TYPEMATERIEL');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $typemateriel[$ref->getLIBELLE()]=$ref->getLIBELLE();
        }

        $builder
            ->add('mATERIELMARQUEID', 'choice', array(
                'choices' => $marque,
                'empty_value' => " -- Aucun -- ",
            ))
            ->add('tYPEMATERIEL', 'choice', array(
                'choices' => $typemateriel,
                'empty_value' => " -- Aucun -- ",
            ))
            ->add('lIBELLE')
            ->add('dESCRIPTION', TextareaType::class, array('required' => false))
        ;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $modelemat = $event->getData();
            $form = $event->getForm();
            if($modelemat && $modelemat->getId() != null) {
                $form->add('dATEINVALIDE', DateType::class, array(
                    'widget' => 'single_text',
                ));
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TechniqueBundle\Entity\ModeleMateriel'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'techniquebundle_modelemateriel';
    }


}
