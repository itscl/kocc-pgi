<?php

namespace TechniqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TechniqueBundle\Entity\DocumentModele;
use TechniqueBundle\Form\DocumentModeleType;

class DocumentModeleController extends Controller
{
    public function ajoutdocumentmodele1Action()
    {

    }

    public function ajoutdocumentmodeleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $modele = $em->getRepository('TechniqueBundle:ModeleMateriel')->find($id);

            $idmarque = $modele->getmATERIELMARQUEID();

            $marque = $em->getRepository('TechniqueBundle:Marque')->find($idmarque);

            $entity = new DocumentModele();

            $form = $this->createForm(new DocumentModeleType(), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    if($form->get('dOCUMENT')->getData() != null)
                    {
                        $entity->setMODELE($id);
                        $em->persist($entity);
                        $em->flush();

                        return $this->redirect($this->generateUrl('ajoutdocumentmodele', array('id' => $id)));
                    }
                    else
                    {
                        return $this->redirect($this->generateUrl('ajoutplanintervention', array('id' => $id)));
                    }
                }
            }

            $cptinterventions = $em->getRepository('TechniqueBundle:CompteurIntervention')->findBymODELEID($id, array
            ('cOMPTEUR' => 'ASC'));

            $arrayopinterventions = array();

            foreach ($cptinterventions as $r)
            {
                $cptsid = $r->getId();
                $opinterventions = $em->getRepository('TechniqueBundle:OperationIntervention')->findBycOMPTEURINTERVENTIONID($cptsid);

                $arrayopinterventions[] = $opinterventions;
            }

            $docmodel= $em->getRepository('TechniqueBundle:DocumentModele')->findBymODELE($id);

            //var_dump($cptinterventions); exit;

            return $this->render('TechniqueBundle:DocumentModele:ajoutdocumentmodele.html.twig', array(
                'modele' => $modele,
                'marque' => $marque,
                'form' => $form->createView(),
                'entity' => $entity,
                'docmodel' => $docmodel,
                'cptinterventions' => $cptinterventions,
                'arrayopinterventions' => $arrayopinterventions
            ));

        }
    }

    public function supprimerdocmodelAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $idoc = $request->request->get('iddoc');

            print($idoc);

            $sql = "DELETE FROM DOCUMENT WHERE ID = $idoc";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            echo "Form Submitted Succesfully";

            return $this->render('TechniqueBundle:MaterielOR:ajax.html.twig');
        }
    }
}
