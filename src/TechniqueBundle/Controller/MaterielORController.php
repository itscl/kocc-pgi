<?php

namespace TechniqueBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TechniqueBundle\Entity\DocumentModele;
use TechniqueBundle\Entity\FamilleMateriel;
use TechniqueBundle\Entity\InterventionArticle;
use TechniqueBundle\Entity\InterventionModel;
use TechniqueBundle\Entity\Lov;
use TechniqueBundle\Entity\Marque;
use TechniqueBundle\Entity\MaterielGeste;
use TechniqueBundle\Entity\ModeleMateriel;
use TechniqueBundle\Entity\SousFamilleMateriel;
use TechniqueBundle\Form\DocumentModeleType;
use TechniqueBundle\Form\FamilleMaterielType;
use TechniqueBundle\Form\InterventionArticleType;
use TechniqueBundle\Form\InterventionModelType;
use TechniqueBundle\Form\LovType;
use TechniqueBundle\Form\MarqueType;
use TechniqueBundle\Form\MaterielGesteType;
use TechniqueBundle\Form\MaterielType;
use TechniqueBundle\Form\ModeleMaterielType;
use TechniqueBundle\Form\SousFamilleMaterielType;

class MaterielORController extends Controller
{
    public function materielorAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();

            $entity = new Marque();
            $entity1 = new FamilleMateriel();
            $entity2 = new SousFamilleMateriel();
            $entity3 = new Lov();

            $user = $this->getUser()->getName();

            $form = $this->createForm(new MarqueType(), $entity);
            $form1 = $this->createForm(new FamilleMaterielType(), $entity1);
            $form2 = $this->createForm(new SousFamilleMaterielType($em), $entity2);
            $form3 = $this->createForm(new LovType(), $entity3);

            if ($request->isMethod('post')) {
                $form->handleRequest($request);
                $form1->handleRequest($request);
                $form2->handleRequest($request);
                $form3->handleRequest($request);

                if ($form->isValid()) {
                    $em->persist($entity);
                    $em->flush();
                    return $this->redirect($this->generateUrl('materielor'));
                }

                if ($form1->isValid())
                {
                    $entity1->setUSERUPDADE($user);
                    $em->persist($entity1);
                    $em->flush();
                    return $this->redirect($this->generateUrl('materielor'));
                }

                if ($form2->isValid())
                {
                    $entity2->setUSERUPDATE($user);
                    $em->persist($entity2);
                    $em->flush();
                    return $this->redirect($this->generateUrl('materielor'));
                }

                if ($form3->isValid())
                {
                    $entity3->setCATEGORIE('INTERVENTIONMODELE');
                    $entity3->setDEFAUT(0);
                    $em->persist($entity3);
                    $em->flush();
                    return $this->redirect($this->generateUrl('materielor'));
                }
            }

            //liste marque
            $repository = $em->getRepository('TechniqueBundle:Marque');
            $marques = $repository->findBy(array(), array('libelle' => 'ASC'));

            //liste famille materiel
            $familles = $em->getRepository('TechniqueBundle:FamilleMateriel')->findAll();

            //liste sous famille materiel
            $sfamilles = $em->getRepository('TechniqueBundle:SousFamilleMateriel')->getAllSousFamille();

            $resModel =  $em->getRepository('TechniqueBundle:ModeleMateriel')->getAllModel();

            //
            //var_dump($resModel); exit;

            //Liste action

            $resLov =  $em->getRepository('TechniqueBundle:Lov')->FindBy(array('cATEGORIE' => 'INTERVENTIONMODELE'));

            return $this->render('TechniqueBundle:MaterielOR:materielor.html.twig', array(
                'marques' => $marques,
                'entity' => $entity,
                'form' => $form->createView(),
                'entity1' => $entity1,
                'form1' => $form1->createView(),
                'familles' => $familles,
                'entity2' => $entity2,
                'form2' => $form2->createView(),
                'sfamilles' => $sfamilles,
                'resModel' => $resModel,
                'form3' => $form3->createView(),
                'entity3' => $entity3,
                'lov' => $resLov
            ));
        }
    }

    public function getallmodeleAction(Request $request)
    {
        //liste modeles

        $em = $this->getDoctrine()->getManager();
        $em1 = $this->getDoctrine()->getManager('sqlserver');
        $conn = $em->getConnection();
        $conn2 = $em1->getConnection();

        $id = $request->query->get('id');

        $arr = array();

        $sql3 = "SELECT INTERVENTIONID as ID, MODELEID, ACTIONID, COMPTEUR, DESCRIPTION
                     FROM INTERVENTIONMODEL
                     WHERE MODELEID = $id
                     ORDER BY COMPTEUR ASC";
        $stmt3 = $conn->prepare($sql3);
        $stmt3->execute();
        $resintervention  = $stmt3->fetchAll();

        foreach ($resintervention as $r){
            $ids = $r['ID'];
            $action = $r['ACTIONID'];
            $cpt = $r['COMPTEUR'];
            $desc = $r['DESCRIPTION'];
            $idmodel = $r['MODELEID'];

            $sql = "SELECT PLANINTERVENTIONARTICLEID as ID, REFARTICLE, DESARTICLE, QUANTITE, UNITE
                     FROM PLANINTERVENTIONARTICLE
                     WHERE INTERVENTIONID = $ids";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $resacticles  = $stmt->fetchAll();

            $article = array();
            foreach ($resacticles as $c){
                $article[] = $c['REFARTICLE'].' '.$c['DESARTICLE'].' '.$c['QUANTITE'].' '.$c['UNITE'];
            }

            $arr[] = array('id'=>$ids, 'modeleid' => $idmodel, 'action'=>$action,'compteur' =>$cpt, 'description' =>$desc, 'articles' => $article);

        }

        return new JsonResponse(array('data' => $arr), JSON_UNESCAPED_UNICODE);
    }

    public function modifierfamilleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $code = $request->request->get('code');
            $libelle = $request->request->get('libelle');
            $dateinvalide = $request->request->get('dateinvalide');
            $idfamille = $request->request->get('idfamillemat');
            $dateupdate = new \DateTime('now');
            $dateupdate1 = $dateupdate->format('Y-m-d');
            $user = $this->getUser()->getName();

            if ($code !=null && $libelle != null)
            {
                $sql = "UPDATE FAMILLEMATERIEL SET CODE = '$code', LIBELLE = '$libelle', USERUPDADE = '$user', DATEUPDATE = '$dateupdate1', DATEINVALIDE = '$dateinvalide' WHERE FAMILLEMATERIELID = $idfamille";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                echo "Form Submitted Succesfully";
            } else
            {
                $this->get('session')->getFlashBag()->add('notice', 'Le champ libelle ne doit pas etre vide');
            }

            return $this->render('TechniqueBundle:MaterielOR:ajax.html.twig');
        }
    }

    public function modifiersousfamilleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $code = $request->request->get('code');
            $libelle = $request->request->get('libelle');
            $dateinvalide = $request->request->get('dateinvalide');
            $famille = $request->request->get('famillesousf');
            $idfsamille = $request->request->get('idsfamillemat');
            $dateupdate = new \DateTime('now');
            $dateupdate1 = $dateupdate->format('Y-m-d');
            $user = $this->getUser()->getName();

            var_dump($code);
            var_dump($libelle);
            var_dump($dateinvalide);
            var_dump($idfsamille);// exit;
            var_dump($dateupdate1); //exit;
            var_dump($famille); //exit;

            if ($code !=null && $libelle != null)
            {
                $sql = "UPDATE SOUSFAMILLEMATERIEL SET CODE = '$code', LIBELLE = '$libelle', USERUPDATE = '$user', DATEUPDATE = '$dateupdate1', DATEINVALIDE = '$dateinvalide', FAMILLEMATERIELID = $famille WHERE SOUSFAMILLEMATERIELID = $idfsamille";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                echo "Form Submitted Succesfully";
            } else
            {
                $this->get('session')->getFlashBag()->add('notice', 'Le champ libelle ne doit pas etre vide');
            }

            return $this->render('TechniqueBundle:MaterielOR:ajax.html.twig');
        }
    }

    public function suprimerfamilleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $idfamille = $request->request->get('idfamillemat');

            $sql = "DELETE FROM FAMILLEMATERIEL WHERE FAMILLEMATERIELID = $idfamille";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            echo "Form Submitted Succesfully";

            return $this->render('TechniqueBundle:MaterielOR:ajax.html.twig');
        }
    }

    public function suprimersfamilleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $idsfamille = $request->request->get('idsfamillemat');

            $sql = "DELETE FROM SOUSFAMILLEMATERIEL WHERE SOUSFAMILLEMATERIELID = $idsfamille";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            echo "Form Submitted Succesfully";

            return $this->render('TechniqueBundle:MaterielOR:ajax.html.twig');
        }
    }

    public function modifiermarqueAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $libelle = $request->request->get('libelle');
            $code = $request->request->get('code');
            $dateinvalide = $request->request->get('dateinvalide');
            $idmarque = $request->request->get('idmarque');

            var_dump($code);

            if ($libelle != null)
            {
                $sql = "UPDATE MATERIELMARQUE SET CODE = '$code', LIBELLE = '$libelle', DATEINVALIDE = '$dateinvalide' WHERE MATERIELMARQUEID = $idmarque";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                echo "Form Submitted Succesfully";
            } else
            {
                $this->get('session')->getFlashBag()->add('notice', 'Le champ libelle ne doit pas etre vide');
            }

            return $this->render('TechniqueBundle:MaterielOR:ajax.html.twig');
        }
    }

    public function supprimermarqueAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $idmarque = $request->request->get('idmarque');

            $sql = "DELETE FROM MATERIELMARQUE WHERE MATERIELMARQUEID = $idmarque";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            echo "Form Submitted Succesfully";

            return $this->render('TechniqueBundle:MaterielOR:ajax.html.twig');
        }
    }

    public function modifieractionAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $libelle = $request->request->get('libelle');
            $idaction = $request->request->get('idaction');

            //var_dump($code);

            if ($libelle != null)
            {
                $sql = "UPDATE LOV SET LIBELLE = '$libelle', CATEGORIE = 'INTERVENTIONMODELE' WHERE LOVID = $idaction";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                echo "Form Submitted Succesfully";
            } else
            {
                $this->get('session')->getFlashBag()->add('notice', 'Le champ libelle ne doit pas etre vide');
            }

            return $this->render('TechniqueBundle:MaterielOR:ajax.html.twig');
        }
    }

    public function supprimeractionAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $idaction = $request->request->get('idaction');

            $sql = "DELETE FROM LOV WHERE LOVID = $idaction";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            echo "Form Submitted Succesfully";

            return $this->render('TechniqueBundle:MaterielOR:ajax.html.twig');
        }
    }

    public function supprimerplaninterventionAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');
            $idintervention = $request->query->get('idintervention');

            $em = $this->getDoctrine()->getManager();

            $intervention = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:InterventionModel')->find($idintervention);

            $em->remove($intervention);
            $em->flush();

            return $this->redirect($this->generateUrl('modifiermodel', array('id' => $id)));
        }
    }
}
