<?php

namespace TechniqueBundle\Controller;

use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TechniqueBundle\Entity\CompteurIntervention;
use TechniqueBundle\Entity\MaintenanceIntervention;
use TechniqueBundle\Entity\OperationIntervention;
use TechniqueBundle\Form\CompteurInterventionType;
use TechniqueBundle\Form\OperationInterventionType;

class PlanInterventionController extends Controller
{
    public function ajoutplaninterventionAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();
            $em1 = $this->getDoctrine()->getManager('sqlserver');
            $conn = $em1->getConnection();

            $id = $request->query->get('id');

            $user = $this->getUser()->getName();

            $modele = $em->getRepository('TechniqueBundle:ModeleMateriel')->find($id);
            $idmarque = $modele->getmATERIELMARQUEID();

            $marque = $em->getRepository('TechniqueBundle:Marque')->find($idmarque);

            $code = $marque->getCode();

            $docmodel= $em->getRepository('TechniqueBundle:DocumentModele')->findBymODELE($id);

            $entity = new CompteurIntervention();

            $entity1 = new OperationIntervention();

            $form = $this->createForm(new CompteurInterventionType(), $entity);

            $form1 = $this->createForm(new OperationInterventionType($em), $entity1);

            $compteuroperations =  $em->getRepository('TechniqueBundle:CompteurIntervention')->findBymODELEID($id);

            if(count($compteuroperations) != 0)
            {
                $compteuroperationmaxid =  $em->getRepository('TechniqueBundle:CompteurIntervention')->getMaxid($id );

                foreach ($compteuroperationmaxid as $r)
                {
                    $compteur = $r['cOMPTEUR'];
                    $idcompteur = $r['id'];
                }

                //Champ compteur de mon formulaire
                $form->get('cOMPTEUR')->setData($compteur);
            }

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);
                $form1->handleRequest($request);

                if(count($compteuroperations) != 0)
                {
                    $cptexist = $em->getRepository('TechniqueBundle:CompteurIntervention')->checkCptExist
                    ($form->get('cOMPTEUR')->getData(), $id);

                    foreach ($cptexist as $r1)
                    {
                        $cptid = $r1->getId();
                    }

                    if(!$cptexist)
                    {
                        if ($form->isValid()) {

                            try
                            {
                                $entity->setMODELEID($id);
                                $entity->setUSERUPDATE($user);
                                $em->persist($entity);
                                $em->flush();
                            }
                            catch(NotNullConstraintViolationException $e)
                            {
                                $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs obligatoire !!");
                            }

                        }
                        if($form1->isValid())
                        {
                            try {
                                $entity1->setCOMPTEURINTERVENTIONID($entity->getId());
                                $em->persist($entity1);
                                $em->flush();
                                return $this->redirect($this->generateUrl('ajoutplanintervention', array('id' => $id)));
                            }
                            catch(NotNullConstraintViolationException $e)
                            {
                                $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs obligatoire !!");
                            }
                        }

                    }
                    else
                    {
                        if($form1->isValid())
                        {
                            try {
                                $entity1->setCOMPTEURINTERVENTIONID($cptid);
                                $em->persist($entity1);
                                $em->flush();
                                return $this->redirect($this->generateUrl('ajoutplanintervention', array('id' => $id)));
                            }
                            catch(NotNullConstraintViolationException $e)
                            {
                                $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs obligatoire !!");
                            }
                        }
                    }
                }
                else
                {
                    if ($form->isValid()) {

                        try {
                            $entity->setMODELEID($id);
                            $entity->setUSERUPDATE($user);
                            $em->persist($entity);
                            $em->flush();
                        }
                        catch(NotNullConstraintViolationException $e)
                            {
                                $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs obligatoire !!");
                            }

                    }
                    if($form1->isValid())
                    {
                        try {
                        $entity1->setCOMPTEURINTERVENTIONID($entity->getId());
                        $em->persist($entity1);
                        $em->flush();
                        return $this->redirect($this->generateUrl('ajoutplanintervention', array('id' => $id)));
                        }
                        catch(NotNullConstraintViolationException $e)
                        {
                            $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs obligatoire !!");
                        }
                    }
                }


            }

            $cptinterventions = $em->getRepository('TechniqueBundle:CompteurIntervention')->findBymODELEID($id, array
            ('cOMPTEUR' => 'ASC'));

            $arrayopinterventions = array();

            foreach ($cptinterventions as $r)
            {
                $cptsid = $r->getId();
                $opinterventions = $em->getRepository('TechniqueBundle:OperationIntervention')->findBycOMPTEURINTERVENTIONID($cptsid);

                $arrayopinterventions[] = $opinterventions;
            }

            $alloperations = $em->getRepository('TechniqueBundle:OperationIntervention')->getAllOperationsByModel($id);

            return $this->render('TechniqueBundle:PlanIntervention:ajoutplanintervention.html.twig',array(
                'modele' => $modele,
                'marque' => $marque,
                'docmodel' => $docmodel,
                'form' => $form->createView(),
                'form1' => $form1->createView(),
                'entity' => $entity,
                'entity1' => $entity1,
                'cptinterventions' => $cptinterventions,
                'arrayopinterventions' => $arrayopinterventions,
                'alloperations' => $alloperations
            ));
        }
    }

    public function modifiercompteurinterventionAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();
            $id = $request->query->get('id');
            $cptid = $request->query->get('compteurid');

            $modele = $em->getRepository('TechniqueBundle:ModeleMateriel')->find($id);
            $idmarque = $modele->getmATERIELMARQUEID();

            $marque = $em->getRepository('TechniqueBundle:Marque')->find($idmarque);

            $code = $marque->getCode();

            $docmodel= $em->getRepository('TechniqueBundle:DocumentModele')->findBymODELE($id);

            $compteurint = $em->getRepository('TechniqueBundle:CompteurIntervention')->find($cptid );

            $edit_form = $this->createForm(new CompteurInterventionType(), $compteurint);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $em->flush();
                }
            }

            $opinterventions = $em->getRepository('TechniqueBundle:OperationIntervention')
                ->findBycOMPTEURINTERVENTIONID($cptid);

            $articles = $em->getRepository('TechniqueBundle:ArticleOperationInt')->findBycOMPTEURINTERVENTIONID($cptid);


            return $this->render('TechniqueBundle:PlanIntervention:modifiercompteurintervention.html.twig', array(
                'modele' => $modele,
                'marque' => $marque,
                'docmodel' => $docmodel,
                'compteurint' => $compteurint,
                'edit_form' => $edit_form->createView(),
                'opinterventions' => $opinterventions,
                'articles' => $articles
            ));
        }
    }

    public function modifieropinterventionAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');
            $cptid = $request->query->get('compteurid');
            $opid = $request->query->get('operationid');

            $modele = $em->getRepository('TechniqueBundle:ModeleMateriel')->find($id);
            $idmarque = $modele->getmATERIELMARQUEID();

            $marque = $em->getRepository('TechniqueBundle:Marque')->find($idmarque);

            $code = $marque->getCode();

            $docmodel= $em->getRepository('TechniqueBundle:DocumentModele')->findBymODELE($id);

            $compteurint = $em->getRepository('TechniqueBundle:CompteurIntervention')->find($cptid );

            $opintervention = $em->getRepository('TechniqueBundle:OperationIntervention')->find($opid);

            $edit_form = $this->createForm(new OperationInterventionType($em), $opintervention);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $em->flush();
                }
            }

            $opinterventions = $em->getRepository('TechniqueBundle:OperationIntervention')
                ->findBycOMPTEURINTERVENTIONID($cptid);

            return $this->render('TechniqueBundle:PlanIntervention:modifieropintervention.html.twig', array(
                'modele' => $modele,
                'marque' => $marque,
                'docmodel' => $docmodel,
                'compteurint' => $compteurint,
                'edit_form' => $edit_form->createView(),
                'opintervention' => $opintervention,
                'opinterventions' => $opinterventions
            ));
        }
    }

    public function supprimeropinterventionAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');
            $opid = $request->query->get('operationid');

            $em = $this->getDoctrine()->getManager();

            $opintervention = $em->getRepository('TechniqueBundle:OperationIntervention')->find($opid);

            $em->remove($opintervention);
            $em->flush();

            return $this->redirect($this->generateUrl('modifiercompteurintervention', array(
                'id' => $id,
                'compteurid' => $opintervention->getCOMPTEURINTERVENTIONID()
            )));
        }
    }

    public function supprimercptinterventionAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');
            $cptid = $request->query->get('compteurid');

            $em = $this->getDoctrine()->getManager();

            $cptintervention = $em->getRepository('TechniqueBundle:CompteurIntervention')->find($cptid);

            $em->remove($cptintervention);
            $em->flush();

            return $this->redirect($this->generateUrl('ajoutplanintervention', array(
                'id' => $id
            )));
        }
    }

    public function getinterventionmodeleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $idmateriel = $request->query->get('idmateriel');

        $cpt = $request->query->get('compteur');

        $opinterventions = $em->getRepository('TechniqueBundle:CompteurIntervention')->getListInterventions
        ($idmateriel, $cpt);

        //var_dump($opinterventions); exit;

        return $this->render('TechniqueBundle:PlanIntervention:getinterventionmodele.html.twig', array(
            'opinterventions' => $opinterventions
        ));
    }

    public function dupliquerplandemaintenanceAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $id = $request->request->get('id');
            $libelle = $request->request->get('libelle1');

            $modele = $em->getRepository('TechniqueBundle:ModeleMateriel')->find($id);

            $mATERIELMARQUEID = $modele->getMATERIELMARQUEID();
            $tYPEMATERIEL = $modele->getTYPEMATERIEL();
            $dESCRIPTION = $modele->getDESCRIPTION();

            $modelelibelle = $em->getRepository('TechniqueBundle:ModeleMateriel')->findBylIBELLE(strtoupper($libelle));

            //var_dump($modele);
            //var_dump($modelelibelle); exit;

            foreach($modelelibelle as $rsmodelelibelle)
            {
                $idmodelelibelle  = $rsmodelelibelle->getId();
            }

            $datecreation = new \DateTime();
            $datecreation = $datecreation->format('Ymd');
            $user = $this->getUser()->getName();

            $compteur = $em->getRepository('TechniqueBundle:CompteurIntervention')->findBymODELEID($id);

            if($compteur)
            {
                foreach($compteur as $rscpt)
                {
                    $cOMPTEURID = $rscpt->getId();
                    $cOMPTEUR = $rscpt->getCOMPTEUR();

                    $conn->insert('COMPTEURINTERVENTION', array('MODELEID' => $idmodelelibelle, 'COMPTEUR' => $cOMPTEUR));

                    $compteurlast = $em->getRepository('TechniqueBundle:CompteurIntervention')->findOneBy(array('cOMPTEUR'=>$cOMPTEUR), array('id' => 'DESC'));

                    $opcpt = $em->getRepository('TechniqueBundle:OperationIntervention')->getOperationMaintenanceByCptId($cOMPTEURID);

                    foreach($opcpt as $rsop)
                    {
                        $dESCRIPTIONOP = $rsop['dESCRIPTION'];
                        $aCTION = $rsop['aCTION'];

                        $conn->insert('OPERATIONINTERVENTION', array('DESCRIPTION' => $dESCRIPTIONOP, 'ACTION' => $aCTION, 'COMPTEURINTERVENTIONID' => $compteurlast->getId()));
                    }

                    $articles = $em->getRepository('TechniqueBundle:ArticleOperationInt')->getArticlesByArrCompteur($cOMPTEURID);

                    foreach($articles as $rsarticle)
                    {
                        $rEFARTICLE = $rsarticle->getREFARTICLE();
                        $dESARTICLE = $rsarticle->getDESARTICLE();
                        $qUANTITE = $rsarticle->getQUANTITE();
                        $uNITE = $rsarticle->getUNITE();

                        $conn->insert('ARTICLEOPERATIONINT', array('REFARTICLE' => $rEFARTICLE, 'DESARTICLE' => $dESARTICLE, 'COMPTEURINTERVENTIONID' => $compteurlast->getId(), 'QUANTITE' => $qUANTITE, 'UNITE' => $uNITE));
                    }

                    $arrcpt[] = $cOMPTEUR;
                }
            }

            return $this->render('TechniqueBundle:MaterielOR:ajax.html.twig');
        }
    }

    public function redirectmodelepmdAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $libelle = $request->query->get('libelle');

        $modelelibelle = $em->getRepository('TechniqueBundle:ModeleMateriel')->findBylIBELLE(strtoupper($libelle));

        //var_dump($modele);
        //var_dump($modelelibelle); exit;

        foreach($modelelibelle as $rsmodelelibelle)
        {
            $idmodelelibelle  = $rsmodelelibelle->getId();
        }

        return $this->redirect($this->generateUrl('modifiermodele', array('id' => $idmodelelibelle)));


    }

}
