<?php

namespace TechniqueBundle\Controller;

use PHPExcel_Style_Fill;
use Swift_Attachment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ExtractionMaterielController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $date = new \DateTime();

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("Aminata Daho")
                       ->setTitle("Extraction excel M.P. Prochaine")
                       ->setDescription("Extraction en format xlsx");
        $i=2;

        //Liste du parc materiel
        $materiels = $em->getRepository('TechniqueBundle:Materiel')->getAllMateriel();
        //Fin

        //var_dump($materiels); exit;

        $sheet = $phpExcelObject->getActiveSheet();
        //Debut du fichier excel (Entete)
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'N PARC')
            ->setCellValue('B1', 'LIBELLE')
            ->setCellValue('C1', 'N SERIE')
            ->setCellValue('D1', 'MODELE')
            ->setCellValue('E1', 'MARQUE')
            ->setCellValue('F1', 'TYPE MATERIEL')
            ->setCellValue('G1', 'COMPTEUR')
            ->setCellValue('H1', 'DATE DERNIERE M.')
            ->setCellValue('I1', 'COMPTEUR DE LA PROCHAINE M.P')
            ->setCellValue('J1', 'COMMENTAIRE');

        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(45);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(18);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(18);
        $phpExcelObject->getActiveSheet()->getColumnDimension('J')->setWidth(45);

        $phpExcelObject
            ->getActiveSheet()
            ->getStyle('A1')
            ->getFill()
            ->getStartColor()
            ->setRGB('FF0000');

        //Parcourir la liste des materiels

        foreach ($materiels as $sr)
        {
            //Recuperer les ids, les compteurs, le type de conso et le type de materiel
            $idmateriel = $sr['materielid'];
            //$cpt = $sr['compteur'];
            $typeconso = $sr['uNITECONSO'];
            $typemateriel = $sr['tYPEMATERIEL'];
            //Fin de la recuperation

            $matconso = $em->getRepository('TechniqueBundle:MaterielConso')->getMaterielCompteur($idmateriel);

            foreach($matconso as $rsmc)
            {
                $cpt_actuelle = $rsmc['compteur'];
                //var_dump($cpt);
            }

            //Recuperer les informations de la derniere maintenance que le materiel a effectuer
            $maintenance = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getLastMaintenanceByMateriel
            ($idmateriel);
            //Fin de la recuperation



            //Parcourir la maintenance
            foreach ($maintenance as $rm)
            {
                //Recuperer le compteur de la maintenance, le compteur de la prochaine maintenance
                $cptm = $rm['cOMPTEUR'];
                $cptpro = $rm['cPTPROCHAIN'];
                //Fin de la recuperation

                //Verifier si le materiel a ete en fonctionne apres  a derniere maintenance
                if($cptm > $cpt_actuelle)
                {
                    //Remplacer le compteur du materiel par le compteur de la maintenance
                    $cpt = $cptm;
                }
                else
                {
                    $cpt = $cpt_actuelle;
                }

                //Recuperer le compteur de la maintenance prochaine
                if(stristr($cptpro, ',') === FALSE)
                {
                    $cptpro = $cptpro;
                }
                else
                {
                    $array = explode(",", $cptpro);
                    $cptpro = $array[0];
                }
                //Fin de la recuperation
            }
            //Fin parcoure

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $sr['cODE'])
                ->setCellValue('B' . $i, $sr['lIBELLE'])
                ->setCellValue('C' . $i, $sr['nOSERIE'])
                ->setCellValue('D' . $i, $sr['MODELE'])
                ->setCellValue('E' . $i, $sr['MARQUE'])
                ->setCellValue('F' . $i, $typemateriel);

            //Recuperer le plan de maintenance d'un materiel
            $compteurpromodel = $em->getRepository('TechniqueBundle:Materiel')->getProIntervention($idmateriel);
            //Fin de la recuperation

            foreach ($compteurpromodel as $r1)
            {
                $cptint = $r1['cOMPTEUR'];
                //$cptproint1 = $r1['cOMPTEUR'];
            }

            if($compteurpromodel && !$maintenance) {
                if($cptint != null)
                {
                    $rscpt = $cpt_actuelle / $cptint;
                    if (is_int($rscpt)) {
                        $rscptproint = $rscpt * $cptint;
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('I' . $i, $cptint);
                    } else {
                        $entiere = intval($rscpt);
                        $rscpt1 =  $cpt_actuelle - (intval($rscpt) * $cptint);

                        if($rscpt1 > $cptint / 2 )
                        {
                            $commentaire = 'Pas de date de maintenance prevu dans le Systeme il, se pourrait que prochaine maintenance est proche';

                            /* $phpExcelObject->getActiveSheet(0)
                                 ->getStyle('A'. $i.':'. 'I' . $i)->applyFromArray(
                                     array(
                                         'fill' => array(
                                             'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                             'color' => array('rgb' => 'FFA726')
                                         ),
                                         'font' => array(
                                             'color' => array('rgb' => 'FFFFFF'),
                                         )
                                     )
                                 );*/
                            $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('I' . $i, $cptint + $cpt_actuelle)
                                ->setCellValue('J' . $i, $commentaire);
                        }
                        else
                        {
                            $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('I' . $i, $cptint + $cpt_actuelle);
                            //$rscptproint = ($rscpt * $cptint)
                        }
                    }
                }

            }

            //var_dump($cptint);
            //exit;


            if($maintenance)
            {
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('G' . $i, $cpt);

            }
            else
            {
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('G' . $i, $rsmc['compteur']);
            }
            foreach ($maintenance as $rm) {
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('H' . $i, $rm['dATEREALISE']);
            }
            if ($maintenance) {
                $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('I' . $i, $cptpro);

            }
            if($maintenance)
            {
                if($cpt > $cptpro)
                {
                    $phpExcelObject->getActiveSheet(0)
                        ->getStyle('A'. $i.':'. 'I' . $i)->applyFromArray(
                            array(
                                'fill' => array(
                                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                    'color' => array('rgb' => 'E53935')
                                ),
                                'font' => array(
                                    'color' => array('rgb' => 'FFFFFF'),
                                )
                            )
                        );
                }
                elseif($cpt <= $cptpro)
                {
                    if($typeconso == 'Heure' || $typeconso == 'heure' || $typemateriel == 'Tracteur')
                    {
                        $cptalert = $cpt + 50;

                        if($cptalert >= $cptpro)
                        {
                            $phpExcelObject->getActiveSheet(0)
                                ->getStyle('A'. $i.':'. 'I' . $i)->applyFromArray(
                                    array(
                                        'fill' => array(
                                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                            'color' => array('rgb' => 'FFA726')
                                        ),
                                        'font' => array(
                                            'color' => array('rgb' => 'FFFFFF'),
                                        )
                                    )
                                );
                        }
                    }
                    elseif($typeconso != 'Heure' && $typeconso != 'heure' && $typemateriel != 'Tracteur')
                    {
                        $cptalert = $cpt + 1000;

                        if($cptalert >= $cptpro)
                        {
                            $phpExcelObject->getActiveSheet(0)
                                ->getStyle('A'. $i.':'. 'I' . $i)->applyFromArray(
                                    array(
                                        'fill' => array(
                                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                            'color' => array('rgb' => 'FFA726')
                                        ),
                                        'font' => array(
                                            'color' => array('rgb' => 'FFFFFF'),
                                        )
                                    )
                                );
                        }
                    }
                }
            }

            $i++;

        }


        $sheet->setTitle('M.P. Prochaine');
        $sheet2 = $phpExcelObject->createSheet();

        $materiels = $em->getRepository('TechniqueBundle:Materiel')->getAllMateriel();

        $maintenanceop = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getRealiseAllMaintenance();


        $j=2;

        //Debut du fichier excel (Entete)
        $phpExcelObject->setActiveSheetIndex(1)
            ->setCellValue('A1', 'N MAINTENANCE')
            ->setCellValue('B1', 'MATERIEL')
            ->setCellValue('C1', 'MODELE')
            ->setCellValue('D1', 'DATE MAINTENANCE')
            ->setCellValue('E1', 'COMPTEUR')
            ->setCellValue('F1', 'COMPTEUR MAINTENANCE PROCHAIN')
            ->setCellValue('G1', 'COMPTEUR OPERATION MAINTENANCE')
            ->setCellValue('H1', 'ACTION')
            ->setCellValue('I1', 'OPERATION MAINTENANCE')
            ->setCellValue('J1', 'DATE DE SORTIE')
            ->setCellValue('K1', 'MECANICIEN(S)');

        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(22);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(18);
        $phpExcelObject->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $phpExcelObject->getActiveSheet()->getColumnDimension('k')->setWidth(22);

        foreach ($maintenanceop as $sr) {
            $idmaintenance = $sr['id'];
            $idmateriel = $sr['mATERIELID'];
            $materiel  = $sr['cODE'].''.$sr['lIBELLE'];
            $compteur = $sr['cOMPTEUR'];
            $datemaintenance = $sr['dATEMAINTENANCE'];
            $cptprochain = $sr['cPTPROCHAIN'];
            $datesortie =$sr['dATESORTIEE'];
            $mecanicien =$sr['nOMMECANICIEN'];

            /*var_dump($idmaintenace);
            var_dump($idmateriel);
            var_dump($compteur);*/

            $opmaintenancep = $em->getRepository('TechniqueBundle:CompteurIntervention')->getListInterventions($idmateriel, $compteur);

            $opmaintenancer = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getinterventionrealiseAction($idmaintenance);

            $array = array();
            //$array1 = array();
            foreach ($opmaintenancer as $sr2)
            {
                $idmaintenance1 = $sr2['id'];
                $listintr = $sr2['dESCRIPTION'];

                $array[] = $listintr;
            }

            foreach ($opmaintenancep as $sr1)
            {

                $listintp[] = $sr1['dESCRIPTION'];
                $descriptionop = $sr1['dESCRIPTION'];

                $phpExcelObject->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $idmaintenance)
                    ->setCellValue('B' . $j, $materiel)
                    ->setCellValue('C' . $j, $sr1['MODELE'])
                    ->setCellValue('D' . $j, $datemaintenance)
                    ->setCellValue('E' . $j, $compteur)
                    ->setCellValue('F' . $j, $cptprochain)
                    ->setCellValue('G' . $j, $sr1['cOMPTEUR'])
                    ->setCellValue('H' . $j, $sr1['aCTION'])
                    ->setCellValue('I' . $j, $descriptionop)
                    ->setCellValue('J' . $j, $datesortie)
                    ->setCellValue('K' . $j, $mecanicien);

                if($idmaintenance1 = $idmaintenance)
                {
                    if(!in_array($descriptionop, $array))
                    {
                        $phpExcelObject->getActiveSheet(0)
                            ->getStyle('A'. $j.':'. 'K' . $j)->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => 'D32F2F')
                                    ),
                                    'font' => array(
                                        'color' => array('rgb' => 'FFFFFF'),
                                    )
                                )
                            );
                    }
                }



            $j++;
        }

        }

        //nom de la feuille
        $sheet2->setTitle('Operations de maintenance');


        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        ob_end_clean();
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'M.P. Prochaine '.$date->format('d-m-Y').'.xlsx'
        );

        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    public function sendfileAction()
    {
        $date = new \DateTime();
        $filename = 'M.P. Prochaine '.$date->format('d-m-Y').'.xlsx';
        $path = 'C:\Users\Administrateur\Downloads/M.P. Prochaine '.$date->format('d-m-Y').'.xlsx';

        $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')->setUsername('intranet@scl.sn')->setPassword('wxcnbv4268SCL');

        $mailer = \Swift_Mailer::newInstance($transport);
        $message = \Swift_Message::newInstance('Maintenances preventives a venir')
            ->setFrom(array('it@scl.sn' => 'IT'))
            ->setTo(array('aminata.daho@scl.sn', 'sylvain.mehat@scl.sn', 'yves.michel@scl.sn', 'maimouna.diakhate@scl.sn', 'francois.mathon@scl.sn' => 'Technique'))
            ->setBody("<p>Ci-joint le fichier des mat&eacute;riels des maintenances pr&eacute;ventive &aacute; venir</p>", 'text/html')
            ->attach(Swift_Attachment::fromPath($path));
        $send = $mailer->send($message);
        ini_set('max_execution_time', 300);

        //return $this->redirect($this->generateUrl('materiel'));
    }

    public function extractionarticleconsoAction()
    {
        $em = $this->getDoctrine()->getManager();

        $date = new \DateTime();

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("Aminata Daho")
            ->setTitle("Extraction excel M.P. Prochaine")
            ->setDescription("Extraction en format xlsx");
        $i=2;

        //Liste du parc materiel
        $maintenances = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getMaintenanceArticleConso();

       // var_dump($maintenances); exit;
        //Fin

        //var_dump($materiels); exit;

        $sheet = $phpExcelObject->getActiveSheet();
        //Debut du fichier excel (Entete)
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'NUM MAINTENANCE')
            ->setCellValue('B1', 'CODE')
            ->setCellValue('C1', 'LIBELLE')
            ->setCellValue('D1', 'DATE MAINTENANCE')
            ->setCellValue('E1', 'DATE ENTREE')
            //->setCellValue('E1', 'DATE SORTIEE')
            ->setCellValue('F1', 'TEMPS REPARATION (H)')
            ->setCellValue('G1', 'TYPE MAINTENANCE')
            ->setCellValue('H1', 'DATE REALISE')
            ->setCellValue('I1', 'REF ARTICLE')
            ->setCellValue('J1', 'DESIGNATION ARTICLE')
            ->setCellValue('K1', 'QUANTITE')
            ->setCellValue('L1', 'STATUT');

        foreach ($maintenances as $sr)
        {

           // $datesortie = new \DateTime($sr['dATESORTIEE']);

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $sr['id'])
                ->setCellValue('B' . $i, $sr['cODE'])
                ->setCellValue('C' . $i, $sr['lIBELLE'])
                ->setCellValue('D' . $i, $sr['dATEMAINTENANCE']->format('d-m-Y'))
                ->setCellValue('E' . $i, $sr['dATEENTREE']->format('d-m-Y'))
               // ->setCellValue('E' . $i, $datesortie)
                ->setCellValue('F' . $i, $sr['tEMPSREPARATION'])
                ->setCellValue('G' . $i, $sr['tYPEMAINTENANCE'])
                ->setCellValue('H' . $i, $sr['dATEREALISE'])
                ->setCellValue('I' . $i, $sr['rEFARTICLE'])
                ->setCellValue('J' . $i, $sr['dESARTICLE'])
                ->setCellValue('K' . $i, $sr['qUANTITE'])
                ->setCellValue('L' . $i, $sr['sTATUT']);

            $i++;

        }


        $sheet->setTitle('Extraction');

        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        ob_end_clean();
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'Extraction '.$date->format('d-m-Y').'.xlsx'
        );

        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }
}
