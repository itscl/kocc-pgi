<?php

namespace TechniqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TechniqueBundle:Default:index.html.twig');
    }
}
