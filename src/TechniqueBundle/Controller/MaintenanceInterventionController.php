<?php

namespace TechniqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TechniqueBundle\Entity\MaintenanceIntervention;
use TechniqueBundle\Form\MaintenanceInterventionType;

class MaintenanceInterventionController extends Controller
{
    public function ajoutinterventioncAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $id = $request->query->get('idmaintenance');

            $em = $this->getDoctrine()->getManager();

            $maintenance = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->find($id);

            $materielid = $maintenance->getMATERIELID();

            $materiel = $em->getRepository('TechniqueBundle:Materiel')->find($materielid);

            $entity = new MaintenanceIntervention();

            $form = $this->createForm(new MaintenanceInterventionType(), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    //var_dump($form->get()); exit;
                    if($form->get('dESCRIPTION')->getData() != null)
                    {
                        $entity->setMAINTENANCEID($id);
                        $entity->setCOMPTEUR($maintenance->getcOMPTEUR());
                        $em->persist($entity);
                        $em->flush();

                        return $this->redirect($this->generateUrl('ajoutinterventionc', array('idmaintenance' => $id)));
                    }
                    else
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Veuillez saisir une description de l'operation à effectuer !!");
                    }

                }
            }

            $intervention = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->findBymAINTENANCEID($id);

            $articles = $em->getRepository('TechniqueBundle:MaintenanceArticle')->findBymAINTENANCEID($id);

            return $this->render('TechniqueBundle:MaintenanceIntervention:ajoutinterventionc.html.twig', array(
                'maintenance' => $maintenance,
                'materiel' => $materiel,
                'entity' => $entity,
                'form' => $form->createView(),
                'intervention' => $intervention,
                'articles' => $articles

            ));
        }
    }

    public function modifierinterventioncAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();
            $idmaintenance = $request->query->get('idmaintenance');
            $idintervention = $request->query->get('id');

            $maintenance = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->find($idmaintenance);

            $materielid = $maintenance->getMATERIELID();

            $materiel = $em->getRepository('TechniqueBundle:Materiel')->find($materielid);

            $intervention = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->find($idintervention);

            $edit_form = $this->createForm(new MaintenanceInterventionType(), $intervention);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $em->flush();

                    return $this->redirect($this->generateUrl('ajoutinterventionc', array('idmaintenance' => $idmaintenance)));
                }
            }

            $interventions = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->findBymAINTENANCEID($idmaintenance);

            $articles = $em->getRepository('TechniqueBundle:MaintenanceArticle')->findBymAINTENANCEID($idmaintenance);

            return $this->render('TechniqueBundle:MaintenanceIntervention:modifierinterventionc.html.twig', array(
                'maintenance' => $maintenance,
                'materiel' => $materiel,
                'intervention' => $intervention,
                'edit_form' => $edit_form->createView(),
                'interventions' => $interventions,
                'articles' => $articles

            ));
        }
    }

    public function supprimerinterventioncAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');
            $idmaintenance = $request->query->get('idmaintenance');

            $maintenance = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:MaintenanceMateriel')->find($idmaintenance);

            $interventionr = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->find($id);

            $em->remove($interventionr);
            $em->flush();

            if($maintenance->getTYPEMAINTENANCE() == 'CURATIVE')
            {
                return $this->redirect($this->generateUrl('ajoutinterventionc', array('idmaintenance' => $idmaintenance)));
            }
            else
                return $this->redirect($this->generateUrl('modifiermaintenance', array('idmaintenance' => $idmaintenance)));
        }
    }

    public function ajoutinterventionpAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $id = $request->query->get('idmaintenance');

            $em = $this->getDoctrine()->getManager();

            $maintenenance = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->find($id);

            $materielid = $maintenenance->getMATERIELID();

            $materiel = $em->getRepository('TechniqueBundle:Materiel')->find($materielid);

            $interventions = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->findBymAINTENANCEID($id);

            return $this->render('TechniqueBundle:MaintenanceIntervention:ajoutinterventionp.html.twig', array(
                'maintenance' => $maintenenance,
                'materiel' => $materiel,
                'interventions' => $interventions
            ));
        }
    }

    public function ajaxcurativemaintenanceAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $id = $request->request->get('idmaintenance');
        $valeur = $_POST['checkmaintenance'];

        $count = count($valeur);

        //var_dump($valeur); exit;

        for($i = 0; $i < $count; $i++)
        {
            $getvaleur = $valeur[$i];

            $intervention = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->getIntervention($getvaleur);

            foreach ($intervention as $r1)
            {
                $cOMPTEUR = $r1['cOMPTEUR'];
                $dESCRIPTION = $r1['dESCRIPTION'];

                $conn->insert('MAINTENANCEINTERVENTION', array('MAINTENANCEID' => $id, 'COMPTEUR' => $cOMPTEUR, 'DESCRIPTION' => $dESCRIPTION));
            }

        }
        return $this->render('TechniqueBundle:MaintenanceMateriel:ajax.html.twig');
    }

}
