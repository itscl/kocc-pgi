<?php

namespace TechniqueBundle\Controller;

use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TechniqueBundle\Entity\ModeleMateriel;
use TechniqueBundle\Form\ModeleMaterielType;

class ModeleMaterielController extends Controller
{
    public function ajoutmodeleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();

            $entity = new ModeleMateriel();
            $user = $this->getUser()->getName();

            $form = $this->createForm(new ModeleMaterielType($em), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    try
                    {
                        $entity->setUSERUPDATE($user);
                        $entity->setDATEUPDATE(new \DateTime());
                        $em->persist($entity);
                        $em->flush();
                        return $this->redirect($this->generateUrl('ajoutdocumentmodele', array('id' => $entity->getId())));
                    }
                    catch(NotNullConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs obligatoire !!");
                    }
                    catch(UniqueConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add(
                            'notice',
                            array(
                                'alert' => 'success',
                                'title' => 'Success!',
                                'message' => 'New word has been added successfully.'
                            )
                        );
                    }
                }
            }

            return $this->render('TechniqueBundle:ModeleMateriel:ajoutmodele.html.twig', array(
                'form' => $form->createView(),
                'entity' => $entity
            ));
        }
    }

    public function modifiermodeleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $modele = $em->getRepository('TechniqueBundle:ModeleMateriel')->find($id);

            $docmodel= $em->getRepository('TechniqueBundle:DocumentModele')->findBymODELE($id);

            $edit_form = $this->createForm(new ModeleMaterielType($em), $modele);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $em->flush();
                }
            }

            $cptinterventions = $em->getRepository('TechniqueBundle:CompteurIntervention')->findBymODELEID($id, array
            ('cOMPTEUR' => 'ASC'));

            $arrayopinterventions = array();

            foreach ($cptinterventions as $r)
            {
                $cptsid = $r->getId();
                $opinterventions = $em->getRepository('TechniqueBundle:OperationIntervention')->findBycOMPTEURINTERVENTIONID($cptsid);

                $arrayopinterventions[] = $opinterventions;
            }

            $alloperations = $em->getRepository('TechniqueBundle:OperationIntervention')->getAllOperationsByModel($id);
            //var_dump($alloperations); exit;

            return $this->render('TechniqueBundle:ModeleMateriel:modifiermodele.html.twig', array(
                'modele' => $modele,
                'edit_form' => $edit_form->createView(),
                'docmodel' => $docmodel,
                'cptinterventions' => $cptinterventions,
                'arrayopinterventions' => $arrayopinterventions,
                'alloperations' => $alloperations
            ));
        }
    }

    public function supprimermodeleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $id = $request->query->get('idmodele');

            $em = $this->getDoctrine()->getManager();

            $modele = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:ModeleMateriel')->find($id);

            $em->remove($modele);
            $em->flush();

            return $this->redirect($this->generateUrl('materielor'));

        }
    }

    public function dupliquermodelematerielAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $id = $request->request->get('id');
            $libelle = $request->request->get('libelle');

            $modele = $em->getRepository('TechniqueBundle:ModeleMateriel')->find($id);

            $mATERIELMARQUEID = $modele->getMATERIELMARQUEID();
            $tYPEMATERIEL = $modele->getTYPEMATERIEL();
            $dESCRIPTION = $modele->getDESCRIPTION();

            $datecreation = new \DateTime();
            $datecreation = $datecreation->format('Ymd');
            $user = $this->getUser()->getName();

            $conn->insert('MODELEMATERIEL', array('LIBELLE' => $libelle, 'MATERIELMARQUEID' => $mATERIELMARQUEID, 'TYPEMATERIEL' => $tYPEMATERIEL, 'DESCRIPTION' => $dESCRIPTION, 'DATECREATION' => $datecreation, 'USERUPDATE' => $user));

            $lastmodele = $em->getRepository('TechniqueBundle:ModeleMateriel')->findOneBy(array('lIBELLE'=>strtoupper($libelle)), array('id' => 'DESC'));

            $compteur = $em->getRepository('TechniqueBundle:CompteurIntervention')->findBymODELEID($id);

            if($compteur)
            {
                foreach($compteur as $rscpt)
                {
                    $cOMPTEURID = $rscpt->getId();
                    $cOMPTEUR = $rscpt->getCOMPTEUR();

                    $conn->insert('COMPTEURINTERVENTION', array('MODELEID' => $lastmodele->getId(), 'COMPTEUR' => $cOMPTEUR));

                    $compteurlast = $em->getRepository('TechniqueBundle:CompteurIntervention')->findOneBy(array('cOMPTEUR'=>$cOMPTEUR), array('id' => 'DESC'));

                    $opcpt = $em->getRepository('TechniqueBundle:OperationIntervention')->getOperationMaintenanceByCptId($cOMPTEURID);

                    foreach($opcpt as $rsop)
                    {
                        $dESCRIPTIONOP = $rsop['dESCRIPTION'];
                        $aCTION = $rsop['aCTION'];

                        $conn->insert('OPERATIONINTERVENTION', array('DESCRIPTION' => $dESCRIPTIONOP, 'ACTION' => $aCTION, 'COMPTEURINTERVENTIONID' => $compteurlast->getId()));
                    }

                    $articles = $em->getRepository('TechniqueBundle:ArticleOperationInt')->getArticlesByArrCompteur($cOMPTEURID);

                    foreach($articles as $rsarticle)
                    {
                        $rEFARTICLE = $rsarticle->getREFARTICLE();
                        $dESARTICLE = $rsarticle->getDESARTICLE();
                        $qUANTITE = $rsarticle->getQUANTITE();
                        $uNITE = $rsarticle->getUNITE();

                        $conn->insert('ARTICLEOPERATIONINT', array('REFARTICLE' => $rEFARTICLE, 'DESARTICLE' => $dESARTICLE, 'COMPTEURINTERVENTIONID' => $compteurlast->getId(), 'QUANTITE' => $qUANTITE, 'UNITE' => $uNITE));
                    }

                    $arrcpt[] = $cOMPTEUR;
                }
            }

            return $this->render('TechniqueBundle:MaterielOR:ajax.html.twig');
        }
    }

    public function getlastidAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $lastmodele = $em->getRepository('TechniqueBundle:ModeleMateriel')->getLastId();

        foreach($lastmodele as $r)
        {
            $id = $r['id'];
        }

        return $this->redirect($this->generateUrl('modifiermodele', array('id' => $id)));


    }
}
