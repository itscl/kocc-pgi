<?php

namespace TechniqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TechniqueBundle\Entity\ArticleOperationInt;
use TechniqueBundle\Form\ArticleOperationIntType;
use Symfony\Component\HttpFoundation\JsonResponse;

class ArticleOperationIntController extends Controller
{
    public function ajoutarticleoperationintAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');
            $cptid = $request->query->get('compteurid');

            $modele = $em->getRepository('TechniqueBundle:ModeleMateriel')->find($id);
            $idmarque = $modele->getmATERIELMARQUEID();

            $marque = $em->getRepository('TechniqueBundle:Marque')->find($idmarque);

            $code = $marque->getCode();

            $docmodel= $em->getRepository('TechniqueBundle:DocumentModele')->findBymODELE($id);

            $cptinterventions = $em->getRepository('TechniqueBundle:CompteurIntervention')->getListCompteurByNoId
            ($cptid, $id);

            $arrayopinterventions = array();

            foreach ($cptinterventions as $r)
            {
                $cptsid = $r->getId();
                $opinterventions = $em->getRepository('TechniqueBundle:OperationIntervention')->findBycOMPTEURINTERVENTIONID($cptsid);

                $arrayopinterventions[] = $opinterventions;
            }

            $cptintervention = $em->getRepository('TechniqueBundle:CompteurIntervention')->find($cptid);

            $opinterventioncpts = $em->getRepository('TechniqueBundle:OperationIntervention')
                ->findBycOMPTEURINTERVENTIONID($cptid);

            $entity = new ArticleOperationInt();

            $form = $this->createForm(new ArticleOperationIntType(), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    //var_dump($form->getData()); exit;
                    $entity->setCOMPTEURINTERVENTIONID($cptid);
                    $em->persist($entity);
                    $em->flush();
                    return $this->redirect($this->generateUrl('ajoutarticleoperationint', array('id' => $id,
                        'compteurid' => $cptid)));
                }
            }

            $articles = $em->getRepository('TechniqueBundle:ArticleOperationInt')->findBycOMPTEURINTERVENTIONID($cptid);

            $alloperations = $em->getRepository('TechniqueBundle:OperationIntervention')->getAllOperationsByModel1($id, $cptid);

            return $this->render('TechniqueBundle:ArticleOperationInt:ajoutarticleoperationint.html.twig',array(
                'modele' => $modele,
                'marque' => $modele,
                'docmodel' => $docmodel,
                'cptinterventions' => $cptinterventions,
                'arrayopinterventions' => $arrayopinterventions,
                'cptintervention' => $cptintervention,
                'opinterventioncpts' => $opinterventioncpts,
                'entity' => $entity,
                'form' => $form->createView(),
                'article' => $articles,
                'alloperations' => $alloperations
            ));
        }
    }

    public function modifierarticleoperationAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');
            $cptid = $request->query->get('compteurid');
            $articleid = $request->query->get('articleid');

            $modele = $em->getRepository('TechniqueBundle:ModeleMateriel')->find($id);
            $idmarque = $modele->getmATERIELMARQUEID();

            $marque = $em->getRepository('TechniqueBundle:Marque')->find($idmarque);

            $code = $marque->getCode();

            $docmodel= $em->getRepository('TechniqueBundle:DocumentModele')->findBymODELE($id);

            $cptinterventions = $em->getRepository('TechniqueBundle:CompteurIntervention')->getListCompteurByNoId
            ($cptid, $id);

            $arrayopinterventions = array();

            foreach ($cptinterventions as $r)
            {
                $cptsid = $r->getId();
                $opinterventions = $em->getRepository('TechniqueBundle:OperationIntervention')->findBycOMPTEURINTERVENTIONID($cptsid);

                $arrayopinterventions[] = $opinterventions;
            }

            $cptintervention = $em->getRepository('TechniqueBundle:CompteurIntervention')->find($cptid);

            $opinterventioncpts = $em->getRepository('TechniqueBundle:OperationIntervention')
                ->findBycOMPTEURINTERVENTIONID($cptid);

            $article = $em->getRepository('TechniqueBundle:ArticleOperationInt')->find($articleid);

            $edit_form = $this->createForm(new ArticleOperationIntType(), $article);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $em->flush();
                }
            }

            $articles = $em->getRepository('TechniqueBundle:ArticleOperationInt')->findBycOMPTEURINTERVENTIONID($cptid);

            $alloperations = $em->getRepository('TechniqueBundle:OperationIntervention')->getAllOperationsByModel1($id, $cptid);

            return $this->render('TechniqueBundle:ArticleOperationInt:modifierarticleoperation.html.twig', array(
                'modele' => $modele,
                'marque' => $modele,
                'docmodel' => $docmodel,
                'cptinterventions' => $cptinterventions,
                'arrayopinterventions' => $arrayopinterventions,
                'cptintervention' => $cptintervention,
                'opinterventioncpts' => $opinterventioncpts,
                'article' => $article,
                'edit_form' => $edit_form->createView(),
                'articles' => $articles,
                'alloperations' => $alloperations
            ));
        }
    }

    public function supprimerarticleoperationAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');
            $cptid = $request->query->get('compteurid');
            $articleid = $request->query->get('articleid');

            $article = $em->getRepository('TechniqueBundle:ArticleOperationInt')->find($articleid);

            $em->remove($article);
            $em->flush();

            return $this->redirect($this->generateUrl('ajoutarticleoperationint', array('id' => $id,
                'compteurid' => $cptid)));

        }
    }

    public function getarticlesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $cptid = $request->query->get('compteur');

        $articles = $em->getRepository('TechniqueBundle:ArticleOperationInt')->findBycOMPTEURINTERVENTIONID($cptid);

        $compteurint = $em->getRepository('TechniqueBundle:CompteurIntervention')->find($cptid);

        return $this->render('TechniqueBundle:ArticleOperationInt:getarticles.html.twig', array(
            'articles' => $articles,
            'compteurint' => $compteurint
        ));
    }

    public function getlistarticleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em1 = $this->getDoctrine()->getManager('sqlserver');
        $conn = $em1->getConnection();

        $motcle = $request->query->get('motcle');
        $page = $request->query->get('page');

        $result_all = $em1->getRepository('TechniqueBundle:F_Famille')->getListArticle($motcle);

        $result = $em1->getRepository('TechniqueBundle:F_Famille')->getListArticlePage($motcle, $page);

        $count = count($result_all);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }

}
