<?php

namespace TechniqueBundle\Controller;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TechniqueBundle\Entity\Materiel;
use TechniqueBundle\Entity\MaterielGeste;
use TechniqueBundle\Form\MaterielGesteType;
use TechniqueBundle\Form\MaterielType;

class MaterielController extends Controller
{
    public function indexAction()
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();

            $materiels = $em->getRepository('TechniqueBundle:MaterielConso')->getAllMateriel();

            //var_dump($materiels); exit;

            $array = array();
            foreach($materiels as $rs)
            {
                $id = $rs['materielid'];

                $materielcompteur = $em->getRepository('TechniqueBundle:MaterielConso')->getMaterielCompteur($id);

                $array[] = $materielcompteur;
            }
           // var_dump($array);
            //exit;

            return $this->render('TechniqueBundle:Materiel:materiel.html.twig', array(
                'materiels' => $materiels,
                'materielcompteur' => $array
            ));
        }
    }

    public function modifiermaterielAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $materiel = $em->getRepository('TechniqueBundle:Materiel')->find($id);

            $materielcompteur = $em->getRepository('TechniqueBundle:MaterielConso')->getMaterielCompteur($id);

            $materielgeste = $em->getRepository('TechniqueBundle:MaterielGeste')->findBymATERIELID($id);

            if($materielgeste != null) {
                foreach ($materielgeste as $rs) {
                    $idsgestemat = $rs->getId();
                    $idsgeste = $rs->getSGESTEID();
                }

                $sgestemat =  $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:MaterielGeste')->find($idsgestemat);

                $sgeste = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:SGeste')->find($idsgeste);

            }
            else {
                $sgestemat = null;
                $idsgeste = null;
                $sgeste = null;
            }

            $materielepi = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:MaterielEPI')->findBymATERIELID($id);

            if($materielepi != null)
            {
                foreach ($materielepi as $rs) {
                    $idepimat[] = $rs->getId();
                    $idepi[] = $rs->getePIID();
                }
                $array = implode(",", $idepi);
                $array1 = implode(",", $idepimat);

                $query = "SELECT LIBELLE
                         FROM EPI e
                         WHERE e.EPIID in ($array)";
                $stmt = $conn->prepare($query);
                $stmt->execute();
                $epi = $stmt->fetchAll();
            }
            else
            {
                $idepi = null;
                $epi = null;
            }


            $edit_form = $this->createForm(new MaterielType($em), $materiel);

            $edit_form1 = $this->createForm(new MaterielGesteType($em), $sgestemat);


            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);
                $edit_form1->handleRequest($request);

                if($edit_form->isValid() && $edit_form1->isValid())
                {
                    $em->flush();
                }
            }

            $epilist = $this->getDoctrine()->getManager()->getRepository('AgroBundle:EPI')->findAll();

            $allmaintenance = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getMaintenanceByMateriel($id);

            //var_dump($allmaintenance); exit;

            return $this->render('TechniqueBundle:Materiel:modifiermateriel.html.twig', array(
                'materiel' => $materiel,
                'edit_form' =>$edit_form->createView(),
                'edit_form1' => $edit_form1->createView(),
                'materielgeste' => $materielgeste,
                'epis' => $epi,
                'epilist' => $epilist,
                'materielcompteur' => $materielcompteur,
                'allmaintenance' => $allmaintenance
            ));
        }
    }

    public function ajouterepiAction(Request $request)
    {

        $id = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $materiel = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:Materiel')->find($id);

        $valeur = $_POST['checkrole'];
        $count = count($valeur);

        var_dump ($valeur);

        for($i = 0; $i < $count; $i++)
        {
            $getvaleur = $valeur[$i];
            var_dump ($getvaleur);

            $conn->insert('MATERIELEPI', array('MATERIELID' => $id, 'EPIID' => $getvaleur));
        }

        return $this->render('TechniqueBundle:Materiel:ajax.html.twig');
    }

    public function supprimerepiAction(Request $request)
    {
        $id = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $materiel = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:Materiel')->find($id);


        $valeur = $_POST['checkrole'];
        $count = count($valeur);

        for($i = 0; $i < $count; $i++)
        {
            $getvaleur = $valeur[$i];

            $sql = "DELETE FROM MATERIELEPI WHERE MATERIELID = $id AND EPIID = $getvaleur ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('TechniqueBundle:Materiel:ajax.html.twig');

    }

    public function supprimermaterielAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');

            $em = $this->getDoctrine()->getManager();

            $materielgeste = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:MaterielGeste')
                ->findBymATERIELID($id);

            if($materielgeste)
            {
            foreach($materielgeste as $rs)
            {
                $idmg = $rs->getId();
            }

            $materielgeste1 = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:MaterielGeste')
                ->find($idmg);

            $em->remove($materielgeste1);
            $em->flush();
            //var_dump($materielgeste1); exit;

            }

            $materiel = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:Materiel')->find($id);

            try
            {
                $em->remove($materiel);
                $em->flush();

                return $this->redirect($this->generateUrl('materiel'));
            }
            catch(ForeignKeyConstraintViolationException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Ce matériel est utilisé impossible de le
                supprimer !!");

                return $this->redirect($this->generateUrl('modifiermateriel',array('id' => $id)));
            }
        }
    }

    public function ajoutermaterielAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new Materiel();
        $entity1 = new MaterielGeste();

        $form = $this->createForm(new MaterielType($em), $entity);
        $form1 = $this->createForm(new MaterielGesteType($em), $entity1);

        if($request->isMethod('post'))
        {
            $form->handleRequest($request);
            $form1->handleRequest($request);

            if($form->isValid() && $form1->isValid())
            {

                $em->persist($entity);
                $em->flush();
                if($form1->get('sGESTEID')->getData() !=null)
                {
                    $entity1->setMATERIELID($entity->getId());
                    $em->persist($entity1);
                }

                $em->flush();

                return $this->redirect($this->generateUrl('modifiermateriel', array('id' => $entity->getId())));
            }
        }

        $epilist = $this->getDoctrine()->getManager()->getRepository('AgroBundle:EPI')->findAll();

        return $this->render('TechniqueBundle:Materiel:ajoutmateriel.html.twig', array(
            'entity' => $entity,
            'entity1' => $entity1,
            'form' => $form->createView(),
            'form1' => $form1->createView(),
            'epilist' => $epilist
        ));
    }

    public function exportAction()
    {
        $em = $this->getDoctrine()->getManager();

        $date = new \DateTime('now');

        $date = $date->format('d-m-Y H:i');

        $arr = array();

        $arr1 = array();
        $nbmateriel = array();

        $arrctr = array();
        $nombre = array();

        $agemateriel = array();

        $nbmaterielfamille = array();

        $familles = $em->getRepository('TechniqueBundle:FamilleMateriel')->getAllFamilleMateriel();

        $nbfamille = count($familles);

        foreach ($familles as $r)
        {
            $id = $r->getId();

            $sfamilles = $em->getRepository('TechniqueBundle:SousFamilleMateriel')->getSousFamilleByFamille($id);

            $nbmaterielfamilles = $em->getRepository('TechniqueBundle:Materiel')->getnbmaterielbyfamille($id);

            $nbmaterielfamille[] = $nbmaterielfamilles;

            $arr[] = $sfamilles;

            foreach ($sfamilles as $r1)
            {
                $sfid = $r1['id'];

                $materiels = $em->getRepository('TechniqueBundle:Materiel')->getAllMaterielBySFamille($sfid);

                $arr1[] = $materiels;

                $nbmateriels = $em->getRepository('TechniqueBundle:Materiel')->getnbmaterielbySfamille($sfid);

                $nbmateriel[] = $nbmateriels;

            }
        }


        //on stocke la vue � convertir en PDF, en n'oubliant pas les param�tres twig si la vue comporte des donn�es dynamiques
        $html = $this->renderView('TechniqueBundle:Materiel:page-pdf.html.twig', array('familles'=>$familles, 'nbfamille' => $nbfamille, 'sfamilles' => $arr, 'materiels' => $arr1,'nbmateriel' => $nbmateriel, 'nbmaterielfamille' => $nbmaterielfamille, 'date' => $date));
        //on appelle le service html2pdf
        $html2pdf = $this->get('html2pdf_factory')->create('L', 'A4', 'fr', true, 'UTF-8', array(15, 10, 15, 10));
        //real : utilise la taille r�elle
        $html2pdf->pdf->SetDisplayMode('real');
        //writeHTML va tout simplement prendre la vue stocker dans la variable $html pour la convertir en format PDF
        $html2pdf->writeHTML($html);
        //Output envoit le document PDF au navigateur internet
        return new Response($html2pdf->Output('Inventaire-Materiel.pdf'), 200, array('Content-Type' => 'application/pdf'));
    }

    public function listecategoriematerielAction(Request $request)
    {
        $motcle = $request->query->get('motcle');
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('TechniqueBundle:FamilleMateriel')->getAllFamilleMaterielSelect($motcle);

        $count = count($categories);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $categories));
    }

    public function listegenrematerielAction(Request $request)
    {
        $motcle = $request->query->get('motcle');
        $em = $this->getDoctrine()->getManager();

        $genre = $em->getRepository('TechniqueBundle:SousFamilleMateriel')->getListSFamilleSelect($motcle);

        $count = count($genre);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $genre));
    }

    public function fitrematerielAction(Request $request)
    {
        $categorieid = $request->query->get('q1');
        $genreid = $request->query->get('q2');

        $em = $this->getDoctrine()->getManager();

        if($genreid =='')
        {
            $materiels = $em->getRepository('TechniqueBundle:Materiel')->getAllMaterielFiltreByFamille($categorieid );
        }
        elseif($categorieid =='')
        {
            $materiels = $em->getRepository('TechniqueBundle:Materiel')->getAllMaterielFiltreBySFamille($genreid );
        }
        else
        {
            $materiels = $em->getRepository('TechniqueBundle:Materiel')->getAllMaterielFiltre($categorieid,$genreid );
        }

        return $this->render('TechniqueBundle:Materiel:fitremateriel.html.twig', array(
            'materiels' => $materiels
        ));
    }

    public function exportfiltreAction(Request $request)
    {
        $categorieid = $request->request->get('categoriemateriel');

        $genreid = $request->request->get('genremateriel');

        $em = $this->getDoctrine()->getManager();

        $date = new \DateTime('now');

        $date = $date->format('d-m-Y H:i');

        $arr = array();

        $arr1 = array();
        $nbmateriel = array();

        $arrctr = array();

        if($genreid =='') {

            $familles = $em->getRepository('TechniqueBundle:FamilleMateriel')->getFamilleMaterielById($categorieid);
            //var_dump($genreid); exit;

            $nbfamille = count($familles);

            foreach ($familles as $r) {
                $id = $r->getId();

                $sfamilles = $em->getRepository('TechniqueBundle:SousFamilleMateriel')->getSousFamilleByFamille($id);

                $nbmaterielfamilles = $em->getRepository('TechniqueBundle:Materiel')->getnbmaterielbyfamille($id);

                $nbmaterielfamille[] = $nbmaterielfamilles;

                $arr[] = $sfamilles;

                foreach ($sfamilles as $r1) {
                    $sfid = $r1['id'];

                    $materiels = $em->getRepository('TechniqueBundle:Materiel')->getAllMaterielBySFamille($sfid);

                    $arr1[] = $materiels;
                    $nbmateriels = $em->getRepository('TechniqueBundle:Materiel')->getnbmaterielbySfamille($sfid);

                    $nbmateriel[] = $nbmateriels;

                }
            }
        }
        elseif($categorieid =='')
        {
            $sfamilles = $em->getRepository('TechniqueBundle:SousFamilleMateriel')->getSFamilleById($genreid);

            foreach ($sfamilles as $r1)
            {
                $idfamille = $r1['idfamille'];
            }

            $familles = $em->getRepository('TechniqueBundle:FamilleMateriel')->getFamilleMaterielById($idfamille);

            $nbfamille = count($familles);

            foreach ($familles as $r) {

                $sfamilles = $em->getRepository('TechniqueBundle:SousFamilleMateriel')->getSFamilleById($genreid);
                $nbmaterielfamilles = $em->getRepository('TechniqueBundle:Materiel')->getnbmaterielbyfamille($idfamille);

                $nbmaterielfamille[] = $nbmaterielfamilles;


                $arr[] = $sfamilles;

                foreach ($sfamilles as $r1) {

                    $materiels = $em->getRepository('TechniqueBundle:Materiel')->getAllMaterielBySFamille($genreid);

                    $arr1[] = $materiels;
                    $nbmateriels = $em->getRepository('TechniqueBundle:Materiel')->getnbmaterielbySfamille($genreid);

                    $nbmateriel[] = $nbmateriels;

                }
            }

        }
        else
        {
            $familles = $em->getRepository('TechniqueBundle:FamilleMateriel')->getFamilleMaterielById($categorieid);
            $nbfamille = count($familles);

            foreach ($familles as $r) {

                $sfamilles = $em->getRepository('TechniqueBundle:SousFamilleMateriel')->getSFamilleById($genreid);

                $nbmaterielfamilles = $em->getRepository('TechniqueBundle:Materiel')->getnbmaterielbyfamille($categorieid);

                $nbmaterielfamille[] = $nbmaterielfamilles;

                $arr[] = $sfamilles;

                foreach ($sfamilles as $r1) {

                    $materiels = $em->getRepository('TechniqueBundle:Materiel')->getAllMaterielBySFamille($genreid);

                    $arr1[] = $materiels;
                    $nbmateriels = $em->getRepository('TechniqueBundle:Materiel')->getnbmaterielbySfamille($genreid);

                    $nbmateriel[] = $nbmateriels;

                }
            }
        }

        //on stocke la vue � convertir en PDF, en n'oubliant pas les param�tres twig si la vue comporte des donn�es dynamiques
        $html = $this->renderView('TechniqueBundle:Materiel:page-pdf.html.twig' ,array('familles'=>$familles, 'nbfamille' => $nbfamille, 'sfamilles' => $arr, 'materiels' => $arr1,'nbmaterielfamille'=>$nbmaterielfamille,'nbmateriel' => $nbmateriel, 'date' => $date));
        //on appelle le service html2pdf
        $html2pdf = $this->get('html2pdf_factory')->create('L', 'A4', 'fr', true, 'UTF-8', array(15, 10, 15, 10));
        //real : utilise la taille r�elle
        $html2pdf->pdf->SetDisplayMode('real');
        //writeHTML va tout simplement prendre la vue stocker dans la variable $html pour la convertir en format PDF
        $html2pdf->writeHTML($html);
        //Output envoit le document PDF au navigateur internet
        return new Response($html2pdf->Output('Inventaire-Materiel.pdf'), 200, array('Content-Type' => 'application/pdf'));

    }

    public function listematerielAction(Request $request)
    {
        $motcle = $request->query->get('motcle');

        $em = $this->getDoctrine()->getManager();

        $materiels = $em->getRepository('TechniqueBundle:Materiel')->ListeMateriel($motcle);

        $count = count($materiels);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $materiels));
    }

}
