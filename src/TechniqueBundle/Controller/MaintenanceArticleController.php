<?php

namespace TechniqueBundle\Controller;

use Doctrine\DBAL\Exception\DriverException;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TechniqueBundle\Entity\MaintenanceArticle;
use TechniqueBundle\Entity\MaintenanceArticleR;
use TechniqueBundle\Form\MaintenanceArticleRType;
use TechniqueBundle\Form\MaintenanceArticleType;

class MaintenanceArticleController extends Controller
{
    public function ajoutarticlemaintenanceAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em1 = $this->getDoctrine()->getManager('sqlserver');
        $conn = $em->getConnection();
        $conn2 = $em1->getConnection();

        $materielid = $request->query->get('materielid');
        $compteur = $request->query->get('compteur');
        $datemaintenance = $request->query->get('date');

        $dateformat = new \DateTime($datemaintenance);
        $dateformat = $dateformat->format('Y-m-d');

        $materiel = $em->getRepository('TechniqueBundle:Materiel')->find($materielid);

        ///var_dump($materiel); exit;

       // $marqueid = $materiel->getmATERIELMARQUEID();

        //$marque = $em->getRepository('TechniqueBundle:Marque')->find($marqueid);

        //$code = $marque->getCode();

        $infomaintenance = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getInfosMaintenance
        ($materielid, $compteur, $dateformat);

        foreach ($infomaintenance as $sr)
        {
            $maitnenanceid = $sr->getId();
            $nomoperateur = $sr->getNOMOPERATEUR();
            $statut = $sr->getSTATUT();
            $mecanicien = $sr->getNOMMECANICIEN();
            $type = $sr->getTYPEMAINTENANCE();
            $dateimmo = $sr->getDATEENTREE();

        }
        $listinterventions = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->findBymAINTENANCEID
        ($maitnenanceid);

        $count = count($listinterventions);
        foreach ($listinterventions as $sr1)
        {
            $interventionid[] = $sr1->getINTERVENTIONID();

        }

        $array = implode(",", $interventionid);

        $query = "SELECT ci.COMPTEUR, oi.DESCRIPTION, oi.ACTION, oi.OPERATIONINTERVENTIONID, ci.COMPTEURINTERVENTIONID
                     FROM OPERATIONINTERVENTION oi, COMPTEURINTERVENTION ci
                     WHERE oi.COMPTEURINTERVENTIONID = ci.COMPTEURINTERVENTIONID
                     AND oi.OPERATIONINTERVENTIONID in ($array)
                     ORDER BY ci.COMPTEUR";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $interventions = $stmt->fetchAll();

        foreach ($interventions as $sr2)
        {
            $compteurid[] = $sr2['COMPTEURINTERVENTIONID'];

        }
        $arrayarticle = implode(",", array_unique($compteurid));

        $query1 = "SELECT ci.COMPTEURINTERVENTIONID, ao.ARTICLEOPERATIONINTID, ao.REFARTICLE, ao.DESARTICLE, ao.QUANTITE, ao.UNITE
                     FROM COMPTEURINTERVENTION ci, ARTICLEOPERATIONINT ao
                     WHERE ci.COMPTEURINTERVENTIONID = ao.COMPTEURINTERVENTIONID
                     AND ci.COMPTEURINTERVENTIONID in ($arrayarticle)
                  ";
        $stmt1 = $conn->prepare($query1);
        $stmt1->execute();
        $interventionarticles = $stmt1->fetchAll();

        if($type == 'CURATIVE')
        {
            $entity = new MaintenanceArticleR();

            $form = $this->createForm(new MaintenanceArticleRType(), $entity);

            if ($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    try
                    {
                        $entity->setMAINTENANCEID($maitnenanceid);
                        $em->persist($entity);
                        $em->flush();
                    }
                    catch(NotNullConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs !!");
                    }
                    catch(DriverException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Veuillez Choisir un article !!");
                    }
                }
            }
        }
        else
        {
            $entity = new MaintenanceArticle();

            $form = $this->createForm(new MaintenanceArticleType(), $entity);

            if ($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    //var_dump($form->getData()); exit;
                    try
                    {
                        $entity->setMAINTENANCEID($maitnenanceid);
                        $em->persist($entity);
                        $em->flush();

                        return $this->redirect($this->generateUrl('ajoutarticlemaintenance', array('materielid' => $materielid, 'compteur' => $compteur, 'date' => $datemaintenance)));
                    }
                    catch(NotNullConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs !!");
                    }
                    catch(DriverException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Veuillez Choisir un article !!");
                    }
                }
            }
        }

        /*$sql1 = "SELECT ar.AR_Ref,ar.AR_Design, DE_Intitule DEPOT, f.FA_Intitule FAMILLE, ars.AS_QteSto
                FROM  F_DEPOT d, F_FAMILLE f, F_ARTICLE ar, F_ARTSTOCK ars
                WHERE  f.FA_CodeFamille = ar.FA_CodeFamille
                AND ar.AR_Ref = ars.AR_Ref
                AND ars.DE_No = d.DE_No
                AND f.FA_CodeFamille IN ('$code','LUBRIFIANT') ";
        $stmt1 = $conn2->prepare($sql1);
        $stmt1->execute();
        $result1 = $stmt1->fetchAll();*/

        $maintenancearticle = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->listArticleMaintenance($maitnenanceid);

        return $this->render('TechniqueBundle:MaintenanceArticle:ajoutarticlemaintenance.html.twig', array(
            'materiel' => $materiel,
            'compteur' => $compteur,
            'interventions' => $interventions,
            'interventionarticles' => $interventionarticles,
            'entity' => $entity,
            'form' => $form->createView(),
            'maintenancearticle' => $maintenancearticle,
            'nomoperateur' => $nomoperateur,
            'statut' => $statut,
            'maitnenanceid' => $maitnenanceid,
            //'result1' => $result1,
            'mecanicien' => $mecanicien,
            'type' => $type,
            'dateimmo' => $dateimmo

        ));
    }

    public function supprimermaintenancearticleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');
            $idmaintenance = $request->query->get('idmaintenance');

            //var_dump($id);
            //var_dump($idmaintenance);exit;

            $em = $this->getDoctrine()->getManager();

            $maintenance = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:MaintenanceMateriel')->find($idmaintenance);
            $statut = $maintenance->getSTATUT();


            $article = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:MaintenanceArticleR')->find($id);

            if($article)
            {
                $article = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:MaintenanceArticleR')
                    ->find($id);
                $em->remove($article);
                $em->flush();
            }
            elseif(!$article)
            {
                $article = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:MaintenanceArticle')->find($id);
                $em->remove($article);
                $em->flush();
            }

            return $this->render('TechniqueBundle:MaintenanceMateriel:ajax.html.twig');
        }
    }



    /*public function modifiermaintenancearticleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em1 = $this->getDoctrine()->getManager('sqlserver');
        $conn = $em->getConnection();
        $conn2 = $em1->getConnection();

        $idmaintenance = $request->query->get('idmaintenance');

        $idarticle = $request->query->get('idarticle');

        $maintenance = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->find($idmaintenance);

        $materielid = $maintenance->getMATERIELID();

        $materiel = $em->getRepository('TechniqueBundle:Materiel')->find($materielid);

        $marqueid = $materiel->getmATERIELMARQUEID();

        $marque = $em->getRepository('TechniqueBundle:Marque')->find($marqueid);

        $code = $marque->getCode();

        $maintenanceintervention = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->findBymAINTENANCEID($idmaintenance);

        foreach ($maintenanceintervention as $sr1)
        {
            $interventionid[] = $sr1->getINTERVENTIONID();

        }

        $array = implode(",", $interventionid);

        $query = "SELECT ci.COMPTEUR, oi.DESCRIPTION, oi.ACTION, oi.OPERATIONINTERVENTIONID, ci.COMPTEURINTERVENTIONID
                     FROM OPERATIONINTERVENTION oi, COMPTEURINTERVENTION ci
                     WHERE oi.COMPTEURINTERVENTIONID = ci.COMPTEURINTERVENTIONID
                     AND oi.OPERATIONINTERVENTIONID in ($array)
                     ORDER BY ci.COMPTEUR";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $interventions = $stmt->fetchAll();

        foreach ($interventions as $sr2)
        {
            $compteurid[] = $sr2['COMPTEURINTERVENTIONID'];

        }
        $arrayarticle = implode(",", array_unique($compteurid));

        $query1 = "SELECT ci.COMPTEURINTERVENTIONID, ao.ARTICLEOPERATIONINTID, ao.REFARTICLE, ao.DESARTICLE, ao.QUANTITE, ao.UNITE
                     FROM COMPTEURINTERVENTION ci, ARTICLEOPERATIONINT ao
                     WHERE ci.COMPTEURINTERVENTIONID = ao.COMPTEURINTERVENTIONID
                     AND ci.COMPTEURINTERVENTIONID in ($arrayarticle)
                  ";
        $stmt1 = $conn->prepare($query1);
        $stmt1->execute();
        $interventionarticles = $stmt1->fetchAll();

        $maintenancearticle = $em->getRepository('TechniqueBundle:MaintenanceArticle')->find($idarticle);

        $idarticleint = $maintenancearticle->getARTICLEID();

        $edit_form = $this->createForm(new MaintenanceArticleType(), $maintenancearticle);

        if($request->isMethod('post'))
        {
            $edit_form->handleRequest($request);

            if($edit_form->isValid())
            {
                $maintenancearticle->setARTICLEID($idarticleint);
                $em->flush();
            }
        }

        $interventionarticle = $em->getRepository('TechniqueBundle:ArticleOperationInt')->find($idarticleint);

        $maintenancearticles = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->listArticleMaintenanceByarticle($idmaintenance, $idarticle);

        $sql1 = "SELECT ar.AR_Ref,ar.AR_Design, DE_Intitule DEPOT, f.FA_Intitule FAMILLE, ars.AS_QteSto
                FROM  F_DEPOT d, F_FAMILLE f, F_ARTICLE ar, F_ARTSTOCK ars
                WHERE  f.FA_CodeFamille = ar.FA_CodeFamille
                AND ar.AR_Ref = ars.AR_Ref
                AND ars.DE_No = d.DE_No
                AND f.FA_CodeFamille IN ('$code','LUBRIFIANT') ";
        $stmt1 = $conn2->prepare($sql1);
        $stmt1->execute();
        $result1 = $stmt1->fetchAll();

        return $this->render('TechniqueBundle:MaintenanceArticle:modifiermaintenancearticle.html.twig', array(
            'maintenance' => $maintenance,
            'materiel' => $materiel,
            'interventions' => $interventions,
            'edit_form' => $edit_form->createView(),
            //'maintenancearticle' => $maintenancearticle,
            'interventionarticle' => $interventionarticle,
            'maintenancearticles' => $maintenancearticles,
            'result1' => $result1
        ));
    }*/

    public function modifierarticlemaintenancerealiseAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('idmaintenance');

            $em = $this->getDoctrine()->getManager();
            $em1 = $this->getDoctrine()->getManager('sqlserver');
            $conn = $em->getConnection();
            $conn2 = $em1->getConnection();

            $maintenancemateriel = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->find($id);

            $materielid = $maintenancemateriel->getMATERIELID();

            $materiel = $em->getRepository('TechniqueBundle:Materiel')->getMaterielId($materielid);

            $interventionp = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->getlistinterventionmaintenance($id);

            $interventionr = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getinterventionrealiseAction($id);

            foreach ($materiel as $rm) {
                $code = $rm['CODEMARQUE'];
            }

            $articlesmaintenance = $em->getRepository('TechniqueBundle:MaintenanceArticle')->findBymAINTENANCEID($id);

            $articles = array();
            $interventionarticles = array();
                foreach ($interventionp as $ri) {
                    $idintervention = $ri['cOMPTEURINTERVENTIONID'];

                    $idinterventions[] = $idintervention;

                    $interventions[] = $em->getRepository('TechniqueBundle:OperationIntervention')->find($idintervention);


            $interventionarticle = $em->getRepository('TechniqueBundle:CompteurIntervention')
                ->getarticlesplanintervention($idinterventions);

            $interventionarticles[] = $interventionarticle;
        }
            

            $sql1 = "SELECT ar.AR_Ref,ar.AR_Design, DE_Intitule DEPOT, f.FA_Intitule FAMILLE, ars.AS_QteSto
                FROM  F_DEPOT d, F_FAMILLE f, F_ARTICLE ar, F_ARTSTOCK ars
                WHERE  f.FA_CodeFamille = ar.FA_CodeFamille
                AND ar.AR_Ref = ars.AR_Ref
                AND ars.DE_No = d.DE_No
                AND f.FA_CodeFamille IN ('$code','LUBRIFIANT') ";
            $stmt1 = $conn2->prepare($sql1);
            $stmt1->execute();
            $result1 = $stmt1->fetchAll();

            if ($interventionr) {
                $entity = new MaintenanceArticleR();

                $form = $this->createForm(new MaintenanceArticleRType(), $entity);

                if ($request->isMethod('post')) {
                    $form->handleRequest($request);

                    if ($form->isValid()) {

                        $entity->setMAINTENANCEID($id);
                        $em->persist($entity);
                        $em->flush();

                        return $this->redirect($this->generateUrl('modifierlistarticlemaintenance', array('idmaintenance' => $entity->getMAINTENANCEID())));
                    }
                }
            } else {
                $entity = new MaintenanceArticle();

                $form = $this->createForm(new MaintenanceArticleType(), $entity);

                if ($request->isMethod('post')) {
                    $form->handleRequest($request);

                    if ($form->isValid()) {
                        //var_dump($form->getData()); exit;
                        $entity->setMAINTENANCEID($id);
                        $em->persist($entity);
                        $em->flush();

                        return $this->redirect($this->generateUrl('modifierlistarticlemaintenance', array('idmaintenance' => $entity->getMAINTENANCEID())));
                    }
                }
            }

            foreach ($articlesmaintenance as $r) {
                $idarticle = $r->getaRTICLEID();

                $article = $em->getRepository('TechniqueBundle:MaintenanceArticle')->getAllArticlesMaintenance($idarticle, $id);
                $articles[] = $article;

            }
            $articlesmaintenancer = $em->getRepository('TechniqueBundle:MaintenanceArticleR')->findBymAINTENANCEID($id);

            if ($maintenancemateriel->getTYPEMAINTENANCE() == 'CURATIVE')
            {
                return $this->render('TechniqueBundle:MaintenanceArticle:modifiermaintenancearticlelistcurative.html.twig', array(
                    'maintenancemateriel' => $maintenancemateriel,
                    'materiel' => $materiel,
                    'interventionp' => $interventionp,
                    'articlesmaintenance' => $articlesmaintenance,
                    'articles' => $articles,
                    'form' => $form->createView(),
                    'result1' => $result1,
                    'interventionr' => $interventionr,
                    'articlesmaintenancer' => $articlesmaintenancer
                ));

            } else {
                return $this->render('TechniqueBundle:MaintenanceArticle:modifierarticlemaintenancerealise.html.twig', array(
                    'maintenancemateriel' => $maintenancemateriel,
                    'materiel' => $materiel,
                    'interventionp' => $interventionp,
                    'articlesmaintenance' => $articlesmaintenance,
                    'articles' => $articles,
                    'form' => $form->createView(),
                    'interventionarticles' => $interventionarticles,
                    'result1' => $result1,
                    'interventionr' => $interventionr,
                    'articlesmaintenancer' => $articlesmaintenancer
                ));
            }
        }
    }

    /*public function listearticleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em1 = $this->getDoctrine()->getManager('sqlserver');
        $conn2 = $em1->getConnection();

        $motcle = $request->query->get('motcle');
        $idmateriel = $request->query->get('materielid');

        $materiel = $em->getRepository('TechniqueBundle:Materiel')->find($idmateriel);

        $marqueid = $materiel->getmATERIELMARQUEID();

        $marque = $em->getRepository('TechniqueBundle:Marque')->find($marqueid);

        $code = $marque->getCode();

        $sql1 = "SELECT ar.AR_Ref as id,ar.AR_Design as name
                FROM  F_DEPOT d, F_FAMILLE f, F_ARTICLE ar, F_ARTSTOCK ars
                WHERE  f.FA_CodeFamille = ar.FA_CodeFamille
                AND ar.AR_Ref = ars.AR_Ref
                AND ars.DE_No = d.DE_No
                AND f.FA_CodeFamille IN ('$code','LUBRIFIANT')
                AND ar.AR_Design LIKE '%$motcle%' ";
        $stmt1 = $conn2->prepare($sql1);
        $stmt1->execute();
        $result1 = $stmt1->fetchAll();

        $count = count($result1);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $result1));
    }*/

    public function articlecurativeAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $id = $request->query->get('idmaintenance');

            $em = $this->getDoctrine()->getManager();
            $em1 = $this->getDoctrine()->getManager('sqlserver');
            $conn2 = $em1->getConnection();

            $maintenance = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->find($id);

            $materielid = $maintenance->getMATERIELID();

            $materiel = $em->getRepository('TechniqueBundle:Materiel')->find($materielid);

            $code = $materiel->getCODE();

            $interventionr = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->findBymAINTENANCEID($id,
                array('cOMPTEUR' => 'ASC'));

            $sql1 = "SELECT ar.AR_Ref,ar.AR_Design, DE_Intitule DEPOT, f.FA_Intitule FAMILLE, ars.AS_QteSto
                FROM  F_DEPOT d, F_FAMILLE f, F_ARTICLE ar, F_ARTSTOCK ars
                WHERE  f.FA_CodeFamille = ar.FA_CodeFamille
                AND ar.AR_Ref = ars.AR_Ref
                AND ars.DE_No = d.DE_No
                AND f.FA_CodeFamille IN ('$code','LUBRIFIANT') ";
            $stmt1 = $conn2->prepare($sql1);
            $stmt1->execute();
            $result1 = $stmt1->fetchAll();

            $entity = new MaintenanceArticle();

            $form = $this->createForm(new MaintenanceArticleType(), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {

                    try
                    {
                        $entity->setMAINTENANCEID($id);
                        $em->persist($entity);
                        $em->flush();

                        return $this->redirect($this->generateUrl('articlecurative', array('idmaintenance' => $id)));
                    }
                    catch(NotNullConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs !!");
                    }

                }
            }

            $articles = $em->getRepository('TechniqueBundle:MaintenanceArticle')->findBymAINTENANCEID($id);

            return $this->render('TechniqueBundle:MaintenanceArticle:articlecurative.html.twig', array(
                'maintenance' => $maintenance,
                'materiel' => $materiel,
                'interventionr' => $interventionr,
                'result1' => $result1,
                'form' => $form->createView(),
                'entity' => $entity,
                'articles' => $articles
            ));
        }
    }

}
