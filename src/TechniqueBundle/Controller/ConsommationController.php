<?php

namespace TechniqueBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TechniqueBundle\Entity\Magasin;
use TechniqueBundle\Entity\MagasinConso;
use TechniqueBundle\Entity\MaterielConso;
use TechniqueBundle\Form\MagasinConsoType;
use TechniqueBundle\Form\MagasinType;
use TechniqueBundle\Form\MaterielConsoType;
use Symfony\Component\HttpFoundation\Response;
use TechniqueBundle\TechniqueBundle;

class ConsommationController extends Controller
{
    public function consommationAction()
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();
            $magasins = $em->getRepository('TechniqueBundle:Magasin')->findAll();

            $magasinconsos = $em->getRepository('TechniqueBundle:MagasinConso')->getAllMagConso();

            $mateconso = $em->getRepository('TechniqueBundle:MaterielConso')->getAllConso();

            $redis = $this->container->get('snc_redis.default');

            $key = "all_conso";

            if($redis->exists($key))
            {
                $response = $redis->get($key);

                $json = json_decode($response, 1);

                $resources = $json['data'];

                //var_dump($resources) ; exit;
            }
            else
            {

                $rows = array();

                foreach($mateconso as $r)
                {
                    $row['mAGASINCONSOID'] =  $r['mAGASINCONSOID'];
                    $row['dATECONSO'] = $r['dATECONSO'];
                    $row['code'] = $r['code'];
                    $row['materiel'] = $r['materiel'];
                    $row['magasin'] = $r['magasin'];
                    $row['pRENOM'] = $r['pRENOM'];
                    $row['nOM'] = $r['nOM'];
                    $row['qTE'] = $r['qTE'];
                    $row['cTR'] = $r['cTR'];
                    $row['id'] = $r['id'];

                    array_push($rows,$row);
                }

                $redis->set($key, json_encode(array('data'=>$rows)));
                $response = $redis->get($key);

                $json = json_decode($response, 1);

                $resources = $json['data'];
            }

            return $this->render('TechniqueBundle:Consommation:consommation.html.twig', array(
                'magasins' => $magasins,
                'magconso' => $magasinconsos,
                'mateconso' => $resources
            ));
        }
    }

    public function modifiermagAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');

            $em = $this->getDoctrine()->getManager();

            $magasin = $em->getRepository('TechniqueBundle:Magasin')->find($id);

            $magstock = $em->getRepository('TechniqueBundle:Magasin')->getMagStock($id);

            if($magstock) {
                foreach ($magstock as $sr) {
                    $qteappro[] = $sr['QTEAPPRO'];
                    $qteconso[] = $sr['QTECONSO'];
                }
                $sumqteappro = array_sum($qteappro);
                $sumqteconso = array_sum($qteconso);
            }
            else
            {
                $sumqteappro = 0;
                $sumqteconso = 0;
            }

            $magconso = $em->getRepository('TechniqueBundle:MagasinConso')->getMagConso($id);

            $magappro = $em->getRepository('TechniqueBundle:MagasinAppro')->getMagAppro($id);

            $edit_form = $this->createForm(new MagasinType($em), $magasin);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $em->flush();
                }
            }

            return $this->render('TechniqueBundle:Consommation:modifiermag.html.twig', array(
                'magasin' => $magasin,
                'edit_form' => $edit_form->createView(),
                'sumqteappro' => $sumqteappro,
                'sumqteconso' => $sumqteconso,
                'magconso' => $magconso,
                'magstock' => $magstock,
                'magappro' => $magappro
            ));

        }
    }

    public function ajoutermagAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();

            $entity = new Magasin();

            $form = $this->createForm(new MagasinType($em), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    $em->persist($entity);
                    $em->flush();

                    return $this->redirect($this->generateUrl('consommation'));
                }
            }

            return $this->render('TechniqueBundle:Consommation:ajoutermag.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
        }
    }

    public function suppmagAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');

            $em = $this->getDoctrine()->getManager();

            $magasin = $em->getRepository('TechniqueBundle:Magasin')->find($id);

            $magconso = $em->getRepository('TechniqueBundle:MagasinConso')->getMagConso($id);

            $magappro = $em->getRepository('TechniqueBundle:MagasinAppro')->getMagAppro($id);

            if($magconso || $magappro)
            {

                $this->get('session')->getFlashBag()->add('notice', "Impossible de supprimer ce magasin car des consommation y sont attach�s");
                return $this->redirect($this->generateUrl('modifiermag', array('id' => $id)));
            }
            else
            {
                $em->remove($magasin);
                $em->flush();
                return $this->redirect($this->generateUrl('consommation'));
            }
        }
    }

    public function modifiermagconsoAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');

            $em = $this->getDoctrine()->getManager();

            $magasinconso = $em->getRepository('TechniqueBundle:MagasinConso')->find($id);

            $magasinid = $magasinconso->getmAGASINID();

            $magconso = $em->getRepository('TechniqueBundle:MagasinConso')->getMagConso($magasinid);

            $edit_form = $this->createForm(new MagasinConsoType($em), $magasinconso);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $em->flush();
                }
            }

            return $this->render('TechniqueBundle:Consommation:modifiermagconso.html.twig', array(
                'magasinconso' => $magasinconso,
                'edit_form' => $edit_form->createView(),
                'magconso' => $magconso
            ));
        }
    }

    public function ajoutermagconsoAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();

            $entity = new MagasinConso();

            $form = $this->createForm(new MagasinConsoType($em), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    $em->persist($entity);
                    $em->flush();

                    return $this->redirect($this->generateUrl('consommation'));
                }
            }

            return $this->render('TechniqueBundle:Consommation:ajoutermagconso.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
        }
    }

    public function getlastindexjaugeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $idmagasin = $request->query->get('idmagasin');

        $result = $em->getRepository('TechniqueBundle:MagasinConso')->getlastindexjauge($idmagasin);

        return new JsonResponse($result);
    }

    public function ajouterconsomaterielAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();

            $entity = new MaterielConso();

            $form = $this->createForm(new MaterielConsoType($em), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                $plein = $form->get('pLEIN')->getData();

                $dateconso = $form->get('dATECONSO')->getData();

                $dateconso = new \DateTime($dateconso);

                if($form->isValid())
                {
                    if($plein == true)
                    {
                        $entity->setPLEIN("-1");
                    }
                    else
                        $entity->setPLEIN(null);
                    $entity->setDATECONSO($dateconso);

                    $em->persist($entity);
                    $em->flush();

                }
            }

            $idmagconso =$entity->getMAGASINCONSOID();

            $magconso = $em->getRepository('TechniqueBundle:MaterielConso')->getconsobymag($idmagconso);

            return $this->render('TechniqueBundle:Consommation:ajouterconsomateriel.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
                'result' => $magconso
            ));
        }
    }

    public function modifiermaterielconsoAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');

            $em = $this->getDoctrine()->getManager();

            $materielconso = $em->getRepository('TechniqueBundle:MaterielConso')->find($id);

            $edit_form = $this->createForm(new MaterielConsoType($em), $materielconso);

            if($request->isMethod('post'))
            {

                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $em->flush();
                }
            }

            return $this->render('TechniqueBundle:Consommation:modifiermaterielconso.html.twig', array(
                'materielconso' => $materielconso,
                'edit_form' => $edit_form->createView()
            ));
        }
    }

    public function getlastcompteurAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $idmateriel = $request->query->get('idmateriel');

        $result = $em->getRepository('TechniqueBundle:MaterielConso')->getMaterielCompteur($idmateriel);

       /* if($result)
        {
            foreach($result as $rs)
            {
                $result[]['CTR'] = $rs;
            }

        }*/

        return new JsonResponse($result);

    }

    public function getdateconsomagAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->query->get('id');

        $result = array();

        $result = $em->getRepository('TechniqueBundle:MagasinConso')->getdateconsomag($id);

        return new JsonResponse($result);

    }

}
