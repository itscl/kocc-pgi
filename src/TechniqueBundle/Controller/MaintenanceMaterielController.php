<?php

namespace TechniqueBundle\Controller;

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TechniqueBundle\Entity\MaintenanceArticle;
use TechniqueBundle\Entity\MaintenanceArticleR;
use TechniqueBundle\Entity\MaintenanceIntervention;
use TechniqueBundle\Entity\MaintenanceInterventionR;
use TechniqueBundle\Entity\MaintenanceMateriel;
use TechniqueBundle\Form\MaintenanceArticleRType;
use TechniqueBundle\Form\MaintenanceArticleType;
use TechniqueBundle\Form\MaintenanceInterventionRType;
use TechniqueBundle\Form\MaintenanceInterventionType;
use TechniqueBundle\Form\MaintenanceMaterielType;

class MaintenanceMaterielController extends Controller
{
    public function indexAction()
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();

            $maintenances = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getAllMaintenance();

            $maintenancesc = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getAllMaintenanceCurative();

            return $this->render('TechniqueBundle:MaintenanceMateriel:maintenance.html.twig', array(
                'maintenances' => $maintenances,
                'maintenancesc' => $maintenancesc
            ));
        }
    }

    public function ajoutfichemaintenancepAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();


            return $this->render('TechniqueBundle:MaintenanceMateriel:ajoutfichemaintenancep.html.twig');
        }
    }

    public function getinterventionmodelmodifAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $idmateriel = $request->query->get('idmateriel');

        $cpt = $request->query->get('compteur');

        $idmaintenance = $request->query->get('idmaintenance');

        $interventions = $em->getRepository('TechniqueBundle:Materiel')->getListIntervention($idmateriel, $cpt);

        $interventionscheck = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->findBymAINTENANCEID($idmaintenance);

        return $this->render('TechniqueBundle:Maintenance:getinterventionmodelmodif.html.twig', array(
            'interventions' => $interventions
        ));
    }

    public function ajaxpersitmaintenanceAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $datemaintenance = $request->request->get('datemaintenance');
        $materielid = $request->request->get('materielid');
        $compteur = $request->request->get('compteur');
        $maintenancepro = $request->request->get('maintenancepro');
        $mecanicien = $request->request->get('mecanicien');
        $dateimmo = $request->request->get('dateimmo');
        $valeur = $_POST['checkmaintenance'];

        $user = $this->getUser()->getName();

        var_dump($datemaintenance);
        var_dump($materielid);
        var_dump($compteur);
        //exit;

        $formdate = new \DateTime($datemaintenance);
        $formdate = $formdate->format('Y-m-d');

        $dateimmo = new \DateTime($dateimmo);
        $dateimmo = $dateimmo->format('Y-m-d');

        //var_dump($valeur);

        if($datemaintenance !=null && $materielid !=null && $compteur !=null)
        {
            $conn->insert('MAINTENANCEMATERIEL', array('MATERIELID' => $materielid, 'COMPTEUR' => $compteur,
                'DATEMAINTENANCE' => $formdate, 'NOMOPERATEUR' => $user, 'STATUT' =>'SAISI', 'CPTPROCHAIN' =>
                    $maintenancepro, 'NOMMECANICIEN' => $mecanicien, 'DATEENTREE' => $dateimmo, 'TYPEMAINTENANCE' =>
                    'PREVENTIVE'
            ));
        }

        $query = "SELECT MAINTENANCEMATERIELID
                  FROM MAINTENANCEMATERIEL
                  WHERE MATERIELID = '$materielid'
                  AND COMPTEUR = '$compteur'
                  AND DATEMAINTENANCE = '$formdate'";
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as $sr)
        {
            $maitnenanceid = $sr['MAINTENANCEMATERIELID'];

        }

        $count = count($valeur);

        for($i = 0; $i < $count; $i++)
        {
            $getvaleur = $valeur[$i];

            $conn->insert('MAINTENANCEINTERVENTION', array('MAINTENANCEID' => $maitnenanceid, 'INTERVENTIONID' => $getvaleur));
        }
        return $this->render('TechniqueBundle:MaintenanceMateriel:ajax.html.twig');
    }

    public function getquantitearticleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $idinterventionarticle = $request->query->get('id');

        $result = $em->getRepository('TechniqueBundle:ArticleOperationInt')->getQunatitearticle($idinterventionarticle);

        return new JsonResponse($result);
    }

    public function imprimerficheAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $id = $request->query->get('id');

        $date = new \DateTime();

        $dateformat = $date->format('Y-m-d');

        $user = $this->getUser()->getName();

        $maintenancemateriel = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->find($id);

        if ($maintenancemateriel->getSTATUT() == 'SAISI') {
            $sql = "UPDATE MAINTENANCEMATERIEL SET STATUT ='DIFFUSE', DATEDIFFUSE = '$dateformat', OPERATEURDIFFUSER = '$user' WHERE
                    MAINTENANCEMATERIELID = $id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }
        $materielid = $maintenancemateriel->getMATERIELID();

        $materiel = $em->getRepository('TechniqueBundle:Materiel')->getMaterielId($materielid);

        foreach ($materiel as $r1) {
            $modele = $r1['MODELEID'];
            $uniteconso = $r1['uNITECONSO'];
        }

        $interventionp = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->findBymAINTENANCEID($id);

        $interventionc = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->findBymAINTENANCEID($id);

        $interventionr = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getinterventionrealiseAction($id);

        $getcompteurcr = array();

        foreach ($interventionr as $r3)
        {
            $compteur = $r3['cOMPTEUR'];
            $getcompteurcr[] = $compteur;

        }
        $getcompteurcr = array_unique($getcompteurcr);

        $arr = array();

        if ($maintenancemateriel->getTYPEMAINTENANCE() == 'PREVENTIVE')
        {
            foreach ($interventionp as $r)
            {
                $interventionid[] = $r->getINTERVENTIONID();
            }
            $cptinterventions = $em->getRepository('TechniqueBundle:CompteurIntervention')->getCptInterventions($interventionid);

            foreach ($cptinterventions as $r1)
            {
                $idcompteur[] = $r1['id'];
            }
            $interventionscpt = $em->getRepository('TechniqueBundle:OperationIntervention')->getInterventionsByCompteur($idcompteur, array('cOMPTEUR' => 'DESC  '));

            $getCcountInterventionByCompteur = $em->getRepository('TechniqueBundle:OperationIntervention')->getCcountInterventionByCompteur($idcompteur);

            $interventions = $em->getRepository('TechniqueBundle:CompteurIntervention')->getInterventions($interventionid);

            $articles = $em->getRepository('TechniqueBundle:MaintenanceArticle')->getarticlesmaintenance($id);

        }

        foreach ($interventionc as $r2)
        {
            $compteur = $r2->getCOMPTEUR();
            $getcompteurc[] = $compteur;

        }


        $getcompteurc = array_unique($getcompteurc);

        $articlesmaintenancer = $em->getRepository('TechniqueBundle:MaintenanceArticleR')->findBymAINTENANCEID($id);

        $articlesmaintenancec =  $em->getRepository('TechniqueBundle:MaintenanceArticle')->findBymAINTENANCEID($id);
        //var_dump($articlesmaintenancer); exit;
        //on stocke la vue � convertir en PDF, en n'oubliant pas les param�tres twig si la vue comporte des donn�es dynamiques
        if ($maintenancemateriel->getTYPEMAINTENANCE() == 'PREVENTIVE')
        {
            $html = $this->renderView('TechniqueBundle:MaintenanceMateriel:imprimerfiche.html.twig', array('materiel' => $materiel, 'maintenancemateriel' => $maintenancemateriel, 'interventions' => $interventions, 'articles' => $articles, 'interventionr' => $interventionr, 'articlesmaintenancer' => $articlesmaintenancer, 'uniteconso' =>$uniteconso, 'cptinterventions' => $cptinterventions, 'interventionscpt' => $interventionscpt, 'countInterventionByCompteur' => $getCcountInterventionByCompteur, 'getcompteurcr' => $getcompteurcr));
        }
        else
        {
            $html = $this->renderView('TechniqueBundle:MaintenanceMateriel:imprimerfiche.html.twig', array('materiel' => $materiel, 'maintenancemateriel' => $maintenancemateriel, 'interventionc' => $interventionc, 'interventionr' => $interventionr, 'articlesmaintenancec' => $articlesmaintenancec,'articlesmaintenancer' => $articlesmaintenancer, 'uniteconso' =>$uniteconso, 'getcompteurc' => $getcompteurc, 'getcompteurcr' => $getcompteurcr));
        }


        //on appelle le service html2pdf
        $html2pdf = $this->get('html2pdf_factory')->create('P', 'A4', 'fr', true, 'UTF-8', array(15, 10, 15, 10));
        //real : utilise la taille r�elle
       // $html2pdf->setTestTdInOnePage(false);
        $html2pdf->pdf->SetDisplayMode('real');
        //writeHTML va tout simplement prendre la vue stocker dans la variable $html pour la convertir en format PDF
        $html2pdf->writeHTML($html);
        //Output envoit le document PDF au navigateur internet
        return new Response($html2pdf->Output('Fiche-inventaire.pdf'), 200, array('Content-Type' =>
            'application/pdf'));
    }

    public function modifiermaintenanceAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('idmaintenance');

            $em = $this->getDoctrine()->getManager();

            $maintenancemateriel = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->find($id);

            $materielid = $maintenancemateriel->getMATERIELID();

            $materiel = $em->getRepository('TechniqueBundle:Materiel')->getMaterielId($materielid);

            $interventionp = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->getlistinterventionmaintenance($id);

            $interventionc = $em->getRepository('TechniqueBundle:MaintenanceIntervention')
                ->findBymAINTENANCEID($id);

            $articlesmaintenance = $em->getRepository('TechniqueBundle:MaintenanceArticle')->findBymAINTENANCEID($id);

            $articles = array();
            foreach ($articlesmaintenance as $r)
            {
                $idarticle = $r->getaRTICLEID();

                $article = $em->getRepository('TechniqueBundle:MaintenanceArticle')->getAllArticlesMaintenance($idarticle, $id);
                $articles[] = $article;

            }

            $interventionr = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->interventiomaintenanceC($id);

            $articlesmaintenancer = $em->getRepository('TechniqueBundle:MaintenanceArticle')->findBymAINTENANCEID($id);

            return $this->render('TechniqueBundle:MaintenanceMateriel:modifiermaintenance.html.twig', array(
                'maintenancemateriel' => $maintenancemateriel,
                'materiel' => $materiel,
                'interventionp' => $interventionp,
                'articlesmaintenance' => $articlesmaintenance,
                'articles' => $articles,
                'interventionr' => $interventionr,
                'articlesmaintenancer' => $articlesmaintenancer,
                'interventionc' => $interventionc
            ));
        }
    }

    public function ajaxmodifiermaintenanceAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();

            $conn = $em->getConnection();

            $user = $this->getUser()->getName();

            $id = $request->request->get('idmaintenance');
            $compteur = $request->request->get('compteur');
            $maintenancepro = $request->request->get('maintenancepro');
            $mecanicien = $request->request->get('mecanicien');
            $dateimmo = $request->request->get('dateimmo');

            $valeur = $_POST['checkmaintenance'];

            var_dump($id);
            var_dump($compteur);
            var_dump($valeur);

            //exit;

            $formdate = new \DateTime();
            $formdate = $formdate->format('Ymd');

            $dateimmo = new \DateTime($dateimmo);
            $dateimmo = $dateimmo->format('Ymd');

            $maintenance = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->find($id);

            if($compteur !='')
            {
                $sql = "UPDATE MAINTENANCEMATERIEL SET COMPTEUR = '$compteur', NOMOPERATEUR = '$user', DATEUPDATE = '$formdate',
                        CPTPROCHAIN = '$maintenancepro', NOMMECANICIEN = '$mecanicien' , DATEENTREE='$dateimmo'  WHERE MAINTENANCEMATERIELID = $id";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }

            if($maintenance->getTYPEMAINTENANCE() != 'CURATIVE')
            {
                $sql1 = "DELETE FROM MAINTENANCEINTERVENTION WHERE MAINTENANCEID = $id";
                $stmt1 = $conn->prepare($sql1);
                $stmt1->execute();

                $count = count($valeur);

                for($i = 0; $i < $count; $i++)
                {
                    $getvaleur = $valeur[$i];

                    var_dump($getvaleur);

                    $conn->insert('MAINTENANCEINTERVENTION', array('MAINTENANCEID' => $id, 'INTERVENTIONID' => $getvaleur));

                }
            }

            return $this->render('TechniqueBundle:MaintenanceMateriel:ajax.html.twig');
        }
    }

    public function realisemaintenanceAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $maintenancemateriel = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->find($id);

            $materielid = $maintenancemateriel->getMATERIELID();
            $compteur = $maintenancemateriel->getCOMPTEUR();

            $materiel = $em->getRepository('TechniqueBundle:Materiel')->getMaterielId($materielid);

            $interventionp = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->getlistinterventionmaintenance($id);

            $interventionc = $em->getRepository('TechniqueBundle:MaintenanceIntervention')->findBymAINTENANCEID($id);

            $articlesmaintenancec = $em->getRepository('TechniqueBundle:MaintenanceArticle')->findBymAINTENANCEID($id);

            $articlesmaintenance = $em->getRepository('TechniqueBundle:MaintenanceArticle')->getarticlesmaintenance($id);

            //var_dump($interventionc); exit;

            if($maintenancemateriel->getTYPEMAINTENANCE() == 'CURATIVE')
            {
                if ($maintenancemateriel->getDATEREALISE() == null) {
                    foreach ($interventionc as $srinterventionc) {
                        $mAINTENANCEID = $srinterventionc->getMAINTENANCEID();
                        $cOMPTEUR = $srinterventionc->getCOMPTEUR();
                        $dESCRIPTION = $srinterventionc->getDESCRIPTION();

                        $conn->insert('MAINTENANCEINTERVENTIONR', array('MAINTENANCEID' => $id, 'COMPTEUR' => $cOMPTEUR, 'ACTION' => 'Changer', 'DESCRIPTION' => $dESCRIPTION));
                    }

                    foreach ($articlesmaintenancec as $rsarticlesmaintenancec) {
                        $rEFARTICLE = $rsarticlesmaintenancec->getREFARTICLE();
                        $dESARTICLE = $rsarticlesmaintenancec->getDESARTICLE();
                        $qUANTITE = $rsarticlesmaintenancec->getQUANTITE();

                        $conn->insert('MAINTENANCEARTICLER', array('MAINTENANCEID' => $id, 'REFARTICLE' => $rEFARTICLE, 'DESARTICLE' => $dESARTICLE, 'QUANTITE' => $qUANTITE));
                    }
                }
            }
            else {
                if ($maintenancemateriel->getDATEREALISE() == null) {
                    foreach ($interventionp as $srinterventionp) {
                        $iidinterventiop = $srinterventionp['id'];
                        $cOMPTEUR = $srinterventionp['cOMPTEUR'];
                        $aCTION = $srinterventionp['aCTION'];
                        $dESCRIPTION = $srinterventionp['dESCRIPTION'];

                        $conn->insert('MAINTENANCEINTERVENTIONR', array('MAINTENANCEID' => $id, 'COMPTEUR' => $cOMPTEUR, 'ACTION' => $aCTION, 'DESCRIPTION' => $dESCRIPTION));
                    }
                    foreach ($articlesmaintenance as $rsarticlesmaintenance) {
                        $rEFARTICLE = $rsarticlesmaintenance['rEFARTICLE'];
                        $dESARTICLE = $rsarticlesmaintenance['dESARTICLE'];
                        $qUANTITE = $rsarticlesmaintenance['qUANTITE'];

                        $conn->insert('MAINTENANCEARTICLER', array('MAINTENANCEID' => $id, 'REFARTICLE' => $rEFARTICLE, 'DESARTICLE' => $dESARTICLE, 'QUANTITE' => $qUANTITE));
                    }
                }
            }
            $maintenancemateriel->setDATEREALISE(new \DateTime());
            $em->flush();

            $entity = new MaintenanceArticleR();

            $form = $this->createForm(new MaintenanceArticleRType(), $entity);

            if ($request->isMethod('post')) {
                $form->handleRequest($request);

                if ($form->isValid()) {

                    $entity->setMAINTENANCEID($id);
                    $em->persist($entity);
                    $em->flush();

                    return $this->redirect($this->generateUrl('realisemaintenance', array('id' => $entity->getMAINTENANCEID())));
                }
            }

            $interventionr = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getinterventionrealiseAction($id);

            $articlesmaintenancer = $em->getRepository('TechniqueBundle:MaintenanceArticleR')->findBymAINTENANCEID($id);

            $listinterventionp = $em->getRepository('TechniqueBundle:CompteurIntervention')->getListInterventions
            ($materielid, $compteur);

            foreach ($listinterventionp as $r1)
            {
                $listintp[] = $r1['dESCRIPTION'];
            }

           if($maintenancemateriel->getsTATUT() == 'REALISE' && $maintenancemateriel->getTYPEMAINTENANCE() == 'PREVENTIVE')
            {
                foreach ($interventionr as $r2)
                {
                    $listintr[] = $r2['dESCRIPTION'];
                }

                $result = array_diff($listintp, $listintr);

            return $this->render('TechniqueBundle:MaintenanceMateriel:realisemaintenance.html.twig', array(
                'maintenancemateriel' => $maintenancemateriel,
                'materiel' => $materiel,
                'interventionp' => $interventionp,
                'articlesmaintenance' => $articlesmaintenance,
//                'articles' => $articles,
                'interventionr' => $interventionr,
                'articlesmaintenancer' => $articlesmaintenancer,
                'interventionc' => $interventionc,
                'articlesmaintenancec' => $articlesmaintenancec,
                'result' => $result,
                'listinterventionp' => $listinterventionp
            ));
            }
            else
            {
                return $this->render('TechniqueBundle:MaintenanceMateriel:realisemaintenance.html.twig', array(
                    'maintenancemateriel' => $maintenancemateriel,
                    'materiel' => $materiel,
                    'interventionr' => $interventionr,
                    'articlesmaintenancer' => $articlesmaintenancer,
                    'form' => $form->createView()
                ));
            }


        }
    }

    public function ajaxrealisemaintenanceAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {
            $em = $this->getDoctrine()->getManager();

            $conn = $em->getConnection();

            $user = $this->getUser()->getName();

            $id = $request->request->get('idmaintenance');
            $compteur = $request->request->get('compteur');
            $maintenancepro = $request->request->get('maintenancepro');
            $mecanicien = $request->request->get('mecanicien');
            $autremaintenace = $request->request->get('autremaintenace');
            $mecaniciens = $request->get('mecanicien');
            $dateimmo = $request->get('dateimmo');
            $temitervent = $request->get('temitervent');
            $datesortie = $request->get('datesortie');
            $commentaire = $request->get('commentaire');

            $valeur = $_POST['checkmaintenance'];

            var_dump($id);
            var_dump($compteur);
            //var_dump($valeur);
            var_dump($autremaintenace);
            var_dump($dateimmo);
            var_dump($temitervent);
            var_dump($datesortie);

            var_dump($mecaniciens);
            $dateimmo = new \DateTime($dateimmo);
            $dateimmo = $dateimmo->format('Ymd');
            $daterealise = new \DateTime('now');
            $datesortie = new \DateTime($datesortie);

            $daterealise = $daterealise->format('Ymd');
            $datesortie = $datesortie->format('Ymd');

            var_dump($daterealise);
            var_dump($commentaire);
            //exit;
            $maintenance = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->find($id);

            if($compteur !=null && $datesortie != null && $maintenancepro != null && $mecaniciens !=null && $dateimmo
                !=null && $temitervent !=null)
            {
                $sql = "UPDATE MAINTENANCEMATERIEL SET OPERATUERREALISE = '$user', CPTPROCHAIN='$maintenancepro',
                        NOMMECANICIEN='$mecaniciens', DATEENTREE='$dateimmo', TEMPSREPARATION = '$temitervent',
                        DATEREALISE='$daterealise', COMMENTAIRE='$commentaire', DATESORTIEE='$datesortie',
                        COMPTEUR = '$compteur',  STATUT = 'REALISE'  WHERE
                        MAINTENANCEMATERIELID = $id";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
            else
            {
                $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs !!");
            }

            $count = count($valeur);
            $interventionr = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getinterventionrealiseAction($id);

            foreach($interventionr as $rsinterventior)
            {
                $interventionrid = $rsinterventior['interventionr'];
                $aCTION = $rsinterventior['aCTION'];
                $cOMPTEUR = $rsinterventior['cOMPTEUR'];
                $dESCRIPTION = $rsinterventior['dESCRIPTION'];

                if (!in_array($interventionrid, $valeur))
                {
                    $sql1 = "DELETE FROM MAINTENANCEINTERVENTIONR WHERE MAINTENANCEINTERVENTIONRID = $interventionrid";
                    $stmt1 = $conn->prepare($sql1);
                    $stmt1->execute();
                }

            }

            if($autremaintenace !=' ' && $autremaintenace != 'undefined')
            {
                $conn->insert('MAINTENANCEINTERVENTIONR', array('MAINTENANCEID' => $id, 'COMPTEUR' => $compteur, 'DESCRIPTION' => $autremaintenace));
            }

            $articlesmaintenance = $em->getRepository('TechniqueBundle:MaintenanceArticle')->getarticlesmaintenance($id);

            $articlesmaintenancec = $em->getRepository('TechniqueBundle:MaintenanceArticle')->findBymAINTENANCEID($id);

            $articlesmaintenancer = $em->getRepository('TechniqueBundle:MaintenanceArticleR')->findBymAINTENANCEID($id);

            return $this->render('TechniqueBundle:MaintenanceMateriel:ajax.html.twig');
        }
    }

    public function fichemaintenancecurativeAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $user = $this->getUser()->getName();
            $em = $this->getDoctrine()->getManager();

            $entity = new MaintenanceMateriel();

            $form = $this->createForm(new MaintenanceMaterielType(), $entity);

            if($request->isMethod('post'))
            {

                $form->handleRequest($request);

                if($form->isValid())
                {
                    try
                    {
                        $entity->setNOMOPERATEUR($user);
                        $entity->setTYPEMAINTENANCE('CURATIVE');

                        $em->persist($entity);
                        $em->flush();

                        return $this->redirect($this->generateUrl('ajoutinterventionc', array('idmaintenance' => $entity->getId())));
                    }
                    catch(NotNullConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs !!");
                    }
                }
            }

            return $this->render('TechniqueBundle:MaintenanceMateriel:fichemaintenancecurative.html.twig', array(
                'form' => $form->createView(),
                'entity' => $entity,
            ));
        }
    }

    public function annulermaintenaceAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $idmaintenance = $request->query->get('idmaintenance');

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $user = $this->getUser()->getName();

            $dateformat = new \DateTime('now');

            $dateformat = $dateformat->format('Ymd');

            $sql = "UPDATE MAINTENANCEMATERIEL SET STATUT ='ANNULE', DATEUPDATE = '$dateformat', NOMOPERATEUR =
            '$user' WHERE MAINTENANCEMATERIELID = $idmaintenance";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            return $this->redirect($this->generateUrl('maintenance'));

        }
    }

    public function supprimermaintenanceAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $idmaintenance = $request->query->get('idmaintenance');

            $em = $this->getDoctrine()->getManager();

            $maintenance = $this->getDoctrine()->getManager()->getRepository('TechniqueBundle:MaintenanceMateriel')->find($idmaintenance);

            $em->remove($maintenance);
            $em->flush();

            return $this->redirect($this->generateUrl('maintenance'));

        }
    }

}