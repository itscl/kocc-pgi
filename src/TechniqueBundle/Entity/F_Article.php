<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * F_Article
 */
class F_Article
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $aRDesign;

    /**
     * @var string
     */
    private $fACodeFamille;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set aRDesign
     *
     * @param string $aRDesign
     * @return F_Article
     */
    public function setARDesign($aRDesign)
    {
        $this->aRDesign = $aRDesign;

        return $this;
    }

    /**
     * Get aRDesign
     *
     * @return string 
     */
    public function getARDesign()
    {
        return $this->aRDesign;
    }

    /**
     * Set fACodeFamille
     *
     * @param string $fACodeFamille
     * @return F_Article
     */
    public function setFACodeFamille($fACodeFamille)
    {
        $this->fACodeFamille = $fACodeFamille;

        return $this;
    }

    /**
     * Get fACodeFamille
     *
     * @return string 
     */
    public function getFACodeFamille()
    {
        return $this->fACodeFamille;
    }
}
