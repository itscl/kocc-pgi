<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MaintenanceArticle
 */
class MaintenanceArticle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $aRTICLEID;

    /**
     * @var int
     */
    private $qUANTITE;

    /**
     * @var int
     */
    private $mAINTENANCEID;

    /**
     * @var string
     */
    private $rEFARTICLE;

    /**
     * @var string
     */
    private $dESARTICLE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set aRTICLEID
     *
     * @param integer $aRTICLEID
     * @return MaintenanceArticle
     */
    public function setARTICLEID($aRTICLEID)
    {
        $this->aRTICLEID = $aRTICLEID;

        return $this;
    }

    /**
     * Get aRTICLEID
     *
     * @return integer 
     */
    public function getARTICLEID()
    {
        return $this->aRTICLEID;
    }

    /**
     * Set qUANTITE
     *
     * @param integer $qUANTITE
     * @return MaintenanceArticle
     */
    public function setQUANTITE($qUANTITE)
    {
        $this->qUANTITE = $qUANTITE;

        return $this;
    }

    /**
     * Get qUANTITE
     *
     * @return integer 
     */
    public function getQUANTITE()
    {
        return $this->qUANTITE;
    }

    /**
     * Set mAINTENANCEID
     *
     * @param integer $mAINTENANCEID
     * @return MaintenanceArticle
     */
    public function setMAINTENANCEID($mAINTENANCEID)
    {
        $this->mAINTENANCEID = $mAINTENANCEID;

        return $this;
    }

    /**
     * Get mAINTENANCEID
     *
     * @return integer 
     */
    public function getMAINTENANCEID()
    {
        return $this->mAINTENANCEID;
    }

    /**
     * Set rEFARTICLE
     *
     * @param string $rEFARTICLE
     * @return MaintenanceArticle
     */
    public function setREFARTICLE($rEFARTICLE)
    {
        $this->rEFARTICLE = $rEFARTICLE;

        return $this;
    }

    /**
     * Get rEFARTICLE
     *
     * @return string
     */
    public function getREFARTICLE()
    {
        return $this->rEFARTICLE;
    }

    /**
     * Set dESARTICLE
     *
     * @param string $dESARTICLE
     * @return MaintenanceArticle
     */
    public function setDESARTICLE($dESARTICLE)
    {
        $this->dESARTICLE = $dESARTICLE;

        return $this;
    }

    /**
     * Get dESARTICLE
     *
     * @return string
     */
    public function getDESARTICLE()
    {
        return $this->dESARTICLE;
    }
}
