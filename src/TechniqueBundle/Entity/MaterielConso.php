<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * MaterielConso
 */
class MaterielConso
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mAGASINCONSOID;

    /**
     * @var int
     */
    private $mATERIELID;

    /**
     * @var \DateTime
     */
    private $dATECONSO;

    /**
     * @var int
     */
    private $qTE;

    /**
     * @var int
     */
    private $cTR;

    /**
     * @var string
     */
    private $pLEIN;

    /**
     * @var int
     */
    private $cHAUFFEURID;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var int
     */
    private $mATERIELCONSOPID;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var string
     */
    private $oRIGINSITEID;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mAGASINCONSOID
     *
     * @param integer $mAGASINCONSOID
     * @return MATERIELCONSO
     */
    public function setMAGASINCONSOID($mAGASINCONSOID)
    {
        $this->mAGASINCONSOID = $mAGASINCONSOID;

        return $this;
    }

    /**
     * Get mAGASINCONSOID
     *
     * @return integer
     */
    public function getMAGASINCONSOID()
    {
        return $this->mAGASINCONSOID;
    }

    /**
     * Set mATERIELID
     *
     * @param integer $mATERIELID
     * @return MATERIELCONSO
     */
    public function setMATERIELID($mATERIELID)
    {
        $this->mATERIELID = $mATERIELID;

        return $this;
    }

    /**
     * Get mATERIELID
     *
     * @return integer
     */
    public function getMATERIELID()
    {
        return $this->mATERIELID;
    }

    /**
     * Set dATECONSO
     *
     * @param \DateTime $dATECONSO
     * @return MATERIELCONSO
     */
    public function setDATECONSO($dATECONSO)
    {
        $this->dATECONSO = $dATECONSO;

        return $this;
    }

    /**
     * Get dATECONSO
     *
     * @return \DateTime
     */
    public function getDATECONSO()
    {
        return $this->dATECONSO;
    }

    /**
     * Set qTE
     *
     * @param integer $qTE
     * @return MATERIELCONSO
     */
    public function setQTE($qTE)
    {
        $this->qTE = $qTE;

        return $this;
    }

    /**
     * Get qTE
     *
     * @return integer
     */
    public function getQTE()
    {
        return $this->qTE;
    }

    /**
     * Set cTR
     *
     * @param integer $cTR
     * @return MATERIELCONSO
     */
    public function setCTR($cTR)
    {
        $this->cTR = $cTR;

        return $this;
    }

    /**
     * Get cTR
     *
     * @return integer
     */
    public function getCTR()
    {
        return $this->cTR;
    }

    /**
     * Set pLEIN
     *
     * @param string $pLEIN
     * @return MATERIELCONSO
     */
    public function setPLEIN($pLEIN)
    {
        $this->pLEIN = $pLEIN;

        return $this;
    }

    /**
     * Get pLEIN
     *
     * @return string
     */
    public function getPLEIN()
    {
        return $this->pLEIN;
    }

    /**
     * Set cHAUFFEURID
     *
     * @param integer $cHAUFFEURID
     * @return MATERIELCONSO
     */
    public function setCHAUFFEURID($cHAUFFEURID)
    {
        $this->cHAUFFEURID = $cHAUFFEURID;

        return $this;
    }

    /**
     * Get cHAUFFEURID
     *
     * @return integer
     */
    public function getCHAUFFEURID()
    {
        return $this->cHAUFFEURID;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return MATERIELCONSO
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return MATERIELCONSO
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set mATERIELCONSOPID
     *
     * @param integer $mATERIELCONSOPID
     * @return MATERIELCONSO
     */
    public function setMATERIELCONSOPID($mATERIELCONSOPID)
    {
        $this->mATERIELCONSOPID = $mATERIELCONSOPID;

        return $this;
    }

    /**
     * Get mATERIELCONSOPID
     *
     * @return integer
     */
    public function getMATERIELCONSOPID()
    {
        return $this->mATERIELCONSOPID;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return MATERIELCONSO
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return MATERIELCONSO
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param string $oRIGINSITEID
     * @return MATERIELCONSO
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return string
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }
}

class MaterielConsoListener
{
    private $cacheDriver;

    public function __construct($cacheDriver)
    {
        $this->cacheDriver = $cacheDriver;
    }

    public function postPersist(MaterielConso $materielConso, LifecycleEventArgs $args)
    {
        $this->cacheDriver->expire('[all_conso][1]', 0);
    }

    public function postUpdate(MaterielConso $materielConso, LifecycleEventArgs $args)
    {
        $this->cacheDriver->expire('[all_conso][1]', 0);
    }

    public function postRemove(MaterielConso $materielConso, LifecycleEventArgs $args)
    {
        $this->cacheDriver->expire('[all_conso][1]', 0);
    }
}