<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MaintenanceArticleR
 */
class MaintenanceArticleR
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mAINTENANCEID;

    /**
     * @var string
     */
    private $rEFARTICLE;

    /**
     * @var string
     */
    private $dESARTICLE;

    /**
     * @var int
     */
    private $qUANTITE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mAINTENANCEID
     *
     * @param integer $mAINTENANCEID
     * @return MaintenanceArticleR
     */
    public function setMAINTENANCEID($mAINTENANCEID)
    {
        $this->mAINTENANCEID = $mAINTENANCEID;

        return $this;
    }

    /**
     * Get mAINTENANCEID
     *
     * @return integer 
     */
    public function getMAINTENANCEID()
    {
        return $this->mAINTENANCEID;
    }

    /**
     * Set rEFARTICLE
     *
     * @param string $rEFARTICLE
     * @return MaintenanceArticleR
     */
    public function setREFARTICLE($rEFARTICLE)
    {
        $this->rEFARTICLE = $rEFARTICLE;

        return $this;
    }

    /**
     * Get rEFARTICLE
     *
     * @return string 
     */
    public function getREFARTICLE()
    {
        return $this->rEFARTICLE;
    }

    /**
     * Set dESARTICLE
     *
     * @param string $dESARTICLE
     * @return MaintenanceArticleR
     */
    public function setDESARTICLE($dESARTICLE)
    {
        $this->dESARTICLE = $dESARTICLE;

        return $this;
    }

    /**
     * Get dESARTICLE
     *
     * @return string 
     */
    public function getDESARTICLE()
    {
        return $this->dESARTICLE;
    }

    /**
     * Set qUANTITE
     *
     * @param integer $qUANTITE
     * @return MaintenanceArticleR
     */
    public function setQUANTITE($qUANTITE)
    {
        $this->qUANTITE = $qUANTITE;

        return $this;
    }

    /**
     * Get qUANTITE
     *
     * @return integer 
     */
    public function getQUANTITE()
    {
        return $this->qUANTITE;
    }
}
