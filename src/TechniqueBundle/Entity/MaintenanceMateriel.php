<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MaintenanceMateriel
 */
class MaintenanceMateriel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mATERIELID;

    /**
     * @var int
     */
    private $cOMPTEUR;

    /**
     * @var \Datetime
     */
    private $dATEMAINTENANCE;

    /**
     * @var string
     */
    private $nOMOPERATEUR;

    /**
     * @var string
     */
    private $sTATUT;

    /**
     * @var \Datetime
     */
    private $dATEDIFFUSE;

    /**
     * @var \Datetime
     */
    private $dATEREALISE;

    /**
     * @var string
     */
    private $oPERATEURDIFFUSER;

    /**
     * @var int
     */
    private $cPTPROCHAIN;

    /**
     * @var string
     */
    private $nOMMECANICIEN;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var string
     */
    private $tYPEMAINTENANCE;

    /**
     * @var \Datetime
     */
    private $dATEENTREE;

    /**
     * @var \Datetime
     */
    private $dATESORTIEE;

    /**
     * @var float
     */
    private $tEMPSREPARATION;


    /**
     * @var string
     */
    private $oPERATUERREALISE;

    public function __construct()
    {
        $this->sTATUT = "SAISI";
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mATERIELID
     *
     * @param integer $mATERIELID
     * @return MaintenanceMateriel
     */
    public function setMATERIELID($mATERIELID)
    {
        $this->mATERIELID = $mATERIELID;

        return $this;
    }

    /**
     * Get mATERIELID
     *
     * @return integer 
     */
    public function getMATERIELID()
    {
        return $this->mATERIELID;
    }

    /**
     * Set cOMPTEUR
     *
     * @param integer $cOMPTEUR
     * @return MaintenanceMateriel
     */
    public function setCOMPTEUR($cOMPTEUR)
    {
        $this->cOMPTEUR = $cOMPTEUR;

        return $this;
    }

    /**
     * Get cOMPTEUR
     *
     * @return integer
     */
    public function getCOMPTEUR()
    {
        return $this->cOMPTEUR;
    }

    /**
     * Set dATEMAINTENANCE
     *
     * @param integer $dATEMAINTENANCE
     * @return MaintenanceMateriel
     */
    public function setDATEMAINTENANCE($dATEMAINTENANCE)
    {
        $this->dATEMAINTENANCE = $dATEMAINTENANCE;

        return $this;
    }

    /**
     * Get dATEMAINTENANCE
     *
     * @return integer
     */
    public function getDATEMAINTENANCE()
    {
        return $this->dATEMAINTENANCE;
    }

    /**
     * Set nOMOPERATEUR
     *
     * @param integer $nOMOPERATEUR
     * @return MaintenanceMateriel
     */
    public function setNOMOPERATEUR($nOMOPERATEUR)
    {
        $this->nOMOPERATEUR = $nOMOPERATEUR;

        return $this;
    }

    /**
     * Get nOMOPERATEUR
     *
     * @return integer
     */
    public function getNOMOPERATEUR()
    {
        return $this->nOMOPERATEUR;
    }

    /**
     * Set sTATUT
     *
     * @param integer $sTATUT
     * @return MaintenanceMateriel
     */
    public function setSTATUT($sTATUT)
    {
        $this->sTATUT = $sTATUT;

        return $this;
    }

    /**
     * Get sTATUT
     *
     * @return integer
     */
    public function getSTATUT()
    {
        return $this->sTATUT;
    }

    /**
     * Set dATEDIFFUSE
     *
     * @param \DateTime $dATEDIFFUSE
     * @return MaintenanceMateriel
     */
    public function setDATEDIFFUSE($dATEDIFFUSE)
    {
        $this->dATEDIFFUSE = $dATEDIFFUSE;

        return $this;
    }

    /**
     * Get dATEDIFFUSE
     *
     * @return \DateTime
     */
    public function getDATEDIFFUSE()
    {
        return $this->dATEDIFFUSE;
    }

    /**
     * Set dATEREALISE
     *
     * @param \DateTime $dATEREALISE
     * @return MaintenanceMateriel
     */
    public function setDATEREALISE($dATEREALISE)
    {
        $this->dATEREALISE = $dATEREALISE;

        return $this;
    }

    /**
     * Get dATEREALISE
     *
     * @return \DateTime
     */
    public function getDATEREALISE()
    {
        return $this->dATEREALISE;
    }

    /**
     * Set oPERATUERREALISE
     *
     * @param string $oPERATUERREALISE
     * @return MaintenanceMateriel
     */
    public function setOPERATUERREALISE($oPERATUERREALISE)
    {
        $this->oPERATUERREALISE = $oPERATUERREALISE;

        return $this;
    }

    /**
     * Get oPERATUERREALISE
     *
     * @return string
     */
    public function getOPERATUERREALISE()
    {
        return $this->oPERATUERREALISE;
    }

    /**
     * Set cPTPROCHAIN
     *
     * @param integer $cPTPROCHAIN
     * @return MaintenanceMateriel
     */
    public function setCPTPROCHAIN($cPTPROCHAIN)
    {
        $this->cPTPROCHAIN = $cPTPROCHAIN;

        return $this;
    }

    /**
     * Get cPTPROCHAIN
     *
     * @return string
     */
    public function getCPTPROCHAIN()
    {
        return $this->cPTPROCHAIN;
    }

    /**
     * Set nOMMECANICIEN
     *
     * @param string $nOMMECANICIEN
     * @return MaintenanceMateriel
     */
    public function setNOMMECANICIEN($nOMMECANICIEN)
    {
        $this->nOMMECANICIEN = $nOMMECANICIEN;

        return $this;
    }

    /**
     * Get nOMMECANICIEN
     *
     * @return string
     */
    public function getNOMMECANICIEN()
    {
        return $this->nOMMECANICIEN;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return MaintenanceMateriel
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set tYPEMAINTENANCE
     *
     * @param string $tYPEMAINTENANCE
     * @return MaintenanceMateriel
     */
    public function setTYPEMAINTENANCE($tYPEMAINTENANCE)
    {
        $this->tYPEMAINTENANCE = $tYPEMAINTENANCE;

        return $this;
    }

    /**
     * Get tYPEMAINTENANCE
     *
     * @return string
     */
    public function getTYPEMAINTENANCE()
    {
        return $this->tYPEMAINTENANCE;
    }

    /**
     * Get dATEENTREE
     *
     * @return \Datetime
     */
    public function getDATEENTREE()
    {
        return $this->dATEENTREE;
    }

    /**
     * Set dATEENTREE
     *
     * @param \Datetime $dATEENTREE
     * @return MaintenanceMateriel
     */
    public function setDATEENTREE($dATEENTREE)
    {
        $this->dATEENTREE = $dATEENTREE;
    }

    /**
     * Get dATESORTIEE
     *
     * @return \Datetime
     */
    public function getDATESORTIEE()
    {
        return $this->dATESORTIEE;
    }

    /**
     * Set dATESORTIEE
     *
     * @param \Datetime $dATESORTIEE
     * @return MaintenanceMateriel
     */
    public function setDATESORTIEE($dATESORTIEE)
    {
        $this->dATESORTIEE = $dATESORTIEE;
    }

    /**
     * Get tEMPSREPARATION
     *
     * @return float
     */
    public function getTEMPSREPARATION()
    {
        return $this->tEMPSREPARATION;
    }

    /**
     * Set tEMPSREPARATION
     *
     * @param float $tEMPSREPARATION
     * @return MaintenanceMateriel
     */
    public function setTEMPSREPARATION($tEMPSREPARATION)
    {
        $this->tEMPSREPARATION = $tEMPSREPARATION;
    }




    /**
     * Set oPERATEURDIFFUSER
     *
     * @param string $oPERATEURDIFFUSER
     * @return MaintenanceMateriel
     */
    public function setOPERATEURDIFFUSER($oPERATEURDIFFUSER)
    {
        $this->oPERATEURDIFFUSER = $oPERATEURDIFFUSER;

        return $this;
    }

    /**
     * Get oPERATEURDIFFUSER
     *
     * @return string 
     */
    public function getOPERATEURDIFFUSER()
    {
        return $this->oPERATEURDIFFUSER;
    }
}
