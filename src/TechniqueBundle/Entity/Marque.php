<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Marque
 */
class Marque
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $libelle;

    /**
     * @var string
     */
    private $code;

    /**
     * @var \DateTime
     */
    private $dateinvalide;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return marque
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return marque
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set dateinvalide
     *
     * @param \DateTime $dateinvalide
     * @return marque
     */
    public function setDateinvalide($dateinvalide)
    {
        $this->dateinvalide = $dateinvalide;

        return $this;
    }

    /**
     * Get dateinvalide
     *
     * @return \DateTime
     */
    public function getDateinvalide()
    {
        return $this->dateinvalide;
    }
}
