<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Famille
 */
class Famille
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $cODEGESCOM;

    /**
     * @var string
     */
    private $lIBELLEGESCOM;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var string
     */
    private $oRIGINSITEID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return Famille
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string 
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return Famille
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set cODEGESCOM
     *
     * @param string $cODEGESCOM
     * @return Famille
     */
    public function setCODEGESCOM($cODEGESCOM)
    {
        $this->cODEGESCOM = $cODEGESCOM;

        return $this;
    }

    /**
     * Get cODEGESCOM
     *
     * @return string 
     */
    public function getCODEGESCOM()
    {
        return $this->cODEGESCOM;
    }

    /**
     * Set lIBELLEGESCOM
     *
     * @param string $lIBELLEGESCOM
     * @return Famille
     */
    public function setLIBELLEGESCOM($lIBELLEGESCOM)
    {
        $this->lIBELLEGESCOM = $lIBELLEGESCOM;

        return $this;
    }

    /**
     * Get lIBELLEGESCOM
     *
     * @return string 
     */
    public function getLIBELLEGESCOM()
    {
        return $this->lIBELLEGESCOM;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return Famille
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return Famille
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return Famille
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param string $oRIGINSITEID
     * @return Famille
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return string 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }
}
