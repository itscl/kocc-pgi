<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 */
class Article
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $aRREF;

    /**
     * @var string
     */
    private $lIBELLEGESCOM;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $dESIGNATION;

    /**
     * @var string
     */
    private $sUIVISTOCK;

    /**
     * @var string
     */
    private $uNITESTOCK;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var int
     */
    private $fAMILLEID;

    /**
     * @var string
     */
    private $tABLEREF;

    /**
     * @var int
     */
    private $tABLEREFID;

    /**
     * @var string
     */
    private $uNITE;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set aRREF
     *
     * @param string $aRREF
     * @return ARTICLE
     */
    public function setARREF($aRREF)
    {
        $this->aRREF = $aRREF;

        return $this;
    }

    /**
     * Get aRREF
     *
     * @return string
     */
    public function getARREF()
    {
        return $this->aRREF;
    }

    /**
     * Set lIBELLEGESCOM
     *
     * @param string $lIBELLEGESCOM
     * @return ARTICLE
     */
    public function setLIBELLEGESCOM($lIBELLEGESCOM)
    {
        $this->lIBELLEGESCOM = $lIBELLEGESCOM;

        return $this;
    }

    /**
     * Get lIBELLEGESCOM
     *
     * @return string
     */
    public function getLIBELLEGESCOM()
    {
        return $this->lIBELLEGESCOM;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return ARTICLE
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set dESIGNATION
     *
     * @param string $dESIGNATION
     * @return ARTICLE
     */
    public function setDESIGNATION($dESIGNATION)
    {
        $this->dESIGNATION = $dESIGNATION;

        return $this;
    }

    /**
     * Get dESIGNATION
     *
     * @return string
     */
    public function getDESIGNATION()
    {
        return $this->dESIGNATION;
    }

    /**
     * Set sUIVISTOCK
     *
     * @param string $sUIVISTOCK
     * @return ARTICLE
     */
    public function setSUIVISTOCK($sUIVISTOCK)
    {
        $this->sUIVISTOCK = $sUIVISTOCK;

        return $this;
    }

    /**
     * Get sUIVISTOCK
     *
     * @return string
     */
    public function getSUIVISTOCK()
    {
        return $this->sUIVISTOCK;
    }

    /**
     * Set uNITESTOCK
     *
     * @param string $uNITESTOCK
     * @return ARTICLE
     */
    public function setUNITESTOCK($uNITESTOCK)
    {
        $this->uNITESTOCK = $uNITESTOCK;

        return $this;
    }

    /**
     * Get uNITESTOCK
     *
     * @return string
     */
    public function getUNITESTOCK()
    {
        return $this->uNITESTOCK;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ARTICLE
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ARTICLE
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ARTICLE
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ARTICLE
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return ARTICLE
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set fAMILLEID
     *
     * @param integer $fAMILLEID
     * @return ARTICLE
     */
    public function setFAMILLEID($fAMILLEID)
    {
        $this->fAMILLEID = $fAMILLEID;

        return $this;
    }

    /**
     * Get fAMILLEID
     *
     * @return integer
     */
    public function getFAMILLEID()
    {
        return $this->fAMILLEID;
    }

    /**
     * Set tABLEREF
     *
     * @param string $tABLEREF
     * @return ARTICLE
     */
    public function setTABLEREF($tABLEREF)
    {
        $this->tABLEREF = $tABLEREF;

        return $this;
    }

    /**
     * Get tABLEREF
     *
     * @return string
     */
    public function getTABLEREF()
    {
        return $this->tABLEREF;
    }

    /**
     * Set tABLEREFID
     *
     * @param integer $tABLEREFID
     * @return ARTICLE
     */
    public function setTABLEREFID($tABLEREFID)
    {
        $this->tABLEREFID = $tABLEREFID;

        return $this;
    }

    /**
     * Get tABLEREFID
     *
     * @return integer
     */
    public function getTABLEREFID()
    {
        return $this->tABLEREFID;
    }

    /**
     * Set uNITE
     *
     * @param string $uNITE
     * @return ARTICLE
     */
    public function setUNITE($uNITE)
    {
        $this->uNITE = $uNITE;

        return $this;
    }

    /**
     * Get uNITE
     *
     * @return string
     */
    public function getUNITE()
    {
        return $this->uNITE;
    }
}
