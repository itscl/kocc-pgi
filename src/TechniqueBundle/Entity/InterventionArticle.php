<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InterventionArticle
 */
class InterventionArticle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $rEFARTICLE;

    /**
     * @var string
     */
    private $dESARTICLE;

    /**
     * @var int
     */
    private $iNTERVENTIONID;

    /**
     * @var int
     */
    private $qUANTITE;

    /**
     * @var string
     */
    private $uNITE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rEFARTICLE
     *
     * @param string $rEFARTICLE
     * @return InterventionArticle
     */
    public function setREFARTICLE($rEFARTICLE)
    {
        $this->rEFARTICLE = $rEFARTICLE;

        return $this;
    }

    /**
     * Get rEFARTICLE
     *
     * @return string 
     */
    public function getREFARTICLE()
    {
        return $this->rEFARTICLE;
    }

    /**
     * Set dESARTICLE
     *
     * @param string $dESARTICLE
     * @return InterventionArticle
     */
    public function setDESARTICLE($dESARTICLE)
    {
        $this->dESARTICLE = $dESARTICLE;

        return $this;
    }

    /**
     * Get dESARTICLE
     *
     * @return string 
     */
    public function getDESARTICLE()
    {
        return $this->dESARTICLE;
    }

    /**
     * Set iNTERVENTIONID
     *
     * @param integer $iNTERVENTIONID
     * @return InterventionArticle
     */
    public function setINTERVENTIONID($iNTERVENTIONID)
    {
        $this->iNTERVENTIONID = $iNTERVENTIONID;

        return $this;
    }

    /**
     * Get iNTERVENTIONID
     *
     * @return integer 
     */
    public function getINTERVENTIONID()
    {
        return $this->iNTERVENTIONID;
    }

    /**
     * Set qUANTITE
     *
     * @param integer $qUANTITE
     * @return InterventionArticle
     */
    public function setQUANTITE($qUANTITE)
    {
        $this->qUANTITE = $qUANTITE;

        return $this;
    }

    /**
     * Get qUANTITE
     *
     * @return integer
     */
    public function getQUANTITE()
    {
        return $this->qUANTITE;
    }

    /**
     * Set uNITE
     *
     * @param string $uNITE
     * @return InterventionArticle
     */
    public function setUNITE($uNITE)
    {
        $this->uNITE = $uNITE;

        return $this;
    }

    /**
     * Get uNITE
     *
     * @return string
     */
    public function getUNITE()
    {
        return $this->uNITE;
    }
}
