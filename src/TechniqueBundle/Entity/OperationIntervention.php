<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OperationIntervention
 */
class OperationIntervention
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dESCRIPTION;

    /**
     * @var string
     */
    private $aCTION;

    /**
     * @var int
     */
    private $cOMPTEURINTERVENTIONID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dESCRIPTION
     *
     * @param string $dESCRIPTION
     * @return OperationIntervention
     */
    public function setDESCRIPTION($dESCRIPTION)
    {
        $this->dESCRIPTION = $dESCRIPTION;

        return $this;
    }

    /**
     * Get dESCRIPTION
     *
     * @return string 
     */
    public function getDESCRIPTION()
    {
        return $this->dESCRIPTION;
    }

    /**
     * Set aCTION
     *
     * @param string $aCTION
     * @return OperationIntervention
     */
    public function setACTION($aCTION)
    {
        $this->aCTION = $aCTION;

        return $this;
    }

    /**
     * Get aCTION
     *
     * @return string
     */
    public function getACTION()
    {
        return $this->aCTION;
    }

    /**
     * Set cOMPTEURINTERVENTIONID
     *
     * @param integer $cOMPTEURINTERVENTIONID
     * @return OperationIntervention
     */
    public function setCOMPTEURINTERVENTIONID($cOMPTEURINTERVENTIONID)
    {
        $this->cOMPTEURINTERVENTIONID = $cOMPTEURINTERVENTIONID;

        return $this;
    }

    /**
     * Get cOMPTEURINTERVENTIONID
     *
     * @return integer 
     */
    public function getCOMPTEURINTERVENTIONID()
    {
        return $this->cOMPTEURINTERVENTIONID;
    }
}
