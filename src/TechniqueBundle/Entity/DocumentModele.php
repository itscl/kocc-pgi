<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentModele
 */
class DocumentModele
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var integer
     */
    private $mODELE;

    /**
     * @var integer
     */
    private $DOCUMENT;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    public function getMODELE()
    {
        return $this->mODELE;
    }


    public function setMODELE($mODELE)
    {
        $this->mODELE = $mODELE;
    }

    public function setDOCUMENT(Document $document = null)
    {
        $this->DOCUMENT = $document;

        //return $this;
    }


    public function getDOCUMENT()
    {
        return $this->DOCUMENT;
    }
}
