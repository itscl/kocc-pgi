<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonnelQ
 */
class PersonnelQ
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $pERSONNELID;

    /**
     * @var int
     */
    private $qUALIFICATIONID;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var string
     */
    private $oRIGINSITEID;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pERSONNELID
     *
     * @param int $pERSONNELID
     * @return PersonnelQ
     */
    public function setPERSONNELID($pERSONNELID)
    {
        $this->pERSONNELID = $pERSONNELID;

        return $this;
    }

    /**
     * Get pERSONNELID
     *
     * @return int
     */
    public function getPERSONNELID()
    {
        return $this->pERSONNELID;
    }

    /**
     * Set qUALIFICATIONID
     *
     * @param int $qUALIFICATIONID
     * @return PersonnelQ
     */
    public function setQUALIFICATIONID($qUALIFICATIONID)
    {
        $this->qUALIFICATIONID = $qUALIFICATIONID;

        return $this;
    }

    /**
     * Get qUALIFICATIONID
     *
     * @return int
     */
    public function getQUALIFICATIONID()
    {
        return $this->qUALIFICATIONID;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return PersonnelQ
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param string $oRIGINSITEID
     * @return PersonnelQ
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return string
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return PersonnelQ
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return PersonnelQ
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return PersonnelQ
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }
}
