<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Magasin
 */
class Magasin
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $fERMEID;

    /**
     * @var string
     */
    private $tYPEMAGASIN;

    /**
     * @var string
     */
    private $cAPACITE;

    /**
     * @var string
     */
    private $qTEMAX;

    /**
     * @var string
     */
    private $qTEALERTE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return MAGASIN
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return MAGASIN
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set fERMEID
     *
     * @param string $fERMEID
     * @return MAGASIN
     */
    public function setFERMEID($fERMEID)
    {
        $this->fERMEID = $fERMEID;

        return $this;
    }

    /**
     * Get fERMEID
     *
     * @return string
     */
    public function getFERMEID()
    {
        return $this->fERMEID;
    }

    /**
     * Set tYPEMAGASIN
     *
     * @param string $tYPEMAGASIN
     * @return MAGASIN
     */
    public function setTYPEMAGASIN($tYPEMAGASIN)
    {
        $this->tYPEMAGASIN = $tYPEMAGASIN;

        return $this;
    }

    /**
     * Get tYPEMAGASIN
     *
     * @return string
     */
    public function getTYPEMAGASIN()
    {
        return $this->tYPEMAGASIN;
    }

    /**
     * Set cAPACITE
     *
     * @param string $cAPACITE
     * @return MAGASIN
     */
    public function setCAPACITE($cAPACITE)
    {
        $this->cAPACITE = $cAPACITE;

        return $this;
    }

    /**
     * Get cAPACITE
     *
     * @return string
     */
    public function getCAPACITE()
    {
        return $this->cAPACITE;
    }

    /**
     * Set qTEMAX
     *
     * @param string $qTEMAX
     * @return MAGASIN
     */
    public function setQTEMAX($qTEMAX)
    {
        $this->qTEMAX = $qTEMAX;

        return $this;
    }

    /**
     * Get qTEMAX
     *
     * @return string
     */
    public function getQTEMAX()
    {
        return $this->qTEMAX;
    }

    /**
     * Set qTEALERTE
     *
     * @param string $qTEALERTE
     * @return MAGASIN
     */
    public function setQTEALERTE($qTEALERTE)
    {
        $this->qTEALERTE = $qTEALERTE;

        return $this;
    }

    /**
     * Get qTEALERTE
     *
     * @return string
     */
    public function getQTEALERTE()
    {
        return $this->qTEALERTE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return MAGASIN
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return MAGASIN
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return MAGASIN
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return MAGASIN
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }
}
