<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Materiel
 */
class Materiel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var int
     */
    private $mATERIELMARQUEID;

    /**
     * @var string
     */
    private $mODELE;

    /**
     * @var int
     */
    private $cOUTHEURE;

    /**
     * @var int
     */
    private $cOUTHECTARE;

    /**
     * @var string
     */
    private $tYPEMATERIEL;

    /**
     * @var string
     */
    private $iMMATRICULATION;

    /**
     * @var string
     */
    private $eTAT;

    /**
     * @var int
     */
    private $cHAUFFEURID;

    /**
     * @var string
     */
    private $cARBURANT;

    /**
     * @var string
     */
    private $nOSERIE;

    /**
     * @var string
     */
    private $tOUR;

    /**
     * @var string
     */
    private $vOLUME;

    /**
     * @var int
     */
    private $pOIDS;

    /**
     * @var int
     */
    private $cONSMIN;

    /**
     * @var int
     */
    private $cONSMAX;

    /**
     * @var int
     */
    private $tRACTION;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var int
     */
    private $dEBITCHANTIERHAJ;

    /**
     * @var int
     */
    private $dEBITCHANTIERHHA;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var string
     */
    private $uNITECONSO;

    /**
     * @var int
     */
    private $aRTICLEID;

    /**
     * @var int
     */
    private $cODEIMMO;

    /**
     * @var int
     */
    private $sERVICEID;

    /**
     * @var int
     */
    private $cAPACITERESERVOIR;

    /**
     * @var int
     */
    private $sITEID;

    /**
     * @var \DateTime
     */
    private $dATEMISESERVICE;

    /**
     * @var int
     */
    private $sOUSFAMILLEID;

    /**
     * @var int
     */
    private $mODELEID;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return MATERIEL
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return MATERIEL
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set mATERIELMARQUEID
     *
     * @param integer $mATERIELMARQUEID
     * @return MATERIEL
     */
    public function setMATERIELMARQUEID($mATERIELMARQUEID)
    {
        $this->mATERIELMARQUEID = $mATERIELMARQUEID;

        return $this;
    }

    /**
     * Get mATERIELMARQUEID
     *
     * @return integer
     */
    public function getMATERIELMARQUEID()
    {
        return $this->mATERIELMARQUEID;
    }

    /**
     * Set mODELE
     *
     * @param string $mODELE
     * @return MATERIEL
     */
    public function setMODELE($mODELE)
    {
        $this->mODELE = $mODELE;

        return $this;
    }

    /**
     * Get mODELE
     *
     * @return string
     */
    public function getMODELE()
    {
        return $this->mODELE;
    }

    /**
     * Set cOUTHEURE
     *
     * @param integer $cOUTHEURE
     * @return MATERIEL
     */
    public function setCOUTHEURE($cOUTHEURE)
    {
        $this->cOUTHEURE = $cOUTHEURE;

        return $this;
    }

    /**
     * Get cOUTHEURE
     *
     * @return integer
     */
    public function getCOUTHEURE()
    {
        return $this->cOUTHEURE;
    }

    /**
     * Set cOUTHECTARE
     *
     * @param integer $cOUTHECTARE
     * @return MATERIEL
     */
    public function setCOUTHECTARE($cOUTHECTARE)
    {
        $this->cOUTHECTARE = $cOUTHECTARE;

        return $this;
    }

    /**
     * Get cOUTHECTARE
     *
     * @return integer
     */
    public function getCOUTHECTARE()
    {
        return $this->cOUTHECTARE;
    }

    /**
     * Set tYPEMATERIEL
     *
     * @param string $tYPEMATERIEL
     * @return MATERIEL
     */
    public function setTYPEMATERIEL($tYPEMATERIEL)
    {
        $this->tYPEMATERIEL = $tYPEMATERIEL;

        return $this;
    }

    /**
     * Get tYPEMATERIEL
     *
     * @return string
     */
    public function getTYPEMATERIEL()
    {
        return $this->tYPEMATERIEL;
    }

    /**
     * Set iMMATRICULATION
     *
     * @param string $iMMATRICULATION
     * @return MATERIEL
     */
    public function setIMMATRICULATION($iMMATRICULATION)
    {
        $this->iMMATRICULATION = $iMMATRICULATION;

        return $this;
    }

    /**
     * Get iMMATRICULATION
     *
     * @return string
     */
    public function getIMMATRICULATION()
    {
        return $this->iMMATRICULATION;
    }

    /**
     * Set eTAT
     *
     * @param string $eTAT
     * @return MATERIEL
     */
    public function setETAT($eTAT)
    {
        $this->eTAT = $eTAT;

        return $this;
    }

    /**
     * Get eTAT
     *
     * @return string
     */
    public function getETAT()
    {
        return $this->eTAT;
    }

    /**
     * Set cHAUFFEURID
     *
     * @param integer $cHAUFFEURID
     * @return MATERIEL
     */
    public function setCHAUFFEURID($cHAUFFEURID)
    {
        $this->cHAUFFEURID = $cHAUFFEURID;

        return $this;
    }

    /**
     * Get cHAUFFEURID
     *
     * @return integer
     */
    public function getCHAUFFEURID()
    {
        return $this->cHAUFFEURID;
    }

    /**
     * Set cARBURANT
     *
     * @param string $cARBURANT
     * @return MATERIEL
     */
    public function setCARBURANT($cARBURANT)
    {
        $this->cARBURANT = $cARBURANT;

        return $this;
    }

    /**
     * Get cARBURANT
     *
     * @return string
     */
    public function getCARBURANT()
    {
        return $this->cARBURANT;
    }

    /**
     * Set nOSERIE
     *
     * @param string $nOSERIE
     * @return MATERIEL
     */
    public function setNOSERIE($nOSERIE)
    {
        $this->nOSERIE = $nOSERIE;

        return $this;
    }

    /**
     * Get nOSERIE
     *
     * @return string
     */
    public function getNOSERIE()
    {
        return $this->nOSERIE;
    }

    /**
     * Set tOUR
     *
     * @param string $tOUR
     * @return MATERIEL
     */
    public function setTOUR($tOUR)
    {
        $this->tOUR = $tOUR;

        return $this;
    }

    /**
     * Get tOUR
     *
     * @return string
     */
    public function getTOUR()
    {
        return $this->tOUR;
    }

    /**
     * Set vOLUME
     *
     * @param string $vOLUME
     * @return MATERIEL
     */
    public function setVOLUME($vOLUME)
    {
        $this->vOLUME = $vOLUME;

        return $this;
    }

    /**
     * Get vOLUME
     *
     * @return string
     */
    public function getVOLUME()
    {
        return $this->vOLUME;
    }

    /**
     * Set pOIDS
     *
     * @param integer $pOIDS
     * @return MATERIEL
     */
    public function setPOIDS($pOIDS)
    {
        $this->pOIDS = $pOIDS;

        return $this;
    }

    /**
     * Get pOIDS
     *
     * @return integer
     */
    public function getPOIDS()
    {
        return $this->pOIDS;
    }

    /**
     * Set cONSMIN
     *
     * @param integer $cONSMIN
     * @return MATERIEL
     */
    public function setCONSMIN($cONSMIN)
    {
        $this->cONSMIN = $cONSMIN;

        return $this;
    }

    /**
     * Get cONSMIN
     *
     * @return integer
     */
    public function getCONSMIN()
    {
        return $this->cONSMIN;
    }

    /**
     * Set cONSMAX
     *
     * @param integer $cONSMAX
     * @return MATERIEL
     */
    public function setCONSMAX($cONSMAX)
    {
        $this->cONSMAX = $cONSMAX;

        return $this;
    }

    /**
     * Get cONSMAX
     *
     * @return integer
     */
    public function getCONSMAX()
    {
        return $this->cONSMAX;
    }

    /**
     * Set tRACTION
     *
     * @param integer $tRACTION
     * @return MATERIEL
     */
    public function setTRACTION($tRACTION)
    {
        $this->tRACTION = $tRACTION;

        return $this;
    }

    /**
     * Get tRACTION
     *
     * @return integer
     */
    public function getTRACTION()
    {
        return $this->tRACTION;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return MATERIEL
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set dEBITCHANTIERHAJ
     *
     * @param integer $dEBITCHANTIERHAJ
     * @return MATERIEL
     */
    public function setDEBITCHANTIERHAJ($dEBITCHANTIERHAJ)
    {
        $this->dEBITCHANTIERHAJ = $dEBITCHANTIERHAJ;

        return $this;
    }

    /**
     * Get dEBITCHANTIERHAJ
     *
     * @return integer
     */
    public function getDEBITCHANTIERHAJ()
    {
        return $this->dEBITCHANTIERHAJ;
    }

    /**
     * Set dEBITCHANTIERHHA
     *
     * @param integer $dEBITCHANTIERHHA
     * @return MATERIEL
     */
    public function setDEBITCHANTIERHHA($dEBITCHANTIERHHA)
    {
        $this->dEBITCHANTIERHHA = $dEBITCHANTIERHHA;

        return $this;
    }

    /**
     * Get dEBITCHANTIERHHA
     *
     * @return integer
     */
    public function getDEBITCHANTIERHHA()
    {
        return $this->dEBITCHANTIERHHA;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return MATERIEL
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return MATERIEL
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return MATERIEL
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set uNITECONSO
     *
     * @param string $uNITECONSO
     * @return MATERIEL
     */
    public function setUNITECONSO($uNITECONSO)
    {
        $this->uNITECONSO = $uNITECONSO;

        return $this;
    }

    /**
     * Get uNITECONSO
     *
     * @return string
     */
    public function getUNITECONSO()
    {
        return $this->uNITECONSO;
    }

    /**
     * Set aRTICLEID
     *
     * @param integer $aRTICLEID
     * @return MATERIEL
     */
    public function setARTICLEID($aRTICLEID)
    {
        $this->aRTICLEID = $aRTICLEID;

        return $this;
    }

    /**
     * Get aRTICLEID
     *
     * @return integer
     */
    public function getARTICLEID()
    {
        return $this->aRTICLEID;
    }

    /**
     * Set cODEIMMO
     *
     * @param integer $cODEIMMO
     * @return MATERIEL
     */
    public function setCODEIMMO($cODEIMMO)
    {
        $this->cODEIMMO = $cODEIMMO;

        return $this;
    }

    /**
     * Get cODEIMMO
     *
     * @return integer
     */
    public function getCODEIMMO()
    {
        return $this->cODEIMMO;
    }

    /**
     * Set sERVICEID
     *
     * @param integer $sERVICEID
     * @return MATERIEL
     */
    public function setSERVICEID($sERVICEID)
    {
        $this->sERVICEID = $sERVICEID;

        return $this;
    }

    /**
     * Get sERVICEID
     *
     * @return integer
     */
    public function getSERVICEID()
    {
        return $this->sERVICEID;
    }

    /**
     * Set cAPACITERESERVOIR
     *
     * @param integer $cAPACITERESERVOIR
     * @return MATERIEL
     */
    public function setCAPACITERESERVOIR($cAPACITERESERVOIR)
    {
        $this->cAPACITERESERVOIR = $cAPACITERESERVOIR;

        return $this;
    }

    /**
     * Get cAPACITERESERVOIR
     *
     * @return integer
     */
    public function getCAPACITERESERVOIR()
    {
        return $this->cAPACITERESERVOIR;
    }

    /**
     * Set sITEID
     *
     * @param integer $sITEID
     * @return MATERIEL
     */
    public function setSITEID($sITEID)
    {
        $this->sITEID = $sITEID;

        return $this;
    }

    /**
     * Get sITEID
     *
     * @return integer
     */
    public function getSITEID()
    {
        return $this->sITEID;
    }

    /**
     * Get dATEMISESERVICE
     *
     * @return \DateTime
     */
    public function getDATEMISESERVICE()
    {
        return $this->dATEMISESERVICE;
    }

    /**
     * Set dATEMISESERVICE
     *
     * @param \DateTime $dATEMISESERVICE
     * @return MATERIEL
     */
    public function setDATEMISESERVICE($dATEMISESERVICE)
    {
        $this->dATEMISESERVICE = $dATEMISESERVICE;

        return $this;
    }

    /**
     * Get sOUSFAMILLEID
     *
     * @return int
     */
    public function getSOUSFAMILLEID()
    {
        return $this->sOUSFAMILLEID;
    }

    /**
     * Set sOUSFAMILLEID
     *
     * @param int $sOUSFAMILLEID
     * @return MATERIEL
     */
    public function setSOUSFAMILLEID($sOUSFAMILLEID)
    {
        $this->sOUSFAMILLEID = $sOUSFAMILLEID;

        return $this;
    }

    /**
     * Get mODELEID
     *
     * @return int
     */
    public function getMODELEID()
    {
        return $this->mODELEID;
    }

    /**
     * Set mODELEID
     *
     * @param int $mODELEID
     * @return MATERIEL
     */
    public function setMODELEID($mODELEID)
    {
        $this->mODELEID = $mODELEID;

        return $this;
    }
}
