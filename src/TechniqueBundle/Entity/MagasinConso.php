<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MagasinConso
 */
class MagasinConso
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mAGASINID;

    /**
     * @var \Datetime
     */
    private $dATECONSO;

    /**
     * @var int
     */
    private $pOMPISTEID;

    /**
     * @var int
     */
    private $iNDEXDEBUT;

    /**
     * @var int
     */
    private $iNDEXFIN;

    /**
     * @var int
     */
    private $jAUGEDEBUT;

    /**
     * @var int
     */
    private $jAUGEFIN;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mAGASINID
     *
     * @param integer $mAGASINID
     * @return MAGASINCONSO
     */
    public function setMAGASINID($mAGASINID)
    {
        $this->mAGASINID = $mAGASINID;

        return $this;
    }

    /**
     * Get mAGASINID
     *
     * @return integer
     */
    public function getMAGASINID()
    {
        return $this->mAGASINID;
    }

    /**
     * Set dATECONSO
     *
     * @param \Datetime $dATECONSO
     * @return MAGASINCONSO
     */
    public function setDATECONSO($dATECONSO)
    {
        $this->dATECONSO = $dATECONSO;

        return $this;
    }

    /**
     * Get dATECONSO
     *
     * @return \Datetime
     */
    public function getDATECONSO()
    {
        return $this->dATECONSO;
    }

    /**
     * Set pOMPISTEID
     *
     * @param integer $pOMPISTEID
     * @return MAGASINCONSO
     */
    public function setPOMPISTEID($pOMPISTEID)
    {
        $this->pOMPISTEID = $pOMPISTEID;

        return $this;
    }

    /**
     * Get pOMPISTEID
     *
     * @return integer
     */
    public function getPOMPISTEID()
    {
        return $this->pOMPISTEID;
    }

    /**
     * Set iNDEXDEBUT
     *
     * @param integer $iNDEXDEBUT
     * @return MAGASINCONSO
     */
    public function setINDEXDEBUT($iNDEXDEBUT)
    {
        $this->iNDEXDEBUT = $iNDEXDEBUT;

        return $this;
    }

    /**
     * Get iNDEXDEBUT
     *
     * @return integer
     */
    public function getINDEXDEBUT()
    {
        return $this->iNDEXDEBUT;
    }

    /**
     * Set iNDEXFIN
     *
     * @param integer $iNDEXFIN
     * @return MAGASINCONSO
     */
    public function setINDEXFIN($iNDEXFIN)
    {
        $this->iNDEXFIN = $iNDEXFIN;

        return $this;
    }

    /**
     * Get iNDEXFIN
     *
     * @return integer
     */
    public function getINDEXFIN()
    {
        return $this->iNDEXFIN;
    }

    /**
     * Set jAUGEDEBUT
     *
     * @param integer $jAUGEDEBUT
     * @return MAGASINCONSO
     */
    public function setJAUGEDEBUT($jAUGEDEBUT)
    {
        $this->jAUGEDEBUT = $jAUGEDEBUT;

        return $this;
    }

    /**
     * Get jAUGEDEBUT
     *
     * @return integer
     */
    public function getJAUGEDEBUT()
    {
        return $this->jAUGEDEBUT;
    }

    /**
     * Set jAUGEFIN
     *
     * @param integer $jAUGEFIN
     * @return MAGASINCONSO
     */
    public function setJAUGEFIN($jAUGEFIN)
    {
        $this->jAUGEFIN = $jAUGEFIN;

        return $this;
    }

    /**
     * Get jAUGEFIN
     *
     * @return integer
     */
    public function getJAUGEFIN()
    {
        return $this->jAUGEFIN;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return MAGASINCONSO
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return MAGASINCONSO
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return MAGASINCONSO
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return MAGASINCONSO
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return MAGASINCONSO
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }
}
