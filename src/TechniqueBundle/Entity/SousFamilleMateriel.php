<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SousFamilleMateriel
 */
class SousFamilleMateriel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var int
     */
    private $fAMILLEMATERIELID;

    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var \DateTime
     */
    private $dATEUPDATE;

    /**
     * @var string
     */
    private $uSERUPDATE;

    public function __construct()
    {
        $this->dATECREATION = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return SOUSFAMILLEMATERIEL
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string 
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return SOUSFAMILLEMATERIEL
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set fAMILLEMATERIELID
     *
     * @param integer $fAMILLEMATERIELID
     * @return SOUSFAMILLEMATERIEL
     */
    public function setFAMILLEMATERIELID($fAMILLEMATERIELID)
    {
        $this->fAMILLEMATERIELID = $fAMILLEMATERIELID;

        return $this;
    }

    /**
     * Get fAMILLEMATERIELID
     *
     * @return integer 
     */
    public function getFAMILLEMATERIELID()
    {
        return $this->fAMILLEMATERIELID;
    }

    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return SOUSFAMILLEMATERIEL
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime 
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return SOUSFAMILLEMATERIEL
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set dATEUPDATE
     *
     * @param \DateTime $dATEUPDATE
     * @return SOUSFAMILLEMATERIEL
     */
    public function setDATEUPDATE($dATEUPDATE)
    {
        $this->dATEUPDATE = $dATEUPDATE;

        return $this;
    }

    /**
     * Get dATEUPDATE
     *
     * @return \DateTime 
     */
    public function getDATEUPDATE()
    {
        return $this->dATEUPDATE;
    }

    /**
     * Set uSERUPDATE
     *
     * @param string $uSERUPDATE
     * @return SOUSFAMILLEMATERIEL
     */
    public function setUSERUPDATE($uSERUPDATE)
    {
        $this->uSERUPDATE = $uSERUPDATE;

        return $this;
    }

    /**
     * Get uSERUPDATE
     *
     * @return string 
     */
    public function getUSERUPDATE()
    {
        return $this->uSERUPDATE;
    }
}
