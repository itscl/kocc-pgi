<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArticlePrix
 */
class ArticlePrix
{
    /**
     * @var integer
     */
    private $aRTICLEID;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var \DateTime
     */
    private $dATEPRIX;

    /**
     * @var integer
     */
    private $oRIGINSITEID;

    /**
     * @var integer
     */
    private $pRIXUNITAIRE;

    /**
     * @var string
     */
    private $uNITE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set aRTICLEID
     *
     * @param integer $aRTICLEID
     * @return ArticlePrix
     */
    public function setARTICLEID($aRTICLEID)
    {
        $this->aRTICLEID = $aRTICLEID;

        return $this;
    }

    /**
     * Get aRTICLEID
     *
     * @return integer 
     */
    public function getARTICLEID()
    {
        return $this->aRTICLEID;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ArticlePrix
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set dATEPRIX
     *
     * @param \DateTime $dATEPRIX
     * @return ArticlePrix
     */
    public function setDATEPRIX($dATEPRIX)
    {
        $this->dATEPRIX = $dATEPRIX;

        return $this;
    }

    /**
     * Get dATEPRIX
     *
     * @return \DateTime 
     */
    public function getDATEPRIX()
    {
        return $this->dATEPRIX;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ArticlePrix
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set pRIXUNITAIRE
     *
     * @param integer $pRIXUNITAIRE
     * @return ArticlePrix
     */
    public function setPRIXUNITAIRE($pRIXUNITAIRE)
    {
        $this->pRIXUNITAIRE = $pRIXUNITAIRE;

        return $this;
    }

    /**
     * Get pRIXUNITAIRE
     *
     * @return integer 
     */
    public function getPRIXUNITAIRE()
    {
        return $this->pRIXUNITAIRE;
    }

    /**
     * Set uNITE
     *
     * @param string $uNITE
     * @return ArticlePrix
     */
    public function setUNITE($uNITE)
    {
        $this->uNITE = $uNITE;

        return $this;
    }

    /**
     * Get uNITE
     *
     * @return string 
     */
    public function getUNITE()
    {
        return $this->uNITE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ArticlePrix
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ArticlePrix
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
