<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModeleMateriel
 */
class ModeleMateriel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var integer
     */
    private $mATERIELMARQUEID;

    /**
     * @var string
     */
    private $tYPEMATERIEL;

    /**
     * @var string
     */
    private $dESCRIPTION;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var \DateTime
     */
    private $dATEUPDATE;

    /**
     * @var string
     */
    private $uSERUPDATE;

    public function __construct()
    {
        $this->dATECREATION = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return ModeleMateriel
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set mATERIELMARQUEID
     *
     * @param integer $mATERIELMARQUEID
     * @return ModeleMateriel
     */
    public function setMATERIELMARQUEID($mATERIELMARQUEID)
    {
        $this->mATERIELMARQUEID = $mATERIELMARQUEID;

        return $this;
    }

    /**
     * Get mATERIELMARQUEID
     *
     * @return integer
     */
    public function getMATERIELMARQUEID()
    {
        return $this->mATERIELMARQUEID;
    }

    /**
     * Set tYPEMATERIEL
     *
     * @param string $tYPEMATERIEL
     * @return ModeleMateriel
     */
    public function setTYPEMATERIEL($tYPEMATERIEL)
    {
        $this->tYPEMATERIEL = $tYPEMATERIEL;

        return $this;
    }

    /**
     * Get tYPEMATERIEL
     *
     * @return string
     */
    public function getTYPEMATERIEL()
    {
        return $this->tYPEMATERIEL;
    }

    /**
     * Set dESCRIPTION
     *
     * @param string $dESCRIPTION
     * @return ModeleMateriel
     */
    public function setDESCRIPTION($dESCRIPTION)
    {
        $this->dESCRIPTION = $dESCRIPTION;

        return $this;
    }

    /**
     * Get tYPEMATERIEL
     *
     * @return string
     */
    public function getDESCRIPTION()
    {
        return $this->dESCRIPTION;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ModeleMateriel
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return ModeleMateriel
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime 
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set dATEUPDATE
     *
     * @param \DateTime $dATEUPDATE
     * @return ModeleMateriel
     */
    public function setDATEUPDATE($dATEUPDATE)
    {
        $this->dATEUPDATE = $dATEUPDATE;

        return $this;
    }

    /**
     * Get dATEUPDATE
     *
     * @return \DateTime 
     */
    public function getDATEUPDATE()
    {
        return $this->dATEUPDATE;
    }

    /**
     * Set uSERUPDATE
     *
     * @param string $uSERUPDATE
     * @return ModeleMateriel
     */
    public function setUSERUPDATE($uSERUPDATE)
    {
        $this->uSERUPDATE = $uSERUPDATE;

        return $this;
    }

    /**
     * Get uSERUPDATE
     *
     * @return string 
     */
    public function getUSERUPDATE()
    {
        return $this->uSERUPDATE;
    }


}
