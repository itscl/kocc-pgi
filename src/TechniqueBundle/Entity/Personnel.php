<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personnel
 */
class Personnel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var string
     */
    private $nOM;

    /**
     * @var string
     */
    private $pRENOM;

    /**
     * @var \DateTime
     */
    private $dATENAISSANCE;

    /**
     * @var string
     */
    private $lIEUNAISSANCE;

    /**
     * @var string
     */
    private $aDRESSE1;

    /**
     * @var string
     */
    private $aDRESSE2;

    /**
     * @var string
     */
    private $cP;

    /**
     * @var string
     */
    private $vILLE;

    /**
     * @var string
     */
    private $tELEPHONE;

    /**
     * @var string
     */
    private $pORTABLE;

    /**
     * @var string
     */
    private $eMAIL;

    /**
     * @var string
     */
    private $nOCI;

    /**
     * @var string
     */
    private $nOPASSEPORT;

    /**
     * @var string
     */
    private $nOMPERE;

    /**
     * @var string
     */
    private $nOMMERE;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return Personnel
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set nOM
     *
     * @param string $nOM
     * @return Personnel
     */
    public function setNOM($nOM)
    {
        $this->nOM = $nOM;

        return $this;
    }

    /**
     * Get nOM
     *
     * @return string
     */
    public function getNOM()
    {
        return $this->nOM;
    }

    /**
     * Set pRENOM
     *
     * @param string $pRENOM
     * @return Personnel
     */
    public function setPRENOM($pRENOM)
    {
        $this->pRENOM = $pRENOM;

        return $this;
    }

    /**
     * Get pRENOM
     *
     * @return string
     */
    public function getPRENOM()
    {
        return $this->pRENOM;
    }

    /**
     * Set dATENAISSANCE
     *
     * @param \DateTime $dATENAISSANCE
     * @return Personnel
     */
    public function setDATENAISSANCE($dATENAISSANCE)
    {
        $this->dATENAISSANCE = $dATENAISSANCE;

        return $this;
    }

    /**
     * Get dATENAISSANCE
     *
     * @return \DateTime
     */
    public function getDATENAISSANCE()
    {
        return $this->dATENAISSANCE;
    }

    /**
     * Set lIEUNAISSANCE
     *
     * @param string $lIEUNAISSANCE
     * @return Personnel
     */
    public function setLIEUNAISSANCE($lIEUNAISSANCE)
    {
        $this->lIEUNAISSANCE = $lIEUNAISSANCE;

        return $this;
    }

    /**
     * Get lIEUNAISSANCE
     *
     * @return string
     */
    public function getLIEUNAISSANCE()
    {
        return $this->lIEUNAISSANCE;
    }

    /**
     * Set aDRESSE1
     *
     * @param string $aDRESSE1
     * @return Personnel
     */
    public function setADRESSE1($aDRESSE1)
    {
        $this->aDRESSE1 = $aDRESSE1;

        return $this;
    }

    /**
     * Get aDRESSE1
     *
     * @return string
     */
    public function getADRESSE1()
    {
        return $this->aDRESSE1;
    }

    /**
     * Set aDRESSE2
     *
     * @param string $aDRESSE2
     * @return Personnel
     */
    public function setADRESSE2($aDRESSE2)
    {
        $this->aDRESSE2 = $aDRESSE2;

        return $this;
    }

    /**
     * Get aDRESSE2
     *
     * @return string
     */
    public function getADRESSE2()
    {
        return $this->aDRESSE2;
    }

    /**
     * Set cP
     *
     * @param string $cP
     * @return Personnel
     */
    public function setCP($cP)
    {
        $this->cP = $cP;

        return $this;
    }

    /**
     * Get cP
     *
     * @return string
     */
    public function getCP()
    {
        return $this->cP;
    }

    /**
     * Set vILLE
     *
     * @param string $vILLE
     * @return Personnel
     */
    public function setVILLE($vILLE)
    {
        $this->vILLE = $vILLE;

        return $this;
    }

    /**
     * Get vILLE
     *
     * @return string
     */
    public function getVILLE()
    {
        return $this->vILLE;
    }

    /**
     * Set tELEPHONE
     *
     * @param string $tELEPHONE
     * @return Personnel
     */
    public function setTELEPHONE($tELEPHONE)
    {
        $this->tELEPHONE = $tELEPHONE;

        return $this;
    }

    /**
     * Get tELEPHONE
     *
     * @return string
     */
    public function getTELEPHONE()
    {
        return $this->tELEPHONE;
    }

    /**
     * Set pORTABLE
     *
     * @param string $pORTABLE
     * @return Personnel
     */
    public function setPORTABLE($pORTABLE)
    {
        $this->pORTABLE = $pORTABLE;

        return $this;
    }

    /**
     * Get pORTABLE
     *
     * @return string
     */
    public function getPORTABLE()
    {
        return $this->pORTABLE;
    }

    /**
     * Set eMAIL
     *
     * @param string $eMAIL
     * @return Personnel
     */
    public function setEMAIL($eMAIL)
    {
        $this->eMAIL = $eMAIL;

        return $this;
    }

    /**
     * Get eMAIL
     *
     * @return string
     */
    public function getEMAIL()
    {
        return $this->eMAIL;
    }

    /**
     * Set nOCI
     *
     * @param string $nOCI
     * @return Personnel
     */
    public function setNOCI($nOCI)
    {
        $this->nOCI = $nOCI;

        return $this;
    }

    /**
     * Get nOCI
     *
     * @return string
     */
    public function getNOCI()
    {
        return $this->nOCI;
    }

    /**
     * Set nOPASSEPORT
     *
     * @param string $nOPASSEPORT
     * @return Personnel
     */
    public function setNOPASSEPORT($nOPASSEPORT)
    {
        $this->nOPASSEPORT = $nOPASSEPORT;

        return $this;
    }

    /**
     * Get nOPASSEPORT
     *
     * @return string
     */
    public function getNOPASSEPORT()
    {
        return $this->nOPASSEPORT;
    }

    /**
     * Set nOMPERE
     *
     * @param string $nOMPERE
     * @return Personnel
     */
    public function setNOMPERE($nOMPERE)
    {
        $this->nOMPERE = $nOMPERE;

        return $this;
    }

    /**
     * Get nOMPERE
     *
     * @return string
     */
    public function getNOMPERE()
    {
        return $this->nOMPERE;
    }

    /**
     * Set nOMMERE
     *
     * @param string $nOMMERE
     * @return Personnel
     */
    public function setNOMMERE($nOMMERE)
    {
        $this->nOMMERE = $nOMMERE;

        return $this;
    }

    /**
     * Get nOMMERE
     *
     * @return string
     */
    public function getNOMMERE()
    {
        return $this->nOMMERE;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return Personnel
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return Personnel
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return Personnel
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return Personnel
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }
}
