<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lov
 */
class Lov
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cATEGORIE;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var string
     */
    private $dEFAUT;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cATEGORIE
     *
     * @param string $cATEGORIE
     * @return Lov
     */
    public function setCATEGORIE($cATEGORIE)
    {
        $this->cATEGORIE = $cATEGORIE;

        return $this;
    }

    /**
     * Get cATEGORIE
     *
     * @return string
     */
    public function getCATEGORIE()
    {
        return $this->cATEGORIE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return Lov
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return Lov
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return Lov
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return Lov
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return Lov
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set dEFAUT
     *
     * @param string $dEFAUT
     * @return Lov
     */
    public function setDEFAUT($dEFAUT)
    {
        $this->dEFAUT = $dEFAUT;

        return $this;
    }

    /**
     * Get dEFAUT
     *
     * @return string
     */
    public function getDEFAUT()
    {
        return $this->dEFAUT;
    }
}
