<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArticleOperationInt
 */
class ArticleOperationInt
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $rEFARTICLE;

    /**
     * @var string
     */
    private $dESARTICLE;

    /**
     * @var int
     */
    private $cOMPTEURINTERVENTIONID;

    /**
     * @var int
     */
    private $qUANTITE;

    /**
     * @var string
     */
    private $uNITE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rEFARTICLE
     *
     * @param string $rEFARTICLE
     * @return ArticleOperationInt
     */
    public function setREFARTICLE($rEFARTICLE)
    {
        $this->rEFARTICLE = $rEFARTICLE;

        return $this;
    }

    /**
     * Get rEFARTICLE
     *
     * @return string 
     */
    public function getREFARTICLE()
    {
        return $this->rEFARTICLE;
    }

    /**
     * Set dESARTICLE
     *
     * @param string $dESARTICLE
     * @return ArticleOperationInt
     */
    public function setDESARTICLE($dESARTICLE)
    {
        $this->dESARTICLE = $dESARTICLE;

        return $this;
    }

    /**
     * Get dESARTICLE
     *
     * @return string 
     */
    public function getDESARTICLE()
    {
        return $this->dESARTICLE;
    }

    /**
     * Set cOMPTEURINTERVENTIONID
     *
     * @param integer $cOMPTEURINTERVENTIONID
     * @return ArticleOperationInt
     */
    public function setCOMPTEURINTERVENTIONID($cOMPTEURINTERVENTIONID)
    {
        $this->cOMPTEURINTERVENTIONID = $cOMPTEURINTERVENTIONID;

        return $this;
    }

    /**
     * Get cOMPTEURINTERVENTIONID
     *
     * @return integer 
     */
    public function getCOMPTEURINTERVENTIONID()
    {
        return $this->cOMPTEURINTERVENTIONID;
    }

    /**
     * Set qUANTITE
     *
     * @param integer $qUANTITE
     * @return ArticleOperationInt
     */
    public function setQUANTITE($qUANTITE)
    {
        $this->qUANTITE = $qUANTITE;

        return $this;
    }

    /**
     * Get qUANTITE
     *
     * @return integer
     */
    public function getQUANTITE()
    {
        return $this->qUANTITE;
    }

    /**
     * Set uNITE
     *
     * @param string $uNITE
     * @return ArticleOperationInt
     */
    public function setUNITE($uNITE)
    {
        $this->uNITE = $uNITE;

        return $this;
    }

    /**
     * Get uNITE
     *
     * @return string
     */
    public function getUNITE()
    {
        return $this->uNITE;
    }
}
