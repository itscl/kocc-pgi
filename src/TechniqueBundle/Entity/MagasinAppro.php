<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MagasinAppro
 */
class MagasinAppro
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mAGASINCONSOID;

    /**
     * @var string
     */
    private $tYPEAPPRO;

    /**
     * @var \DateTime
     */
    private $dATEAPPRO;

    /**
     * @var int
     */
    private $qTEAPPRO;

    /**
     * @var string
     */
    private $rEFAPPRO;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mAGASINCONSOID
     *
     * @param integer $mAGASINCONSOID
     * @return MagasinAppro
     */
    public function setMAGASINCONSOID($mAGASINCONSOID)
    {
        $this->mAGASINCONSOID = $mAGASINCONSOID;

        return $this;
    }

    /**
     * Get mAGASINCONSOID
     *
     * @return integer 
     */
    public function getMAGASINCONSOID()
    {
        return $this->mAGASINCONSOID;
    }

    /**
     * Set tYPEAPPRO
     *
     * @param string $tYPEAPPRO
     * @return MagasinAppro
     */
    public function setTYPEAPPRO($tYPEAPPRO)
    {
        $this->tYPEAPPRO = $tYPEAPPRO;

        return $this;
    }

    /**
     * Get tYPEAPPRO
     *
     * @return string 
     */
    public function getTYPEAPPRO()
    {
        return $this->tYPEAPPRO;
    }

    /**
     * Set dATEAPPRO
     *
     * @param \DateTime $dATEAPPRO
     * @return MagasinAppro
     */
    public function setDATEAPPRO($dATEAPPRO)
    {
        $this->dATEAPPRO = $dATEAPPRO;

        return $this;
    }

    /**
     * Get dATEAPPRO
     *
     * @return \DateTime
     */
    public function getDATEAPPRO()
    {
        return $this->dATEAPPRO;
    }

    /**
     * Set qTEAPPRO
     *
     * @param integer $qTEAPPRO
     * @return MagasinAppro
     */
    public function setQTEAPPRO($qTEAPPRO)
    {
        $this->qTEAPPRO = $qTEAPPRO;

        return $this;
    }

    /**
     * Get qTEAPPRO
     *
     * @return integer 
     */
    public function getQTEAPPRO()
    {
        return $this->qTEAPPRO;
    }

    /**
     * Set rEFAPPRO
     *
     * @param string $rEFAPPRO
     * @return MagasinAppro
     */
    public function setREFAPPRO($rEFAPPRO)
    {
        $this->rEFAPPRO = $rEFAPPRO;

        return $this;
    }

    /**
     * Get rEFAPPRO
     *
     * @return string 
     */
    public function getREFAPPRO()
    {
        return $this->rEFAPPRO;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return MagasinAppro
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return MagasinAppro
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return MagasinAppro
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return MagasinAppro
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }
}
