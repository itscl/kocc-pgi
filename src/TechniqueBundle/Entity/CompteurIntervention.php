<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CompteurIntervention
 */
class CompteurIntervention
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mODELEID;

    /**
     * @var int
     */
    private $cOMPTEUR;

    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var \DateTime
     */
    private $dATEUPDATE;

    /**
     * @var string
     */
    private $uSERUPDATE;

    public function __construct()
    {
        $this->dATECREATION = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mODELEID
     *
     * @param integer $mODELEID
     * @return CompteurIntervention
     */
    public function setMODELEID($mODELEID)
    {
        $this->mODELEID = $mODELEID;

        return $this;
    }

    /**
     * Get mODELEID
     *
     * @return integer 
     */
    public function getMODELEID()
    {
        return $this->mODELEID;
    }

    /**
     * Set cOMPTEUR
     *
     * @param integer $cOMPTEUR
     * @return CompteurIntervention
     */
    public function setCOMPTEUR($cOMPTEUR)
    {
        $this->cOMPTEUR = $cOMPTEUR;

        return $this;
    }

    /**
     * Get cOMPTEUR
     *
     * @return integer 
     */
    public function getCOMPTEUR()
    {
        return $this->cOMPTEUR;
    }

    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return CompteurIntervention
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return CompteurIntervention
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set dATEUPDATE
     *
     * @param \DateTime $dATEUPDATE
     * @return CompteurIntervention
     */
    public function setDATEUPDATE($dATEUPDATE)
    {
        $this->dATEUPDATE = $dATEUPDATE;

        return $this;
    }

    /**
     * Get dATEUPDATE
     *
     * @return \DateTime 
     */
    public function getDATEUPDATE()
    {
        return $this->dATEUPDATE;
    }

    /**
     * Set uSERUPDATE
     *
     * @param string $uSERUPDATE
     * @return CompteurIntervention
     */
    public function setUSERUPDATE($uSERUPDATE)
    {
        $this->uSERUPDATE = $uSERUPDATE;

        return $this;
    }

    /**
     * Get uSERUPDATE
     *
     * @return string 
     */
    public function getUSERUPDATE()
    {
        return $this->uSERUPDATE;
    }
}
