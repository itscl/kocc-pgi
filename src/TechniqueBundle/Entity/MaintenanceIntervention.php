<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MaintenanceIntervention
 */
class MaintenanceIntervention
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mAINTENANCEID;

    /**
     * @var int
     */
    private $iNTERVENTIONID;

    /**
     * @var int
     */
    private $cOMPTEUR;

    /**
     * @var string
     */
    private $dESCRIPTION;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mAINTENANCEID
     *
     * @param integer $mAINTENANCEID
     * @return MaintenanceIntervention
     */
    public function setMAINTENANCEID($mAINTENANCEID)
    {
        $this->mAINTENANCEID = $mAINTENANCEID;

        return $this;
    }

    /**
     * Get mAINTENANCEID
     *
     * @return integer 
     */
    public function getMAINTENANCEID()
    {
        return $this->mAINTENANCEID;
    }

    /**
     * Set iNTERVENTIONID
     *
     * @param integer $iNTERVENTIONID
     * @return MaintenanceIntervention
     */
    public function setINTERVENTIONID($iNTERVENTIONID)
    {
        $this->iNTERVENTIONID = $iNTERVENTIONID;

        return $this;
    }

    /**
     * Get iNTERVENTIONID
     *
     * @return integer 
     */
    public function getINTERVENTIONID()
    {
        return $this->iNTERVENTIONID;
    }

    /**
     * Set cOMPTEUR
     *
     * @param integer $cOMPTEUR
     * @return MaintenanceIntervention
     */
    public function setCOMPTEUR($cOMPTEUR)
    {
        $this->cOMPTEUR = $cOMPTEUR;

        return $this;
    }

    /**
     * Get cOMPTEUR
     *
     * @return integer
     */
    public function getCOMPTEUR()
    {
        return $this->cOMPTEUR;
    }

    /**
     * Set dESCRIPTION
     *
     * @param integer $dESCRIPTION
     * @return MaintenanceIntervention
     */
    public function setDESCRIPTION($dESCRIPTION)
    {
        $this->dESCRIPTION = $dESCRIPTION;

        return $this;
    }

    /**
     * Get dESCRIPTION
     *
     * @return integer
     */
    public function getDESCRIPTION()
    {
        return $this->dESCRIPTION;
    }

}
