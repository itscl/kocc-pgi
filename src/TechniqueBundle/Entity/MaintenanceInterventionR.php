<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MaintenanceInterventionR
 */
class MaintenanceInterventionR
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mAINTENANCEID;

    /**
     * @var int
     */
    private $cOMPTEUR;

    /**
     * @var string
     */
    private $aCTION;

    /**
     * @var string
     */
    private $dESCRIPTION;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mAINTENANCEID
     *
     * @param integer $mAINTENANCEID
     * @return MaintenanceInterventionR
     */
    public function setMAINTENANCEID($mAINTENANCEID)
    {
        $this->mAINTENANCEID = $mAINTENANCEID;

        return $this;
    }

    /**
     * Get mAINTENANCEID
     *
     * @return integer 
     */
    public function getMAINTENANCEID()
    {
        return $this->mAINTENANCEID;
    }

    /**
     * Set cOMPTEUR
     *
     * @param integer $cOMPTEUR
     * @return MaintenanceInterventionR
     */
    public function setCOMPTEUR($cOMPTEUR)
    {
        $this->cOMPTEUR = $cOMPTEUR;

        return $this;
    }

    /**
     * Get cOMPTEUR
     *
     * @return integer
     */
    public function getCOMPTEUR()
    {
        return $this->cOMPTEUR;
    }

    /**
     * Set aCTION
     *
     * @param string $aCTION
     * @return MaintenanceInterventionR
     */
    public function setACTION($aCTION)
    {
        $this->aCTION = $aCTION;

        return $this;
    }

    /**
     * Get aCTION
     *
     * @return integer
     */
    public function getACTION()
    {
        return $this->aCTION;
    }

    /**
     * Set dESCRIPTION
     *
     * @param string $dESCRIPTION
     * @return MaintenanceInterventionR
     */
    public function setDESCRIPTION($dESCRIPTION)
    {
        $this->dESCRIPTION = $dESCRIPTION;

        return $this;
    }

    /**
     * Get dESCRIPTION
     *
     * @return integer
     */
    public function getDESCRIPTION()
    {
        return $this->dESCRIPTION;
    }
}
