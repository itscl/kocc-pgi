<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MaterielGeste
 */
class MaterielGeste
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mATERIELID;

    /**
     * @var int
     */
    private $sGESTEID;

    /**
     * @var int
     */
    private $dEFAUT;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mATERIELID
     *
     * @param integer $mATERIELID
     * @return MaterielGeste
     */
    public function setMATERIELID($mATERIELID)
    {
        $this->mATERIELID = $mATERIELID;

        return $this;
    }

    /**
     * Get mATERIELID
     *
     * @return integer
     */
    public function getMATERIELID()
    {
        return $this->mATERIELID;
    }

    /**
     * Set sGESTEID
     *
     * @param integer $sGESTEID
     * @return MaterielGeste
     */
    public function setSGESTEID($sGESTEID)
    {
        $this->sGESTEID = $sGESTEID;

        return $this;
    }

    /**
     * Get sGESTEID
     *
     * @return integer
     */
    public function getSGESTEID()
    {
        return $this->sGESTEID;
    }

    /**
     * Set dEFAUT
     *
     * @param integer $dEFAUT
     * @return MaterielGeste
     */
    public function setDEFAUT($dEFAUT)
    {
        $this->dEFAUT = $dEFAUT;

        return $this;
    }

    /**
     * Get dEFAUT
     *
     * @return integer
     */
    public function getDEFAUT()
    {
        return $this->dEFAUT;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return MaterielGeste
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return MaterielGeste
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return MaterielGeste
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return MaterielGeste
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }
}
