<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FamilleMateriel
 */
class FamilleMateriel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var \DateTime
     */
    private $dATEUPDATE;

    /**
     * @var string
     */
    private $uSERUPDADE;

    /**
     * @var string
     */
    private $cODE;


    public function __construct()
    {
        $this->dATECREATION = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return FamilleMateriel
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return FamilleMateriel
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime 
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return FamilleMateriel
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set dATEUPDATE
     *
     * @param \DateTime $dATEUPDATE
     * @return FamilleMateriel
     */
    public function setDATEUPDATE($dATEUPDATE)
    {
        $this->dATEUPDATE = $dATEUPDATE;

        return $this;
    }

    /**
     * Get dATEUPDATE
     *
     * @return \DateTime 
     */
    public function getDATEUPDATE()
    {
        return $this->dATEUPDATE;
    }

    /**
     * Set uSERUPDADE
     *
     * @param string $uSERUPDADE
     * @return FamilleMateriel
     */
    public function setUSERUPDADE($uSERUPDADE)
    {
        $this->uSERUPDADE = $uSERUPDADE;

        return $this;
    }

    /**
     * Get uSERUPDADE
     *
     * @return string 
     */
    public function getUSERUPDADE()
    {
        return $this->uSERUPDADE;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return FamilleMateriel
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string
     */
    public function getCODE()
    {
        return $this->cODE;
    }
}
