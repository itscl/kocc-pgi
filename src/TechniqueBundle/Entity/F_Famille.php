<?php

namespace TechniqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * F_Famille
 */
class F_Famille
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $fAIntitule;

    /**
     * @var int
     */
    private $cLNo1;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fAIntitule
     *
     * @param string $fAIntitule
     * @return F_Famille
     */
    public function setFAIntitule($fAIntitule)
    {
        $this->fAIntitule = $fAIntitule;

        return $this;
    }

    /**
     * Get fAIntitule
     *
     * @return string 
     */
    public function getFAIntitule()
    {
        return $this->fAIntitule;
    }

    /**
     * Set cLNo1
     *
     * @param integer $cLNo1
     * @return F_Famille
     */
    public function setCLNo1($cLNo1)
    {
        $this->cLNo1 = $cLNo1;

        return $this;
    }

    /**
     * Get cLNo1
     *
     * @return integer
     */
    public function getCLNo1()
    {
        return $this->cLNo1;
    }
}
