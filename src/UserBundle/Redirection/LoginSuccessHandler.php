<?php
/**
 * Created by PhpStorm.
 * User: akdaho
 * Date: 15/04/2016
 * Time: 12:44
 */

namespace UserBundle\Redirection;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;


class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    protected $router;
    protected $security;

    public function __construct(Router $router, SecurityContext $security)
    {
        $this->router = $router;
        $this->security = $security;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        //On r�cup�re les information de l'utilisateur

        $user = $token->getUser();

        //var_dump($user); exit;

        // On r�cup�re la liste des r�les d'un utilisateur
        $roles = $token->getRoles();

        // On transforme le tableau d'instance en tableau simple
        $rolesTab = array_map(function($role){
            return $role->getRole();
        }, $roles);

        //var_dump($rolesTab); exit;

        // S'il s'agit d'un admin ou d'un super admin on le redirige vers le backoffice

        if (in_array('ROLE_ADMIN', $rolesTab, true) || in_array('ROLE_SUPER_ADMIN', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('gestionutilisateur'));
        }

        // Sinon s'il s'agit d'un autre utilisateur
        elseif(in_array('ROLE_CGESTION', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('indexcontrolegestion'));
        }
              // Sinon s'il s'agit d'un autre utilisateur
        elseif(in_array('ROLE_PRODUCTION', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('familleculture'));
        }

        // Sinon s'il s'agit d'un autre utilisateur
        elseif(in_array('ROLE_TECHNICIEN', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('instructionsemis'));
        }

        /*// Sinon s'il s'agit d'un utilisateur de la production
        elseif(in_array('ROLE_PRODUCTION', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('indexcontrolegestion'));
        }*/

        // Sinon s'il s'agit d'un utilisateur de la technique
        elseif(in_array('ROLE_TECHNIQUE', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('materiel'));
        }

        elseif(in_array('ROLE_QUALITE', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('listeficheintervention'));
        }

        elseif(in_array('ROLE_STATION', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('listeficheintervention'));
        }

        // Sinon s'il s'agit d'un autre utilisateur
        elseif(in_array('ROLE_POINTEUR', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('fichepointage_list'));
        }

        // Sinon s'il s'agit d'un autre utilisateur
        elseif(in_array('ROLE_USER', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('user_homepage'));
        }

        return $response;
    }

}