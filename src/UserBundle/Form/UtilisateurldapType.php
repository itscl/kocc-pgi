<?php

namespace UserBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class UtilisateurldapType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $sites=array();
        $fermes = array();
        $query=$this->em->getRepository('CultureBundle:Site')
                ->createQueryBuilder('s')
                ->orderBy('s.lIBELLE');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $sites[$ref->getId()]=$ref->getlIBELLE();
        }

        $query1=$this->em->getRepository('CultureBundle:Ferme')
            ->createQueryBuilder('f')
            ->orderBy('f.lIBELLE');

        //on cr�e la liste de choix
        foreach($query1->getQuery()->getResult() as $ref){
            $fermes[$ref->getId()]=$ref->getlIBELLE();
        }

        $builder
            ->add('siteid','choice',array(
                    'choices' => $sites,
                    'required' => false
                ))
            ->add('fermeid','choice',array(
                'choices' => $fermes,
                'required' => false
            ))
            ->add('diffusion', 'choice',array(
                'choices' => array('1' => 'Oui',
                    '0' => 'Non'),
                'required' => false
            ))
            ->add('enable', 'choice',array(
                'choices' => array('1' => 'Oui',
                    '0' => 'Non'),
                'required' => false
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\Utilisateurldap'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userbundle_utilisateurldap';
    }


}
