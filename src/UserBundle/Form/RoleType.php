<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class RoleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nomRole')->add('description');

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $role = $event->getData();
            $form = $event->getForm();
            if($role && $role->getId() != null) {
                $form->add('actif', 'choice', array(
                    'choices' => array('1' => 'Oui',
                        '0' => 'Non'),
                    'required' => true,
                    'expanded' => false
                ));
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\Role'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userbundle_role';
    }


}
