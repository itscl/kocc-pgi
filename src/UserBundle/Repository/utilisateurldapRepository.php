<?php

namespace UserBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * utilisateurldapRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class utilisateurldapRepository extends EntityRepository
{
    public function getRoles()
    {
        $qb = $this->createQueryBuilder('r');
        $qb->where('r.id != :identifier')
            ->setParameter('identifier', $id);

        return $qb->getQuery()
            ->getResult();
    }
}
