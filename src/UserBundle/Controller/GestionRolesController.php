<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Role;
use UserBundle\Form\RoleType;
use UserBundle\Repository\RoleRepository;

class GestionRolesController extends Controller
{
    public function indexAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        } else {
            $em = $this->getDoctrine()->getManager();

            $entity = new Role();

            $form = $this->createForm(new RoleType(), $entity);

            if ($request->isMethod('post')) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $em->persist($entity);
                    $em->flush();
                    return $this->redirect($this->generateUrl('gestionrole'));
                }
            }

            $repositoty = $this->getDoctrine()->getManager()->getRepository('UserBundle:Role');
            $roles = $repositoty->findAll();

            return $this->render('UserBundle:GestionRoles:index.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
                'roles' => $roles
            ));
        }
    }

    public function modifierroleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        } else {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $repositoty = $this->getDoctrine()->getManager()->getRepository('UserBundle:Role');
            $role = $repositoty->find($id);

            $edit_form = $this->createForm(new RoleType(), $role);

            if ($request->isMethod('post')) {
                $edit_form->handleRequest($request);

                if ($edit_form->isValid()) {
                    $em->flush();
                    return $this->redirect($this->generateUrl('gestionrole'));
                }
            }

            $repositoty = $this->getDoctrine()->getManager()->getRepository('UserBundle:Role');
            $roles = $repositoty->getRoles($id);

            return $this->render('UserBundle:GestionRoles:modifierrole.html.twig', array(
                'role' => $role,
                'edit_form' => $edit_form->createView(),
                'roles' => $roles
            ));
        }
    }
}
