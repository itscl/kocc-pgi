<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Utilisateurldap;
use UserBundle\Form\UtilisateurldapType;
use UserBundle\Repository\RoleRepository;

class GestionUtilisateurController extends Controller
{
    public function indexAction()
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $userManager = $this->get('fos_user.user_manager');
            $users = $userManager->findUsers();
           // var_dump($users); exit;

            return $this->render('UserBundle:GestionUtilisateur:index.html.twig',array(
                'user' => $users
            ));
        }
    }

    public function detailutilisateurAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $repository = $this->getDoctrine()->getManager()->getRepository('UserBundle:Utilisateurldap');

            $user = $repository->find($id);

            $repository1 = $this->getDoctrine()->getManager()->getRepository('UserBundle:Role');

            $roles = $repository1->findAll();

            $edit_form = $this->createForm(new UtilisateurldapType($em), $user);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $em->flush();
                }
            }
            return $this->render('UserBundle:GestionUtilisateur:detailutilisateur.html.twig', array(
                'entity' => $user,
                'edit_form' => $edit_form->createView(),
                'roles' => $roles
            ));
        }
    }

    public function ajouterroleAction(Request $request)
    {

        $id = $request->request->get('id');

        $repository = $this->getDoctrine()->getManager()->getRepository('UserBundle:Utilisateurldap');
        $user = $repository->find($id);

        $userManager = $this->get('fos_user.user_manager');

        $valeur = $_POST['checkrole'];
        $count = count($valeur);

        var_dump ($valeur);
        var_dump ($user);

        for($i = 0; $i < $count; $i++)
        {
            $getvaleur = $valeur[$i];
            $user->addRole($getvaleur);
            $userManager->updateUser($user);
        }


        return $this->render('UserBundle:GestionUtilisateur:ajax.html.twig');
    }

    public function supprimerroleAction(Request $request)
    {
        $id = $request->request->get('id');

        $repository = $this->getDoctrine()->getManager()->getRepository('UserBundle:Utilisateurldap');
        $user = $repository->find($id);

        $userManager = $this->get('fos_user.user_manager');

        $valeur = $_POST['checkrole'];
        $count = count($valeur);

        for($i = 0; $i < $count; $i++)
        {
            $getvaleur = $valeur[$i];
            $user->removeRole($getvaleur);
            $userManager->updateUser($user);
        }
        return $this->render('UserBundle:GestionUtilisateur:ajax.html.twig');

    }
}
