<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $userManager = $this->get('fos_user.user_manager');

        $user = $this->getUser();

        $userdep = $this->getUser()->getDepartment();

        if($userdep == 'IT')
        {
            $user->addRole('ROLE_ADMIN');
            $userManager->updateUser($user);

            return $this->redirect($this->generateUrl('gestionutilisateur'));
        }

        if($userdep == 'TECHNIQUE')
        {
            $user->addRole('ROLE_TECHNIQUE');
            $userManager->updateUser($user);

            return $this->redirect($this->generateUrl('materiel'));
        }

        return $this->render('UserBundle:Default:index.html.twig');
    }

}
