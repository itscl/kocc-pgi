<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\Service;
use UserBundle\Form\ServiceType;
use UserBundle\Repository\ServiceRepository;

class GestionServicesController extends Controller
{
    public function indexAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        } else {
            $em = $this->getDoctrine()->getManager();

            $entity = new Service();

            $form = $this->createForm(new ServiceType(), $entity);

            if ($request->isMethod('post')) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $em->persist($entity);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('noticeok', "Le service a bien été crée !");
                    return $this->redirect($this->generateUrl('gestionservices'));
                }
            }

            $repositoty = $this->getDoctrine()->getManager()->getRepository('UserBundle:Service');
            $services = $repositoty->findAll();

            return $this->render('UserBundle:GestionServices:index.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
                'services' => $services
            ));
        }
    }

    public function supprimerserviceAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $entity = new Service();

            $form = $this->createForm(new ServiceType(), $entity);

            $id = $request->query->get('id');

            $service = $em->getRepository('UserBundle:Service')->find($id);

            try
            {
                $em->remove($service);
                $em->flush();
                $this->get('session')->getFlashBag()->add('noticeok', "Le service a bien été supprimé !");

            } catch (ForeignKeyConstraintViolationException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Impossible de supprimer ce service car des
                utilisateurs y
                sont attachés");
            }
            catch(\Doctrine\ORM\ORMInvalidArgumentException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Ce service n'existe plus dans la base");
            }

            $services = $em->getRepository('UserBundle:Service')->findAll();

            return $this->render('UserBundle:GestionServices:index.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
                'services' => $services
            ));
        }
    }
}
