<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 *
 * @ORM\Table(name="service")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\ServiceRepository")
 */
class Service
{
//    /**
//     * @var int
//     *
//     * @ORM\Column(name="SERVICEID", type="integer")
//     * @ORM\Id
//     * @ORM\GeneratedValue(strategy="AUTO")
//     */

    /**
     * @var int
     *
     * @ORM\Column(name="SERVICEID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="Seq_PgaParam", initialValue=1, allocationSize=100)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="CODE", type="string", length=20, unique=true)
     */
    private $cODE;

    /**
     * @var string
     *
     * @ORM\Column(name="LIBELLE", type="string", length=30, unique=true)
     */
    private $lIBELLE;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATEINVALIDE", type="date", nullable=true)
     */
    private $dATEINVALIDE;

    /**
     * @var string
     *
     * @ORM\Column(name="UPDATE_DATE", type="string", length=15, nullable=true)
     */
    private $uPDATEDATE;

    /**
     * @var string
     *
     * @ORM\Column(name="UPDATE_USER", type="string", length=30, nullable=true)
     */
    private $uPDATEUSER;

    /**
     * @var int
     *
     * @ORM\Column(name="ORIGINSITEID", type="integer", nullable=true)
     */
    private $oRIGINSITEID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return Service
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string 
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return Service
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return Service
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return Service
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return Service
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return Service
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }
}
