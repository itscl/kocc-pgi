<?php

namespace UserBundle\Entity;

use UserBundle\Model\UserInterface as LdapUserInterface;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="utilisateurldap")
 */
class Utilisateurldap extends BaseUser implements LdapUserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $dn;
    /**
     * @ORM\Column(type="string")
     *
     */
    /**
     * @ORM\Column(type="string")
     */
    protected $manager;
    /**
     * @ORM\Column(type="string")
     *
     */
    protected $name;
    /**
     * @ORM\Column(type="string")
     */
    protected $language;

    /**
     * @ORM\Column(type="integer")
     */
    protected $siteid;

    /**
     * @ORM\Column(type="integer")
     */
    protected $fermeid;

    /**
     * @ORM\Column(type="integer")
     */
    protected $diffusion;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $department;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        parent::__construct();
        if (empty($this->roles)) {
            $this->roles[] = 'ROLE_USER';
        }
        $this->diffusion = 0;
    }

    public function setDn($dn) {
        $this->dn = $dn;
    }
    public function getDn() {
        return $this->dn;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function getName()
    {
        return $this->name;
    }
    public function setLanguage($language) {
        $this->language = $language;
    }

    public function setSiteid($siteid)
    {
        $this->siteid = $siteid;
    }

    public function getSiteid()
    {
        return $this->siteid;
    }

    public function setFermeid($fermerid)
    {
        $this->fermeid = $fermerid;
    }

    public function getFermeid()
    {
        return $this->fermeid;
    }

    public function setDiffusion($diffusion)
    {
        $this->diffusion = $diffusion;
    }

    public function getDiffusion()
    {
        return $this->diffusion;
    }

    public function setEnable($enable)
    {
        $this->enabled = $enable;
    }

    public function getEnable()
    {
        return $this->enabled;
    }

    public function setDepartement($department)
    {
        $this->department = $department;
    }

    public function getDepartment()
    {
        return $this->department;
    }

    public function getManager()
    {
        return $this->manager;
    }

    public function setManager($manager)
    {
        $this->manager = $manager;
    }

}
