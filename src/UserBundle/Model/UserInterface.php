<?php
/**
 * Created by PhpStorm.
 * User: akdaho
 * Date: 09/01/2017
 * Time: 16:05
 */
namespace UserBundle\Model;

use FR3D\LdapBundle\Model\LdapUserInterface as LdapUserInterface;

interface UserInterface extends LdapUserInterface
{
}