<?php
/**
 * Created by PhpStorm.
 * User: akdaho
 * Date: 15/04/2016
 * Time: 11:38
 */

namespace UserBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface;

class UserDependentRole implements RoleInterface
{
    private $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function getRole()
    {
        return 'ROLE_' . strtoupper($this->user->getUsername());
    }
}