<?php

namespace DashboardBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * NotificationALlertRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NotificationALlertRepository extends EntityRepository
{
    public function getAllNotification()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('na.id, na.cOULEUR, na.dATENOTIFICATIONALERTE, na.tYPEALERTE, na.mATERIELID, m.lIBELLE as MATERIEL, m.cODE')
            ->from('DashboardBundle:NotificationALlert', 'na')
            ->innerJoin('TechniqueBundle:Materiel','m', 'WITH', 'na.mATERIELID = m.id')
            ->getQuery()->getResult();
        return $query;
    }
}
