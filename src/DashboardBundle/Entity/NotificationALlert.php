<?php

namespace DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NotificationALlert
 */
class NotificationALlert
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $mATERIELID;

    /**
     * @var string
     */
    private $cOULEUR;

    /**
     * @var \DateTime
     */
    private $dATENOTIFICATIONALERTE;

    /**
     * @var string
     */
    private $tYPEALERTE;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mATERIELID
     *
     * @param integer $mATERIELID
     * @return NotificationALlert
     */
    public function setMATERIELID($mATERIELID)
    {
        $this->mATERIELID = $mATERIELID;

        return $this;
    }

    /**
     * Get mATERIELID
     *
     * @return integer 
     */
    public function getMATERIELID()
    {
        return $this->mATERIELID;
    }

    /**
     * Set cOULEUR
     *
     * @param string $cOULEUR
     * @return NotificationALlert
     */
    public function setCOULEUR($cOULEUR)
    {
        $this->cOULEUR = $cOULEUR;

        return $this;
    }

    /**
     * Get cOULEUR
     *
     * @return string 
     */
    public function getCOULEUR()
    {
        return $this->cOULEUR;
    }

    /**
     * Set dATENOTIFICATIONALERTE
     *
     * @param \DateTime $dATENOTIFICATIONALERTE
     * @return NotificationALlert
     */
    public function setDATENOTIFICATIONALERTE($dATENOTIFICATIONALERTE)
    {
        $this->dATENOTIFICATIONALERTE = $dATENOTIFICATIONALERTE;

        return $this;
    }

    /**
     * Get dATENOTIFICATIONALERTE
     *
     * @return \DateTime 
     */
    public function getDATENOTIFICATIONALERTE()
    {
        return $this->dATENOTIFICATIONALERTE;
    }

    /**
     * Set tYPEALERTE
     *
     * @param string $tYPEALERTE
     * @return NotificationALlert
     */
    public function setTYPEALERTE($tYPEALERTE)
    {
        $this->tYPEALERTE = $tYPEALERTE;

        return $this;
    }

    /**
     * Get tYPEALERTE
     *
     * @return string
     */
    public function getTYPEALERTE()
    {
        return $this->tYPEALERTE;
    }
}
