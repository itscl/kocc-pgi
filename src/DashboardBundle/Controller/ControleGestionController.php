<?php

namespace DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ControleGestionController extends Controller
{
    public function indexAction()
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $maintenances = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getListAllMaintenance();

            $articles = array();
            foreach ($maintenances as $sr)
            {
                $id = $sr['id'];

                $article = $em->getRepository('TechniqueBundle:MaintenanceArticleR')->findBymAINTENANCEID
                ($id);

                $articles[] = $article;
            }
            return $this->render('DashboardBundle:ControleGestion:index.html.twig', array(
                'maintenances' => $maintenances,
                'articles' => $articles
            ));
        }
    }

    public function getinterventionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $idmaintenance = $request->query->get('idmaintenance');

        $interventions = $em->getRepository('TechniqueBundle:MaintenanceInterventionR')->findBymAINTENANCEID
        ($idmaintenance, array('cOMPTEUR' => 'ASC'));

        //var_dump($interventions); exit;

        return $this->render('DashboardBundle:ControleGestion:intervention.html.twig', array(
            'interventions' => $interventions
        ));
    }

    public function getarticlesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $idmaintenance = $request->query->get('idmaintenance');

        $articles = $em->getRepository('TechniqueBundle:MaintenanceArticleR')->findBymAINTENANCEID
        ($idmaintenance);

        //var_dump($interventions); exit;

        return $this->render('DashboardBundle:ControleGestion:getarticles.html.twig', array(
            'articles' => $articles
        ));
    }

}
