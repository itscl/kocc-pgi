<?php

namespace DashboardBundle\Controller;

use DashboardBundle\Entity\NotificationAlert;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TechniqueController extends Controller
{
    public function notificationAction()
    {
        $em = $this->getDoctrine()->getManager();

       /* $redis = $this->container->get('snc_redis.default');

        $key = "key_materiel_dashboard";

        $redis->del($key);*/

        $notifications = $em->getRepository('DashboardBundle:NotificationAlert')->getAllNotification();

        return $this->render('layout/notitication.html.twig', array(
            'notifications' => $notifications
        ));
    }

    public function indexAction()
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();

            $materiels = $em->getRepository('TechniqueBundle:Materiel')->getAllMateriel();

            //var_dump($materiels); exit;

            $notifications = $em->getRepository('DashboardBundle:NotificationAlert')->getAllNotification();

           /* $redis = $this->container->get('snc_redis.default');

            $key = "key_materiel_dashboard";

            $redis->del($key);*/

            $arrayid = array();

           /* if(!$redis->exists($key))
            {*/
                $rows = array();

                foreach($materiels as $r)
                {
                    $materielid = $r['materielid'];
                    $row['MATERIELID'] = $materielid;
                    $row['CODE'] = $r['cODE'];
                    $row['LIBELLE'] = $r['lIBELLE'];
                    $row['NOSERIE'] = $r['nOSERIE'];
                    $row['IMMATRICULATION'] = $r['iMMATRICULATION'];
                    $row['MODELE'] = $r['MODELE'];
                    $row['ETAT'] = $r['eTAT'];
                    $row['SITE'] = $r['SITE'];
                    $row['NOM'] = $r['NOM'];
                    $row['PRENOM'] = $r['PRENOM'];
                    $row['SERVICE'] = $r['SERVICE'];
                    $row['uNITECONSO'] = $r['uNITECONSO'];

                    $matconso = $em->getRepository('TechniqueBundle:MaterielConso')->getMaterielCompteur($materielid);

                    //var_dump($matconso);

                    if($matconso)
                    {
                        foreach($matconso as $r1)
                        {
                            $row['COMPTEUR'] = $r1['compteur'];
                            $dateconso = $r1['dATECONSO'];
                            $dateconso = $dateconso->format('d-m-Y H:m:s');
                            $row['DATECONSO'] = $dateconso;
                        }
                    }

                    else
                    {
                        $row['COMPTEUR'] = "Pas de conso";
                        $dateconso =  "Pas de conso";
                        $row['DATECONSO'] =  "Pas de conso";
                    }

                    $matmaintenancelastcpt = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getLastCptMaintenance($materielid);

                    //var_dump($matmaintenancelastcpt);

                    if($matmaintenancelastcpt)
                    {
                        foreach($matmaintenancelastcpt as $rmatmaintenancelastcpt)
                        {
                            $idmaintenance = $rmatmaintenancelastcpt['id'];

                            if($idmaintenance == null)
                            {
                                $row['COMPTEUR_DM'] = "";
                                $row['COMPTEUR_PM'] = "";
                                $row['DATEREALISEM'] = "";
                                $row['NOMMECANICIEN'] = "";
                                $row['TYPEMAINTENANCE'] = "";
                            }
                            else
                            {
                                $matmaintenance = $em->getRepository('TechniqueBundle:MaintenanceMateriel')->getLastMaintenanceDetail($idmaintenance);

                                foreach($matmaintenance as $r2)
                                {
                                    $row['COMPTEUR_DM'] = $r2['cOMPTEUR'];
                                    $row['COMPTEUR_PM'] = $r2['cPTPROCHAIN'];
                                    $row['NOMMECANICIEN'] = $r2['nOMMECANICIEN'];
                                    $row['TYPEMAINTENANCE'] = $r2['tYPEMAINTENANCE'];
                                    $dATEREALISE = $r2['dATEREALISE'];
                                    $dATEREALISE = $dATEREALISE->format('d-m-Y H:m:s');
                                    $row['DATEREALISEM'] = $dATEREALISE;
                                }
                            }
                        }
                    }

                    //var_dump($matmaintenancelastcpt);

                    /*if(count($matmaintenance) == 0)
                    {
                        $row['COMPTEUR_DM'] = "";
                        $row['COMPTEUR_PM'] = "";
                        $row['DATEREALISEM'] = "";
                        $row['NOMMECANICIEN'] = "";
                        $row['TYPEMAINTENANCE'] = "";
                        // echo "pas existe". "<br>";
                    }
                    else
                    {
                        foreach($matmaintenance as $r2)
                        {
                            $row['COMPTEUR_DM'] = $r2['cOMPTEUR'];
                            $row['COMPTEUR_PM'] = $r2['cPTPROCHAIN'];
                            $row['NOMMECANICIEN'] = $r2['nOMMECANICIEN'];
                            $row['TYPEMAINTENANCE'] = $r2['tYPEMAINTENANCE'];
                            $dATEREALISE = $r2['dATEREALISE'];
                            $dATEREALISE = $dATEREALISE->format('d-m-Y H:m:s');
                            $row['DATEREALISEM'] = $dATEREALISE;
                        }


                    }*/

                    $notification = $em->getRepository('DashboardBundle:NotificationAlert')->findBy(array('mATERIELID' => $materielid));

                    if($notification)
                    {
                        foreach($notification as $r3)
                        {
                            $row['MATERIELID_NOTIF'] = $r3->getMATERIELID();
                            $row['COULEUR'] = $r3->getCOULEUR();
                            $row['TYPEALERTE'] = $r3->getTYPEALERTE();
                            $datealert = $r3->getDATENOTIFICATIONALERTE();
                            $row['DATEALERT'] = $datealert->format('d-m-Y');
                            $row['IDALERT'] = $r3->getId();
                        }
                    }
                    else
                    {
                        $row['MATERIELID_NOTIF'] ="";
                        $row['COULEUR'] = "";
                        $row['TYPEALERTE'] = "";
                        $row['DATEALERT'] = "";
                        $row['IDALERT'] = "";
                    }

                    array_push($rows, $row);
                }
//var_dump($rows);
            //exit;

                /*$redis->set($key, json_encode(array('data'=>$rows)));
                $response = $redis->get($key);

                $json = json_decode($response, 1);

                $resources = $json['data'];
            }
            else
            {
                $response = $redis->get($key);

                $json = json_decode($response, 1);

                $resources = $json['data'];
            }*/

            return $this->render('DashboardBundle:Technique:index.html.twig', array(
                'materiel' => $rows,
                //'notifications' => $notification,
                //'maVar' => $maVar
            ));
        }
    }

    public function ajouteralertAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $materielid = $request->request->get('materielid');
        $couleur = $request->request->get('couleur');
        $typealert = $request->request->get('typealert');

        $notif = $em->getRepository('DashboardBundle:NotificationAlert')->findBy(array('mATERIELID' => $materielid));

        if($notif)
        {
            foreach($notif as $r)
            {
                $id = $r->getId();
            }

            $notification = $em->getRepository('DashboardBundle:NotificationAlert')->find($id);

            var_dump($notification);
            $notification->setCOULEUR($couleur);
            $notification->setTYPEALERTE($typealert);
            $notification->setDATENOTIFICATIONALERTE(new \DateTime());

            $em->flush();
        }
        else
        {
            $entity = new NotificationALert();

            $entity->setMATERIELID($materielid);
            $entity->setCOULEUR($couleur);
            $entity->setTYPEALERTE($typealert);
            $entity->setDATENOTIFICATIONALERTE(new \DateTime());

            $em->persist($entity);
            $em->flush();
        }


        var_dump($materielid);
        var_dump($couleur);

        return $this->render('DashboardBundle:Technique:ajax.html.twig');
    }

    public function supprimeralertAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $materielid = $request->query->get('materielid');

        $notifications = $em->getRepository('DashboardBundle:NotificationAlert')->findBy(array('mATERIELID' => $materielid));

        foreach($notifications as $rs)
        {
            $id = $rs->getId();

            $notification = $em->getRepository('DashboardBundle:NotificationAlert')->find($id);

            $em->remove($notification);
            $em->flush();
        }

        return $this->render('DashboardBundle:Technique:ajax.html.twig');
    }

    public function imprimeAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $notifications = $em->getRepository('DashboardBundle:NotificationAlert')->getAllNotification();

            //var_dump($notifications); exit;

            $html = $this->renderView('DashboardBundle:Technique:imprime.html.twig', array('notifications' => $notifications));

            //on appelle le service html2pdf
            $html2pdf = $this->get('html2pdf_factory')->create('P', 'A4', 'fr', true, 'UTF-8', array(15, 10, 15, 10));
            //real : utilise la taille r�elle
            $html2pdf->pdf->SetDisplayMode('real');
            //writeHTML va tout simplement prendre la vue stocker dans la variable $html pour la convertir en format PDF
            $html2pdf->writeHTML($html);
            //Output envoit le document PDF au navigateur internet
            return new Response($html2pdf->Output('Fiche-inventaire.pdf'), 200, array('Content-Type' => 'application/pdf'));
        }
    }

}
