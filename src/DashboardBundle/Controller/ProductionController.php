<?php

namespace DashboardBundle\Controller;

use DateInterval;
use DatePeriod;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ProductionController extends Controller
{

    public function itmocache()
    {
        $em = $this->getDoctrine()->getManager();

        $itmo = $em->getRepository('CultureBundle:ItMO')->getitmodetailresp();

        //var_dump($reception); exit;

        $cache = $this->get('memcache.default');
        // $cache->delete('itmo_list');
        //$cache->delete('itmo_total');
        //$cache->delete('itmo_all_list');

        if (!$cache->get('itmo_list'))
        {
            $rows = array();

            foreach ($itmo as $r_detail) {

                $cyclecultural_detail = $r_detail['CYCLECULTURAL'];
                $cycleculturalid_detail = $r_detail['CYCLECULTURALID'];
                $site_detail = $r_detail['SITE'];
                $siteid_detail = $r_detail['SITEID'];
                $ferme_detail = $r_detail['FERME'];
                $fermeid_detail = $r_detail['FERMEID'];
                $culture_detail = $r_detail['CULTURE'];
                $cultureid_detail = $r_detail['CULTUREID'];
                $famillecultureid_detail = $r_detail['FAMILLECULTUREID'];
                $typeirri_detail = $r_detail['TYPEIRRIGATION'];
                $ilot_detail = $r_detail['ILOT'];
                $typeop_detail = $r_detail['tYPEOP'];
                $periode_detail = $r_detail['PERIODE'];

                $surface_detail = $r_detail['sURFACE'];
                $surface_detail = str_replace(',', '.', $surface_detail);
                $surface_detail = (float)$surface_detail;

                $datesemis_detail = $r_detail['dATEDEBUTSEMIS'];

                $quantite_detail  = $r_detail['qUANTITE'];
                $quantite_detail = str_replace(',', '.', $quantite_detail);
                $quantite_detail = (float)$quantite_detail;

                $geste_detail = $r_detail['GESTE'];
                $gesteid_detail = $r_detail['GESTEID'];
                $mois_detail = $r_detail['MOIS'];
                $semaine_detail = new DateTime($r_detail['SEMAINE']);
                $semaine_detail = $semaine_detail->format('Wy');
                $semaine1_detail = $r_detail['SEMAINE1'];
                $unite_detail = $r_detail['uNITE'];

                $numrecolte_detail =  $r_detail['NUMRECOLTE'];
                $pourcentrend_detail =  $r_detail['POURCENTREND'];
                $datedebutrecolte = $r_detail['DATEDEBUTRECOLTE'];
                if($datedebutrecolte != null)
                {
                    $datedebutrecolte = $datedebutrecolte->format('d-m-Y');
                }

                $row['CYCLECULTURAL'] = $cyclecultural_detail;
                $row['CYCLECULTURALID'] = $cycleculturalid_detail;
                $row['SITE'] = $site_detail;
                $row['SITEID'] = $siteid_detail;
                $row['FERMEID'] = $fermeid_detail;
                $row['FERME'] = $ferme_detail;
                $row['CULTURE'] = $culture_detail;
                $row['CULTUREID'] = $cultureid_detail;
                $row['TYPEIRRIGATION'] = $typeirri_detail;
                $row['SURFACE'] = $surface_detail;
                $row['DATEDEBUTSEMIS'] = $datesemis_detail->format('d-m-Y');
                $row['MOIS'] = $mois_detail;
                $row['SEMAINE'] = $semaine_detail;
                $row['GESTE'] = $geste_detail;
                $row['GESTEID'] = $gesteid_detail;
                $row['QUANTITE'] = $quantite_detail;
                $row['ILOT'] = $ilot_detail;
                $row['TYPEOP'] = $typeop_detail;
                $row['FAMILLECULTUREID'] = $famillecultureid_detail;
                $row['NUMRECOLTE'] = $numrecolte_detail;
                $row['POURCENTREND'] = $pourcentrend_detail;
                $quantite_detail_ha = ($quantite_detail * 8) * $surface_detail;
                $quantitejh_detail_ha = $quantite_detail * $surface_detail;
                $row['QUANTITET'] = round($quantite_detail_ha,2);
                $row['QUANTITETJH'] = round($quantitejh_detail_ha,2);
                $row['FAMILLECULTURE'] = $r_detail['FAMILLECULTURE'];
                $row['JOUR'] = $r_detail['JOUR'];
                $row['NOMCHEFFERME'] = $r_detail['NOMCHEFFERME'];
                $row['PRENOMCHEFFERME'] = $r_detail['PRENOMCHEFFERME'];
                $row['CHEFFERMEID'] = $r_detail['CHEFFERMEID'];
                $row['DATEDEBUTRECOLTE'] = $datedebutrecolte;
                $row['PERIODE'] = $periode_detail;
                $mois1 = new \DateTime($r_detail['JOUR']);
                $mois1 = $mois1->format('M y');
                $row['MOIS1'] = $mois1 ;
                $row['PRENOMCHEFSITE'] = $r_detail['PRENOMCHEFSITE'];
                $row['NOMCHEFSITE'] = $r_detail['NOMCHEFSITE'];
                $row['GESTECODE'] = $r_detail['GESTECODE'];

                $poidnetreception = 0;
                //$row['POIDNETRECEP'] = $poidnetreception;

                $semaine_semis_detail = $datesemis_detail->format("W");

                $datesemis_detail = $datesemis_detail->format('d-m-Y');

                $cycles_detail = $em->getRepository('CultureBundle:ITKCycle')->findBy(array
                ('fAMILLECULTUREID' =>$famillecultureid_detail,
                    'sEMAINESEMIS' => $semaine_semis_detail));

                foreach($cycles_detail as $cycle_detail)
                {
                    $dureecycle_detail = $cycle_detail->getDUREECYCLE();
                    $row['DURRECYCLE'] = $dureecycle_detail;

                    if ($geste_detail == 'RECOLTE' || $geste_detail == 'MANUTENTION PROD') {

                        $firstrecolte = $em->getRepository('CultureBundle:ItMO')->getfirstrecolte($cultureid_detail);

                        //var_dump($firstrecolte);

                        if($datedebutrecolte == null)
                        {
                            foreach($firstrecolte as $rsfirstrecolte)
                            {

                                $periodefirst = $rsfirstrecolte['pERIODE'];

                                $periode1 = $periodefirst * 7;

                                $datedebutrecolte = date('d-m-Y', strtotime($datesemis_detail . " +$periode1 days"));

                               // $row['DATEDEBUTRECOLTE_'] = $datedebutrecolte;

                                $nb_jours = ($numrecolte_detail * 7);
                                //$nb_jours = $nb_jours - 7;

                                $date_r_aj_detail = date('d-m-Y', strtotime($datedebutrecolte . " +$nb_jours days"));

                            }
                            $row['POIDNETRECEP'] = null;

                        }
                        else
                        {
                            $nb_jours = ($numrecolte_detail * 7);
                            $nb_jours = $nb_jours - 7;

                            $date_r_aj_detail = date('d-m-Y', strtotime($datedebutrecolte . " +$nb_jours days"));

                            $date1 = new DateTime($date_r_aj_detail);

                            $semaine_r_aj_detail2 = $date1->format('Wy');

                            $reception = $em->getRepository('ReceptionBundle:ReceptionR')->getSumByOP($cycleculturalid_detail, $semaine_r_aj_detail2);

                            foreach($reception as $rsreception)
                            {
                                $poidnetreception = $rsreception['SUMPOIDNET'];
                                $row['POIDNETRECEP'] = $poidnetreception;
                            }
                            //var_dump($reception);
                        }


                        $date = new DateTime($date_r_aj_detail);

                        $semaine_r_aj_detail = $date->format('W');
                        $semaine_r_aj_detail1 = $date->format('Wy');

                        $mois_r_aj_detail = $date->format('my');
                        $mois_r_aj_detail1 = $date->format('M y');

                        $row['DATERECOLTE_AJUSTER'] = $date_r_aj_detail;
                        $row['DURRECYCLE'] = $dureecycle_detail;
                        $row['SEMAINE'] = $semaine_r_aj_detail1;
                        $row['JOUR'] = $date->format('d-m-Y');
                        $row['MOIS'] = $mois_r_aj_detail;
                        $row['MOIS1'] = $mois_r_aj_detail1;

                        $itkcyclecv_detail = $em->getRepository('CultureBundle:ITKCycleCV')->findBy(array
                        ('sEMAINERECOLTE' => $semaine_r_aj_detail, 'cULTUREID' => $cultureid_detail));

                        if($poidnetreception != null || $poidnetreception != 0 )
                        {
                            if ($quantite_detail != null) {
                                $quantitef_detail = ($poidnetreception / ($quantite_detail * 8)) * $surface_detail;
                                $quantitef_detailjh = ($poidnetreception / $quantite_detail) * $surface_detail;


                                $row['QUANTITET'] = round($quantitef_detail, 2);
                                $row['QUANTITETJH'] = round($quantitef_detailjh, 2);
                            }
                            else
                            {
                                $row['QUANTITET'] = 0;
                                $row['QUANTITETJH'] = 0;
                            }
                        }
                        else
                        {
                            if ($itkcyclecv_detail) {
                                foreach ($itkcyclecv_detail as $r1) {
                                    $rdtbrut_detail = $r1->getRDTBRUT();

                                    if ($pourcentrend_detail != null)
                                    {
                                        if ($quantite_detail != null)
                                        {
                                            $new_rdtbrut_detail = ($rdtbrut_detail * $pourcentrend_detail) / 100;

                                            $quantitef_detail = ($new_rdtbrut_detail / ($quantite_detail * 8)) * $surface_detail;
                                            $quantitef_detailjh = ($new_rdtbrut_detail / $quantite_detail) * $surface_detail;


                                            $row['QUANTITET'] = round($quantitef_detail,2);
                                            $row['QUANTITETJH'] = round($quantitef_detailjh,2);
                                            $row['RENDEMENTBRUT'] = $rdtbrut_detail;

                                        }
                                        else
                                        {
                                            $row['QUANTITET'] = 0;
                                            $row['QUANTITETJH'] = 0;
                                        }
                                    }
                                    else
                                    {
                                        $row['QUANTITET'] = 0;
                                        $row['QUANTITETJH'] = 0;
                                    }

                                }
                            } else {
                                $row['QUANTITET'] = 0;
                                $row['RENDEMENTBRUT'] = 0;
                                $row['QUANTITETJH'] = 0;
                            }
                        }
                    }
                    else
                    {
                        $row['POIDNETRECEP'] = null;
                    }

                }
                $row['UNITE'] = $unite_detail;

                array_push($rows,$row);
            }

            $rowst = array();

            $datedebut = '22-01-2018';
            $datefin = '04-03-2018';

            foreach($rows as $rs_semaine)
            {

                if (strtotime($rs_semaine['JOUR']) >=  strtotime($datedebut) && strtotime($rs_semaine['JOUR']) <= strtotime($datefin))
                {
                    $rowst[] = $rs_semaine;
                }
            }

            foreach($rowst as $rst)
            {
                $QTET[] = $rst['QUANTITETJH'];
            }

            $qt_global = array_sum($QTET);

            $cache->delete('itmo_all_list');
            $cache->delete('itmo_list');
            $cache->delete('itmo_total');
            $cache->set('itmo_list', $rowst, false, 3600);
            $cache->set('itmo_total', $qt_global, false, 3600);
            $cache->set('itmo_all_list', $rows, false, 3600);

        }
        else
        {
            $rowst = $cache->get('itmo_list');
            $qt_global = $cache->get('itmo_total');
            $rows = $cache->get('itmo_all_list');
        }

        //echo '<pre>';
        //print_r($rowst);
        //echo '</pre>';
        //var_dump($rowst);
        //exit;

        return array(
            'rowst' => $rowst,
            'qt_global' => $qt_global,
            'rows' => $rows
        );
    }

    public function contractuelrealisecache()
    {
        $em = $this->getDoctrine()->getManager();
        $cache = $this->get('memcache.default');

       // $cache->delete('contractuel_list');

        if (!$cache->get('contractuel_list'))
        {
            $contractuel = $em->getRepository('PointageBundle:PointageLigne')->getAllContractuelProdPointee();

            $rows = array();
            foreach($contractuel as $rs)
            {
                //$horaire =  str_replace(".",",",$rs['hORAIRE']);
                $row['HORAIRE'] = floatval($rs['hORAIRE']);
                $row['MATRICULE'] = $rs['mATRICULE'];
                $row['DATEPOINTAGE'] = new \DateTime($rs['dATEPOINTAGE']);
                $row['GESTE'] =$rs['gESTE'];
                $row['sERVICE'] =$rs['sERVICE'];
                $row['dIRECTION'] =$rs['dIRECTION'];
                $row['sITE'] =$rs['sITE'];

                array_push($rows, $row);
            }

            foreach($rows as $rs1)
            {
                $QTETH[] = $rs1['HORAIRE'];
            }

            $QTETJH = array_sum($QTETH) / 8;


            $itmo_list = $this->itmocache();

            $itmo = $itmo_list['rowst'];

            foreach($itmo as $rsitmo)
            {
                $arraygestep[] = $rsitmo['GESTECODE'];
            }

            $arraygestepr =array();
            $arraygestenpr =array();
            foreach($rows as $rsrows)
            {
                if(in_array($rsrows['GESTE'], array_unique($arraygestep)))
                {
                    $arraygestepr[] = $rsrows;
                }
                else
                {
                    $arraygestenpr[] = $rsrows;
                }

            }

            foreach($arraygestepr as $rsgestepr)
            {
                $QTEPRJH[] = $rsgestepr['HORAIRE'] / 8;
            }

            $QTETPRJH = array_sum($QTEPRJH);

            foreach($arraygestenpr as $rsgestenpr)
            {
                $QTENPRJH[] = $rsgestenpr['HORAIRE'] / 8;
            }

            $QTETNPRJH = array_sum($QTENPRJH);

            /***************************************************************************/



            $cache->delete('contractuel_list');
            $cache->delete('contractuel_total');
            $cache->delete('contractuel_total_resalise_prevu');
            $cache->delete('contractuel_total_non_resalise_prevu');
            $cache->delete('contractuel_list_resalise_prevu');
            $cache->delete('contractuel_list_non_resalise_prevu');
            $cache->set('contractuel_list', $rows, false, 3600);
            $cache->set('contractuel_total', $QTETJH, false, 3600);
            $cache->set('contractuel_total_resalise_prevu', $QTETPRJH, false, 3600);
            $cache->set('contractuel_total_non_resalise_prevu', $QTETNPRJH, false, 3600);
            $cache->set('contractuel_list_resalise_prevu', $arraygestepr, false, 3600);
            $cache->set('contractuel_list_non_resalise_prevu', $arraygestenpr, false, 3600);



        }
        else
        {

            $rows = $cache->get('contractuel_list');
            $QTETJH = $cache->get('contractuel_total');
            $QTETPRJH = $cache->get('contractuel_total_resalise_prevu');
            $QTETNPRJH = $cache->get('contractuel_total_non_resalise_prevu');
            $arraygestepr = $cache->get('contractuel_list_resalise_prevu');
            $arraygestenpr = $cache->get('contractuel_list_non_resalise_prevu');

           // var_dump($QTETPR);exit;



            // var_dump($itmo);exit;
        }//exit;
        return array(
            'list_contractuel' => $rows,
            'total_contractuel' => $QTETJH,
            'contractuel_realise_prevu' => $QTETPRJH,
            'contractuel_non_realise_prevu' => $QTETNPRJH,
            'arraygestepr' => $arraygestepr,
            'arraygestenpr' => $arraygestenpr
           // 'arraygeste' => $arraygeste
        );



    }

    public function detailsemaine($semaine, $array)
    {
        $em = $this->getDoctrine()->getManager();

        $cache = $this->get('memcache.default');

        $itmo_semaine = array();

        foreach($array as $rs)
        {
            if ($rs['SEMAINE'] ==  $semaine)
            {
                $itmo_semaine[] = $rs;
            }
        }

        $QTET_SP = array();
        foreach($itmo_semaine as $rsgrsp)
        {
            $QTET_SP[] = $rsgrsp['QUANTITETJH'];
        }

        $qte_total_semaine = array_sum($QTET_SP);

        //var_dump($qte_total_semaine); exit;

        $journalier_semaine = $em->getRepository('PointageBundle:PointageJournalierTemp')->getByWeek($semaine);

        $contractuel_semaine = $em->getRepository('PointageBundle:PointageLigne')->getPointageProdSemByWeek($semaine);

        $effectif_total_journalier = array();
        foreach ($journalier_semaine as $rssemjt)
        {
            $effectif_total_journalier[] = $rssemjt['eFFECTIF'];
        }

        $total_journalier = array_sum($effectif_total_journalier);

        //LES QUNATITES TOTAL PAR SITE
        $rows_total_site_semaine = array();
        foreach($itmo_semaine as $rsgrsitsp)
        {

            if(array_key_exists( $rsgrsitsp['SITEID'], $rows_total_site_semaine)) {
                $rows_total_site_semaine[$rsgrsitsp['SITEID']]['QUANTITETJH'] += $rsgrsitsp['QUANTITETJH'];
            } else
            {
                $rows_total_site_semaine[$rsgrsitsp['SITEID']] = $rsgrsitsp;
            }
        }

       // var_dump($rows_total_site_semaine); exit;

        //Tableau cummul des quantité par geste ferme
        $rows_gr_ferme_semaine = array();
        foreach($itmo_semaine as $rsfes)
        {
            if(array_key_exists( $rsfes['FERMEID'], $rows_gr_ferme_semaine)) {
                $rows_gr_ferme_semaine[$rsfes['FERMEID']]['QUANTITETJH'] += $rsfes['QUANTITETJH'];
            } else
            {
                $rows_gr_ferme_semaine[$rsfes['FERMEID']] = $rsfes;
            }
        }

        $semaine_prevu_realise = array();
        foreach ($rows_gr_ferme_semaine as $rssem)
        {
            $ferme = $rssem['FERME'];
            $row_ferme['FERME'] = $rssem['FERME'];
            $row_ferme['SITE'] = $rssem['SITE'];
            $row_ferme['SITEID'] = $rssem['SITEID'];
            $row_ferme['CHEFFERMEID'] = $rssem['CHEFFERMEID'];
            $row_ferme['FERMEID'] = $rssem['FERMEID'];
            $row_ferme['NOMCHEFFERME'] = $rssem['NOMCHEFFERME'];
            $row_ferme['PRENOMCHEFFERME'] = $rssem['PRENOMCHEFFERME'];
            $row_ferme['PREVU'] = $rssem['QUANTITETJH'];

            $journalier_semaine_ferme = $em->getRepository('PointageBundle:PointageJournalierTemp')->getByFerme
            ($ferme, $semaine);

            $contractuel_semaine_ferme = $em->getRepository('PointageBundle:PointageLigne')->getPointageProdSemByFerme
            ($ferme,  $semaine);


            $effectif_journalier = array();
            foreach ($journalier_semaine_ferme as $rssemj)
            {
                $effectif_journalier[] = $rssemj['eFFECTIF'];
            }

            $row_ferme['REALISE'] = array_sum($effectif_journalier) + count($contractuel_semaine_ferme);
            $row_ferme['REALISEJ'] = array_sum($effectif_journalier);
            $row_ferme['REALISEC'] = count($contractuel_semaine_ferme);

            //+

            array_push($semaine_prevu_realise, $row_ferme);

        }

        $qte_socas = array();
        foreach($semaine_prevu_realise as $rsses)
        {
            if($rsses['FERMEID'] == 122 || $rsses['FERMEID'] == 123 || $rsses['FERMEID'] == 124)
            {
                $qte_socas[] = round($rsses['PREVU']);
            }
        }
        $qte_socas_semaine = array_sum($qte_socas);

        //$array_semaine = array_merge($rows_gr_geste_site_semaine, $rows_gr_ferme_semaine);

        return array(
            'rowsweek' => $itmo_semaine,
            'totalweek' => $qte_total_semaine,
            'rows_gr_ferme_semaine' => $rows_gr_ferme_semaine,
            'rows_total_site_semaine' => $rows_total_site_semaine,
            'semaine_prevu_realise' => $semaine_prevu_realise,
            'qte_socas_semaine' => $qte_socas_semaine


            //'array_semaine' => $array_semaine
        );
    }

    public function extractionAction()
    {
        $em = $this->getDoctrine()->getManager();

        $date = new \DateTime();

        $semaine_suiv = '0518';

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("Aminata Daho")
            ->setTitle("Extraction excel M.P. Prochaine")
            ->setDescription("Extraction en format xlsx");
        $i=2;

        $itmo = $this->itmocache();

        $semaine_prec = $this->detailsemaine($semaine_suiv, $itmo);

        $data_semaine_prec = $semaine_prec['rowsweek'];


        //var_dump($data_semaine_prec); exit;

        $sheet = $phpExcelObject->getActiveSheet();
        //Debut du fichier excel (Entete)
        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'SITE')
            ->setCellValue('B1', 'FERME')
            ->setCellValue('C1', 'NOMCHEFFERME')
            ->setCellValue('D1', 'PRENOMCHEFFERME')
            ->setCellValue('E1', 'CULTURE')
            ->setCellValue('F1', 'GESTE')
            ->setCellValue('G1', 'SEMAINE')
            ->setCellValue('H1', 'JOUR')
            ->setCellValue('I1', 'QUANTITE')
            ->setCellValue('J1', 'SURFACE')
            ->setCellValue('K1', 'RENDEMENTBRUT')
            ->setCellValue('L1', 'POURCENTREND')
            ->setCellValue('M1', 'QUANTITE TOTAL')
            ->setCellValue('N1', 'QUANTITE TOTAL JH');

        foreach ($data_semaine_prec as $sr)
        {

            // $datesortie = new \DateTime($sr['dATESORTIEE']);

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $sr['SITE'])
                ->setCellValue('B' . $i, $sr['FERME'])
                ->setCellValue('C' . $i, $sr['NOMCHEFFERME'])
                ->setCellValue('D' . $i, $sr['PRENOMCHEFFERME'])
                ->setCellValue('E' . $i, $sr['CULTURE'])
                // ->setCellValue('E' . $i, $datesortie)
                ->setCellValue('F' . $i, $sr['GESTE'])
                ->setCellValue('G' . $i, $sr['SEMAINE'])
                ->setCellValue('H' . $i, $sr['JOUR'])
                ->setCellValue('I' . $i, $sr['QUANTITE'])
                ->setCellValue('J' . $i, $sr['SURFACE'])
                ->setCellValue('K' . $i, $sr['RENDEMENTBRUT'])
                ->setCellValue('L' . $i, $sr['POURCENTREND'])
                ->setCellValue('M' . $i, $sr['QUANTITET'])
                ->setCellValue('N' . $i, $sr['QUANTITETJH']);
            $i++;

        }


        $sheet->setTitle('Extraction');

        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        ob_end_clean();
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'Extraction '.$date->format('d-m-Y').'.xlsx'
        );

        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    public function indexAction()
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else {
            $em = $this->getDoctrine()->getManager();

            $date_cur = new \DateTime('now');

            $date_semaine_cur = $date_cur->format('Wy');

            $date_semaine_suiv = date('Wy', strtotime('+1 week', strtotime($date_cur->format('Y-m-d'))));

            $date_semaine_prec = date('Wy', strtotime('-1 week', strtotime($date_cur->format('Y-m-d'))));

            $mois_curr = $date_cur->format('my');

            $itmo_list = $this->itmocache();

            $itmo = $itmo_list['rowst'];

            //TOTAL GLOBAL
            $QTETG_PREVU = $itmo_list['qt_global'];
            $QTETG_RALISE = 0;

            //var_dump($QTETG_PREVU); exit;

            //FONCTION DES DONNEES DE LA SEMAINE PRECEDENTE
            $semaine_prec = $this->detailsemaine($date_semaine_prec, $itmo);

            //LES DONNEES DE LA SEMAINE PRECEDENTE TOUT SITE ET FERME
            $data_semaine_prec = $semaine_prec['rowsweek'];

            //TOTAL DES DONNEES DE LA SEMAINE PREC TOUT SITE ET FERME
            $total_semaine_prec = $semaine_prec['totalweek'];

            //GROUPAGE DES DONNEES DE LA SEMAINE PREC PAR SITE
            $rows_total_site_semaine_prec = $semaine_prec['rows_total_site_semaine'];

            //LES DONNEES DE LA SEMAINE PRECEDENT SANS PRISE EN CHARGE DU REALISE
            $rows_gr_ferme_semaine_prec = $semaine_prec['rows_gr_ferme_semaine'];

            //LES DONNEES DE LA SEMAINE PRECEDENT AVEC LE REALISE
            $semaine_prevu_realise_prec = $semaine_prec['semaine_prevu_realise'];

            //QUANTITES POUR LES FERME SOCAS 1 2 ET 3 POUR LA SEMAINE PRECDENTE
            $qte_socas_semaine_prec = $semaine_prec['qte_socas_semaine'];

/********************************************************************************************************************************************/

            //FONCTION DES DONNEES DE LA SEMAINE SUIVANTE
            $semaine_suiv = $this->detailsemaine($date_semaine_suiv, $itmo);

            //LES DONNEES DE LA SEMAINE SUIVANTE TOUT SITE ET FERME
            $data_semaine_suiv = $semaine_suiv['rowsweek'];

            //TOTAL DES DONNEES DE LA SEMAINE SUIVANTE TOUT SITE ET FERME
            $total_semaine_suiv= $semaine_suiv['totalweek'];

            //GROUPAGE DES DONNEES DE LA SEMAINE SUIV PAR SITE
            $rows_total_site_semaine_suiv = $semaine_suiv['rows_total_site_semaine'];

            //LES DONNEES DE LA SEMAINE SUIV SANS PRISE EN CHARGE DU REALISE
            $rows_gr_ferme_semaine_suiv = $semaine_suiv['rows_gr_ferme_semaine'];

            //LES DONNEES DE LA SEMAINE SUIV AVEC LE REALISE
            $semaine_prevu_realise_suiv = $semaine_suiv['semaine_prevu_realise'];

            //QUANTITES POUR LES FERME SOCAS 1 2 ET 3 POUR LA SEMAINE SUIV
            $qte_socas_semaine_suiv = $semaine_suiv['qte_socas_semaine'];

           //

/********************************************************************************************************************************************/

            //FONCTION DES DONNEES DE LA SEMAINE COURANTE
            $semaine_cur = $this->detailsemaine($date_semaine_cur, $itmo);

            //LES DONNEES DE LA SEMAINE COURANTE TOUT SITE ET FERME
            $data_semaine_cur = $semaine_cur['rowsweek'];

            //TOTAL DES DONNEES DE LA SEMAINE COURANTE TOUT SITE ET FERME
            $total_semaine_cur = $semaine_cur['totalweek'];

            //GROUPAGE DES DONNEES DE LA SEMAINE COURANTE PAR SITE
            $rows_total_site_semaine_cur = $semaine_cur['rows_total_site_semaine'];

            //LES DONNEES DE LA SEMAINE COURANTE SANS PRISE EN CHARGE DU REALISE
            $rows_gr_ferme_semaine_cur = $semaine_cur['rows_gr_ferme_semaine'];

            //LES DONNEES DE LA SEMAINE COURANTE AVEC LE REALISE
            $semaine_prevu_realise_cur = $semaine_cur['semaine_prevu_realise'];

            //QUANTITES POUR LES FERME SOCAS 1 2 ET 3 POUR LA SEMAINE COURANTE
            $qte_socas_semaine_cur = $semaine_suiv['qte_socas_semaine'];

           // var_dump($rows_total_site_semaine_cur); exit;




            //var_dump($QTETG); exit;

            return $this->render('DashboardBundle:Production:index.html.twig', array(
                'total_global_prevu' => $QTETG_PREVU,
                'total_global_realise' => $QTETG_RALISE,
                'date_semaine_prec' => $date_semaine_prec,
                'rows_total_semaine_prec' => $rows_total_site_semaine_prec,
                'total_semaine_prec' => $total_semaine_prec,
                'semaine_prevu_realise_prec' => $semaine_prevu_realise_prec,
                'rows_total_site_semaine_suiv' => $rows_total_site_semaine_suiv,
                'date_semaine_suiv' => $date_semaine_suiv,
                'semaine_prevu_realise_suiv' => $semaine_prevu_realise_suiv,
                'rows_total_site_semaine_cur' => $rows_total_site_semaine_cur,
                'date_semaine_cur' => $date_semaine_cur,
                'total_semaine_suiv' => $total_semaine_suiv,
                'total_semaine_cur' => $total_semaine_cur,
                'semaine_prevu_realise_cur' => $semaine_prevu_realise_cur,
                'qte_socas_semaine_prec' => $qte_socas_semaine_prec,
                'qte_socas_semaine_suiv' => $qte_socas_semaine_suiv,
                'qte_socas_semaine_cur' => $qte_socas_semaine_cur
            ));
        }
    }

    public function itmoglobalcharteAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cache = $this->get('memcache.default');

        $contractuel =  $this->contractuelrealisecache();

        //$QTETG_REALISE_PREVU = $contractuel['contractuel_realise_prevu'];

        //var_dump($cache->get('itmo_list')); exit;

       // $cache->delete('itmo_list');
       // $cache->delete('contractuel_list');
       // $cache->delete('contractuel_total_non_resalise_prevu');

        if ($cache->get('itmo_list') && $cache->get('contractuel_list') )
        {

            $itmo = $cache->get('itmo_list');

            $contractuel =  $this->contractuelrealisecache();

            $QTETG_REALISE_PREVU = $contractuel['contractuel_realise_prevu'];

            $QTETG_REALISE_NON_PREVU = $contractuel['contractuel_non_realise_prevu'];

            $QTETG_PREVU = $cache->get('itmo_total');
            $QTETG_REALISE = $cache->get('contractuel_total');


           // var_dump($QTETG_REALISE_PREVU); exit;

            $total_p = array("Quantite prévu",round($QTETG_PREVU));
            $total_r = array("Quantite réalisé",round($QTETG_REALISE));
            $total_p_r = array("Quantite prévu réalisé",round($QTETG_REALISE_PREVU));
            $total_n_p_r = array("Quantite non prévu réalisé",round($QTETG_REALISE_NON_PREVU));

            $result = array($total_p, $total_r, $total_p_r, $total_n_p_r);

            return new JsonResponse($result);
        }
        else
        {
            $contractuel_list = $this->contractuelrealisecache();

            $contractuel = $contractuel_list['list_contractuel'];

            $itmo_list = $this->itmocache();

            $rows = $itmo_list['rowst'];

            $cache->delete('itmo_list');
            $cache->set('itmo_list', $rows, false, 3600);
            $cache->delete('contractuel_list');
            $cache->set('contractuel_list', $contractuel, false, 3600);

            var_dump($contractuel_list); exit;

            return new JsonResponse();

          // var_dump($a);
        }
    }

    public function itmositesemchartAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $sem = $request->query->get('semaine');
        $site = $request->query->get('site');

        $jour_sem_ferme = $em->getRepository('PointageBundle:PointageJournalierTemp')->getBySite
        ($site, $sem);

        $contr_sem_ferme = $em->getRepository('PointageBundle:PointageLigne')->getPointageProdSemByFerme
        ($site,  $sem);


        $effectif_prec = array();
        foreach ($jour_sem_ferme as $rssemprecj)
        {
            $effectif_prec[] = $rssemprecj['eFFECTIF'];
        }

        $total_realise = array_sum($effectif_prec) + count($contr_sem_ferme);
       // $row_ferme_prec['REALISEJ'] = array_sum($effectif_prec);
       // $row_ferme_prec['REALISEC'] = count($contr_sem_ferme);

        //+

       // array_push($total_sem_prec, $row_ferme_prec);

        //$contractuels = $em->getRepository('PointageBundle:PointageLigne')->getPointageProdSiteSem();

        //$journalierS = $em->getRepository('PointageBundle:PointageJournalierTemp')->getAll();


        $cache = $this->get('memcache.default');


        if ($cache->get('itmo_list'))
        {

            $rows = $cache->get('itmo_list');
            $rows_semaine = array();
            foreach ($rows as $rssem)
            {
                if ($rssem['SEMAINE'] == $sem) {
                    $rows_semaine[] = $rssem;
                }
            }

            $rows_site = array();
            foreach ($rows_semaine as $rssi)
            {
                if ($rssi['SITE'] == $site) {
                    $rows_site[] = $rssi;
                }
            }

            foreach($rows_site as $rssi1)
            {
                $qte[] = $rssi1['QUANTITET'];
            }

            $total = array_sum($qte);

            $mop = array();
            $mor = array();

            $mop['name'] = 'MO PREVU';
            $mop['color'] = '#11a9cc';

            $mor['name'] = 'MO REALISE';
            $mor['color'] = '#ffce55';

            $mop['data'][] = round($total);
            $mor['data'][] = $total_realise;

            $result = array();
            array_push($result,$mop);
            array_push($result,$mor);

            return new JsonResponse($result);
        }

    }

    public function itmostrespchartAction()
    {
        $cache = $this->get('memcache.default');

        $date_cur = new \DateTime('now');

        $semaine_curr = $date_cur->format('Wy');


        if ($cache->get('itmo_list'))
        {

            $rows = $cache->get('itmo_list');
            $rows_semaine = array();
            foreach ($rows as $rssem)
            {
                if ($rssem['SEMAINE'] == $semaine_curr) {
                    $rows_semaine[] = $rssem;
                }
            }

            //Tableau avec les gestes FERTI-IRRIG-PHYTO et DESHERBAGE
            $rows_geste_site = array();
            foreach($rows_semaine as $rsgsisp)
            {
                if ($rsgsisp['GESTE'] ==  'GARDIENNAGE' || $rsgsisp['GESTE'] ==  'FERTI-IRRIG-PHYTO')
                {
                    $rows_geste_site[] = $rsgsisp;
                }
            }

            //Tableau sans les gestes FERTI-IRRIG-PHYTO et DESHERBAGE
            $rows_geste_ferme = array();
            foreach($rows_semaine as $rsgfesp)
            {
                if ($rsgfesp['GESTE'] !=  'GARDIENNAGE' && $rsgfesp['GESTE'] !=  'FERTI-IRRIG-PHYTO')
                {
                    $rows_geste_ferme[] = $rsgfesp;
                }
            }


            //Tableau cummul des quantité par geste ferme
            $rows_gr_ferme = array();
            foreach($rows_geste_ferme as $rsfesp)
            {
                if(array_key_exists( $rsfesp['CHEFFERMEID'], $rows_gr_ferme)) {
                    $rows_gr_ferme[$rsfesp['CHEFFERMEID']]['QUANTITET'] += $rsfesp['QUANTITET'];
                } else
                {
                    $rows_gr_ferme[$rsfesp['CHEFFERMEID']] = $rsfesp;
                }
            }

            // var_dump($rows_gr_ferme); exit;

            //Tableau cummul des quantité par geste site
            $rows_gr_geste_site = array();

            foreach($rows_geste_site as $rsgrgesisp)
            {
                if(array_key_exists( $rsgrgesisp['SITEID'], $rows_gr_geste_site)) {
                    $rows_gr_geste_site[$rsgrgesisp['SITEID']]['QUANTITET'] += $rsgrgesisp['QUANTITET'];
                } else
                {
                    $rows_gr_geste_site[$rsgrgesisp['SITEID']] = $rsgrgesisp;
                }
            }

            foreach($rows_gr_geste_site as $rssite)
            {
                $site[] = $rssite['NOMCHEFFERME'].' '.$rssite['PRENOMCHEFFERME'];
                $qtesite[] = round($rssite['QUANTITET']);
            }

            $ferme = array();
            $qteferme = array();
            foreach($rows_gr_ferme as $rsferme)
            {
                $ferme[] = $rsferme['NOMCHEFFERME'].' '.$rsferme['PRENOMCHEFFERME'];
                $qteferme[] = round($rsferme['QUANTITET']);

            }
            // var_dump($qteferme);
            // exit;

            $realise = array(1000,0,0,0,0,0,0,0,0,0);
            $category = array();
            $category['name'] = 'Responsable';

            $mop = array();
            $mor = array();

            $mop['name'] = 'MO PREVU';
            $mop['color'] = '#11a9cc';
            $mop['showInLegend'] = false;
            $mop['xAxis'] = 1;
            $mop['pointWidth'] = 25;
            $mop['groupPadding'] = 0.5;

            $mor['name'] = 'MO REALISE';
            $mor['color'] = '#ffce55';
            $mor['showInLegend'] = false;

            $category['data'] = array_merge($site, $ferme);
            $mop['data'] = array_merge($qtesite, $qteferme);
            $mor['data'] = $realise;

            //var_dump($realise); exit;

            $result = array();
            array_push($result,$category);
            array_push($result,$mop);
            array_push($result,$mor);


            return new JsonResponse($result);
        }
    }

    public function chartgrsiteAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cache = $this->get('memcache.default');

        $itmo_list = $this->itmocache();

        $itmo = $itmo_list['rowst'];

        $rows_gr_site = array();

        foreach($itmo as $rs)
        {
            if(array_key_exists( $rs['SITE'], $rows_gr_site)) {
                $rows_gr_site[$rs['SITE']]['QUANTITETJH'] += $rs['QUANTITETJH'];
            } else
            {
                $rows_gr_site[$rs['SITE']] = $rs;
            }
        }

        $prevu = array();
        $realise_prevu = array();
        $result_prevu = array();
        $category = array();
        foreach($rows_gr_site as $r)
        {
            //$category['data'][] = $r['SITE'];
            //$result_prevu[] = $r['SITE'];
            $row['name'] = $r['SITE'];
            $row['y'] = round($r['QUANTITETJH']);
            $row['drilldown'] = true;
                //$r['SITE'];
            //$category['category'][] = $r['SITE'];

            array_push($result_prevu,$row);
        }

        $contractuel_list =  $this->contractuelrealisecache();

        $contractuels_prevu_realise = $contractuel_list['arraygestepr'];

        $contractuels_realise_non_prenu = $contractuel_list['arraygestenpr'];

        $contractuels = $contractuel_list['list_contractuel'];

        $rows_gr_service_rp = array();
        foreach($contractuels_prevu_realise as $rs1)
        {
            if(array_key_exists( $rs1['sITE'], $rows_gr_service_rp)) {

                $rows_gr_service_rp[$rs1['sITE']]['HORAIRE'] += $rs1['HORAIRE'];
            } else
            {
                $rows_gr_service_rp[$rs1['sITE']] = $rs1;
            }

        }

        $result_prevu_realise = array();
        foreach($rows_gr_service_rp as $rs11)
        {
            $row1['name'] = $rs11['sITE'];
            $row1['y'] = round($rs11['HORAIRE'] / 8);
            $row1['drilldown'] = true;
                //$rs11['sITE'];

            array_push($result_prevu_realise,$row1);
        }



        $rows_gr_service_nrp = array();
        foreach($contractuels_realise_non_prenu as $rs2)
        {
            if(array_key_exists( $rs2['sITE'], $rows_gr_service_nrp)) {

                $rows_gr_service_nrp[$rs2['sITE']]['HORAIRE'] += $rs2['HORAIRE'];
            } else
            {
                $rows_gr_service_nrp[$rs2['sITE']] = $rs2;
            }

        }

        $result_non_prevu_realise = array();
        foreach($rows_gr_service_nrp as $rs22)
        {
            $row2['name'] = $rs22['sITE'];
            $row2['y'] = round($rs22['HORAIRE'] / 8);
            $row2['drilldown'] = $rs22['sITE'];

            array_push($result_non_prevu_realise,$row2);
        }

        $rows_gr_service = array();
        foreach($contractuels as $rs3)
        {
            if(array_key_exists( $rs3['sITE'], $rows_gr_service)) {

                $rows_gr_service[$rs3['sITE']]['HORAIRE'] += $rs3['HORAIRE'];
            } else
            {
                $rows_gr_service[$rs3['sITE']] = $rs3;
            }

        }

        $result_realise = array();
        foreach($rows_gr_service as $rs33)
        {
            $row3['name'] = $rs33['sITE'];
            $row3['y'] = round($rs33['HORAIRE'] / 8);
            $row3['drilldown'] = true;
                //$rs33['sITE'];

            array_push($result_realise,$row3);
        }



        //var_dump($result_non_prevu_realise); exit;


        //var_dump($result_prevu_realise); exit;

        $realise_prevu= array();
        $non_realise_prevu= array();
        $realise= array();

        $realise_prevu['name'] = "prevu realise";
        $realise_prevu['data'] =$result_prevu_realise;
        $realise_prevu['color'] = '#f3b621';

        $non_realise_prevu['name'] = "non prevu realise";
        $non_realise_prevu['data'] = $result_non_prevu_realise;
        $non_realise_prevu['color'] = '#cda135';

        $realise['name'] = "realise";
        $realise['data'] = $result_realise;
        $realise['color'] = '#ffce55';

        $prevu['name'] = "prevu";
        $prevu['data'] = $result_prevu;
        $prevu['color'] = '#11a9cc';

        $result = array();
        array_push($result,$prevu);
        array_push($result,$realise);
        array_push($result,$realise_prevu);
        array_push($result,$non_realise_prevu);

        //array_push($result,$category);

        return new JsonResponse($result);

    }

    public function drillchartgrsiteAction(Request $request)
    {
        $site = $request->query->get("site");

        $itmo_list = $this->itmocache();

        $itmo = $itmo_list['rowst'];

        $rows = array();
        foreach($itmo as $rs)
        {
            if ($rs['SITE'] == $site)
            {
                $rows[] = $rs;
            }
        }

        $rows_gr_site_mois = array();
        foreach($rows as $rs1)
        {
            if(array_key_exists( $rs1['MOIS1'], $rows_gr_site_mois)) {
                $rows_gr_site_mois[$rs1['MOIS1']]['QUANTITETJH'] += $rs1['QUANTITETJH'];
            } else
            {
                $rows_gr_site_mois[$rs1['MOIS1']] = $rs1;
            }
        }
        $result['name'] = 'prevu des sites par mois';

        foreach($rows_gr_site_mois as $rs2)
        {
            $mois = $rs2['MOIS1'];

            $result['category'][] =  $mois;
            $result['data'][] = round($rs2['QUANTITETJH']);
        }

       // exit;
        return new JsonResponse($result);
    }


}
