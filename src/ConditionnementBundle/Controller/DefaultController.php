<?php

namespace ConditionnementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ConditionnementBundle:Default:index.html.twig');
    }
}
