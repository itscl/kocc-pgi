<?php

namespace PointageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PointageLigneType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cONTRACTUELID', 'hidden')
            ->add('jOUR', 'date', array('widget' => 'single_text','input' =>'datetime',
                'format' => 'yyyy-MM-dd',
                'constraints' => array(
                    new NotBlank(array("message" => "Veuillez renseignez ce champ")),
                )))/*, ChoiceType::class, array(
                'choices' => array(
                    'lundi' => 'Lundi',
                    'mardi' => 'Mardi',
                    'mercredi'   => 'Mercredi',
                    'jeudi' => 'Jeudi',
                    'vendredi' => 'Vendredi',
                    'samedi' => 'Samedi',
                    'dimanche' => 'Dimanche'
                )
                ))*/
            ->add('fERMEID', 'hidden')
            ->add('aBSENT', 'checkbox')
            ->add('fERIE', 'checkbox')
            ->add('gESTEID','hidden')
            ->add('oPID', 'hidden')
            ->add('cULTUREID', 'hidden')
            ->add('hEURED', 'datetime', array('widget' => 'single_text','input' =>'datetime',
                'format' => 'H:mm'))
            ->add('hEUREFIN', 'datetime', array('widget' => 'single_text','input' =>'datetime',
                'format' => 'H:mm'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PointageBundle\Entity\PointageLigne'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'pointagebundle_pointageligne';
    }


}
