<?php

namespace PointageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class FichePointageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dESCRIPTION', 'textarea', array(
                'required' => false
            ))
            ->add('dATEDEBUT', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd-mm-yyyy'
                ],
                'constraints' => array(
                    new NotBlank(array("message" => "Champ obligatoire")),
                )
            ))
            ->add('dATEFIN', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd-mm-yyyy'
                ],
                'constraints' => array(
                    new NotBlank(array("message" => "Champ obligatoire")),
                )
            ))
            ;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $fichepointage = $event->getData();
            $form = $event->getForm();
            if($fichepointage && $fichepointage->getId() != null) {
                $form->remove('dATEFIN');
                $form->remove('dATEDEBUT');
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PointageBundle\Entity\FichePointage'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'pointagebundle_fichepointage';
    }


}
