<?php

namespace PointageBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use PointageBundle\Entity\Contractuel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContractuelController extends Controller
{
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();

        $contractuels = $em->getRepository('PointageBundle:Contractuel')->getAllMatricules();

        return $this->render('PointageBundle:Contractuel:list.html.twig',array(
            'contractuels' => $contractuels
        ));
    }

    public function importcontractuelAction()
    {

        $contractuels = array(); // Tableau qui va contenir les éléments extraits du fichier CSV
        $row = 0; // Représente la ligne
        // Import du fichier CSV
        echo $filename=$_FILES["file"]["tmp_name"];

        $row = 1;
        if($_FILES["file"]["size"] > 0) {

            $handle = fopen($filename, "r");
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) { // Eléments séparés par un point-virgule, à modifier si necessaire
                $num = count($data); // Nombre d'éléments sur la ligne traitée
                if($row == 1){ $row++; continue; }
                {
                    $row++;
                    for ($c = 0; $c < $num; $c++) {
                        try
                        {
                            $contractuels[$row] = array(
                                "matricule" => utf8_encode($data[0]),
                                "prenom" => utf8_encode($data[1]),
                                "nom" => utf8_encode($data[2]),
                                "direction" => utf8_encode($data[3]),
                                "service" => utf8_encode($data[4]),
                                "poste" => utf8_encode($data[5]),
                                "affectation" => utf8_encode($data[6]),
                                "codedeste" => utf8_encode($data[8]),
                                "site" => utf8_encode($data[10]),
                                "emploi" => utf8_encode($data[11]),
                                "typecontrat" => utf8_encode($data[14]),
                                "cle1" => utf8_encode($data[15]),
                                "cle2" => utf8_encode($data[16]),
                                "culture" => utf8_encode($data[7]),
                                "pointeur" => utf8_encode($data[12]),
                                "resprecr" => utf8_encode($data[13]),


                            );
                        }
                        catch (Exception $e)
                        {
                            $this->get('session')->getFlashBag()->add(
                                'notice',
                                array(
                                    'alert' => 'danger',
                                    'title' => 'Erreur!',
                                    'message' => 'Le format ou le contenu du fichier'
                                )
                            );
                            return $this->redirect($this->generateUrl('contractuel_list'));

                        }

                    }
                }
            }
            fclose($handle);
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Erreur!',
                    'message' => 'Veuillez joindre un fichier'
                )
            );
        }
        $em = $this->getDoctrine()->getManager(); // EntityManager pour la base de données

        // Lecture du tableau contenant les utilisateurs et ajout dans la base de données
        foreach ($contractuels as $contractuel) {

            // On crée un objet utilisateur
            $entity = new Contractuel();

            // Hydrate l'objet avec les informations provenants du fichier CSV

            $entity->setMATRICULE($contractuel["matricule"]);
            $entity->setPRENOM($contractuel["prenom"]);
            $entity->setNOM($contractuel["nom"]);
            $entity->setDIRECTION($contractuel["direction"]);
            $entity->setSERVICE($contractuel["service"]);
            $entity->setPOSTE($contractuel["poste"]);
            $entity->setAFFECTATION($contractuel["affectation"]);
            $entity->setCODEGESTE($contractuel["codedeste"]);
            $entity->setSITE($contractuel["site"]);
            $entity->setEMPLOI($contractuel["emploi"]);
            $entity->setTYPECONTRAT($contractuel["typecontrat"]);
            $entity->setCLE1($contractuel["cle1"]);
            $entity->setCLE2($contractuel["cle2"]);
            $entity->setPOINTEUR($contractuel["pointeur"]);
            $entity->setRESPRECR($contractuel["resprecr"]);

            if($contractuel["culture"] == 'BU')
            {
                $entity->setCULTUREID(2);
            }
            if($contractuel["culture"] == 'CO')
            {
                $entity->setCULTUREID(61);
            }
            if($contractuel["culture"] == 'HV')
            {
                $entity->setCULTUREID(101);
            }
            if($contractuel["culture"] == 'MD')
            {
                $entity->setCULTUREID(5);
            }
            if($contractuel["culture"] == 'MIEL')
            {
                $entity->setCULTUREID(6);
            }
            if($contractuel["culture"] == 'PD')
            {
                $entity->setCULTUREID(8);
            }
            if($contractuel["culture"] == 'PI')
            {
                $entity->setCULTUREID(9);
            }

            //$entity->setPOINTEURID($contractuel["pointeur"]);
            /* $entity->setSITE($contractuel["site"]);*/

            // Enregistrement de l'objet en vu de son écriture dans la base de données
            //$em->persist($entity);

            //Verifié si le matricule existe dans la base de donnée

            $matriculeexist = $em->getRepository('PointageBundle:Contractuel')->findBy(array('mATRICULE' =>
                $contractuel["matricule"]));

            if(!$matriculeexist)
            {
                // Enregistrement de l'objet en vu de son écriture dans la base de données
                $em->persist($entity);
            }
            else
            {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'avertissement !',
                        'message' => 'Ce matricule '.$contractuel["matricule"].' existe déjà'
                    )
                );
            }
        }

        $em->flush();

        return $this->redirect($this->generateUrl('contractuel_list'));

    }

    public function listcontractuelAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');


        $usersite = $this->getUser()->getSiteId();

        $userferme = $this->getUser()->getFermeId();

        $userdepartement = $this->getUser()->getDepartment();

        var_dump($usersite);
        var_dump($userferme);
        var_dump($userdepartement);

        $result = $em->getRepository('PointageBundle:Contractuel')->getContractuel($motcle, $page);
        $result_total = $em->getRepository('PointageBundle:Contractuel')->getAllMatricule($motcle);

        $rows = array();
        foreach($result as $rs)
        {
            if($userdepartement == 'PRODUCTION')
            {
                //FERME DIAMA 1
                if($userferme == 1)
                {
                    if ($rs['sERVICE'] == 'DIAM' || $rs['sERVICE'] == 'CEPR')
                    {
                        $rows[] = $rs;
                    }
                }
                //FERME SERRE
                if($userferme == 4)
                {
                    if ($rs['sERVICE'] == 'SERR')
                    {
                        $rows[] = $rs;
                    }
                }
                //FERME ALNGOURANE
                if($userferme == 62)
                {
                    if ($rs['sERVICE'] == 'DIDN')
                    {
                        $rows[] = $rs;
                    }
                }
                //FERME DIAMA 2
                if($userferme == 21)
                {
                    if ($rs['sERVICE'] == 'DJAM')
                    {
                        $rows[] = $rs;
                    }
                }
                //FERME AGROVAL
                if($userferme == 22)
                {
                    if ($rs['sERVICE'] == 'SVAG')
                    {
                        $rows[] = $rs;
                    }
                }
                //FERME SOCAS1 OU SOCAS2 OU SOCAS3
                if($userferme == 122 || $userferme == 123 || $userferme == 124)
                {
                    if ($rs['sERVICE'] == 'SVSO')
                    {
                        $rows[] = $rs;
                    }
                }

                //SITE NGALAM
                if($usersite == 2)
                {
                    if ($rs['sERVICE'] == 'NGAL' || $rs['sERVICE'] == 'SPNG')
                    {
                        $rows[] = $rs;
                    }
                }
                //SITE DIAMA
                if($usersite == 1)
                {
                    if ($rs['sERVICE'] == 'SPDI')
                    {
                        $rows[] = $rs;
                    }
                }
                //SITE SAVOIGNE
                if($userferme == 3)
                {
                    if ($rs['sERVICE'] == 'SVAN' || $rs['sERVICE'] == 'SPSV')
                    {
                        $rows[] = $rs;
                    }
                }
            }
            else
            {
                //SERVICE TECHNIQUE
                if($userdepartement == 'TECHNIQUE')
                {
                    if ($rs['dEPARTEMENT'] == 'TECH')
                    {
                        $rows[] = $rs;
                    }
                }
                //SERVICE COMPTABILITE SERVICE RH SERVICE CONTRÔLE DE GESTION SERVICE IT SERVICE ACHAT VENTE LOCALE
                if($userdepartement == 'RH')
                {
                    if ($rs['sERVICE'] == 'CECC' || $rs['sERVICE'] == 'CERH' || $rs['sERVICE'] == 'CECG' || $rs['sERVICE'] == 'CEIT' || $rs['sERVICE'] == 'CEAL' || $rs['sERVICE'] == 'COLO')
                    {
                        $rows[] = $rs;
                    }
                }
                //SERVICE MAGASIN CENTRALE
                if($userdepartement == 'MAGASIN CENTRALE')
                {
                    if ($rs['sERVICE'] == 'CEMG')
                    {
                        $rows[] = $rs;
                    }
                }
                //SERVICE QUALITE STATION
                if($userdepartement == 'QUALITE STATION')
                {
                    if ($rs['sERVICE'] == 'QUAL')
                    {
                        $rows[] = $rs;
                    }
                }
                //SERVICE AGRONOMIE
                if($userdepartement == 'AGRONOMIE')
                {
                    if ($rs['sERVICE'] == 'CEAG')
                    {
                        $rows[] = $rs;
                    }
                }
                //SERVICE QUALITE ET CERTIF
                if($userdepartement == 'SERVICE QUALITE ET CERTIFICATION')
                {
                    if ($rs['sERVICE'] == 'CEQU')
                    {
                        $rows[] = $rs;
                    }
                }
                //SERVICE STATTION NGALAM
                if($userdepartement == 'SATTION NGALAM')
                {
                    if ($rs['sERVICE'] == 'STNG')
                    {
                        $rows[] = $rs;
                    }
                }
                //SERVICE STATION DIAMA
                if($userdepartement == 'SATTION DIAMA')
                {
                    if ($rs['sERVICE'] == 'STDI')
                    {
                        $rows[] = $rs;
                    }
                }
            }
        }

        $count = count($result_total);


        //var_dump($rows); exit;

       /* if($user_siteid == null)
        {
            $result = $em->getRepository('PointageBundle:Contractuel')->getContractuel($motcle, $page);
            $result_total = $em->getRepository('PointageBundle:Contractuel')->getAllMatricule($motcle);
        }
        else
        {
            $site = $em->getRepository('CultureBundle:Site')->find($user_siteid);

            $result = $em->getRepository('PointageBundle:Contractuel')->getContractuelBySite($motcle,
                $page,$site->getLIBELLE());
            $result_total = $em->getRepository('PointageBundle:Contractuel')->getAllMatriculeBySite($motcle, $site->getLIBELLE());
        }*/

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $rows));
    }


}
