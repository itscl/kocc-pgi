<?php

namespace PointageBundle\Controller;

use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use PHPExcel_Calculation_Exception;
use PHPExcel_Cell;
use PHPExcel_Cell_DataType;
use PHPExcel_Shared_Date;
use PHPExcel_Style_NumberFormat;
use PointageBundle\Entity\FichePointage;
use PointageBundle\Entity\PointageLigne;
use PointageBundle\Form\FichePointageType;
use PointageBundle\Form\PointageLigneType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FichePointageController extends Controller
{

    public function pointageAction()
    {

        $em = $this->getDoctrine()->getManager();

        $filenames = '\\\\10.33.30.2\\SHARES\\PUBLIC\\POINTAGE_DES_CONTRACTUELS\\PAIE MARS.xlsx';
            //'C:\Users\Administrateur\Downloads\SCL  KOCC-PGI.xlsx';
            //'\\10.33.30.2\\SHARES\\PUBLIC\\POINTAGE_DES_CONTRACTUELS\\PAIE FEVRIER.xlsx';

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($filenames);

        //var_dump($phpExcelObject); exit;

        $sheet = $phpExcelObject->getSheetByName('Pointage');
        $highestRow = $sheet->getHighestRow();
        $highestColumn = 'BW';
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        for ($col = 9; $col < 51; ++ $col) {
            $datepointage = $sheet->getCellByColumnAndRow($col, 4)->getValue();
           // var_dump($sheet->getCellByColumnAndRow(74, 834)->getCalculatedValue()); exit;
            //if($datepointage != null)
            // {
            if($datepointage !=null) {
                $datepointage = PHPExcel_Shared_Date::ExcelToPHP($datepointage);
                $datepointage = date('d-m-Y', $datepointage);

                for ($row = 6; $row <= 885; $row++)
                {
                    //$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, TRUE);

                    if($sheet->getCellByColumnAndRow($col, $row)->getCalculatedValue() != null)
                    {
                        $entity = new PointageLigne();

                        $matricule = $sheet->getCellByColumnAndRow(0, $row)->getCalculatedValue();

                        $horaire = $sheet->getCellByColumnAndRow($col, $row)->getCalculatedValue();

                        $culture = $sheet->getCellByColumnAndRow($col+1, $row)->getCalculatedValue();

                        $geste = $sheet->getCellByColumnAndRow($col+2, $row)->getCalculatedValue();

                        $matriculedateexist = $em->getRepository('PointageBundle:PointageLigne')->findBy(array('mATRICULE' => $matricule, 'dATEPOINTAGE' => $datepointage));

                        if($matricule !='MTL' )
                        {
                            if($horaire != null)
                            {

                                if (!$matriculedateexist) {
                                    $entity->setMATRICULE($matricule);
                                    $entity->setDATEPOINTAGE($datepointage);
                                    $entity->setHORAIRE($horaire);
                                    $entity->setCULTURE($culture);
                                    $entity->setGESTE($geste);


                                        $em->persist($entity);

                                        $em->flush();

                                    echo $datepointage . ' ' . $matricule . ' ' . $horaire . ' ' . $culture . ' ' . $geste . '<br>';

                                    //exit;

                                } else
                                {
                                   foreach($matriculedateexist as $rs)
                                   {
                                       $id = $rs->getId();

                                   }

                                    $pointage = $em->getRepository('PointageBundle:PointageLigne')->find($id);

                                    if($horaire != $pointage->getHORAIRE())
                                    {
                                        $pointage->setHORAIRE($horaire);

                                        $em->flush();

                                        echo "Modifier Horaire ! ".  $datepointage . ' Ancien ' . $pointage->getHORAIRE(). '=> '.$horaire. '<br>';
                                    }
                                    elseif($geste != $pointage->getGESTE())
                                    {
                                        $pointage->setGESTE($geste);

                                        $em->flush();

                                        echo "Modifier geste ! ".  $datepointage . ' Ancien ' . $pointage->getGESTE(). '=> '.$geste. '<br>';
                                    }
                                    elseif($culture != $pointage->getCULTURE())
                                    {
                                        $pointage->setCULTURE($culture);

                                        $em->flush();

                                        echo "Modifier culture ! ".  $datepointage . ' Ancien ' . $pointage->getCULTURE(). '=> '.$culture. '<br>';
                                    }
                                    else
                                    {
                                        echo "Rien à modifier";
                                    }

                                    //echo "Pointage existant ! ".  $datepointage . ' ' . $matricule . ' ' . $horaire . ' ' . $culture . ' ' . $geste . '<br>';
                                }

                                //echo $datepointage . ' ' . $matricule . ' ' . $horaire . ' ' . $culture . ' ' . $geste . '<br>';
                            }

                        }

                    }

                }

            }


        }


        //exit;

        return $this->render('PointageBundle:FichePointage:pointage.html.twig');
    }

    public function listAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();


            $pointages = $em->getRepository('PointageBundle:PointageLigne')->getAllPointage();

           // var_dump($pointages); exit;


            return $this->render('PointageBundle:FichePointage:list.html.twig', array(
              'pointages' => $pointages
            ));
        }
    }

    public function updateficheAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $fichepointage = $em->getRepository('PointageBundle:FichePointage')->find($id);

            $edit_form = $this->createForm(new FichePointageType(), $fichepointage);

            $user = $this->getUser()->getName();

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {

                    $fichepointage->setDATEUPDATE(new \DateTime());
                    $fichepointage->setUSERUPDATE($user);



                    $em->flush();

                    return $this->redirect($this->generateUrl('fichepointage_list'));
                }
            }

            $fichespointages = $em->getRepository('PointageBundle:FichePointage')->findAll();

            return $this->render('PointageBundle:FichePointage:update.html.twig', array(
                'edit_form' => $edit_form->createView(),
                'fichepointage' => $fichepointage,
                'fichespointages' => $fichespointages
            ));
        }
    }

    public function deleteficheAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $id = $request->query->get('id');

            $em = $this->getDoctrine()->getManager();

            $fichepointage = $em->getRepository('PointageBundle:FichePointage')->find($id);

            $pointageligne = $em->getRepository('PointageBundle:PointageLigne')->findByfICHEPOINTAGEID($id);

            if($pointageligne)
            {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'warning',
                        'title' => 'avertissement !',
                        'message' => 'Impossible de supprimer cette fiche car elle contient des enregistrements'
                    )
                );
            }
            else
            {
                $em->remove($fichepointage);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    array(
                        'alert' => 'success',
                        'title' => 'succès  !',
                        'message' => 'Fiche de pointage supprimer'
                    )
                );
            }

                return $this->redirect($this->generateUrl('fichepointage_list'));
        }
    }

    public function createpointageAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $user = $this->getUser()->getName();

            $user_site = $this->getUser()->getSiteId();

            $entity = new PointageLigne();

            $form = $this->createForm(new PointageLigneType(), $entity);



            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                //var_dump($form->getData()); exit;

                if($form->isValid())
                {
                    $fichepointage = $em->getRepository('PointageBundle:FichePointage')->find($id);

                    //$getDateExistFiche = $em->getRepository('PointageBundle:FichePointage')->getDateExistFiche($id);

                    //var_dump($form->getData()); exit;

                    $entity->setFICHEPOINTAGEID($id);
                    $entity->setDATEADD(new \DateTime());
                    $entity->setUSERADD($user);

                    if($form->get('aBSENT')->getData() == true)
                    {
                        $entity->setABSENT('A');
                    }

                    if($form->get('fERIE')->getData() == true)
                    {
                        $entity->setfERIE(1);
                    }
                    else
                        $entity->setfERIE(0);

                    try
                    {
                        if($entity->getOPID() == null && $entity->getCULTUREID() == null)
                        {
                            $this->get('session')->getFlashBag()->add(
                                'notice',
                                array(
                                    'alert' => 'danger',
                                    'title' => 'Erreur!',
                                    'message' => 'Veuillez renseigner le champ "OP" ou "CULTURE" '
                                )
                            );
                        }
                        else
                        {
                            $pointageexist = $em->getRepository('PointageBundle:PointageLigne')->findBy(array
                            ('cONTRACTUELID' => $entity->getCONTRACTUELID(), 'jOUR' => $entity->getJOUR()));

                            if($pointageexist)
                            {
                                $this->get('session')->getFlashBag()->add(
                                    'notice',
                                    array(
                                        'alert' => 'warning',
                                        'title' => 'avertissement !',
                                        'message' => 'Un pointage existe déjà pour ce matricule à ce jour'
                                    )
                                );
                            }
                            else
                            {
                                $em->persist($entity);

                                //var_dump($entity); exit;

                                if($entity->getJOUR() < $fichepointage->getDATEDEBUT() || $entity->getJOUR() >
                                    $fichepointage->getDATEFIN())
                                {
                                    $this->get('session')->getFlashBag()->add(
                                        'notice',
                                        array(
                                            'alert' => 'warning',
                                            'title' => 'avertissement !',
                                            'message' => 'Veuillez choisir une date entre '
                                        .$fichepointage->getDATEDEBUT()->format('d-m-Y').' et '.$fichepointage->getDATEFIN()->format('d-m-Y')
                                        )
                                    );
                                }
                                else
                                {
                                    $em->flush();
                                }
                            }
                        }
                    }
                    catch(NotNullConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add(
                            'notice',
                            array(
                                'alert' => 'danger',
                                'title' => 'Erreur!',
                                'message' => 'Veuillez renseigner les champs "MATRICULE" et "LIEU"'
                            )
                        );
                    }
                }
            }

            $pointages = $em->getRepository('PointageBundle:PointageLigne')->getPointageByFiche($id);
           // $total = $em->getRepository('PointageBundle:PointageLigne')->getTotalPointageByContr();

            //var_dump($pointages); exit;

            $rows = array();
            foreach($pointages as $pointage)
            {

                $heured = $pointage['hEURED'];
                $heuref = $pointage['hEUREFIN'];

                if($heured != null && $heuref != null)
                {
                    $heure_tra =  $heured->diff($heuref);

                    $heure_tra = $heure_tra->h;

                    $heured = $heured->format('H:i');
                    $heuref = $heuref->format('H:i');

                    $row['HEURED'] = $heured;
                    $row['HEUREFIN'] = $heuref;
                    $row['HEURE_TRA'] = $heure_tra;

                }
                else
                {
                    $row['HEURED'] = '';
                    $row['HEUREFIN'] = '';
                    $row['HEURE_TRA'] = '';
                }


                $jour = $pointage['jOUR'];

                // = $jour->format('U');



                $row['pointageligneid'] = $pointage['pointageligneid'];
                $row['cONTRACTUELID'] = $pointage['cONTRACTUELID'];
                $row['mATRICULE'] = $pointage['mATRICULE'];
                $row['pRENOM'] = $pointage['pRENOM'];
                $row['nOM'] = $pointage['nOM'];
                $row['jOUR'] = $pointage['jOUR'];
                $row['jOURCOMPLET'] = $pointage['jourcomplet'];
                $row['DAY'] = $pointage['day'];
                $row['cULTUREID'] = $pointage['cULTUREID'];
                $row['CULTURE'] = $pointage['CULTURE'];
                $row['fERMEID'] = $pointage['fERMEID'];
                $row['FERME'] = $pointage['FERME'];
                $row['OP'] = $pointage['OP'];
                $row['gESTEID'] = $pointage['gESTEID'];
                $row['uSERADD'] = $pointage['uSERADD'];
                $row['dATEADD'] = $pointage['dATEADD'];
                $row['dATEADD'] = $pointage['dATEADD'];
                $row['aBSENT'] = $pointage['aBSENT'];


                array_push($rows, $row);
            }

            $data = array();
            foreach($rows as $row) {
                if(array_key_exists( $row['cONTRACTUELID'], $data)) {
                    $data[$row['cONTRACTUELID']]['HEURE_TRA'] += $row['HEURE_TRA'];
                } else {
                    $data[$row['cONTRACTUELID']] = $row;
                }
            }
            //var_dump($data); exit;
            return $this->render('PointageBundle:FichePointage:create.html.twig', array(
                'form' => $form->createView(),
                'entity' => $entity,
                'pointages' => $rows,
                'totals' => $data,
                'id' => $id,
                'user_site' => $user_site
            ));
        }
    }

    public function exportpdfAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $user_siteid = $this->getUser()->getSiteId();

            $site = $em->getRepository('CultureBundle:Site')->find($user_siteid);

            $fichepointage = $em->getRepository('PointageBundle:FichePointage')->find($id);
            $pointageslignes = $em->getRepository('PointageBundle:PointageLigne')->getPointageByFiche($id);
            $getJourDistinct = $em->getRepository('PointageBundle:PointageLigne')->getJourDistinct($id);

            $rows = array();
            foreach($pointageslignes as $pointage)
            {
                $heured = $pointage['hEURED'];
                $heuref = $pointage['hEUREFIN'];

                $heure_tra =  $heured->diff($heuref);

                $heure_tra = $heure_tra->h;

                $heured = $heured->format('H:i');
                $heuref = $heuref->format('H:i');

                $row['DAY'] = trim($pointage['day'], " ");
                $row['HEURED'] = $heured;
                $row['HEUREFIN'] = $heuref;
                $row['HEURE_TRA'] = $heure_tra;
                $row['pointageligneid'] = $pointage['pointageligneid'];
                $row['cONTRACTUELID'] = $pointage['cONTRACTUELID'];
                $row['mATRICULE'] = $pointage['mATRICULE'];
                $row['pRENOM'] = $pointage['pRENOM'];
                $row['nOM'] = $pointage['nOM'];
                $row['jOUR'] = $pointage['jOUR'];
                $row['cULTUREID'] = $pointage['cULTUREID'];
                $row['CULTURE'] = $pointage['CULTURE'];
                $row['fERMEID'] = $pointage['fERMEID'];
                $row['FERME'] = $pointage['FERME'];
                $row['OP'] = $pointage['OP'];
                $row['gESTEID'] = $pointage['gESTEID'];
                $row['uSERADD'] = $pointage['uSERADD'];
                $row['dATEADD'] = $pointage['dATEADD'];
                $row['dATEADD'] = $pointage['dATEADD'];


                array_push($rows, $row);
            }
            //var_dump($rows); exit;
            $data = array();
            foreach($rows as $row) {
                if(array_key_exists( $row['cONTRACTUELID'], $data)) {
                    $data[$row['cONTRACTUELID']]['HEURE_TRA'] += $row['HEURE_TRA'];
                } else {
                    $data[$row['cONTRACTUELID']] = $row;
                }
            }


            //on stocke la vue � convertir en PDF, en n'oubliant pas les param�tres twig si la vue comporte des donn�es dynamiques
            $html = $this->renderView('PointageBundle:FichePointage:exportpdf.html.twig', array('pointageslignes' =>
                $rows, 'arrayj' => $getJourDistinct, 'totals' => $data, 'site' => $site, 'fichepointage' => $fichepointage));
            //on appelle le service html2pdf
            $html2pdf = $this->get('html2pdf_factory')->create('P', 'A4', 'fr', true, 'UTF-8', array(15, 10, 15, 10));
            //real : utilise la taille r�elle
            $html2pdf->pdf->SetDisplayMode('real');
            //writeHTML va tout simplement prendre la vue stocker dans la variable $html pour la convertir en format PDF
            $html2pdf->writeHTML($html);
            //Output envoit le document PDF au navigateur internet

            return new Response($html2pdf->Output('Pointage.pdf'), 200, array('Content-Type' => 'application/pdf'));
        }
    }

    public function getsgesteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');

        $result_total = $em->getRepository('TechniqueBundle:SGeste')->getAllSGeste($motcle);

        $result = $em->getRepository('TechniqueBundle:SGeste')->getSGestePage($motcle, $page);

       // var_dump($result);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }
}
