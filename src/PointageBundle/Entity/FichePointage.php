<?php

namespace PointageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FichePointage
 */
class FichePointage
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $dESCRIPTION;

    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var string
     */
    private $uSERCREATE;

    /**
     * @var \DateTime
     */
    private $dATEDEBUT;

    /**
     * @var \DateTime
     */
    private $dATEFIN;

    /**
     * @var \DateTime
     */
    private $dATEUPDATE;

    /**
     * @var string
     */
    private $uSERUPDATE;

    /**
     * @var int
     */
    private $sITEID;

    /**
     * @var int
     */
    private $fERMEID;

    /**
     * @var string
     */
    private $dEPARTEMENT;

    /**
     * @var int
     */
    private $sERVICE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dESCRIPTION
     *
     * @param string $dESCRIPTION
     * @return FichePointage
     */
    public function setDESCRIPTION($dESCRIPTION)
    {
        $this->dESCRIPTION = $dESCRIPTION;

        return $this;
    }

    /**
     * Get dESCRIPTION
     *
     * @return string 
     */
    public function getDESCRIPTION()
    {
        return $this->dESCRIPTION;
    }

    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return FichePointage
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime 
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set uSERCREATE
     *
     * @param string $uSERCREATE
     * @return FichePointage
     */
    public function setUSERCREATE($uSERCREATE)
    {
        $this->uSERCREATE = $uSERCREATE;

        return $this;
    }

    /**
     * Get uSERCREATE
     *
     * @return string 
     */
    public function getUSERCREATE()
    {
        return $this->uSERCREATE;
    }

    /**
     * Get dATEDEBUT
     *
     * @return \DateTime
     */
    public function getDATEDEBUT()
    {
        return $this->dATEDEBUT;
    }

    /**
     * Set dATEDEBUT
     *
     * @param \DateTime $dATEDEBUT
     * @return FichePointage
     */
    public function setDATEDEBUT($dATEDEBUT)
    {
        $this->dATEDEBUT = $dATEDEBUT;
    }

    /**
     * Get dATEFIN
     *
     * @return \DateTime
     */
    public function getDATEFIN()
    {
        return $this->dATEFIN;
    }

    /**
     * Set dATEFIN
     *
     * @param \DateTime $dATEFIN
     * @return FichePointage
     */
    public function setDATEFIN($dATEFIN)
    {
        $this->dATEFIN = $dATEFIN;
    }

    /**
     * Get dATEUPDATE
     *
     * @return \DateTime
     */
    public function getDATEUPDATE()
    {
        return $this->dATEUPDATE;
    }

    /**
     * Set dATEUPDATE
     *
     * @param \DateTime $dATEUPDATE
     * @return FichePointage
     */
    public function setDATEUPDATE($dATEUPDATE)
    {
        $this->dATEUPDATE = $dATEUPDATE;
    }

    /**
     * Get uSERUPDATE
     *
     * @return string
     */
    public function getUSERUPDATE()
    {
        return $this->uSERUPDATE;
    }

    /**
     * Set uSERUPDATE
     *
     * @param string $uSERUPDATE
     * @return FichePointage
     */
    public function setUSERUPDATE($uSERUPDATE)
    {
        $this->uSERUPDATE = $uSERUPDATE;
    }

    /**
     * Get sITEID
     *
     * @return int
     */
    public function getSITEID()
    {
        return $this->sITEID;
    }

    /**
     * Set sITEID
     *
     * @param string $sITEID
     * @return FichePointage
     */
    public function setSITEID($sITEID)
    {
        $this->sITEID = $sITEID;
    }

    /**
     * Get fERMEID
     *
     * @return int
     */
    public function getFERMEID()
    {
        return $this->fERMEID;
    }

    /**
     * Set fERMEID
     *
     * @param string $fERMEID
     * @return FichePointage
     */
    public function setFERMEID($fERMEID)
    {
        $this->fERMEID = $fERMEID;
    }

    /**
     * Get dEPARTEMENT
     *
     * @return string
     */
    public function getDEPARTEMENT()
    {
        return $this->dEPARTEMENT;
    }

    /**
     * Set dEPARTEMENT
     *
     * @param string $dEPARTEMENT
     * @return FichePointage
     */
    public function setDEPARTEMENT($dEPARTEMENT)
    {
        $this->dEPARTEMENT = $dEPARTEMENT;
    }

    /**
     * Get sERVICE
     *
     * @return int
     */
    public function getSERVICE()
    {
        return $this->sERVICE;
    }

    /**
     * Set sERVICE
     *
     * @param string $sERVICE
     * @return FichePointage
     */
    public function setSERVICE($sERVICE)
    {
        $this->sERVICE = $sERVICE;
    }
}
