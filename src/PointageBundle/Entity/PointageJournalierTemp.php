<?php

namespace PointageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PointageJournalierTemp
 */
class PointageJournalierTemp
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dATEPOINTAGE;

    /**
     * @var int
     */
    private $eFFECTIF;

    /**
     * @var string
     */
    private $fERME;

    /**
     * @var string
     */
    private $sITE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dATEPOINTAGE
     *
     * @param \DateTime $dATEPOINTAGE
     * @return PointageJournalierTemp
     */
    public function setDATEPOINTAGE($dATEPOINTAGE)
    {
        $this->dATEPOINTAGE = $dATEPOINTAGE;

        return $this;
    }

    /**
     * Get dATEPOINTAGE
     *
     * @return \DateTime 
     */
    public function getDATEPOINTAGE()
    {
        return $this->dATEPOINTAGE;
    }

    /**
     * Set eFFECTIF
     *
     * @param integer $eFFECTIF
     * @return PointageJournalierTemp
     */
    public function setEFFECTIF($eFFECTIF)
    {
        $this->eFFECTIF = $eFFECTIF;

        return $this;
    }

    /**
     * Get eFFECTIF
     *
     * @return integer 
     */
    public function getEFFECTIF()
    {
        return $this->eFFECTIF;
    }

    /**
     * Set fERME
     *
     * @param string $fERME
     * @return PointageJournalierTemp
     */
    public function setFERME($fERME)
    {
        $this->fERME = $fERME;

        return $this;
    }

    /**
     * Get fERME
     *
     * @return string 
     */
    public function getFERME()
    {
        return $this->fERME;
    }

    /**
     * Set sITE
     *
     * @param string $sITE
     * @return PointageJournalierTemp
     */
    public function setSITE($sITE)
    {
        $this->sITE = $sITE;

        return $this;
    }

    /**
     * Get sITE
     *
     * @return string
     */
    public function getSITE()
    {
        return $this->sITE;
    }
}
