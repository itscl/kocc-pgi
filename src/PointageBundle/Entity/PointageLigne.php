<?php

namespace PointageBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;

/**
 * PointageLigne
 */
class PointageLigne
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $mATRICULE;

    /**
     * @var \DateTime
     */
    private $dATEPOINTAGE;

    /**
     * @var string
     */
    private $hORAIRE;

    /**
     * @var string
     */
    private $cULTURE;

    /**
     * @var string
     */
    private $gESTE;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mATRICULE
     *
     * @param string $mATRICULE
     * @return PointageLigne
     */
    public function setMATRICULE($mATRICULE)
    {
        $this->mATRICULE = $mATRICULE;

        return $this;
    }

    /**
     * Get mATRICULE
     *
     * @return string
     */
    public function getMATRICULE()
    {
        return $this->mATRICULE;
    }

    /**
     * Set dATEPOINTAGE
     *
     * @param \DateTime $dATEPOINTAGE
     * @return PointageLigne
     */
    public function setDATEPOINTAGE($dATEPOINTAGE)
    {
        $this->dATEPOINTAGE = $dATEPOINTAGE;

        return $this;
    }

    /**
     * Get dATEPOINTAGE
     *
     * @return \DateTime
     */
    public function getDATEPOINTAGE()
    {
        return $this->dATEPOINTAGE;
    }

    /**
     * Set hORAIRE
     *
     * @param string $hORAIRE
     * @return PointageLigne
     */
    public function setHORAIRE($hORAIRE)
    {
        $this->hORAIRE = $hORAIRE;

        return $this;
    }

    /**
     * Get hORAIRE
     *
     * @return string
     */
    public function getHORAIRE()
    {
        return $this->hORAIRE;
    }

    /**
     * Set cULTURE
     *
     * @param string $cULTURE
     * @return PointageLigne
     */
    public function setCULTURE($cULTURE)
    {
        $this->cULTURE = $cULTURE;

        return $this;
    }

    /**
     * Get cULTURE
     *
     * @return string
     */
    public function getCULTURE()
    {
        return $this->cULTURE;
    }

    /**
     * Set gESTE
     *
     * @param string $gESTE
     * @return PointageLigne
     */
    public function setGESTE($gESTE)
    {
        $this->gESTE = $gESTE;

        return $this;
    }

    /**
     * Get gESTE
     *
     * @return string
     */
    public function getGESTE()
    {
        return $this->gESTE;
    }
}
