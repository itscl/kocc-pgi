<?php

namespace PointageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contractuel
 */
class Contractuel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $mATRICULE;

    /**
     * @var string
     */
    private $nOM;

    /**
     * @var string
     */
    private $pRENOM;

    /**
     * @var string
     */
    private $dIRECTION;

    /**
     * @var string
     */
    private $sERVICE;

    /**
     * @var string
     */
    private $pOSTE;

    /**
     * @var string
     */
    private $aFFECTATION;

    /**
     * @var string
     */
    private $sITE;

    /**
     * @var string
     */
    private $eMPLOI;

    /**
     * @var string
     */
    private $tYPECONTRAT;

    /**
     * @var string
     */
    private $cLE1;

    /**
     * @var string
     */
    private $cLE2;

    /**
     * @var integer
     */
    private $cULTUREID;

    /**
     * @var string
     */
    private $cODEGESTE;

    /**
     * @var string
     */
    private $pOINTEUR;

    /**
     * @var string
     */
    private $rESPRECR;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mATRICULE
     *
     * @param string $mATRICULE
     * @return Contractuel
     */
    public function setMATRICULE($mATRICULE)
    {
        $this->mATRICULE = $mATRICULE;

        return $this;
    }

    /**
     * Get mATRICULE
     *
     * @return string
     */
    public function getMATRICULE()
    {
        return $this->mATRICULE;
    }

    /**
     * Set nOM
     *
     * @param string $nOM
     * @return Contractuel
     */
    public function setNOM($nOM)
    {
        $this->nOM = $nOM;

        return $this;
    }

    /**
     * Get nOM
     *
     * @return string
     */
    public function getNOM()
    {
        return $this->nOM;
    }

    /**
     * Set pRENOM
     *
     * @param string $pRENOM
     * @return Contractuel
     */
    public function setPRENOM($pRENOM)
    {
        $this->pRENOM = $pRENOM;

        return $this;
    }

    /**
     * Get pRENOM
     *
     * @return string
     */
    public function getPRENOM()
    {
        return $this->pRENOM;
    }

    /**
     * Set dIRECTION
     *
     * @param string $dIRECTION
     * @return Contractuel
     */
    public function setDIRECTION($dIRECTION)
    {
        $this->dIRECTION = $dIRECTION;

        return $this;
    }

    /**
     * Get dIRECTION
     *
     * @return string
     */
    public function getDIRECTION()
    {
        return $this->dIRECTION;
    }

    /**
     * Set sERVICE
     *
     * @param string $sERVICE
     * @return Contractuel
     */
    public function setSERVICE($sERVICE)
    {
        $this->sERVICE = $sERVICE;

        return $this;
    }

    /**
     * Get sERVICE
     *
     * @return string
     */
    public function getSERVICE()
    {
        return $this->sERVICE;
    }

    /**
     * Set pOSTE
     *
     * @param string $pOSTE
     * @return Contractuel
     */
    public function setPOSTE($pOSTE)
    {
        $this->pOSTE = $pOSTE;

        return $this;
    }

    /**
     * Get pOSTE
     *
     * @return string
     */
    public function getPOSTE()
    {
        return $this->pOSTE;
    }

    /**
     * Set aFFECTATION
     *
     * @param string $aFFECTATION
     * @return Contractuel
     */
    public function setAFFECTATION($aFFECTATION)
    {
        $this->aFFECTATION = $aFFECTATION;

        return $this;
    }

    /**
     * Get aFFECTATION
     *
     * @return string
     */
    public function getAFFECTATION()
    {
        return $this->aFFECTATION;
    }

    /**
     * Set sITE
     *
     * @param string $sITE
     * @return Contractuel
     */
    public function setSITE($sITE)
    {
        $this->sITE = $sITE;

        return $this;
    }

    /**
     * Get sITE
     *
     * @return string
     */
    public function getSITE()
    {
        return $this->sITE;
    }

    /**
     * Set eMPLOI
     *
     * @param string $eMPLOI
     * @return Contractuel
     */
    public function setEMPLOI($eMPLOI)
    {
        $this->eMPLOI = $eMPLOI;

        return $this;
    }

    /**
     * Get eMPLOI
     *
     * @return string
     */
    public function getEMPLOI()
    {
        return $this->eMPLOI;
    }

    /**
     * Set tYPECONTRAT
     *
     * @param string $tYPECONTRAT
     * @return Contractuel
     */
    public function setTYPECONTRAT($tYPECONTRAT)
    {
        $this->tYPECONTRAT = $tYPECONTRAT;

        return $this;
    }

    /**
     * Get tYPECONTRAT
     *
     * @return string
     */
    public function getTYPECONTRAT()
    {
        return $this->tYPECONTRAT;
    }

    /**
     * Set cLE1
     *
     * @param string $cLE1
     * @return Contractuel
     */
    public function setCLE1($cLE1)
    {
        $this->cLE1 = $cLE1;

        return $this;
    }

    /**
     * Get cLE1
     *
     * @return string
     */
    public function getCLE1()
    {
        return $this->cLE1;
    }

    /**
     * Set cLE2
     *
     * @param string $cLE2
     * @return Contractuel
     */
    public function setCLE2($cLE2)
    {
        $this->cLE2 = $cLE2;

        return $this;
    }

    /**
     * Get cLE2
     *
     * @return string
     */
    public function getCLE2()
    {
        return $this->cLE2;
    }

    /**
     * Set cULTUREID
     *
     * @param string $cULTUREID
     * @return Contractuel
     */
    public function setCULTUREID($cULTUREID)
    {
        $this->cULTUREID = $cULTUREID;

        return $this;
    }

    /**
     * Get cULTUREID
     *
     * @return integer
     */
    public function getCULTUREID()
    {
        return $this->cULTUREID;
    }

    /**
     * Set cODEGESTE
     *
     * @param integer $cODEGESTE
     * @return Contractuel
     */
    public function setCODEGESTE($cODEGESTE)
    {
        $this->cODEGESTE = $cODEGESTE;

        return $this;
    }

    /**
     * Get cODEGESTE
     *
     * @return string
     */
    public function getCODEGESTE()
    {
        return $this->cODEGESTE;
    }

    /**
     * Set pOINTEUR
     *
     * @param string $pOINTEUR
     * @return Contractuel
     */
    public function setPOINTEUR($pOINTEUR)
    {
        $this->pOINTEUR = $pOINTEUR;

        return $this;
    }

    /**
     * Get pOINTEUR
     *
     * @return string
     */
    public function getPOINTEUR()
    {
        return $this->pOINTEUR;
    }

    /**
     * Set rESPRECR
     *
     * @param string $rESPRECR
     * @return Contractuel
     */
    public function setRESPRECR($rESPRECR)
    {
        $this->rESPRECR = $rESPRECR;

        return $this;
    }

    /**
     * Get rESPRECRID
     *
     * @return string
     */
    public function getRESPRECR()
    {
        return $this->rESPRECR;
    }
}
