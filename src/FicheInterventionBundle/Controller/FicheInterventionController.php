<?php

namespace FicheInterventionBundle\Controller;

use DoctrineExtensions\Query\Sqlite\Date;
use FicheInterventionBundle\Entity\Tache;
use FicheInterventionBundle\Entity\TacheArticle;
use FicheInterventionBundle\Entity\TacheArticleR;
use FicheInterventionBundle\Form\FicheInterventionType;
use FicheInterventionBundle\Form\TacheType;
use Swift_Attachment;
use Swift_TransportException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FicheInterventionBundle\Entity\FicheIntervention;

use \Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use \Doctrine\DBAL\Exception\DriverException;
use \Doctrine\DBAL\Exception\OCI8Exception;
use FicheInterventionBundle\Entity\DocFicheIntervention;
use FicheInterventionBundle\Form\DocFicheInterventionType;
use UserBundle\Entity\Service;

class FicheInterventionController extends Controller
{
    public function listeficheinterventionAction()
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            if (!$response->isNotModified($this->getRequest()))
            {
                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();

                $user = $this->getUser();

                $userid = $user->getId();
                $username = $user->getName();

                $usersiteid = $user->getSiteid();

                $department = $user->getDepartment();

                $dn= $user->getDn();
                $manager = $user->getManager();
                $userManager = $this->get('fos_user.user_manager');

                if($dn == $manager && $dn != null)
                {
                    $user->addRole("ROLE_CHEFSERVICE");
                    $userManager->updateUser($user);
                }

                $role = $this->getUser()->getRoles();

                $fichesAttribuees = [];
                if (in_array('ROLE_TECHNIQUE', $role, true)) {
                    if($userid == 101)
                    {
                        $fichesAttribuees = $em->getRepository('FicheInterventionBundle:FicheIntervention')
                            ->getAllFicheByName("Moustapha KA");
                    }
                    else if($userid == 161)
                    {
                        $fichesAttribuees = $em->getRepository('FicheInterventionBundle:FicheIntervention')
                            ->getAllFicheByName("Hamedine BA");
                    }
                    else if($userid == 221)
                    {
                        $fichesAttribuees = $em->getRepository('FicheInterventionBundle:FicheIntervention')
                            ->getAllFicheIntervention();
                    }
                }

                for($i=0; $i<sizeof($fichesAttribuees); $i++)
                {
                    $name=$fichesAttribuees[$i]['eDITEURID'];
                    $dep=$em->getRepository('UserBundle:Utilisateurldap')->findByName($name)[0]->getDepartment();
                    $fichesAttribuees[$i]['department']=$dep;
                    if ($fichesAttribuees[$i]['sTATUT'] == 'EN COURS') {
                        $ficheInterventionid = $fichesAttribuees[$i]['id'];

                        $taches = $em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID
                        ($ficheInterventionid);
                        if (count($taches) == 0) {
                            $sql = "DELETE FROM FICHEINTERVENTION WHERE FICHEINTERVENTIONID = $ficheInterventionid";
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();
                        }
                    }
                }

                $nbJours = 3;
                $mesFichesInterventions = $em->getRepository('FicheInterventionBundle:FicheIntervention')
                    ->getAllFicheByEditeur($username);

                for($i=0; $i<sizeof($mesFichesInterventions); $i++)
                {
                    $name=$mesFichesInterventions[$i]['eDITEURID'];
                    $dep=$em->getRepository('UserBundle:Utilisateurldap')->findByName($name)[0]->getDepartment();
                    $mesFichesInterventions[$i]['department']=$dep;

                    if ($mesFichesInterventions[$i]['sTATUT'] == 'EN COURS') {
                        $ficheInterventionid = $mesFichesInterventions[$i]['id'];

                        $taches = $em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID
                        ($ficheInterventionid);
                        if (count($taches) == 0) {
                            $sql = "DELETE FROM FICHEINTERVENTION WHERE FICHEINTERVENTIONID = $ficheInterventionid";
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();
                        }
                    }
                    if ($mesFichesInterventions[$i]['sTATUT'] == 'CLOTURE' && $mesFichesInterventions[$i]['REACTIVEPAR'] == null) {

                        $daterea = $mesFichesInterventions[$i]['DATEREALISATION'];

                        if ($daterea != null) {
                            $date = new \DateTime();
                            $datenow = $date->format('d-m-Y');
                            $datere = $daterea->format('d-m-Y');
                            $date1 = strtotime($datere);
                            $date2 = strtotime($datenow);
                            $nbJoursTimestamp = $date2 - $date1;
                            $nbJours = $nbJoursTimestamp / 86400;
                            $mesFichesInterventions[$i]['nbJours']=$nbJours;
                        }
                    }
                }

                $ficheInterventionsServices = $em->getRepository('FicheInterventionBundle:FicheIntervention')
                    ->getAllFicheByService($department);

                for($i=0; $i<sizeof($ficheInterventionsServices); $i++)
                {
                    $name=$ficheInterventionsServices[$i]['eDITEURID'];
                    $dep=$em->getRepository('UserBundle:Utilisateurldap')->findByName($name)[0]->getDepartment();
                    $ficheInterventionsServices[$i]['department']=$dep;
                    if ($ficheInterventionsServices[$i]['sTATUT'] == 'EN COURS') {
                        $ficheInterventionid = $ficheInterventionsServices[$i]['id'];
                        $taches = $em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID
                        ($ficheInterventionid);
                        if (count($taches) == 0) {
                            $sql = "DELETE FROM FICHEINTERVENTION WHERE FICHEINTERVENTIONID = $ficheInterventionid";
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();
                        }
                    }
                }

                $ficheInterventionsSite = $em->getRepository('FicheInterventionBundle:FicheIntervention')
                    ->getAllFicheBySite($usersiteid);

                for($i=0; $i<sizeof($ficheInterventionsSite); $i++)
                {
                    $name=$ficheInterventionsSite[$i]['eDITEURID'];
                    $dep=$em->getRepository('UserBundle:Utilisateurldap')->findByName($name)[0]->getDepartment();
                    $ficheInterventionsSite[$i]['department']=$dep;
                    if ($ficheInterventionsSite[$i]['sTATUT'] == 'EN COURS') {
                        $ficheInterventionid = $ficheInterventionsSite[$i]['id'];
                        $taches = $em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID
                        ($ficheInterventionid);
                        if (count($taches) == 0) {
                            $sql = "DELETE FROM FICHEINTERVENTION WHERE FICHEINTERVENTIONID = $ficheInterventionid";
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();
                        }
                    }
                }

                $ficheInterventions = $em->getRepository('FicheInterventionBundle:FicheIntervention')
                    ->getAllFicheIntervention();
                for($i=0; $i<sizeof($ficheInterventions); $i++)
                {
                    $name=$ficheInterventions[$i]['eDITEURID'];
                    $dep=$em->getRepository('UserBundle:Utilisateurldap')->findByName($name)[0]->getDepartment();
                    $ficheInterventions[$i]['department']=$dep;
                    if ($ficheInterventions[$i]['sTATUT'] == 'EN COURS') {
                        $ficheInterventionid = $ficheInterventions[$i]['id'];
                        $taches = $em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID
                        ($ficheInterventionid);
                        if (count($taches) == 0) {
                            $sql = "DELETE FROM FICHEINTERVENTION WHERE FICHEINTERVENTIONID = $ficheInterventionid";
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();
                        }
                    }
                }

                return $this->render('FicheInterventionBundle:FicheIntervention:ficheintervention.html.twig', array(
                    'mesFichesInterventions' => $mesFichesInterventions,
                    'ficheInterventionsServices' => $ficheInterventionsServices,
                    'ficheInterventions' => $ficheInterventions,
                    'fichesAttribuees' => $fichesAttribuees,
                    'ficheInterventionsSite' => $ficheInterventionsSite,
                    'department' => $department
                ));
            }
        }
    }

    public function ajouterficheinterventionAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {

            $username = $this->getUser()->getName();
            $date = new \DateTime();
            $em = $this->getDoctrine()->getManager();

            $ficheIntervention = new FicheIntervention();
            $form = $this->createForm(new FicheInterventionType($em), $ficheIntervention);
            $form->remove('sTATUT');
            $form->remove('iDENTIFICATION');
            $form->remove('pRIORISATION');
            $form->remove('oBSERVATIONS');

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    //var_dump($form->getData()); exit;
                    try
                    {
                        $ficheIntervention->setEDITEURID($username);
                        $ficheIntervention->setPROGRESSION(0);
                        $ficheIntervention->setSTATUT("EN CREATION");
                        $ficheIntervention->setDATECREATION($date);

                        $em->persist($ficheIntervention);
                        $em->flush();
                        $id = $ficheIntervention->getId();

//                        return $this->redirect($this->generateUrl('ajoutertaches', array('id' => $id)));
                        return $this->redirect($this->generateUrl('modifierficheintervention', array('id' => $id)));
                    }
                    catch(NotNullConstraintViolationException $e)
                    {
                        $form=$this->createForm(new FicheInterventionType($em), $ficheIntervention);
                        $form->remove('sTATUT');
                        $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs !!");
                    }
                }
            }
        }
        return $this->render('FicheInterventionBundle:FicheIntervention:ajouterficheintervention.html.twig' , array(
            'entity'=>$ficheIntervention,
            'form'=>$form->createView(),
            'username' => $username,
            'date' => $date
        ));
    }

    public function ajoutertachesAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();
            $id = $request->query->get('id');
            $ficheIntervention=$em->getRepository('FicheInterventionBundle:FicheIntervention')->getFicheById($id)[0];

            $tache = new Tache();
            $edit_form = $this->createForm(new TacheType($em), $tache);


            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    try
                    {
                        $tache->setFICHEINTERVENTIONID($ficheIntervention['id']);
                        $tache->setREALISE('false');

                        $em->persist($tache);
                        $em->flush();

                    }
                    catch(NotNullConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('noticetache', "Veuillez renseigner tous les champs !!");
                    }

                    return $this->redirect($this->generateUrl('ajoutertaches', array('id' =>
                        $ficheIntervention['id'])));
                }
            }
        }

        $taches =$em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID
        ($ficheIntervention['id']);

        return $this->render('FicheInterventionBundle:FicheIntervention:ajoutertachesFI.html.twig' , array(
            'entity'=>$tache,
            'edit_form'=>$edit_form->createView(),
            'ficheIntervention'=>$ficheIntervention,
            'taches' => $taches
        ));
    }

    public function supprimerficheinterventionAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->request->get('idfiche');
            $ficheIntervention = $em->getRepository('FicheInterventionBundle:FicheIntervention')->find($id);

            try
            {
                $em->remove($ficheIntervention);
                $em->flush();

            }
            catch(\Doctrine\ORM\ORMInvalidArgumentException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Cette donnée n'existe plus dans la base");
            }
        }

        $ficheInterventions = $em->getRepository('FicheInterventionBundle:FicheIntervention')
            ->getAllFicheIntervention();
//        $this->redirect($this->generateUrl('listeficheintervention' ));

        return $this->render('FicheInterventionBundle:FicheIntervention:index.html.twig');
    }

    public function addtacheAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $ficheinterventionid=(int)$request->request->get('ficheinterventionid');
            $libelle=$request->request->get('libelle');

            $tache = new Tache();
            $tache->setLIBELLE($libelle);
            $tache->setFICHEINTERVENTIONID($ficheinterventionid);
            $em->persist($tache);
            $em->flush();

            $tacheid = $tache->getId();

            $return = array('idtache' => $tacheid);

            $response = new Response(json_encode($return));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    public function modifierficheinterventionAction(Request $request)
    {


        $id = $request->query->get('id');

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $ficheIntervention = $em->getRepository('FicheInterventionBundle:FicheIntervention')->find($id);

            $fermeid = $ficheIntervention->getFERMEID();
            $fermename = $em->getRepository('CultureBundle:Ferme')->find($fermeid)->getLIBELLE();

            $ediT_form = $this->createForm(new FicheInterventionType($em),$ficheIntervention);
            $ediT_form->remove('sTATUT');

            $tache = new Tache();
            $ediT_form1 = $this->createForm(new TacheType(), $tache);

            $taches =$em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID($id);

            $listetachearticles=[];

            foreach($taches as $tache)
            {
                $tachearticles= $em->getRepository('FicheInterventionBundle:TacheArticle')->getAllTacheArticles
                ($tache->getId());
                $tachearticle['TACHE']=$tache;
                $datas=[];
                foreach($tachearticles as $ta)
                {
                    array_push($datas, $ta['ARTICLE'] . ' (' . $ta['QUANTITE'].')');
                }

                $tachearticle['ARTICLE']=$datas;
                array_push($listetachearticles,$tachearticle);
            }

            $editeur=$ficheIntervention->getEDITEURID();
            $date = $ficheIntervention->getDATECREATION();

            $docs=$em->getRepository('FicheInterventionBundle:DocFicheIntervention')->findByFICHEINTERVENTIONID($id);

            $imgid= [];
            $imgalt = [];
            $imgpath = [];

            foreach ($docs as $d)
            {
                $iddoc=$d->getDocument()->getId();
                $alt=$d->getDocument()->getAlt();
                $path='Documents/'.$iddoc.'.'.$alt;
                array_push($imgid,$iddoc);
                array_push($imgalt,$alt);
                array_push($imgpath,$path);

            }

            if($request->isMethod('post'))
            {
                $ediT_form->handleRequest($request);

                if($ediT_form->isValid())
                {

                    $editeur = $ficheIntervention->getEDITEURID();
                    $datecre = $ficheIntervention->getDATECREATION();
                    $ficheIntervention->setEDITEURID($editeur);
                    $ficheIntervention->setDATECREATION($datecre);

                    $today = new \DateTime();

                    if($ficheIntervention->getDATEINTERVENTION()<$today)
                    {
                        $ficheIntervention->setDATEINTERVENTION(null);
                        $this->get('session')->getFlashBag()->add('notice', "La date d'intervention choisie n'est
                        pas valide. Veuillez choisir une date ultérieure !!");
                    }

                    $em->flush();

                    return $this->redirect($request->getUri());
                }
            }
        }

        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_TECHNIQUE', $roles, true))
        {
            $site=$em->getRepository('CultureBundle:Site')->find($ficheIntervention->getSITEID())->getLIBELLE();
            $service=$em->getRepository('UserBundle:Service')->find($ficheIntervention->getSERVICEID())->getLIBELLE();

            return $this->render('FicheInterventionBundle:FicheIntervention:modifierficheintervention.html.twig' , array(
                'entity'=>$ficheIntervention,
                'edit_form'=>$ediT_form->createView(),
                'entity1'=>$tache,
                'ediT_form1'=>$ediT_form1->createView(),
                'ficheIntervention'=>$ficheIntervention,
                'taches' => $taches,
                'listetachearticles' =>$listetachearticles,
                'imgid' => $imgid,
                'imgalt' => $imgalt,
                'imgpath' => $imgpath,
                'site' => $site,
                'service' => $service,
                'fermename' => $fermename
            ));
        }
        else
        {
            return $this->render('FicheInterventionBundle:FicheIntervention:modifierficheinterventionmoins.html.twig' ,
                array(
                    'entity'=>$ficheIntervention,
                    'edit_form'=>$ediT_form->createView(),
                    'entity1'=>$tache,
                    'ediT_form1'=>$ediT_form1->createView(),
                    'ficheIntervention'=>$ficheIntervention,
                    'taches' => $taches,
                    'imgid' => $imgid,
                    'imgalt' => $imgalt,
                    'imgpath' => $imgpath,
                    'fermename' => $fermename
                ));
        }
    }

    public function realiserficheinterventionAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $id = $request->query->get('id');
            $em = $this->getDoctrine()->getManager();
            $ficheIntervention = $em->getRepository('FicheInterventionBundle:FicheIntervention')->getFicheById($id)[0];
            $tache = new Tache();
            $form = $this->createForm(new TacheType($em), $tache);

            $taches =$em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID($id);

            $listetachearticles=[];

            foreach($taches as $tache)
            {
                $tachearticles= $em->getRepository('FicheInterventionBundle:TacheArticle')->getAllTacheArticles
                ($tache->getId());
                $tachearticle['TACHE']=$tache;
                $datas=[];
                foreach($tachearticles as $ta)
                {
                    array_push($datas, $ta['ARTICLE'] . ' (' . $ta['QUANTITE'] . ')');
                }


                $tachearticlesR= $em->getRepository('FicheInterventionBundle:TacheArticleR')->getAllTacheArticlesR
                ($tache->getId());
                $datasR['intrant']=[];
                $datasR['qte']=[];
                $datasR['id']=[];
                foreach($tachearticlesR as $ta)
                {
                    array_push($datasR['intrant'], $ta['ARTICLE']);
                    array_push($datasR['qte'], $ta['QUANTITE']);
                    array_push($datasR['id'], $ta['id']);
                }

                $tachearticle['ARTICLE']=$datas;
                $tachearticle['ARTICLER']=$datasR;
                array_push($listetachearticles,$tachearticle);
            }

            return $this->render('FicheInterventionBundle:FicheIntervention:realiserficheintervention.html.twig' , array(
                'entity'=>$tache,
                'form'=>$form->createView(),
                'ficheIntervention'=>$ficheIntervention,
                'taches' => $listetachearticles
            ));
        }
    }

    public function realisertachesAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $user = $this->getUser()->getName();

            $date = new \DateTime();
            $dateformat = $date->format('Ymd');

            $num = (int)$request->request->get('num');
            $ficheInterventionsid=(int)$request->request->get('ficheinterventionid');
            $debuttravaux1=new \DateTime($request->request->get('debuttravaux'));
            $debuttravaux=$debuttravaux1->format('Ymd');
            $fintravaux1=new \DateTime($request->request->get('fintravaux'));
            $fintravaux=$fintravaux1->format('Ymd');
            $duree=$request->request->get('duree');
            var_dump($duree);
            $observations=$request->request->get('observations');

            $numrealise=0;
            for($i=0; $i<$num; $i++)
            {
                $tacheid=(int)$request->request->get('id'.$i);
                $intervenants=(string)$request->request->get('intervenants'.$tacheid);
                $realise=(string)$request->request->get('realise'.$tacheid);
                if($realise=='true')
                {
                    $numrealise++;
                }

                $sql = "UPDATE TACHE SET REALISATEURID ='$user', INTERVENANTS = '$intervenants', DATEREALISATION = '$dateformat',
REALISE = '$realise'  WHERE TACHEID = $tacheid";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }

            $progression=($numrealise*100)/$num;

            if($progression==100)
            {
                $sql = "UPDATE FICHEINTERVENTION SET PROGRESSION ='$progression', DATEDEBUT = '$debuttravaux',
DATEFIN ='$fintravaux', DUREE ='$duree', OBSERVATIONS = '$observations', STATUT = 'CLOTURE', DATEREALISATION ='$dateformat' WHERE
FICHEINTERVENTIONID =
$ficheInterventionsid";
                $stmt = $conn->prepare($sql);
                $stmt->execute();

            }
            elseif($progression<100 and $progression>0)
            {
                $sql = "UPDATE FICHEINTERVENTION SET PROGRESSION ='$progression', DATEDEBUT = '$debuttravaux',
DATEFIN ='$fintravaux', DUREE ='$duree', OBSERVATIONS = '$observations'  WHERE FICHEINTERVENTIONID =
$ficheInterventionsid";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }

            return $this->render('FicheInterventionBundle:FicheIntervention:index.html.twig' );
        }
    }

    public function supprimertacheAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $tacheid = $request->request->get('idtache');

            $sql = "DELETE FROM TACHE WHERE TACHEID = $tacheid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            return $this->render('FicheInterventionBundle:FicheIntervention:index.html.twig' );
        }
    }

    public function deleteimageAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $imageid = $request->request->get('imageid');

            $sql = "DELETE FROM DOCFICHEINTERVENTION WHERE DOCUMENT_ID = $imageid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $sql = "DELETE FROM DOCUMENT WHERE ID = $imageid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            return $this->render('FicheInterventionBundle:FicheIntervention:index.html.twig' );
        }
    }

    public function annulertacheAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $tacheid = $request->request->get('idtache');

            $sql = "UPDATE TACHE SET REALISE ='ANNULE' WHERE TACHEID = $tacheid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            return $this->render('FicheInterventionBundle:FicheIntervention:index.html.twig' );
        }
    }

    public function imprimerficheAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $user = $this->getUser();
        $username = $user->getName();
        $userid = $user->getId();
        $role = $user->getRoles();
        $id = $request->query->get('id');
        $dn = $user->getDn();
        $taches = $em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID($id);

        if(sizeof($taches)==0)
        {
            $this->get('session')->getFlashBag()->add('notice', "Veuillez ajouter des tâches !!");
            return $this->redirect($this->generateUrl('modifierficheintervention', array('id' =>$id)));
        }
        elseif (in_array('ROLE_TECHNIQUE', $role, true)) {
            $typeinterventions = $em->getRepository('FicheInterventionBundle:TypeIntervention')->findAll();

            $ficheIntervention=$em->getRepository('FicheInterventionBundle:FicheIntervention')->getFicheById($id)[0];
//            var_dump($ficheIntervention); exit;
//            $count = 0;
//            foreach ($typeinterventions as $typeintervention) {
//
//                if ($userid == $typeintervention->getRESPONSABLE() && $typeintervention->getLIBELLE() ==
//                    $ficheIntervention['TYPEINTERVENTIONLIB']
//                ) {
//                    $count = $count + 1;
//                }
//            }
//
//            if($count == 1)
//            {
                $date = new \DateTime();
                $dateformat = $date->format('Ymd');

                $sql = "UPDATE FICHEINTERVENTION SET STATUT ='EN COURS', DATEDIFFUSION = '$dateformat', OPERATEURDIFFUSER =
             '$username'
WHERE FICHEINTERVENTIONID = $id";
                $stmt = $conn->prepare($sql);
                $stmt->execute();

                $userDepartement = ucfirst(strtolower($this->getUser()->getDepartment()));
                $editeur=$ficheIntervention['EDITEUR'];
                $dn=$em->getRepository('UserBundle:Utilisateurldap')->findByName($editeur)[0]->getDepartment();

                $listetachearticles=[];

                foreach($taches as $tache) {
                    $tachearticles = $em->getRepository('FicheInterventionBundle:TacheArticle')->getAllTacheArticles
                    ($tache->getId());
                    $tachearticle['TACHE'] = $tache;
                    $datas = [];
                    foreach ($tachearticles as $ta) {
                        array_push($datas, $ta['ARTICLE'] . ' (' . $ta['QUANTITE'] . ')');
                        $tarid = $ta['id'];
                        $datasR = $em->getRepository('FicheInterventionBundle:TacheArticleR')->find($tarid);
                        if(sizeof($datasR) == 0){
                            $sql = "CALL SP_TARGENERE($tarid)";
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();
                        }

                    }
                    $tachearticle['ARTICLE'] = $datas;
                    array_push($listetachearticles, $tachearticle);
                }

                //on stocke la vue � convertir en PDF, en n'oubliant pas les param�tres twig si la vue comporte des donn�es dynamiques
                $html = $this->renderView('FicheInterventionBundle:FicheIntervention:imprimerfiche.html.twig', array('ficheintervention' =>
                    $ficheIntervention, 'taches' => $taches, 'userdepartement' => $dn, 'listeArticle' =>
                    $listetachearticles));
                //on appelle le service html2pdf
                $html2pdf = $this->get('html2pdf_factory')->create('P', 'A4', 'fr', true, 'UTF-8', array(15, 10, 15, 10));
                //real : utilise la taille r�elle
                $html2pdf->pdf->SetDisplayMode('real');
                //writeHTML va tout simplement prendre la vue stocker dans la variable $html pour la convertir en format PDF
                $html2pdf->writeHTML($html);
                //Output envoit le document PDF au navigateur internet
                return new Response($html2pdf->Output('Fiche-intervention.pdf'), 200, array('Content-Type' =>
                    'application/pdf'));
//            }
//            else
//            {
//                $responsable = $em->getRepository('UserBundle:Utilisateurldap')->find
//                ($ficheIntervention['TYPEINTERVENTIONRES']);
//                $this->get('session')->getFlashBag()->add('notice', "Désolée, seul "
//                    .$responsable->getName()." est habilité
//                 à imprimer cette
//                fiche !!");
//                return $this->redirect($this->generateUrl('modifierficheintervention', array('id' =>$id)));
//            }
        }
        else
        {
            $this->get('session')->getFlashBag()->add('notice', "Désolée, vous n'étes pas habilité à imprimer cette
            fiche !!");
            return $this->redirect($this->generateUrl('modifierficheintervention', array('id' =>$id)));
        }
    }

    public function imprimerficheclotureAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $id = $request->query->get('id');
        $taches = $em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID($id);

        $listetachearticles=[];

        foreach($taches as $tache) {
            $tachearticles = $em->getRepository('FicheInterventionBundle:TacheArticle')->getAllTacheArticles
            ($tache->getId());
            $tachearticle['TACHE'] = $tache;
            $datas = [];
            foreach ($tachearticles as $ta) {
                array_push($datas, $ta['ARTICLE'] . ' (' . $ta['QUANTITE'] . ')');
            }

            $tachearticle['ARTICLE'] = $datas;
            array_push($listetachearticles, $tachearticle);
        }

        $date = new \DateTime();
        $dateformat = $date->format('Ymd');

        $ficheIntervention=$em->getRepository('FicheInterventionBundle:FicheIntervention')->getFicheById($id)[0];

        $userDepartement = $this->getUser()->getDepartment();

        //on stocke la vue � convertir en PDF, en n'oubliant pas les param�tres twig si la vue comporte des donn�es dynamiques
        $html = $this->renderView('FicheInterventionBundle:FicheIntervention:imprimerfichecloture.html.twig', array
        ('ficheintervention' => $ficheIntervention, 'taches' => $listetachearticles, 'userdepartement' => $userDepartement));
        //on appelle le service html2pdf
        $html2pdf = $this->get('html2pdf_factory')->create('P', 'A4', 'fr', true, 'UTF-8', array(15, 10, 15, 10));
        //real : utilise la taille r�elle
        $html2pdf->pdf->SetDisplayMode('real');
        //writeHTML va tout simplement prendre la vue stocker dans la variable $html pour la convertir en format PDF
        $html2pdf->writeHTML($html);
        //Output envoit le document PDF au navigateur internet
        return new Response($html2pdf->Output('Fiche-intervention-cloture.pdf'), 200, array('Content-Type' =>
            'application/pdf'));
    }

    public function detailsficheinterventionAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $id = $request->query->get('id');
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $ficheIntervention=$em->getRepository('FicheInterventionBundle:FicheIntervention')->getFicheById($id)[0];

            $taches =$em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID
            ($ficheIntervention['id']);
            $listetachearticles=[];

            foreach($taches as $tache)
            {
                $tachearticles = $em->getRepository('FicheInterventionBundle:TacheArticle')->getAllTacheArticles
                ($tache->getId());
                $tachearticle['TACHE'] = $tache;
                $datas = [];
                foreach ($tachearticles as $ta) {
                    array_push($datas, $ta['ARTICLE'] . ' (' . $ta['QUANTITE'] . ')');
                }

                $tachearticlesR= $em->getRepository('FicheInterventionBundle:TacheArticleR')->getAllTacheArticlesR
                ($tache->getId());

                $datasR=[];
                foreach($tachearticlesR as $ta)
                {
                    array_push($datasR, $ta['ARTICLE'] . ' (' . $ta['QUANTITE'] . ')');
                }

                $tachearticle['ARTICLE']=$datas;
                $tachearticle['ARTICLER']=$datasR;

                if(sizeof($tachearticlesR) == 0)
                {
                    $tachearticle['REA'] = 0;
                }
                else
                {
                    $tachearticle['REA'] = 1;
                }
                array_push($listetachearticles, $tachearticle);
            }

            $docs=$em->getRepository('FicheInterventionBundle:DocFicheIntervention')->findByFICHEINTERVENTIONID($id);

            $imgid= [];
            $imgalt = [];
            $imgpath = [];

            foreach ($docs as $d)
            {
                $iddoc=$d->getDocument()->getId();
                $alt=$d->getDocument()->getAlt();
                $path='Documents/'.$iddoc.'.'.$alt;
                array_push($imgid,$iddoc);
                array_push($imgalt,$alt);
                array_push($imgpath,$path);

            }

            $roles = $this->getUser()->getRoles();
            if (in_array('ROLE_TECHNIQUE', $roles, true)) {
                return $this->render('FicheInterventionBundle:FicheIntervention:detailsfiche.html.twig' , array(
                    'ficheIntervention'=>$ficheIntervention,
                    'date' => $ficheIntervention['dATECREATION']->format('d/m/Y'),
                    'taches' => $listetachearticles,
                    'imgid' => $imgid,
                    'imgalt' => $imgalt,
                    'imgpath' => $imgpath
                ));
            }
            else
            {
                return $this->render('FicheInterventionBundle:FicheIntervention:detailsfichemoins.html.twig' , array(
                    'ficheIntervention'=>$ficheIntervention,
                    'date' => $ficheIntervention['dATECREATION']->format('d/m/Y'),
                    'taches' => $listetachearticles,
                    'imgid' => $imgid,
                    'imgalt' => $imgalt,
                    'imgpath' => $imgpath
                ));
            }
        }
    }

    public function ajoutertachearticleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $username = $this->getUser()->getName();

            $id = $request->request->get('id');
            $idtache = (int)$request->request->get('idtache');
            $nomarticle = $request->request->get('nomarticle');
            $unitearticle = $request->request->get('unitearticle');
            $qte = (int)$request->request->get('qte');
            $type = $request->request->get('type');

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            if($type == "mod")
            {
                $article = $em->getRepository('FicheInterventionBundle:TacheArticle')->findBy(
                    array('tACHEID' => $idtache , 'aRTICLE' => $nomarticle )                        // Offset
                );
                if(sizeof($article) == 0)
                {
                    $return = array('exist' => 0, 'id' => 0);

                    $tachearticle = new TacheArticle();
                    $tachearticle->setTACHEID($idtache);
                    $tachearticle->setARTICLE($nomarticle);
                    $tachearticle->setUNITE($unitearticle);
                    $tachearticle->setQUANTITE($qte);
                    $em->persist($tachearticle);
                    $em->flush();
                }
                else
                {
                    $return = array('exist' => 1, 'id' => 0);
                }
            }
            else
            {
                $articler = $em->getRepository('FicheInterventionBundle:TacheArticleR')->findBy(
                    array('tACHEID' => $idtache , 'aRTICLE' => $nomarticle )                        // Offset
                );
                if(sizeof($articler) == 0)
                {

                    $tachearticler = new TacheArticleR();
                    $tachearticler->setTACHEID($idtache);
                    $tachearticler->setARTICLE($nomarticle);
                    $tachearticler->setUNITE($unitearticle);
                    $tachearticler->setQUANTITE($qte);
                    $em->persist($tachearticler);
                    $em->flush();

                    $tachearticlerid = $tachearticler->getId();
                    $return = array('exist' => 0, 'id' => $tachearticlerid);

                    $sql = "UPDATE TACHE SET REALISE = 'REALISE' WHERE TACHEID = $idtache ";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                }
                else
                {
                    $return = array('exist' => 1, 'id' => 0);

                }


                $taches =$em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID
                ($id);
                $count=0;
                foreach ($taches as $ta)
                {
                    $tar = $em->getRepository('FicheInterventionBundle:TacheArticleR')->findByTACHEID($ta->getId());
                    if(sizeof($tar) != 0)
                    {
                        $count = $count + 1;
                    }
                }

                $progression=($count*100)/sizeof($taches);

                $sql = "UPDATE FICHEINTERVENTION SET PROGRESSION ='$progression' WHERE FICHEINTERVENTIONID = $id";
                $stmt = $conn->prepare($sql);
                $stmt->execute();

            }
        }

        $response = new Response(json_encode($return));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function modiftacheintervenantsAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $username = $this->getUser()->getName();

            $idtache = (int)$request->request->get('idtache');
            $intervenants = $request->request->get('intervenants');
            $id = $request->request->get('id');

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            if($intervenants != null)
            {
                $sql = "UPDATE TACHE SET INTERVENANTS = '$intervenants', REALISATEURID = '$username' WHERE FICHEINTERVENTIONID = $id AND
TACHEID = $idtache" ;
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }

        }

        return $this->render('FicheInterventionBundle:FicheIntervention:index.html.twig' );
    }

    public function reactiverficheAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $id = (int)$request->request->get('id');
            $comment = $request->request->get('comment');

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $username = $this->getUser()->getName();
            $usermail = $this->getUser()->getEmail();

            $date = new \DateTime();
            $dateformat = $date->format('Ymd');

            $sql = "UPDATE FICHEINTERVENTION SET STATUT = 'REOUVERT', REACTIVPAR = '$username', DATEREACTIVATION =
            '$dateformat', OBSERVATIONS = '$comment' WHERE FICHEINTERVENTIONID = $id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $ficheIntervention = $em->getRepository('FicheInterventionBundle:FicheIntervention')->find
            ($id);

//            $libelle = $ficheIntervention->getLIBELLE();
            $typeinterventionid = $ficheIntervention->getTYPEINTERVENTIONID();
            $typeintervention = $em->getRepository('FicheInterventionBundle:TypeIntervention')->find
            ($typeinterventionid)->getLIBELLE();

            $taches = $em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID
            ($id);

            $datas = '<ul style="list-style-type: none;">';

            for($i=0; $i<sizeof($taches); $i++)
            {
                $datas = $datas.'<li>&rarr;';
                $datas = $datas.'Tâche '.($i+1).' : '.'<b style ="color:blue">'.$taches[$i]->getLIBELLE().' ;'.'</b>';
                $datas = $datas.'</li>';
            }
            $datas = $datas.'</ul>';

            try
            {
                $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')->setUsername
                ('intranet@scl.sn')->setPassword('SAVOIR2@SCL');

                $mailer = \Swift_Mailer::newInstance($transport);
                $message = \Swift_Message::newInstance('Réouverture de la fiche d\'intervention N°'.$id)
                    ->setFrom(array('it@scl.sn' => 'IT'))
                    ->setTo(array('maimouna.diakhate@scl.sn', 'raiolagrace.nzikou@scl.sn', 'moustapha.ka@scl.sn', 'hamedine.ba@scl.sn' ,$usermail => 'Technique'))
//                    ->setTo(array('ndeyecoumba.fall@scl.sn' => 'Technique'))
                    ->setBody("<p>La fiche d'intervention N°".'<b style ="color:blue">'.$id."</b> vient d'être réactivée par ".'<b style ="color:blue">'.$username."</b> ! </p><h4>Informations
</h4><ul style='list-style-type: none;'><li>&diams; Type d'intervention : ".'<b style ="color:blue">'
                        .$typeintervention."</b></li><li>&diams; Tâches : ".$datas
                        ."</li><li>&diams; Commentaire du demandeur :
                            ". '<b style ="color:#ff260f">' .$comment."'</b>'.</li></ul>",
                        'text/html');
                $send = $mailer->send($message);
                ini_set('max_execution_time', 300);
            }
            catch(Swift_TransportException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Désolé, nous rencontrons actuellement un
                problème de de connexion internet. Veuillez appeler la
                Technique pour les informer de la réactivation
                            de la fiche d'intervention N°".$id." !!");
                        $this->redirect($this->generateUrl('listeficheintervention' ));

            }

        }
        return $this->render('FicheInterventionBundle:FicheIntervention:index.html.twig' );
    }


    public function terminercreaAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else {
            $id = (int)$request->request->get('id');

            $user = $this->getUser();
            $username = $user->getName();
            $usermail = $user->getEmail();

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $ficheIntervention = $em->getRepository('FicheInterventionBundle:FicheIntervention')->find
            ($id);

            $taches = $em->getRepository('FicheInterventionBundle:Tache')->findByFICHEINTERVENTIONID
            ($id);

            if (sizeof($taches) == 0)
            {
                $return = array('taches' => 0);
            }
            else
            {
                $return = array('taches' => 1);

                $sql = "UPDATE FICHEINTERVENTION SET STATUT = 'ENVOYE' WHERE FICHEINTERVENTIONID = $id";
                $stmt = $conn->prepare($sql);
                $stmt->execute();

//                $libelle = $ficheIntervention->getLIBELLE();
                $typeinterventionid = $ficheIntervention->getTYPEINTERVENTIONID();
                $typeintervention = $em->getRepository('FicheInterventionBundle:TypeIntervention')->find
                ($typeinterventionid)->getLIBELLE();

                $serviceid = $ficheIntervention->getSERVICEID();

                $datas = '<ul style="list-style-type: none;">';
                for($i=0; $i<sizeof($taches); $i++)
                {
                    $datas = $datas.'<li>&rarr;';
                    $datas = $datas.'Tâche '.($i+1).' : '.'<b style ="color:blue">'.$taches[$i]->getLIBELLE().' ;'.'</b>';
                    $datas = $datas.'</li>';
                }
                $datas = $datas.'</ul>';

                try
                {
                    $docs=$em->getRepository('FicheInterventionBundle:DocFicheIntervention')->findByFICHEINTERVENTIONID($id);

                    $paths = [];

                    foreach ($docs as $d) {
                        $iddoc = $d->getDocument()->getId();
                        $alt = $d->getDocument()->getAlt();
                        $path = 'http://srv-pgiweb/kocc-pgi/Documents/' . $iddoc . '.' . $alt;
                        array_push($paths, $path);
                    }

                    $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')->setUsername
                    ('intranet@scl.sn')->setPassword('SAVOIR2@SCL');

                    $mailer = \Swift_Mailer::newInstance($transport);

                    if($username == "Alain ABINADER" && $serviceid == 568){
                        $message = \Swift_Message::newInstance('Nouvelle fiche d\'intervention N°'.$id)
                            ->setFrom(array('it@scl.sn' => 'IT'))
                            ->setTo(array('maimouna.diakhate@scl.sn', 'raiolagrace.nzikou@scl.sn', 'moustapha.ka@scl.sn', 'hamedine.ba@scl.sn', 'alain.abinader@scl.sn', $usermail => 'Technique'))
                            ->setBody("<p>La fiche d'intervention N°".'<b style ="color:blue">'.$id."</b> vient d'être créée par ".'<b style ="color:blue">'.$username."</b> ! </p><h4>Informations
</h4><ul style='list-style-type: none;'><li>&diams; Type d'intervention : ".'<b style ="color:blue">'
                                .$typeintervention."</b></li><li>&diams; Tâches : ".$datas
                                ."</li></ul>",
                                'text/html');
                    }
                    else
                    {
                        $message = \Swift_Message::newInstance('Nouvelle fiche d\'intervention N°'.$id)
                            ->setFrom(array('it@scl.sn' => 'IT'))
                            ->setTo(array('maimouna.diakhate@scl.sn', 'raiolagrace.nzikou@scl.sn', 'moustapha.ka@scl.sn', 'hamedine.ba@scl.sn', $usermail => 'Technique'))
                            ->setBody("<p>La fiche d'intervention N°".'<b style ="color:blue">'.$id."</b> vient d'être créée par ".'<b style ="color:blue">'.$username."</b> ! </p><h4>Informations
</h4><ul style='list-style-type: none;'><li>&diams; Type d'intervention : ".'<b style ="color:blue">'
                                .$typeintervention."</b></li><li>&diams; Tâches : ".$datas
                                ."</li></ul>",
                                'text/html');
                    }

                    foreach($paths as $p)
                    {
                        $message->attach(Swift_Attachment::fromPath($p));
                    }

                    $send = $mailer->send($message);
                    ini_set('max_execution_time', 300);
                }
                catch(Swift_TransportException $e)
                {
                    $this->get('session')->getFlashBag()->add('notice', "Désolé, nous rencontrons actuellement un
                problème de de connexion internet. Veuillez appeler la technique pour l'informer de la création d'une
                            nouvelle fiche d'intervention N°".$id." !!");
                    $this->redirect($this->generateUrl('listeficheintervention' ));

                }
            }
        }

        $response = new Response(json_encode($return));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function cloturerficheAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $id = (int)$request->request->get('id');

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $debuttravaux1=new \DateTime($request->request->get('debuttravaux'));
            $debuttravaux=$debuttravaux1->format('Ymd');
            $fintravaux1=new \DateTime($request->request->get('fintravaux'));
            $fintravaux=$fintravaux1->format('Ymd');
            $duree=$request->request->get('duree');
            $observations=$request->request->get('observations');

            $username = $this->getUser()->getName();

            $date = new \DateTime();
            $dateformat = $date->format('Ymd');

            $sql = "UPDATE FICHEINTERVENTION SET STATUT = 'CLOTURE', DATEREALISATION = '$dateformat', DATEDEBUT ='$debuttravaux', DATEFIN 
='$fintravaux', DUREE ='$duree', OBSERVATIONS = '$observations', PROGRESSION = 100
WHERE FICHEINTERVENTIONID = $id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('FicheInterventionBundle:FicheIntervention:index.html.twig' );
    }

    public function getarticlegcmAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager('sqlserver');
        $conn = $em->getConnection();

        $motcle=$request->query->get('motcle');

        $sql="SELECT a.cbMarq as id, a.AR_Design as name, u.U_Intitule as unite
                FROM F_ARTICLE a, F_FAMILLE f, P_UNITE u
                WHERE a.AR_Design<>'NE PLUS UTILISER'AND a.CL_No1 in (6,5) AND a.FA_CodeFamille=f.FA_CodeFamille AND
                a.AR_UniteVen=u.cbMarq AND a
                .AR_Design LIKE '%$motcle%' ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $count = count($result);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $result));

    }

    public function deletetachearticleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $id = (int)$request->request->get('id');

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $sql = "DELETE FROM TACHEARTICLER WHERE TACHEARTICLERID = $id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('FicheInterventionBundle:FicheIntervention:index.html.twig' );
    }

    public function modifytachearticleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $id = (int)$request->request->get('id');
            $qte = (int)$request->request->get('qte');

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $sql = "UPDATE TACHEARTICLER SET QUANTITE = $qte WHERE TACHEARTICLERID = $id ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('FicheInterventionBundle:FicheIntervention:index.html.twig' );
    }

    public function planningfichesAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            return $this->render('FicheInterventionBundle:FicheIntervention:planningfiches.html.twig' );

        }
    }

    public function modifierplanningAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $ficheid = (int)$request->request->get('ficheid');
        $dateinterv = $request->request->get('dateinterv');

        $sql = "UPDATE FICHEINTERVENTION SET DATEINTERVENTION ='$dateinterv'  WHERE FICHEINTERVENTIONID = $ficheid ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $this->render('FicheInterventionBundle:FicheIntervention:index.html.twig' );

    }

    public function getplanningfichesattrAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $user = $this->getUser();
        $userid = $user->getId();

        $typeintervention = $em->getRepository('FicheInterventionBundle:TypeIntervention')->findByRESPONSABLE
        ($userid);

        $count = 0;
        $resulttot =[];

        foreach ($typeintervention as $ti)
        {
            $id = $ti->getId();
            $sql="SELECT FI.FICHEINTERVENTIONID, FI.DATEINTERVENTION, FI.TYPEINTERVENTIONID, S.LIBELLE as SITE, F.LIBELLE as FERME, FI.EDITEUR, FI
.DATECREATION  FROM FICHEINTERVENTION FI, SITE S, FERME F WHERE FI.TYPEINTERVENTIONID = $id AND FI.STATUT <> 'EN CREATION' AND FI.DATEINTERVENTION IS NOT NULL
            AND FI .FERMEID = F.FERMEID AND FI.SITEID = S.SITEID ";
//            $sql="SELECT FICHEINTERVENTIONID, DATEINTERVENTION FROM FICHEINTERVENTION
//            WHERE TYPEINTERVENTIONID = $id AND STATUT <> 'EN CREATION' AND DATEINTERVENTION IS NOT NULL";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            foreach ($result as $res)
            {
                array_push($resulttot, $res);
            }
            $count = $count + count($result);
        }

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $resulttot));

    }

    public function ajoutdocficheinterventionAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $entity = new DocFicheIntervention();

            $form = $this->createForm(new DocFicheInterventionType(), $entity);

            $docs=$em->getRepository('FicheInterventionBundle:DocFicheIntervention')->findByFICHEINTERVENTIONID($id);

            $imgid= [];
            $imgalt = [];
            $imgpath = [];

            foreach ($docs as $d)
            {
                $iddoc=$d->getDocument()->getId();
                $alt=$d->getDocument()->getAlt();
                $path='Documents/'.$iddoc.'.'.$alt;
                array_push($imgid,$iddoc);
                array_push($imgalt,$alt);
                array_push($imgpath,$path);

            }

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    if($form->get('dOCUMENT')->getData() != null)
                    {
                        $entity->setFICHEINTERVENTIONID($id);
                        $em->persist($entity);
                        $em->flush();
                    }
                    return $this->redirect($this->generateUrl('ajoutdocficheintervention', array('id' => $id)));

                }
            }

            return $this->render('FicheInterventionBundle:FicheIntervention:ajoutdocficheintervention.html.twig', array(
                'form' => $form->createView(),
                'entity' => $entity,
                'id' => $id,
                'imgid' => $imgid,
                'imgalt' => $imgalt,
                'imgpath' => $imgpath
            ));

        }
    }


}



