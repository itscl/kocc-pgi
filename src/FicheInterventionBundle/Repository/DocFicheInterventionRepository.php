<?php

namespace FicheInterventionBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * DocFicheInterventionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DocFicheInterventionRepository extends EntityRepository
{
//    public function getAllDocByFicheIntervention($id)
//    {
//        $query = $this->_em->createQueryBuilder()
//            ->select('dm.id,dm.mODELE, mm.lIBELLE, d.id as documentid, d.name, d.alt')
//            ->from('FicheInterventionBundle:DocFicheIntervention','df')
//            ->innerJoin('FicheInterventionBundle:FicheIntervention','fi', 'WITH', 'df.fICHEINTERVENTIONID = fi.id')
//            ->innerJoin('FicheInterventionBundle:Document','d', 'WITH', 'df.DOCUMENT = d.id')
//            ->andWhere('df.fICHEINTERVENTIONID =:identified')
//            ->setParameter('identified', $id)
//            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();;
//        return $query;
//    }
}
