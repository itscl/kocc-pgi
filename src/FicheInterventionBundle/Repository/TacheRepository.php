<?php

namespace FicheInterventionBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * TacheRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TacheRepository extends EntityRepository
{
    public function getAllTache()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('ta.id, ta.lIBELLE, ta.iNTERVENANTS, ta.dATEREALISATION, ta.rEALISE  ta.lIBELLE as
            FICHEINTERVENTION')
            ->from('FicheInterventionBundle:Tache','ta')
            ->innerJoin('FicheInterventionBundle:FicheIntervention', 'fi', 'WITH', 'fi.id = ta.fICHEINTERVENTIONID')
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();;
        return $query;
    }
}
