<?php

namespace FicheInterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeIntervention
 */
class TypeIntervention
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var integer
     */
    private $rESPONSABLE;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return TypeIntervention
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * @return integer
     */
    public function getRESPONSABLE()
    {
        return $this->rESPONSABLE;
    }

    /**
     * @param integer $rESPONSABLE
     */
    public function setRESPONSABLE($rESPONSABLE)
    {
        $this->rESPONSABLE = $rESPONSABLE;
    }
}
