<?php

namespace FicheInterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tache
 */
class TacheArticleR
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $aRTICLE;

    /**
     * @var int
     */
    private $qUANTITE;

    /**
     * @var int
     */
    private $tACHEID;

    /**
     * @var int
     */
    private $uNITE;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getQUANTITE()
    {
        return $this->qUANTITE;
    }

    /**
     * @param int $qUANTITE
     */
    public function setQUANTITE($qUANTITE)
    {
        $this->qUANTITE = $qUANTITE;
    }

    /**
     * @return int
     */
    public function getTACHEID()
    {
        return $this->tACHEID;
    }

    /**
     * @param int $tACHEID
     */
    public function setTACHEID($tACHEID)
    {
        $this->tACHEID = $tACHEID;
    }

    /**
     * @return int
     */
    public function getUNITE()
    {
        return $this->uNITE;
    }

    /**
     * @param int $uNITE
     */
    public function setUNITE($uNITE)
    {
        $this->uNITE = $uNITE;
    }

    /**
     * @return string
     */
    public function getARTICLE()
    {
        return $this->aRTICLE;
    }

    /**
     * @param string $aRTICLE
     */
    public function setARTICLE($aRTICLE)
    {
        $this->aRTICLE = $aRTICLE;
    }

}
