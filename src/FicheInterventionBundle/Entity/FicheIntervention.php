<?php

namespace FicheInterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FicheIntervention
 */
class FicheIntervention
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $aTTRIBUEA;

    /**
     * @var string
     */
    private $eDITEURID;

    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var \DateTime
     */
    private $dATEINTERVENTION;

    /**
     * @var \DateTime
     */
    private $dATEDEBUT;

    /**
     * @var \DateTime
     */
    private $dATEFIN;

    /**
     * @var \DateTime
     */
    private $dATEREACTIVATION;

    /**
     * @var \DateTime
     */
    private $dATEREALISATION;

    /**
     * @var string
     */
    private $dUREE;

    /**
     * @var int
     */
    private $sERVICEID;

    /**
     * @var int
     */
    private $fERMEID;

    /**
     * @var int
     */
    private $tYPEINTERVENTIONID;

    /**
     * @var int
     */
    private $sITEID;

    /**
     * @var string
     */
    private $sTATUT;

    /**
     * @var string
     */
    private $iDENTIFICATION;

    /**
     * @var string
     */
    private $pRIORISATION;

    /**
     * @var int
     */
    private $pROGRESSION;

    /**
     * @var \DateTime
     */
    private $dATEDIFFUSION;

    /**
     * @var string
     */
    private $oPERATEURDIFFUSER;

    /**
     * @var string
     */
    private $oBSERVATIONS;

    /**
     * @var string
     */
    private $rEACTIVEPAR;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return FicheIntervention
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set eDITEURID
     *
     * @param string $eDITEURID
     * @return FicheIntervention
     */
    public function setEDITEURID($eDITEURID)
    {
        $this->eDITEURID = $eDITEURID;

        return $this;
    }

    /**
     * Get eDITEURID
     *
     * @return string
     */
    public function getEDITEURID()
    {
        return $this->eDITEURID;
    }

    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return FicheIntervention
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime 
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set sERVICEID
     *
     * @param integer $sERVICEID
     * @return FicheIntervention
     */
    public function setSERVICEID($sERVICEID)
    {
        $this->sERVICEID = $sERVICEID;

        return $this;
    }

    /**
     * Get sERVICEID
     *
     * @return integer 
     */
    public function getSERVICEID()
    {
        return $this->sERVICEID;
    }

    /**
     * Set tYPEINTERVENTIONID
     *
     * @param integer $tYPEINTERVENTIONID
     * @return FicheIntervention
     */
    public function setTYPEINTERVENTIONID($tYPEINTERVENTIONID)
    {
        $this->tYPEINTERVENTIONID = $tYPEINTERVENTIONID;

        return $this;
    }

    /**
     * Get tYPEINTERVENTIONID
     *
     * @return integer 
     */
    public function getTYPEINTERVENTIONID()
    {
        return $this->tYPEINTERVENTIONID;
    }

    /**
     * Set sITEID
     *
     * @param integer $sITEID
     * @return FicheIntervention
     */
    public function setSITEID($sITEID)
    {
        $this->sITEID = $sITEID;

        return $this;
    }

    /**
     * Get sITEID
     *
     * @return integer 
     */
    public function getSITEID()
    {
        return $this->sITEID;
    }

    /**
     * Set sTATUT
     *
     * @param string $sTATUT
     * @return FicheIntervention
     */
    public function setSTATUT($sTATUT)
    {
        $this->sTATUT = $sTATUT;

        return $this;
    }

    /**
     * Get sTATUT
     *
     * @return string 
     */
    public function getSTATUT()
    {
        return $this->sTATUT;
    }

    /**
     * @return int
     */
    public function getPROGRESSION()
    {
        return $this->pROGRESSION;
    }

    /**
     * @param int $pROGRESSION
     */
    public function setPROGRESSION($pROGRESSION)
    {
        $this->pROGRESSION = $pROGRESSION;
    }

    /**
     * @return \DateTime
     */
    public function getDATEDIFFUSION()
    {
        return $this->dATEDIFFUSION;
    }

    /**
     * @param \DateTime $dATEDIFFUSION
     */
    public function setDATEDIFFUSION($dATEDIFFUSION)
    {
        $this->dATEDIFFUSION = $dATEDIFFUSION;
    }

    /**
     * @return string
     */
    public function getOPERATEURDIFFUSER()
    {
        return $this->oPERATEURDIFFUSER;
    }

    /**
     * @param string $oPERATEURDIFFUSER
     */
    public function setOPERATEURDIFFUSER($oPERATEURDIFFUSER)
    {
        $this->oPERATEURDIFFUSER = $oPERATEURDIFFUSER;
    }

    /**
     * @return string
     */
    public function getOBSERVATIONS()
    {
        return $this->oBSERVATIONS;
    }

    /**
     * @param string $oBSERVATIONS
     */
    public function setOBSERVATIONS($oBSERVATIONS)
    {
        $this->oBSERVATIONS = $oBSERVATIONS;
    }

    /**
     * @return \DateTime
     */
    public function getDATEDEBUT()
    {
        return $this->dATEDEBUT;
    }

    /**
     * @param \DateTime $dATEDEBUT
     */
    public function setDATEDEBUT($dATEDEBUT)
    {
        $this->dATEDEBUT = $dATEDEBUT;
    }

    /**
     * @return \DateTime
     */
    public function getDATEFIN()
    {
        return $this->dATEFIN;
    }

    /**
     * @param \DateTime $dATEFIN
     */
    public function setDATEFIN($dATEFIN)
    {
        $this->dATEFIN = $dATEFIN;
    }

    /**
     * @return string
     */
    public function getDUREE()
    {
        return $this->dUREE;
    }

    /**
     * @param string $dUREE
     */
    public function setDUREE($dUREE)
    {
        $this->dUREE = $dUREE;
    }

    /**
     * @return string
     */
    public function getATTRIBUEA()
    {
        return $this->aTTRIBUEA;
    }

    /**
     * @param string $aTTRIBUEA
     */
    public function setATTRIBUEA($aTTRIBUEA)
    {
        $this->aTTRIBUEA = $aTTRIBUEA;
    }

    /**
     * @return \DateTime
     */
    public function getDATEREACTIVATION()
    {
        return $this->dATEREACTIVATION;
    }

    /**
     * @param \DateTime $dATEREACTIVATION
     */
    public function setDATEREACTIVATION($dATEREACTIVATION)
    {
        $this->dATEREACTIVATION = $dATEREACTIVATION;
    }

    /**
     * @return string
     */
    public function getREACTIVEPAR()
    {
        return $this->rEACTIVEPAR;
    }

    /**
     * @param string $rEACTIVEPAR
     */
    public function setREACTIVEPAR($rEACTIVEPAR)
    {
        $this->rEACTIVEPAR = $rEACTIVEPAR;
    }

    /**
     * @return \DateTime
     */
    public function getDATEREALISATION()
    {
        return $this->dATEREALISATION;
    }

    /**
     * @param \DateTime $dATEREALISATION
     */
    public function setDATEREALISATION($dATEREALISATION)
    {
        $this->dATEREALISATION = $dATEREALISATION;
    }

    /**
     * @return string
     */
    public function getIDENTIFICATION()
    {
        return $this->iDENTIFICATION;
    }

    /**
     * @param string $iDENTIFICATION
     */
    public function setIDENTIFICATION($iDENTIFICATION)
    {
        $this->iDENTIFICATION = $iDENTIFICATION;
    }

    /**
     * @return string
     */
    public function getPRIORISATION()
    {
        return $this->pRIORISATION;
    }

    /**
     * @param string $pRIORISATION
     */
    public function setPRIORISATION($pRIORISATION)
    {
        $this->pRIORISATION = $pRIORISATION;
    }

    /**
     * @return int
     */
    public function getFERMEID()
    {
        return $this->fERMEID;
    }

    /**
     * @param int $fERMEID
     */
    public function setFERMEID($fERMEID)
    {
        $this->fERMEID = $fERMEID;
    }

    /**
     * @return \DateTime
     */
    public function getDATEINTERVENTION()
    {
        return $this->dATEINTERVENTION;
    }

    /**
     * @param \DateTime $dATEINTERVENTION
     */
    public function setDATEINTERVENTION($dATEINTERVENTION)
    {
        $this->dATEINTERVENTION = $dATEINTERVENTION;
    }

}
