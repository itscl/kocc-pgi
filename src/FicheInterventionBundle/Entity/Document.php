<?php

namespace FicheInterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Document
 */
class Document
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alt;

    private $file;

    // On ajoute cet attribut pour y stocker le nom du fichier temporairement
    private $tempFilename;

    public function preUpload()
    {
// Si jamais il n'y a pas de fichier (champ facultatif)
        if (null === $this->file) {
            return;
        }

        foreach ($this->file as $f)
        {
            // Le nom du fichier est son id, on doit juste stocker également son extension
            $this->alt = $f->guessExtension();
            // Et on génère l'attribut alt de la balise <img>, �  la valeur du nom du fichier sur le PC de l'internaute
            $this->name = $f->getClientOriginalName();
        }
    }

    public function upload()
    {
// Si jamais il n'y a pas de fichier (champ facultatif)
        if (null === $this->file) {
            return;
        }
// Si on avait un ancien fichier, on le supprime
        if (null !== $this->tempFilename) {
            $oldFile = $this->getUploadRootDir() . '/' . $this->id . '.' . $this->tempFilename;
            if (file_exists($oldFile)) {
                unlink($oldFile);
            }
        }

// On déplace le fichier envoyé dans le répertoire de notre choix
//        $this->file->move(
//            $this->getUploadRootDir(), // Le répertoire de destination
//            $this->id . '.' . $this->alt // Le nom du fichier �  créer, ici "id.extension"
//
//        );
        foreach ($this->file as $f)
        {
            $f->move(
                $this->getUploadRootDir(), // Le répertoire de destination
                $this->id . '.' . $this->alt // Le nom du fichier �  créer, ici "id.extension"

            );
        }

        /*$this->file->copy(
            $this->getUploadRootDir(),$this->id.'.'.$this->url,'http://srv-file/SHARES/DEMANDE_ACHATS'
        );*/
    }

    public function preRemoveUpload()
    {
// On sauvegarde temporairement le nom du fichier car il dépend de l'id
        $this->tempFilename = $this->getUploadRootDir() . '/' . $this->id . '.' . $this->name;
    }

    public function removeUpload()
    {
// En PostRemove, on n'a pas accès �  l'id, on utilise notre nom sauvegardé
        if (file_exists($this->tempFilename)) {
// On supprime le fichier
            unlink($this->tempFilename);
        }
    }

    public function getUploadDir()
    {
// On retourne le chemin relatif vers l'image pour un navigateur
        return 'Documents';
    }

    protected function getUploadRootDir()
    {
        //var_dump(__DIR__ . '/../../../../web/' . $this->getUploadDir()); exit;
// On retourne le chemin absolu vers l'image pour notre code PHP
        return __DIR__ . '/../../../web/' . $this->getUploadDir();

    }

    public function getWebPath()
    {
        return $this->getUploadDir() . '/' . $this->getId() . '.' . $this->getAlt();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alt
     *
     * @param string $alt
     * @return Document
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    public function setFile($file)
    {
        $this->file = $file;
// On vérifie si on avait déj�  un fichier pour cette entité
        if (null !== $this->alt) {
// On sauvegarde l'extension du fichier pour le supprimer plus tard
            $this->tempFilename = $this->alt;
// On réinitialise les valeurs des attributs url et alt
            $this->alt = null;
            $this->name = null;
        }
    }

    public function getFile()
    {
        return $this->file;
    }
}
