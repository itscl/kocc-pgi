<?php

namespace FicheInterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tache
 */
class Tache
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $rEALISATEURID;

    /**
     * @var string
     */
    private $iNTERVENANTS;

    /**
     * @var \DateTime
     */
    private $dATEREALISATION;

    /**
     * @var int
     */
    private $fICHEINTERVENTIONID;

    /**
     * @var int
     */
    private $tACHEARTICLEID;

    /**
     * @var string
     */
    private $rEALISE;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return Tache
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set rEALISATEURID
     *
     * @param string $rEALISATEURID
     * @return Tache
     */
    public function setREALISATEURID($rEALISATEURID)
    {
        $this->rEALISATEURID = $rEALISATEURID;

        return $this;
    }

    /**
     * Get rEALISATEURID
     *
     * @return string
     */
    public function getREALISATEURID()
    {
        return $this->rEALISATEURID;
    }

    /**
     * Set intervenants
     *
     * @param string $iNTERVENANTS
     * @return Tache
     */
    public function setINTERVENANTS($iNTERVENANTS)
    {
        $this->iNTERVENANTS = $iNTERVENANTS;

        return $this;
    }

    /**
     * Get iNTERVENANTS
     *
     * @return string 
     */
    public function getINTERVENANTS ()
    {
        return $this->iNTERVENANTS;
    }

    /**
     * Set dATEREALISATION
     *
     * @param \DateTime $dATEREALISATION
     * @return Tache
     */
    public function setDATEREALISATION($dATEREALISATION)
    {
        $this->dATEREALISATION = $dATEREALISATION;

        return $this;
    }

    /**
     * Get dATEREALISATION
     *
     * @return \DateTime 
     */
    public function getDATEREALISATION()
    {
        return $this->dATEREALISATION;
    }

    /**
     * Set fICHEINTERVENTIONID
     *
     * @param integer $fICHEINTERVENTIONID
     * @return Tache
     */
    public function setFICHEINTERVENTIONID($fICHEINTERVENTIONID)
    {
        $this->fICHEINTERVENTIONID = $fICHEINTERVENTIONID;

        return $this;
    }

    /**
     * Get fICHEINTERVENTIONID
     *
     * @return integer 
     */
    public function getFICHEINTERVENTIONID()
    {
        return $this->fICHEINTERVENTIONID;
    }

    /**
     * @return string
     */
    public function getREALISE()
    {
        return $this->rEALISE;
    }

    /**
     * @param string $rEALISE
     */
    public function setREALISE($rEALISE)
    {
        $this->rEALISE = $rEALISE;
    }

    /**
     * @return int
     */
    public function getTACHEARTICLEID()
    {
        return $this->tACHEARTICLEID;
    }

    /**
     * @param int $tACHEARTICLEID
     */
    public function setTACHEARTICLEID($tACHEARTICLEID)
    {
        $this->tACHEARTICLEID = $tACHEARTICLEID;
    }

}
