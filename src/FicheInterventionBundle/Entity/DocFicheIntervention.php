<?php

namespace FicheInterventionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * DocFicheIntervention
 */
class DocFicheIntervention
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var integer
     */
    private $fICHEINTERVENTIONID;

    /**
     * @var integer
     */
    private $DOCUMENT;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getFICHEINTERVENTIONID()
    {
        return $this->fICHEINTERVENTIONID;
    }

    public function setFICHEINTERVENTIONID($fICHEINTERVENTIONID)
    {
        $this->fICHEINTERVENTIONID = $fICHEINTERVENTIONID;
    }

    public function getDOCUMENT()
    {
        return $this->DOCUMENT;
    }

    public function setDOCUMENT(Document $document = null)
    {
        $this->DOCUMENT = $document;
    }


}
