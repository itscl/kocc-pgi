<?php

namespace FicheInterventionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;
use FicheInterventionBundle\Form\DocFicheInterventionType;

class FicheInterventionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sites = array();
        $services = array();
        $typeInterventions = array();

        $query=$this->em->getRepository('CultureBundle:Site')
            ->createQueryBuilder('si')
            ->orderBy('si.lIBELLE');

        foreach($query->getQuery()->getResult() as $ref){
            $sites[$ref->getId()]=$ref->getLIBELLE();
        }

        $query=$this->em->getRepository('UserBundle:Service')
            ->createQueryBuilder('se')
            ->orderBy('se.lIBELLE');

        foreach($query->getQuery()->getResult() as $ref){
            $services[$ref->getId()]=$ref->getLIBELLE();
        }

        //liste des interventions
        $query=$this->em->getRepository('FicheInterventionBundle:TypeIntervention')->getAllTypeIntervention();

        foreach($query as $ref)
        {
            $typeInterventions[$ref['id']]=$ref['LIBELLE'];
        }

        $builder
//            ->add('lIBELLE', 'textarea', array(
//                'required'   => true
//            ))
            ->add('sERVICEID', 'choice', array(
                'choices' => $services,  'required' => false,
                'attr' => [
                    'class' => 'form-control selectpicker'
                ]
            ))->add('tYPEINTERVENTIONID', 'choice', array(
                'choices' => $typeInterventions,  'required' => false,
                'attr' => [
                    'class' => 'form-control selectpicker'
                ]
            ))
            ->add('sITEID', 'choice', array(
                'choices' => $sites,  'required' => false,
                'attr' => [
                    'class' => 'form-control selectpicker'
                ]
            ))
            ->add('dATEINTERVENTION', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy HH:mm:ss',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'data-date-format' => 'DD-MM-YYYY HH:mm:ss'
                ]
            ))
            ->add('sTATUT')
            ->add('fERMEID','hidden')
            ->add('iDENTIFICATION', 'textarea')
            ->add('oBSERVATIONS', 'textarea')
            ->add('pRIORISATION');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FicheInterventionBundle\Entity\FicheIntervention'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ficheinterventionbundle_ficheintervention';
    }


}
