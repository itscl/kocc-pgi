<?php

namespace AgroBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ITKITLCIBLERepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ITKITLCIBLERepository extends EntityRepository
{

    public function getAllItkitlCibleByITKITLID($id)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('i.id, ci.id as CIBLEID, ci.lIBELLE as CIBLE ')
            ->from('AgroBundle:ITKITLCIBLE', 'i')
            ->innerJoin('AgroBundle:Cible', 'ci', 'WITH', 'ci.id = i.cIBLEID')
            ->andWhere('i.iTKITLID =:identifier')
            ->setParameter('identifier',$id )
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }
}
