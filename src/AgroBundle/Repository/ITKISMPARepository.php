<?php

namespace AgroBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ITKISMPARepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ITKISMPARepository extends EntityRepository
{

    public function getAllItkismpaByITKISMID($id)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('i.id ,i.sURFACE as SURFACE, pa.lIBELLE as PARCELLE, i.pARCELLECHECK as PARCELLECHECK, i
            .pARCELLEID as PARCELLEID, ir.id as TYPEIRRIGATIONID ')
            ->from('AgroBundle:ITKISMPA', 'i')
            ->innerJoin('CultureBundle:Parcelle', 'pa', 'WITH', 'pa.id = i.pARCELLEID')
            ->innerJoin('CultureBundle:TypeIrrigation', 'ir', 'WITH', 'ir.id = pa.tYPEIRRIGATIONID')
            ->andWhere('i.iTKISMID =:identifier')
            ->setParameter('identifier',$id )
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }
}
