<?php

namespace AgroBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ITKISMRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ITKISMRepository extends EntityRepository
{

    public function getAllItkism()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('i.id ,i.dATECREATION as DATECREATION, i.dATEP as DATEP, i.eTATINSTRUCTION as STATUT, i.sURFACE
            as SURFACE, fe.lIBELLE as FERME, cc.lIBELLE as OP, cu.lIBELLE as CULTUREVARIETE, il.lIBELLE as ILOT, il
            .id as ILOTID, p.nOM as NOM, p.pRENOM as PRENOM ')
            ->from('AgroBundle:ITKISM', 'i')
            ->innerJoin('CultureBundle:Ferme', 'fe', 'WITH', 'fe.id = i.fERMEID')
            ->innerJoin('CultureBundle:CycleCultural', 'cc', 'WITH', 'cc.id = i.cYCLECULTURALID')
            ->innerJoin('CultureBundle:Culture', 'cu', 'WITH', 'cu.id = i.cULTUREID')
            ->innerJoin('CultureBundle:Ilot', 'il', 'WITH', 'il.id = cc.iLOTID')
            ->innerJoin('TechniqueBundle:Personnel', 'p', 'WITH', 'p.id = i.pRESCRIPTEURID')
            ->andWhere('i.eTATINSTRUCTION !=:statut')
            ->setParameter('statut','REALISE' )
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }

    public function getItkism($id)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('i.id ,i.dATECREATION as DATECREATION, i.dATEP as DATEP, i.dATEPFIN as DATEPFIN, i
            .eTATINSTRUCTION as STATUT, i.sURFACE as SURFACE, i.sURFACETOT as SURFACETOT, i
            .qTESEM as QTESEM, i.qTESEMTOT as QTESEMTOT, i.mODEAPPLICATION as MODEAPPLICATION, i.eLIGNE as ELIGNE, i.eSEMIS as ESEMIS, i.rANGNB as RANG, i.eFFECTIF as EFFECTIF, fe.lIBELLE as FERME, si
            .lIBELLE as SITE, cc.lIBELLE as
             OP, cu.lIBELLE as CULTUREVARIETE, il
            .lIBELLE as ILOT, il.id as ILOTID, fc.lIBELLE as FAMILLECULTURALE, p.nOM as NOM, p.pRENOM as PRENOM, i.aPPAREILID as APPAREILID, i.mATERIELID as MATERIELID, i.aPPLICATEURID as APPLICATEURID ')
            ->from('AgroBundle:ITKISM', 'i')
            ->innerJoin('CultureBundle:Ferme', 'fe', 'WITH', 'fe.id = i.fERMEID')
            ->innerJoin('CultureBundle:Site', 'si', 'WITH', 'si.id = fe.sITEID')
            ->innerJoin('CultureBundle:CycleCultural', 'cc', 'WITH', 'cc.id = i.cYCLECULTURALID')
            ->innerJoin('CultureBundle:Culture', 'cu', 'WITH', 'cu.id = i.cULTUREID')
            ->innerJoin('CultureBundle:FamilleCulture', 'fc', 'WITH', 'fc.id = cu.fAMILLECULTUREID')
            ->innerJoin('CultureBundle:Ilot', 'il', 'WITH', 'il.id = cc.iLOTID')
            ->innerJoin('TechniqueBundle:Personnel', 'p', 'WITH', 'p.id = i.pRESCRIPTEURID')
//            ->innerJoin('TechniqueBundle:Materiel', 'ma', 'WITH', 'ma.id = i.mATERIELID')
//            ->innerJoin('TechniqueBundle:Materiel', 'appareil', 'WITH', 'appareil.id = i.aPPAREILID')
            ->andWhere('i.id =:identifier')
            ->setParameter('identifier',$id )
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }

    public function getAllItkismByFerme($fermeid)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('i.id ,i.dATECREATION as DATECREATION, i.dATEP as DATEP, i.eTATINSTRUCTION as STATUT, i.sURFACE
            as SURFACE, fe.lIBELLE as FERME, cc.lIBELLE as OP, cu.lIBELLE as CULTUREVARIETE, il.lIBELLE as ILOT, il
            .id as ILOTID, p.nOM as NOM, p.pRENOM as PRENOM ')
            ->from('AgroBundle:ITKISM', 'i')
            ->innerJoin('CultureBundle:Ferme', 'fe', 'WITH', 'fe.id = i.fERMEID')
            ->innerJoin('CultureBundle:CycleCultural', 'cc', 'WITH', 'cc.id = i.cYCLECULTURALID')
            ->innerJoin('CultureBundle:Culture', 'cu', 'WITH', 'cu.id = i.cULTUREID')
            ->innerJoin('CultureBundle:Ilot', 'il', 'WITH', 'il.id = cc.iLOTID')
            ->innerJoin('TechniqueBundle:Personnel', 'p', 'WITH', 'p.id = i.pRESCRIPTEURID')
            ->andWhere('fe.id =:fermeid')
            ->andWhere('i.eTATINSTRUCTION !=:statut')
            ->setParameter('fermeid',$fermeid )
            ->setParameter('statut','REALISE' )
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }

    public function getInstructionDiffuseSemis($motcle, $fermeid)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('itkism.id as id, itkism.id as name')
            ->from('AgroBundle:ITKISM', 'itkism')
            ->innerJoin('CultureBundle:Ferme', 'fe', 'WITH', 'itkism.fERMEID = fe.id')
            ->orWhere('itkism.eTATINSTRUCTION =:diffuse')
            ->andWhere('itkism.id LIKE :motcle')
            ->andWhere('itkism.fERMEID =:fermeid')
            ->setParameter('fermeid',$fermeid )
            ->setParameter('motcle', '%'.$motcle.'%')
            ->setParameter('diffuse', 'DIFFUSE')
            ->orderBy('itkism.id', 'DESC')
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }


    public function getinstructiondiffuseop($id)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('i.id')
            ->from('AgroBundle:ITKISM', 'i')
            ->innerJoin('CultureBundle:CycleCultural', 'cc', 'WITH', 'cc.id = i.cYCLECULTURALID')
            ->innerJoin('AgroBundle:ITKP', 'itkp', 'WITH', 'itkp.cYCLECULTURALID = cc.id')
            ->innerJoin('AgroBundle:ITKPSG', 'itkpsg', 'WITH', 'itkpsg.iTKPID = itkp.id')
            ->andWhere('cc.id =:identifier')
            ->andWhere('i.eTATINSTRUCTION =:diffuse')
            ->andWhere('itkpsg.eTATINSTRUCTION =:diffuse')
            ->setParameter('identifier',$id )
            ->setParameter('diffuse', 'DIFFUSE')
            ->orderBy('cc.lIBELLE', 'ASC')
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }

}
