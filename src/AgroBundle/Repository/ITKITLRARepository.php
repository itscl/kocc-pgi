<?php

namespace AgroBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ITKITLRARepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ITKITLRARepository extends EntityRepository
{

    public function getAllItkitlraByITKITLRID($id)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('i.id, i.qTE as QTE, i.uNITE as UNITE, a.lIBELLE as ARTICLE, i.aRTICLEID as ARTICLEID, i.qTET as
             QTETOT, i.cOMMENTAIRE as COMMENTAIRE, i.rANG as RANG, i.bOUILLIE as BOUILLIE ')
            ->from('AgroBundle:ITKITLRA', 'i')
            ->innerJoin('TechniqueBundle:Article', 'a', 'WITH', 'a.id = i.aRTICLEID')
            ->andWhere('i.iTKITLRID =:identifier')
            ->setParameter('identifier',$id )
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }
}
