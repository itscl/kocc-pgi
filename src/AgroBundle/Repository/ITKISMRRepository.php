<?php

namespace AgroBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ITKISMRRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ITKISMRRepository extends EntityRepository
{

    public function getAllItkismrByFerme($fermeid)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('i.id ,i.dATECREATION as DATECREATION, i.dATEP as DATEP, i.sURFACE
            as SURFACE, fe.lIBELLE as FERME, cc.lIBELLE as OP, cu.lIBELLE as CULTUREVARIETE, il.lIBELLE as ILOT, il
            .id as ILOTID, p.nOM as NOM, p.pRENOM as PRENOM, i.hEUREDEBUT as HEUREDEBUT, i.hEUREFIN as HEUREFIN ')
            ->from('AgroBundle:ITKISMR', 'i')
            ->innerJoin('CultureBundle:Ferme', 'fe', 'WITH', 'fe.id = i.fERMEID')
            ->innerJoin('CultureBundle:CycleCultural', 'cc', 'WITH', 'cc.id = i.cYCLECULTURALID')
            ->innerJoin('CultureBundle:Culture', 'cu', 'WITH', 'cu.id = i.cULTUREID')
            ->innerJoin('CultureBundle:Ilot', 'il', 'WITH', 'il.id = cc.iLOTID')
            ->innerJoin('TechniqueBundle:Personnel', 'p', 'WITH', 'p.id = i.pRESCRIPTEURID')
            ->andWhere('fe.id =:fermeid')
//            ->andWhere('i.eTATINSTRUCTION =:statut')
            ->setParameter('fermeid',$fermeid )
//            ->setParameter('statut','REALISE' )
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }

    public function getItkismr($id)
    {
        $query = $this->_em->createQueryBuilder()
            ->select('i.id ,i.dATECREATION as DATECREATION, i.dATEP as DATEP, i.dATEPFIN as DATEPFIN, i.sURFACE as SURFACE, i.sURFACETOT as SURFACETOT, fe.lIBELLE as FERME, si
            .lIBELLE as SITE, cc.lIBELLE as
             OP, cc.id as OPID, cc.tYPEIMPLANTATION as TYPEIMPLANTATION, cc.dATEPREVISRECOLTE as DATEPREV, cu.lIBELLE as
             CULTUREVARIETE, il
            .lIBELLE as ILOT, il.id as ILOTID, p.nOM as NOM, p.pRENOM as PRENOM, fc.lIBELLE as FAMILLECULTURALE, i.qTESEM as QTESEM, i
            .qTESEMTOT as QTESEMTOT, i.mODEAPPLICATION as MODEAPPLICATION, ma.lIBELLE as MATERIEL, appareil.lIBELLE
            as APPAREIL, i.eLIGNE as ELIGNE, i.eSEMIS as ESEMIS, i.rANGNB as RANG, i.eFFECTIF as EFFECTIF, ap.nOM as
            NOMAP, ap.pRENOM as PRENOMAP, i.dUREE as DUREE, i.cOMPTEURFIN as COMPTEURFIN, i.cOMPTEURDEBUT as
            COMPTEURDEBUT, i.dATEREALISATION as DATEREALISATION, i.uSERMAJ as REALISATEUR, i.hEUREDEBUT as HEUREDEBUT,
             i.hEUREFIN as HEUREFIN, i.uPDATEDATE as UPDATEDATE, i.uPDATEUSER as UPDATEUSER')
            ->from('AgroBundle:ITKISMR', 'i')
            ->innerJoin('CultureBundle:Ferme', 'fe', 'WITH', 'fe.id = i.fERMEID')
            ->innerJoin('CultureBundle:Site', 'si', 'WITH', 'si.id = fe.sITEID')
            ->innerJoin('CultureBundle:CycleCultural', 'cc', 'WITH', 'cc.id = i.cYCLECULTURALID')
            ->innerJoin('CultureBundle:Culture', 'cu', 'WITH', 'cu.id = i.cULTUREID')
            ->innerJoin('CultureBundle:FamilleCulture', 'fc', 'WITH', 'fc.id = cu.fAMILLECULTUREID')
            ->innerJoin('CultureBundle:Ilot', 'il', 'WITH', 'il.id = cc.iLOTID')
            ->innerJoin('TechniqueBundle:Personnel', 'p', 'WITH', 'p.id = i.pRESCRIPTEURID')
            ->innerJoin('TechniqueBundle:Personnel', 'ap', 'WITH', 'ap.id = i.aPPLICATEURID')
            ->innerJoin('TechniqueBundle:Materiel', 'ma', 'WITH', 'ma.id = i.mATERIELID')
            ->innerJoin('TechniqueBundle:Materiel', 'appareil', 'WITH', 'appareil.id = i.aPPAREILID')
            ->andWhere('i.id =:identifier')
            ->setParameter('identifier',$id )
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }

    public function getAllItkismr()
    {
        $query = $this->_em->createQueryBuilder()
            ->select('i.id ,i.dATECREATION as DATECREATION, i.dATEP as DATEP, i.sURFACE
            as SURFACE, fe.lIBELLE as FERME, cc.lIBELLE as OP, cu.lIBELLE as CULTUREVARIETE, il.lIBELLE as ILOT, il
            .id as ILOTID, p.nOM as NOM, p.pRENOM as PRENOM, i.hEUREDEBUT as HEUREDEBUT, i.hEUREFIN as HEUREFIN ')
            ->from('AgroBundle:ITKISMR', 'i')
            ->innerJoin('CultureBundle:Ferme', 'fe', 'WITH', 'fe.id = i.fERMEID')
            ->innerJoin('CultureBundle:CycleCultural', 'cc', 'WITH', 'cc.id = i.cYCLECULTURALID')
            ->innerJoin('CultureBundle:Culture', 'cu', 'WITH', 'cu.id = i.cULTUREID')
            ->innerJoin('CultureBundle:Ilot', 'il', 'WITH', 'il.id = cc.iLOTID')
            ->innerJoin('TechniqueBundle:Personnel', 'p', 'WITH', 'p.id = i.pRESCRIPTEURID')
            ->getQuery()->useQueryCache(true)->useResultCache(true)->setResultCacheLifetime(3600)->getResult();
        return $query;
    }
}
