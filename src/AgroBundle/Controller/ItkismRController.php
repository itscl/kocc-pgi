<?php

namespace AgroBundle\Controller;

use AgroBundle\Entity\ITKISMEPI;
use AgroBundle\Entity\ITKISMPA;
use AgroBundle\Entity\ITKISMR;
use AgroBundle\Entity\ITKISMREPI;
use AgroBundle\Entity\ITKISMRPA;
use AgroBundle\Form\ITKISMRType;
use AgroBundle\Form\ITKISMType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

class ItkismRController extends Controller
{
    public function indexAction()
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();

            $fermes =$em->getRepository('CultureBundle:Ferme')->getFermesValide();

            return $this->render('AgroBundle:ItkismR:generer.html.twig', array(
                'fermes' => $fermes
            ));
        }
    }

    public function getinstructiondiffuseAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');
        $fermeid = $request->query->get('fermeid');
        $page = $request->query->get('page');

        $result1 = $em->getRepository('AgroBundle:ITKISM')->getInstructionDiffuseSemis($motcle, $fermeid);

        $result2 = $em->getRepository('AgroBundle:ITKPSG')->getInstructionDiffusePhyto($motcle, $fermeid);

        $result=[];

        foreach($result1 as $rs)
        {
            $rs['name']='SEMIS';
            if(!in_array($rs,$result))
            {
                array_push($result,$rs);
            }
        }

        foreach($result2 as $rs)
        {
            $rs['name']='PHYTO';
            if(!in_array($rs,$result))
            {
                array_push($result,$rs);
            }
        }

        $count = count($result1) + count($result2);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }

    public function getcultureAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');

        $result_total = $em->getRepository('CultureBundle:Culture')->getCultureValide($motcle);

        $result = $em->getRepository('CultureBundle:Culture')->getCultureValidePage($motcle, $page);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }

    public function getilotAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');

        $result_total = $em->getRepository('CultureBundle:Ilot')->getIlotValide($motcle);

        $result = $em->getRepository('CultureBundle:Ilot')->getIlotValidePage($motcle, $page);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }

    public function genererAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $user = $this->getUser()->getName();
            $date=new \DateTime();
            $date = $date->format('d-m-Y');

            $idinstruction = $request->request->get('idinstruction');

            $sql = "CALL sp_ISMRGenere('$idinstruction')";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $sql = "UPDATE ITKISM SET ETATINSTRUCTION ='REALISE' WHERE ITKISMID = $idinstruction ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $sql = "UPDATE ITKISMR SET USERMAJ = '$user', DATEREALISATION = '$date' WHERE ITKISMRID = $idinstruction ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            return $this->render('AgroBundle:ItkismR:generer.html.twig');
        }
    }

    public function realiseeAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $user = $this->getUser()->getName();

            $idinstruction = $request->query->get('id');

            $itkismr = $em->getRepository('AgroBundle:ITKISMR')->find($idinstruction);

            $ccid=$itkismr->getCYCLECULTURALID();
            $cc=$em->getRepository('CultureBundle:CycleCultural')->find($ccid);

            $cvid=$itkismr->getCULTUREID();
            $cv=$em->getRepository('CultureBundle:Culture')->find($cvid);

            $ilotid=$cc->getILOTID();
            $ilot=$em->getRepository('CultureBundle:Ilot')->find($ilotid)->getLIBELLE();

            $sql = "SELECT FN_ITK_PLANNING_PARCELLE($ilotid,'') AS result FROM dual";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->fetchAll();
            $parcelletot=$res[0]['RESULT'];

            $sql = "SELECT FN_ITK_DAS($ccid) AS result FROM dual";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $das=$result[0]['RESULT'];
//                var_dump($ccid);
//                var_dump($das); exit;

            $itkismrpa=$em->getRepository('AgroBundle:ITKISMRPA')->getAllItkismrpaByITKISMRID($idinstruction);

            $itkismrlot=$em->getRepository('AgroBundle:ITKISMRLOT')->getAllItkismrlotByITKISMRID($idinstruction);
            if(sizeof($itkismrlot)==0)
            {
                $itkismrlot['LIBELLE']='';
                $itkismrlot['QUANTITE']='';
                $itkismrlot['LOTNO']='';
                $itkismrlot['UNITE']='';
                $semence='local';
            }
            else
            {
                $itkismrlot=$itkismrlot[0];
                if(strpos($itkismrlot['LOTNO'],'IN0') !== false)
                {
                    $semence='importe';
                }
                else
                {
                    $semence='local';
                }
            }

            $itkismra=$em->getRepository('AgroBundle:ITKISMRA')->getAllItkismraByITKISMRID($idinstruction);
//                var_dump($itkismra); exit;
            if(sizeof($itkismra)==0)
            {
                $itkismra['ARTICLEID']='';
                $itkismra['ARTICLE']='';
                $itkismra['QTE']='';
                $itkismra['UNITE']='';
                $itkismra['PRIXUNITAIRE']='';
                $itkismra['QTETOT']='';
                $itkismra['PRIXTOTAL']='';
                $itkismra['COMMENTAIRE']='';
            }
            else
            {
                $itkismra=$itkismra[0];
            }

            $itkismrepi=$em->getRepository('AgroBundle:ITKISMREPI')->getAllItkismrepiByITKISMRID($idinstruction);

            $materielid=$itkismr->getMATERIELID();
            $appareilid=$itkismr->getAPPAREILID();
            $materiel='';
            $appareil='';
            if ($materielid!=null)
            {
                $materiel=$em->getRepository('TechniqueBundle:Materiel')->find($materielid)->getLIBELLE();
            }
            if($appareilid!=null)
            {
                $appareil=$em->getRepository('TechniqueBundle:Materiel')->find($appareilid)->getLIBELLE();
            }
//            var_dump($em->getRepository('TechniqueBundle:Materiel')->find($appareilid)); exit;
            $edit_form = $this->createForm(new ITKISMRType($em), $itkismr);
            $edit_form->remove('pRESCRIPTEURID');
            $edit_form->remove('tYPEMVEGETAL');
            $edit_form->remove('mODEAPPLICATION');
            $edit_form->remove('aPPLICATEURID');

            return $this->render('AgroBundle:ItkismR:realisee.html.twig',array(
                'entity' => $itkismr,
                'edit_form' => $edit_form->createView(),
                'itkismr' => $itkismr,
                'ilot' => $ilot,
                'ilotid' => $ilotid,
                'parcelles' => $parcelletot,
                'cv' => $cv,
                'cc' => $cc,
                'itkismrpa' => $itkismrpa,
                'itkismrlot' => $itkismrlot,
                'itkismra' => $itkismra,
                'itkismrepi' => $itkismrepi,
                'materiel' => $materiel,
                'appareil' => $appareil,
                'das' => $das,
                'semence' => $semence
            ));
        }
    }

    public function updateitkismrAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $user = $this->getUser()->getName();
            $date=new \DateTime();
            $date = $date->format('d-m-Y');

            $id = $request->request->get('id');
            $cvid = $request->request->get('cvid');
            $eligne = (int)$request->request->get('eligne');
            $esemis = (int)$request->request->get('esemis');
            $rang = (int)$request->request->get('rang');
            $datep = $request->request->get('datep');
            $datep=new \DateTime($datep);
            $datepformat=$datep->format('Y/m/d');
            $datepfin = $request->request->get('datepfin');
            $datepfin=new \DateTime($datepfin);
            $datepfinformat=$datep->format('Y/m/d');

            $duree = $request->request->get('duree');
            $qtesem = $request->request->get('qtesem');
            $qtesemtot = $request->request->get('qtesemtot');

//            $qtesemence=round((10000/$eligne)*(10000/$esemis)*$rang,2);

            //parcelles
            $surfaceutilisee=0;
            $itkismrpa = $em->getRepository('AgroBundle:ITKISMRPA')->findByITKISMRID($id);                     // Offset

            var_dump($itkismrpa);
            for($i=0; $i<sizeof($itkismrpa); $i++)
            {
                $parcelleid=(int)$request->request->get('id'.$i);
                $surfaceparcelle=$request->request->get('surface'.$parcelleid);
                $checked=(string)$request->request->get('checked'.$parcelleid);
                var_dump($checked);
                var_dump($surfaceparcelle);

                if($checked=='false')
                {
                    $parcellecheck='0';
                }
                else
                {
                    $parcellecheck='-1';
                    $surfaceparcelle=str_replace(',', '.',$surfaceparcelle);
                    $surfaceutilisee=($surfaceutilisee) + ($surfaceparcelle);
                }

                $sql = "UPDATE ITKISMRPA SET PARCELLECHECK ='$parcellecheck', SURFACE = '$surfaceparcelle' WHERE
                ITKISMRID = $id AND
                PARCELLEID = $parcelleid" ;
                $stmt = $conn->prepare($sql);
                $stmt->execute();

            }

            $surfaceutilisee=str_replace('.', ',',$surfaceutilisee);

            $quantite=(int)$request->request->get('quantite');
            $unite=$request->request->get('unite');
            $lotno=$request->request->get('LOTNO');
            $libelle=$request->request->get('libelle');
            $semencelotid=(int)$request->request->get('semencelotid');

            $sql = "UPDATE ITKISMRLOT SET QTE ='$quantite', LIBELLE = '$libelle', UNITE = '$unite', SEMENCELOTID
='$semencelotid', LOTNO = '$lotno' WHERE ITKISMRID =$id" ;
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            //intrant
            $intrantid=(int)$request->request->get('intrantid');
            $dose=(int)$request->request->get('dose');
            $prixunit=(int)$request->request->get('prixunit');
            $qtetot=(int)$request->request->get('qtetot');
            $prixtot=(int)$request->request->get('prixtot');
            $commentaire=$request->request->get('commentaire');
            $uniteint=$request->request->get('uniteint');

            $sql = "UPDATE ITKISMRA SET QTE = $dose, ARTICLEID = '$intrantid', PRIXUNITAIRE =
'$prixunit',
 PRIXTOTAL = '$prixtot', QTET = '$qtetot', UNITE = '$uniteint', RANG = 1, COMMENTAIRE = '$commentaire'
 WHERE ITKISMRID =
 $id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();


            $sql = "UPDATE ITKISMR SET SURFACE ='$surfaceutilisee', CULTUREID = $cvid, ELIGNE = '$eligne', ESEMIS = '$esemis',
RANGNB = '$rang', DATEP =
'$datepformat', DATEPFIN = '$datepfinformat', DUREE = '$duree', QTESEM = '$qtesem', QTESEMTOT =
'$qtesemtot',
UPDATE_USER = '$user', UPDATE_DATE = '$date', DATEMAJ = '$date'  WHERE ITKISMRID = $id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $type=$request->request->get('type');
            if($type=='mecanique')
            {
                $materielid=(int)$request->request->get('materielid');
                $appareilid=(int)$request->request->get('appareilid');
                $cdebut=(int)$request->request->get('cdebut');
                $cfin=(int)$request->request->get('cfin');
                $sql = "UPDATE ITKISMR SET APPAREILID =$appareilid, MATERIELID = $materielid, COMPTEURDEBUT =
                $cdebut, COMPTEURFIN = $cfin WHERE ITKISMRID = $id";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
            else
            {
                $effectif=(int)$request->request->get('effectif');
                $sql = "UPDATE ITKISMR SET EFFECTIF ='$effectif' WHERE ITKISMRID = $id";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }

            $hdebut = $request->request->get('heuredebut');
            $hfin = $request->request->get('heurefin');
            if($hdebut!='' && $hfin!='')
            {
                $datasdebut=explode(":",$hdebut);
                $heuredebut = new \DateTime();
                $heuredebut->setTime((int)$datasdebut[0], (int)$datasdebut[1]);
                $heuredebut=$heuredebut->format('Y/m/d H:m:s');
                $datasfin=explode(":",$hfin);
                $heurefin = new \DateTime();
                $heurefin->setTime((int)$datasfin[0], (int)$datasfin[1]);
                $heurefin=$heurefin->format('Y/m/d H:m:s');

                $sql = "UPDATE ITKISMR SET HEUREDEBUT = '$heuredebut', HEUREFIN = '$heurefin' WHERE ITKISMRID = $id";
                $stmt = $conn->prepare($sql);
                $stmt->execute();

                $opid=$em->getRepository('AgroBundle:ITKISMR')->find($id)->getCYCLECULTURALID();

                $sql = "CALL sp_MajSurfaceISMR('$id')";
                $stmt = $conn->prepare($sql);
                $stmt->execute();

                $sql = "CALL sp_ISMR_CC('$id', '$opid')";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
        }
        return $this->render('AgroBundle:Itkism:generer.html.twig');
    }


    public function clotureritkismrAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $username = $this->getUser()->getName();

            $id = $request->request->get('id');
            $hdebut = $request->request->get('heuredebut');
            $datasdebut=explode(":",$hdebut);
            $heuredebut = new \DateTime();
            $heuredebut->setTime((int)$datasdebut[0], (int)$datasdebut[1]);
            $heuredebut=$heuredebut->format('Y/m/d H:m:s');
            $hfin = $request->request->get('heurefin');
            $datasfin=explode(":",$hfin);
            $heurefin = new \DateTime();
            $heurefin->setTime((int)$datasfin[0], (int)$datasfin[1]);
            $heurefin=$heurefin->format('Y/m/d H:m:s');

            $sql = "UPDATE ITKISMR SET HEUREDEBUT = '$heuredebut', HEUREFIN = '$heurefin' WHERE ITKISMRID = $id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();


            $opid= (int)$request->request->get('opid');

            $sql = "CALL sp_MajSurfaceISMR('$id')";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $sql = "CALL sp_ISMR_CC('$id', '$opid')";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('AgroBundle:Itkism:generer.html.twig');
    }

    public function viewitkismrAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $id = $request->query->get('id');
            $itkismr = $em->getRepository('AgroBundle:ITKISMR')->getItkismr($id)[0];

            $ccid=$itkismr['OPID'];

            $sql = "SELECT FN_ITK_DAS($ccid) AS result FROM dual";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $das=$result[0]['RESULT'];

            $itkismrpa = $em->getRepository('AgroBundle:ITKISMRPA')->getAllItkismrpaByITKISMRID($id);
            $itkismrlot = $em->getRepository('AgroBundle:ITKISMRLOT')->getAllItkismrlotByITKISMRID($id)[0];
            $itkismra = $em->getRepository('AgroBundle:ITKISMRA')->getAllItkismraByITKISMRID($id)[0];
            $itkismrepi = $em->getRepository('AgroBundle:ITKISMREPI')->getAllItkismrepiByITKISMRID($id);

        }

        return $this->render('AgroBundle:ItkismR:detailsitkismr.html.twig',array(
            'itkismr' => $itkismr,
            'itkismrpa' => $itkismrpa,
            'itkismrlot' => $itkismrlot,
            'itkismra' => $itkismra,
            'itkismrepi' => $itkismrepi,
            'das' => $das
        ));
    }

    public function updateepirAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $itkismid = (int)$request->request->get('itkismrid');
        $epiid = (int)$request->request->get('epiid');
        $itkismrepi = new ITKISMREPI();
        $itkismrepi->setEPIID($epiid);
        $itkismrepi->setITKISMRID($itkismid);
        $em->persist($itkismrepi);
        $em->flush();
        return $this->render('AgroBundle:Itkism:generer.html.twig');
    }

    public function supprimerepirAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $itkismrid=(int)$request->request->get('itkismrid');
        $epiid =(int) $request->request->get('epiid');

        $sql = "DELETE FROM ITKISMREPI WHERE ITKISMRID = $itkismrid AND EPIID = $epiid";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $this->render('AgroBundle:Itkism:generer.html.twig');
    }

}
