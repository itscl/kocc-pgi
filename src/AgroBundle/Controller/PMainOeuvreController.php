<?php

namespace AgroBundle\Controller;

use AgroBundle\Entity\RendementParcelle;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PMainOeuvreController extends Controller
{
    public function flushsheetgoogleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $site = $request->query->get('site');
        $ferme = $request->query->get('ferme');
        $parcelle = $request->query->get('parcelle');
        $variete = $request->query->get('variete');
        $datesemis = $request->query->get('datesemis');
        $surface = $request->query->get('surface');
        $rend = $request->query->get('rend');

        if($site == 'NGALAM')
        {
            $site = 2;
        }
        elseif ($site == 'DIAMA')
        {
            $site = 1;
        }
        if($ferme == 'NG2')
        {
            $ferme = 24;
        }
        if($ferme == 'NG1')
        {
            $ferme = 24;
        }
        elseif ($ferme == 'DIAMA 1')
        {
            $ferme = 1;
        }

        $entity = new RendementParcelle();

        $entity->setSITE($site);
        $entity->setFERME($ferme);
        $entity->setCULTUREVARIETE($variete);
        $entity->setDATESEMIS($datesemis);
        $entity->setPARCELLE($parcelle);
        if($rend != 'undefined')
        {
            $entity->setRENDEMENTBRUT($rend);
        }
        $entity->setSURFACE($surface);

        $em->persist($entity);
        $em->flush();

        //var_dump($site);
        //elseif ($site == '')

        return $this->render('@Agro/PMainOeuvre/readsheetgoogle.html.twig');
    }

    public function readsheetgoogleAction(Request $request)
    {

        return $this->render('@Agro/PMainOeuvre/readsheetgoogle.html.twig');
    }

    public function pmaineouvreAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else {
            $em = $this->getDoctrine()->getManager();

            $itmo_detail = $em->getRepository('CultureBundle:ItMO')->getitmodetail();
            $itmo = $em->getRepository('CultureBundle:ItMO')->getitmo();

            $cache = $this->get('memcache.default');

            //$cache->delete('itmo_list1');
            //$cache->delete('itmo_list_total1');

            /*$redis = $this->container->get('snc_redis.default');

            $key_detail = "all_itmo_detail";
            $key_qtet = "all_itmo";*/


           /* if(!$redis->exists($key_qtet))
            {*/
               /* $rows1 = array();
            foreach ($itmo as $r)
            {

                $uni = $r['uNITE'];

                $qte  = $r['QTET'];
                $qte = str_replace(',', '.', $qte);
                $qte = (float)$qte;

                $sruf = $r['sURFACE'];
                $sruf = str_replace(',', '.', $sruf);
                $sruf = (float)$sruf;

                $datesemis1 = $r['DATEDEBUTSEMIS'];
                $periode1 = $r['pERIODE'];
                $cultureid1 = $r['CULTUREID'];
                $semainer = $r['SEMAINE'];

                $row1['CYCLECULTURALID'] = $r['CYCLECULTURALID'];

                $row1['CYCLECULTURAL'] = $r['CYCLECULTURAL'];
                $row1['TYPEIRRIGATION'] = $r['TYPEIRRIGATION'];
                $row1['SITE'] = $r['SITE'];
                $row1['FERME'] = $r['FERME'];
                $row1['CULTURE'] = $r['CULTURE'];
                $row1['DATEDEBUTSEMIS'] = $datesemis1;
                $row1['ILOT'] = $r['ILOT'];
                $row1['TYPEOP'] = $r['tYPEOP'];

                $row1['SURFACE'] = $sruf;

                if($uni == 'jh/ha')
                {
                    $row1['QTET'] =  $qte * $sruf;
                }
                else
                {

                    $itkcyclecv =  $em->getRepository('CultureBundle:ITKCycleCV')->findBy(array('sEMAINERECOLTE' =>$semainer, 'cULTUREID' => $cultureid1));

                    if($itkcyclecv)
                    {
                        foreach($itkcyclecv as $r2)
                        {
                            $rdtbrut = $r2->getRDTBRUT();

                            if($qte == 0)
                            {
                                $quantitef = 0;
                            }
                            else
                            {

                                $quantitef = ($rdtbrut / $qte) * $sruf;
                            }

                            $row1['QTET'] = round($quantitef,2);
                        }
                    }
                    else
                    {
                        $row['QTET'] =$qte;
                    }
                }

                array_push($rows1,$row1);
            }
            $data = array();
            foreach($rows1 as $row) {
                if(array_key_exists( $row['CYCLECULTURALID'], $data)) {
                    $data[$row['CYCLECULTURALID']]['QTET'] += $row['QTET'];
                } else {
                    $data[$row['CYCLECULTURALID']] = $row;
                }
            }*/
            //var_dump($data); exit;

               /* $redis->set($key_qtet, json_encode(array('data'=>$data)));

                $response_qtet = $redis->get($key_qtet);

                $json = json_decode($response_qtet, 1);

                $resources_qtet = $json['data'];

            }
            else
            {
                $response_qtet = $redis->get($key_qtet);

                $json = json_decode($response_qtet, 1);

                $resources_qtet = $json['data'];
            }*/

            /*if(!$redis->exists($key_detail))
            {*/
            if ($cache->get('itmo_list1'))
            {
                $rows = $cache->get('itmo_list1');
                $data = $cache->get('itmo_list_total1');

                return $this->render('AgroBundle:PMainOeuvre:pmaineouvre.html.twig', array(
                    'itmo_detail' => $rows,
                    'data' => $data,
                ));
            }
            else
            {
                $rows = array();
                foreach ($itmo_detail as $r_detail) {

                    $cyclecultural_detail = $r_detail['CYCLECULTURAL'];
                    $cycleculturalid_detail = $r_detail['CYCLECULTURALID'];
                    $site_detail = $r_detail['SITE'];
                    $siteid_detail = $r_detail['SITEID'];
                    $ferme_detail = $r_detail['FERME'];
                    $fermeid_detail = $r_detail['FERMEID'];
                    $culture_detail = $r_detail['CULTURE'];
                    $cultureid_detail = $r_detail['CULTUREID'];
                    $famillecultureid_detail = $r_detail['FAMILLECULTUREID'];
                    $typeirri_detail = $r_detail['TYPEIRRIGATION'];
                    $ilot_detail = $r_detail['ILOT'];
                    $typeop_detail = $r_detail['tYPEOP'];
                    $periode_detail = $r_detail['PERIODE'];

                    $surface_detail = $r_detail['sURFACE'];
                    $surface_detail = str_replace(',', '.', $surface_detail);
                    $surface_detail = (float)$surface_detail;

                    $datesemis_detail = $r_detail['dATEDEBUTSEMIS'];

                    $quantite_detail = $r_detail['qUANTITE'];
                    $quantite_detail = str_replace(',', '.', $quantite_detail);
                    $quantite_detail = (float)$quantite_detail;

                    $geste_detail = $r_detail['GESTE'];
                    $gesteid_detail = $r_detail['GESTEID'];
                    $mois_detail = $r_detail['MOIS'];
                    $semaine_detail = new DateTime($r_detail['SEMAINE']);
                    $semaine_detail = $semaine_detail->format('Wy');
                    $semaine1_detail = $r_detail['SEMAINE1'];
                    $unite_detail = $r_detail['uNITE'];

                    $numrecolte_detail = $r_detail['NUMRECOLTE'];
                    $pourcentrend_detail = $r_detail['POURCENTREND'];

                    $row['CYCLECULTURAL'] = $cyclecultural_detail;
                    $row['CYCLECULTURALID'] = $cycleculturalid_detail;
                    $row['SITE'] = $site_detail;
                    $row['SITEID'] = $siteid_detail;
                    $row['FERMEID'] = $fermeid_detail;
                    $row['FERME'] = $ferme_detail;
                    $row['CULTURE'] = $culture_detail;
                    $row['CULTUREID'] = $cultureid_detail;
                    $row['TYPEIRRIGATION'] = $typeirri_detail;
                    $row['SURFACE'] = $surface_detail;
                    $row['DATEDEBUTSEMIS'] = $datesemis_detail->format('d-m-Y');
                    $row['MOIS'] = $mois_detail;
                    $row['SEMAINE'] = $semaine_detail;
                    $row['GESTE'] = $geste_detail;
                    $row['GESTEID'] = $gesteid_detail;
                    $row['QUANTITE'] = $quantite_detail;
                    $row['ILOT'] = $ilot_detail;
                    $row['TYPEOP'] = $typeop_detail;
                    $row['SEMAINE_REEL'] = $semaine1_detail;
                    $row['FAMILLECULTUREID'] = $famillecultureid_detail;
                    $row['NUMRECOLTE'] = $numrecolte_detail;
                    $row['POURCENTREND'] = $pourcentrend_detail;
                    $quantite_detail_ha = $quantite_detail * $surface_detail;
                    $row['QUANTITET'] = round($quantite_detail_ha, 1);
                    $row['FAMILLECULTURE'] = $r_detail['FAMILLECULTURE'];
                    $row['SEMAINE2'] = $r_detail['SEMAINE2'];

                    if ($geste_detail == 'RECOLTE' || $geste_detail == 'MANUTENTION PRODUCTION') {

                        $semaine_semis_detail = $datesemis_detail->format("W");

                        $datesemis_detail = $datesemis_detail->format('Y-m-d');

                        $cycles_detail = $em->getRepository('CultureBundle:ITKCycle')->findBy(array
                        ('fAMILLECULTUREID' => $famillecultureid_detail,
                            'sEMAINESEMIS' => $semaine_semis_detail));

                        foreach ($cycles_detail as $cycle_detail) {
                            $dureecycle_detail = $cycle_detail->getDUREECYCLE();

                            $date_r_aj_detail = date('Y-m-d', strtotime($datesemis_detail . " +$dureecycle_detail days"));

                            $nb_jours = $numrecolte_detail * 7;

                            $date_r_aj_detail = date('Y-m-d', strtotime($date_r_aj_detail . " +$nb_jours days"));

                            $date = new DateTime($date_r_aj_detail);

                            $semaine_r_aj_detail = $date->format('W');
                            $semaine_r_aj_detail1 = $date->format('Wy');

                            $mois_r_aj_detail = $date->format('my');

                            $row['DURRECYCLE'] = $dureecycle_detail;
                            $row['SEMAINE'] = $semaine_r_aj_detail1;
                            $row['MOIS'] = $mois_r_aj_detail;

                            $itkcyclecv_detail = $em->getRepository('CultureBundle:ITKCycleCV')->findBy(array
                            ('sEMAINERECOLTE' => $semaine_r_aj_detail, 'cULTUREID' => $cultureid_detail));

                            if ($itkcyclecv_detail) {
                                foreach ($itkcyclecv_detail as $r1) {
                                    $rdtbrut_detail = $r1->getRDTBRUT();

                                    if ($pourcentrend_detail != null) {
                                        if ($quantite_detail != null) {
                                            $new_rdtbrut_detail = ($rdtbrut_detail * $pourcentrend_detail) / 100;

                                            $quantitef_detail = ($new_rdtbrut_detail / $quantite_detail) * $surface_detail;

                                            $row['QUANTITET'] = round($quantitef_detail);
                                        } else $row['QUANTITET'] = 0;
                                    } else $row['QUANTITET'] = 0;
                                }
                            } else {
                                $row['QUANTITET'] = 0;
                            }
                        }
                    }
                    $row['UNITE'] = $unite_detail;

                    array_push($rows, $row);
                }

                // var_dump($rows); exit;

                $data = array();
                foreach ($rows as $row) {
                    if (array_key_exists($row['CYCLECULTURALID'], $data)) {
                        $data[$row['CYCLECULTURALID']]['QUANTITET'] += $row['QUANTITET'];
                    } else {
                        $data[$row['CYCLECULTURALID']] = $row;
                    }
                }

                $cache->delete('itmo_list1');
                $cache->delete('itmo_list_total1');
                $cache->set('itmo_list1', $rows, false, 3600);
                $cache->set('itmo_list_total1', $data, false, 3600);

                //var_dump($rows); exit;

                return $this->render('AgroBundle:PMainOeuvre:pmaineouvre.html.twig', array(
                    'itmo_detail' => $rows,
                    'data' => $data,
                ));

            }

            //var_dump($rows); exit;

               /* $redis->set($key_detail, json_encode(array('data'=>$rows)));
                $response = $redis->get($key_detail);

                $json = json_decode($response, 1);

                $resources = $json['data'];

                $redis->set($key_qtet, json_encode(array('data'=>$data)));

                $response_qtet = $redis->get($key_qtet);

                $json = json_decode($response_qtet, 1);

                $resources_qtet = $json['data'];
            }
            else
            {
                $response = $redis->get($key_detail);

                $json = json_decode($response, 1);

                $resources = $json['data'];

                $response_qtet = $redis->get($key_qtet);

                $json = json_decode($response_qtet, 1);

                $resources_qtet = $json['data'];
            }*/


        }
    }

    public function phebdomadaireAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();


            $semaine = $request->query->get('semaine');
            $ferme = $request->query->get('ferme');

            $gestemo = $em->getRepository('CultureBundle:GesteMo')->findAll();

            $fermes = $em->getRepository('CultureBundle:Ferme')->getFermesValide();

            if($ferme != '')
            {
                $fermelibelle = $em->getRepository('CultureBundle:Ferme')->find($ferme);
            }
            else
                $fermelibelle = array();

            $data = $em->getRepository('CultureBundle:ItMO')->getitmoByGeste();

            /*$redis = $this->container->get('snc_redis.default');

            $key = "itmo_geste";

            if(!$redis->exists($key))
            {*/
                $rows = array();
                foreach ($data as $r) {

                    $unite = $r['UNITE'];
                    $surface = $r['sURFACE'];
                    $surface = str_replace(',', '.', $surface);
                    $surface = (float)$surface;

                    $quantite = $r['qUANTITE'];
                    $quantite = str_replace(',', '.', $quantite);
                    $quantite = (float)$quantite;

                    $row['CYCLECULTURAL'] = $r['CYCLECULTURAL'];
                    $row['SITE'] = $r['SITE'];
                    $row['FERME'] = $r['FERME'];
                    $row['FERMEID'] = $r['FERMEID'];
                    $row['CULTURE'] = $r['CULTURE'];
                    $row['SURFACE'] = $surface;
                    $row['DATEDEBUTSEMIS'] = $r['DATEDEBUTSEMIS'];
                    $row['GESTE'] = $r['GESTE'];
                    $row['ILOT'] = $r['ILOT'];
                    $row['UNITE'] = $unite;
                    $row['QUANTITE'] = $quantite;
                    $semaine_recolte =  $r['SEMAINE'];
                    $row['SEMAINE'] = $semaine_recolte;
                    $cultureid =  $r['CULTUREID'];
                    $row['CULTUREID'] = $cultureid;

                    if ($unite == 'jh/ha')
                    {
                        $row['QUANTITET'] = $quantite * $surface;
                    }
                    else
                    {
                        $itkcyclecv =  $em->getRepository('CultureBundle:ITKCycleCV')->findBy(array('sEMAINERECOLTE'
                        =>$semaine_recolte, 'cULTUREID' => $cultureid));

                        //var_dump($itkcyclecv);exit;

                        if($itkcyclecv)
                        {
                            foreach($itkcyclecv as $r1)
                            {
                                $rdtbrut = $r1->getRDTBRUT();

                                $quantitef = ($rdtbrut / $quantite) * $surface;

                                $row['QUANTITET'] = $quantitef;
                            }
                        }
                        else
                        {
                            $row['QUANTITET'] = "Pas de rendement";
                        }
                    }

                    array_push($rows, $row);
                }

                /*$redis->set($key, json_encode(array('data'=>$rows)));
                $response = $redis->get($key);

                $json = json_decode($response, 1);

                $resources = $json['data'];
            }
            else
            {
                $response = $redis->get($key);

                $json = json_decode($response, 1);

                $resources = $json['data'];
            }*/

            $sql = "SELECT DISTINCT Cod_AnnSem FROM Calendar ORDER BY Cod_AnnSem";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();

            $customers = array();
            foreach ($rows as $resource)
            {
                if ($semaine != null && $ferme == null)
                {
                    if ($resource['SEMAINE'] == $semaine)
                    {
                        $customers[] = $resource;
                    }

                }
                if($semaine != null && $ferme != null)
                {
                    if ($resource['SEMAINE'] == $semaine && $resource['FERMEID'] == $ferme)
                    {
                        $customers[] = $resource;
                    }


                }

            } //var_dump($fermelibelle); exit;

            return $this->render('AgroBundle:PMainOeuvre:phebdomadaire.html.twig', array(
                'geste' => $gestemo,
                'data' => $customers,
                'result' => $result,
                'semaine' => $semaine,
                'fermes' => $fermes,
                'ferme' => $fermelibelle,
            ));
        }
    }

    public function itkmoopAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $op = $request->query->get('op');
            $rend = $request->query->get('rendement');

            $em = $this->getDoctrine()->getManager();

            $itmoop = $em->getRepository('CultureBundle:ItMO')->getitmoByOP($op);

            $rows = array();

            foreach ($itmoop as $r) {

                $unite = $r['UNITE'];
                $surface = $r['sURFACE'];
                $surface = str_replace(',', '.', $surface);
                $surface = (float)$surface;

                $quantite = $r['qUANTITE'];
                $quantite = str_replace(',', '.', $quantite);
                $quantite = (float)$quantite;

                $row['CYCLECULTURAL'] = $r['CYCLECULTURAL'];
                $row['CYCLECULTURALID'] = $r['CYCLECULTURALID'];
                $row['SITE'] = $r['SITE'];
                $row['FERME'] = $r['FERME'];
                $row['CULTURE'] = $r['CULTURE'];
                $row['SURFACE'] = $surface;
                $row['DATEDEBUTSEMIS'] = $r['DATEDEBUTSEMIS'];
                $row['GESTE'] = $r['GESTE'];
                $row['QUANTITE'] = $quantite;
                $row['ILOT'] = $r['ILOT'];
                $row['UNITE'] = $unite;
                $row['MOIS'] = $r['MOIS'];
                $row['SEMAINE'] = $r['SEMAINE'];

                if ($unite == 'jh/ha') {
                    $row['QUANTITET'] = $quantite * $surface;
                } else {

                    $row['QUANTITET'] = ($rend / $quantite) * $surface;
                }

                array_push($rows, $row);
            }

            //var_dump($rows); exit;

            return new JsonResponse(array('data' => $rows));

            /* return $this->render('AgroBundle:PMainOeuvre:itkmoop.html.twig', array(
                 'itkmoop' => $rows
             ));*/
        }
    }
}
