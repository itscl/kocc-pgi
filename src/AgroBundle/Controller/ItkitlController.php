<?php

namespace AgroBundle\Controller;

use AgroBundle\Entity\EPI;
use AgroBundle\Entity\ITKITL;
use AgroBundle\Entity\ITKITLA;
use AgroBundle\Entity\ITKITLCIBLE;
use AgroBundle\Entity\ITKITLEPI;
use AgroBundle\Form\ITKITLType;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use \Doctrine\DBAL\Exception\NotNullConstraintViolationException;

class ItkitlController extends Controller
{

    public function listeinstructionlocaliseAction()
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $username = $this->getUser();
            $userfermeid=$this->getUser()->getFermeid();
            $usersiteid=$this->getUser()->getSiteid();
            $roles=$this->getUser()->getRoles();

            if (in_array('ROLE_ADMIN', $roles, true) || in_array('ROLE_SUPER_ADMIN', $roles, true)) {
                $itkitl= $em->getRepository('AgroBundle:ITKITL')->getAllItkitl();
                $itkitlr= $em->getRepository('AgroBundle:ITKITLR')->getAllItkitlr();
            }
            else
            {
                $itkitlferm=array();
                $itkitl=array();
                $itkitlr=array();

                if($usersiteid!=null)
                {

                    $fermes=$em->getRepository('CultureBundle:Ferme')->findBySITEID($usersiteid);

                    foreach($fermes as $ferme)
                    {
                        $itkitlferm= $em->getRepository('AgroBundle:ITKITL')->getAllItkitlByFerme($ferme->getId());
                        foreach($itkitlferm as $i)
                        {
                            $itkitl[] = $i;
                        }

                        $itkitlrferm= $em->getRepository('AgroBundle:ITKITLR')->getAllItkitlrByFerme($ferme->getId());
                        foreach($itkitlrferm as $i)
                        {
                            $itkitlr[] = $i;
                        }
                    }
                }
                elseif($userfermeid!=null)
                {
                    $itkitlferm= $em->getRepository('AgroBundle:ITKITL')->getAllItkitlByFerme($userfermeid);
                    foreach($itkitlferm as $i)
                    {
                        $itkitl[] = $i;
                    }

                    $itkitlrferm= $em->getRepository('AgroBundle:ITKITLR')->getAllItkitlrByFerme($userfermeid->getId());
                    foreach($itkitlrferm as $i)
                    {
                        $itkitlr[] = $i;
                    }
                }
                else
                {
                    $itkitl=[];
                    $itkitlr=[];
                }
            }

            return $this->render('AgroBundle:Itkitl:instructionlocalise.html.twig' , array(
                'itkitl'=>$itkitl,
                'itkitlr'=>$itkitlr
            ));
        }
    }

    public function ajouteritkitlAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else {
            $em = $this->getDoctrine()->getManager();

            $date = new \DateTime();
            $dateformat = $date->format('d-m-Y');

            $itkitl = new ITKITL();
            $form = $this->createForm(new ITKITLType($em), $itkitl);

            $form->remove('pRESCRIPTEURID');
            $form->remove('aPPLICATEURID');

            return $this->render('AgroBundle:Itkitl:ajouteritkitl.html.twig' , array(
                'entity'=>$itkitl,
                'form'=>$form->createView(),
                'date'=>$dateformat,
            ));
        }
    }

    public function creeritkitlAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $date = new \DateTime();

            $fermeid= $request->request->get('fermeid');
            $duree= $request->request->get('duree');
            $datep = $request->request->get('datep');
            $datep = new \DateTime($datep);
            $qtelot = $request->request->get('qteutl');
            $bouillieutl = $request->request->get('bouillieutl');
            $utlid = $request->request->get('utlid');
            $unitelot = $request->request->get('uniteutl');
            $datepfin = $request->request->get('datepfin');
            $datepfin = new \DateTime($datepfin);

            $itkitldata = $em->getRepository('AgroBundle:ITKITL')->findBy(
                array('fERMEID' => $fermeid , 'uNITETLOCALISEID' => $utlid )                        // Offset
            );
//            if(sizeof($itkitldata)==0) {

                $itkitl = new ITKITL();
                $itkitl->setBOUILLIE($bouillieutl);
                $itkitl->setFERMEID($fermeid);
                $itkitl->setQTELOT($qtelot);
                $itkitl->setDATEPFIN($datepfin);
                $itkitl->setDATEP($datep);
                $itkitl->setDUREE($duree);
                $itkitl->setUNITETLOCALISEID($utlid);
                $itkitl->setUNITELOT($unitelot);
                $itkitl->setDATECREATION($date);
                $itkitl->setETATINSTRUCTION('SAISI');
                $em->persist($itkitl);
                $em->flush();

                $itkitlid = $itkitl->getId();

                $sql = "CALL SP_ITLMETEOINSERT($itkitlid)";
                $stmt = $conn->prepare($sql);
                $stmt->execute();

                $vent=$itkitl->getVENT();
                $temperature=$itkitl->getTEMPERATURE();
                $humidite=$itkitl->getHUMIDITE();
                $pluie=$itkitl->getPLUIE();

                $return = array('itkitlid' => $itkitlid, 'vent' => $vent, 'temperature' => $temperature, 'humidite'
                => $humidite, 'pluie' => $pluie);

                $response = new Response(json_encode($return));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
//            }
//            else
//            {
//                $return = array('itkitlid' => 0);
//
//                $response = new Response(json_encode($return));
//                $response->headers->set('Content-Type', 'application/json');
//                return $response;
//            }

        }
    }

    public function modifieritkitlAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else {
            $em = $this->getDoctrine()->getManager();


            $idinstruction = $request->query->get('id');

            $itkitl=$em->getRepository('AgroBundle:ITKITL')->find($idinstruction);
            $edit_form = $this->createForm(new ITKITLType($em), $itkitl);

            $fermeid = $itkitl->getFERMEID();
            $ferme=$em->getRepository('CultureBundle:Ferme')->find($fermeid)->getLIBELLE();

            $userfermeid = $this->getUser()->getFERMEID();
            $userdiffusion = $this->getUser()->getDIFFUSION();

            if($fermeid == $userfermeid && $userdiffusion == 1)
            {
                $diffusion = 1;
            }
            else
            {
                $diffusion = 0;
            }

            $utlid=$itkitl->getUNITETLOCALISEID();
            $utldata=$em->getRepository('AgroBundle:UnitetLocalise')->find($utlid);
            $utl=$utldata->getLIBELLE();

            $cultureid=$utldata->getCULTUREID();
//            var_dump($em->getRepository('CultureBundle:Culture')->find($cultureid)); exit;
            $culture=$em->getRepository('CultureBundle:Culture')->find($cultureid);
            if($culture == null)
            {
                $culturelib='';
            }
            else
            {
               $culturelib=$culture->getLIBELLE();
            }

            $materielid=$itkitl->getMATERIELID();
            $appareilid=$itkitl->getAPPAREILID();
            $materiel='';
            $appareil='';
            if ($materielid!=null)
            {
                $materiel=$em->getRepository('TechniqueBundle:Materiel')->find($materielid)->getLIBELLE();
            }
            if($appareilid!=null)
            {
                $appareil=$em->getRepository('TechniqueBundle:Materiel')->find($appareilid)->getLIBELLE();
            }

            $chauffeurid=$itkitl->getCHAUFFEURID();
            $chauffeur='';
            if ($chauffeurid!=null)
            {
                $nom = $em->getRepository('TechniqueBundle:Personnel')->find($chauffeurid)->getNOM();
                $prenom = $em->getRepository('TechniqueBundle:Personnel')->find($chauffeurid)->getPRENOM();

                $chauffeur=$prenom.' '.$nom;
            }

            $itkitla=$em->getRepository('AgroBundle:ITKITLA')->getAllItkitlaByITKITLID($idinstruction);

            $itkitlepi=$em->getRepository('AgroBundle:ITKITLEPI')->getAllItkitlEpiByITKITLID($idinstruction);

            $itkitlcible=$em->getRepository('AgroBundle:ITKITLCIBLE')->getAllItkitlCibleByITKITLID($idinstruction);
//            var_dump($itkitlcible); exit;
            return $this->render('AgroBundle:Itkitl:modifieritkitl.html.twig' , array(
                'entity'=>$itkitl,
                'edit_form'=>$edit_form->createView(),
                'utl' => $utl,
                'culture' => $culturelib,
                'cultureid' => $cultureid,
                'ferme' => $ferme,
                'itkitl' => $itkitl,
                'materiel' => $materiel,
                'appareil' => $appareil,
                'chauffeur' => $chauffeur,
                'itkitlepi' => $itkitlepi,
                'itkitlcible' => $itkitlcible,
                'itkitla' => $itkitla,
                'diffusion' => $diffusion
            ));
        }
    }

    public function updateitkitlAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $id = $request->request->get('id');

            $prescripteurid = (int)$request->request->get('prescripteurid');
            $applicateurid = (int)$request->request->get('applicateurid');
            $chauffeurid = (int)$request->request->get('chauffeurid');
            $modeapplication = $request->request->get('modeapplication');

            $datep = $request->request->get('datep');
            $datep = new \DateTime($datep);
            $datepformat = $datep->format('Y/m/d');
            $duree = (int)$request->request->get('duree');

            $datepfin = strtotime("+".($duree-1)." day", strtotime($datepformat));
            $datepfin=new \DateTime(date('Y-m-d',$datepfin));
            $datepfinformat = $datepfin->format('Y/m/d');

            $bouillie = $request->request->get('bouillie');
            $quantite = $request->request->get('quantite');

            $materielid = $request->request->get('materielid');
            $appareilid = $request->request->get('appareilid');


            $sql = "UPDATE ITKITL SET PRESCRIPTEURID = $prescripteurid, APPLICATEURID = $applicateurid, CHAUFFEURID = $chauffeurid, MODEAPPLICATION = '$modeapplication', BOUILLIE = '$bouillie', QTELOT = '$quantite', MATERIELID =
$materielid, APPAREILID = $appareilid, DATEP = '$datepformat', DUREE = '$duree', DATEPFIN = '$datepfinformat' WHERE ITKITLID = $id ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }

        return $this->render('AgroBundle:Itkitl:index.html.twig');
    }

    public function updateepiitkitlAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlid = (int)$request->request->get('itkitlid');
            $epiid =(int)$request->request->get('epiid');

            $itkitlepidata = $em->getRepository('AgroBundle:ITKITLEPI')->findBy(
                array('iTKITLID' => $itkitlid , 'ePIID' => $epiid )                        // Offset
            );
            if(sizeof($itkitlepidata)==0) {
                $itkitlepi = new ITKITLEPI();
                $itkitlepi->setEPIID($epiid);
                $itkitlepi->setITKITLID($itkitlid);
                $em->persist($itkitlepi);
                $em->flush();

                $return = array('exist' => 'false');
            }
            else
            {
                $return = array('exist' => 'true');
            }
            $response = new Response(json_encode($return));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    public function supprimerepiitkitlAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlid=(int)$request->request->get('itkitlid');
            $epiid =(int) $request->request->get('epiid');

            $sql = "DELETE FROM ITKITLEPI WHERE ITKITLID = $itkitlid AND EPIID = $epiid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('AgroBundle:Itkism:index.html.twig');
    }

    public function updatecibleitkitlAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlid=(int)$request->request->get('itkitlid');
            $cibleid =(int) $request->request->get('cibleid');

            $itkitlcibledata = $em->getRepository('AgroBundle:ITKITLCIBLE')->findBy(
                array('iTKITLID' => $itkitlid , 'cIBLEID' => $cibleid )                        // Offset
            );
            if(sizeof($itkitlcibledata)==0) {
                $itkitlcible = new ITKITLCIBLE();
                $itkitlcible->setCIBLEID($cibleid);
                $itkitlcible->setITKITLID($itkitlid);
                $itkitlcible->setSEUILTRAITEMENT(0);
                $em->persist($itkitlcible);
                $em->flush();

                $return = array('exist' => 'false');
            }
            else
            {
                $return = array('exist' => 'true');
            }
            $response = new Response(json_encode($return));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
            }
    }

    public function supprimercibleitkitlAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlid=(int)$request->request->get('itkitlid');
            $cibleid =(int) $request->request->get('cibleid');

            $sql = "DELETE FROM ITKITLCIBLE WHERE ITKITLID = $itkitlid AND CIBLEID = $cibleid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('AgroBundle:Itkism:index.html.twig');
    }

    public function getallutlAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $ferme=$request->query->get('fermeid');
        $motcle=$request->query->get('motcle');

        $utl=$em->getRepository('AgroBundle:UnitetLocalise')->getAllUtlFerme($ferme,$motcle);

        $count = count($utl);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $utl));
    }

    public function getallintrantitkitlAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $motcle=$request->query->get('motcle');
        $cultureid=$request->query->get('cultureid');

//        $result1=$em->getRepository('TechniqueBundle:Article')->getAllIntrantItkitl($motcle);

        $sql = "SELECT ARTICLEID as id ,LIBELLE as name,'-' as Dar, '-' as Das, '-' as DDR ,'-' as
MatiereActive, '-' as DoseMini, '-' as DoseMaxi, '-' as DoseminiHL, '-' as DoseMaxiHL, '-' as NbAppliMaxi,
'-' as Culture, TABLEREF as TableRef, '-' as DateInvalide, '-' as UNITE FROM ARTICLE  WHERE TABLEREF ='HEURE' AND 
LIBELLE 
LIKE'%$motcle%'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result1 = $stmt->fetchAll();

        $count1 = count($result1);


        $sql = "SELECT ARTICLEID as id, ARTICLE as name, DAR as Dar, DAS as Das, DDR as DDR ,MATIEREACTIVE, DOSEMINIMA as
DoseMini, DOSEMAXIMA as DoseMaxi, DOSEMINIHL,DOSEMAXIHL,NBAPPLIMAXI,CULTURE,TABLEREF,DATEINVALIDE, UNITE
FROM VW_PHYTOCULTURE  WHERE CULTUREID = $cultureid AND ARTICLE LIKE'%$motcle%' ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result2 = $stmt->fetchAll();

        $count2 = count($result2);
        $count = $count1+$count2;

        $result = array_merge($result1,$result2);

        for($i=0; $i<sizeof($result); $i++)
        {
            $result[$i]['id'] = $result[$i]['ID'];
            unset($result[$i]['ID']);
            $result[$i]['name'] = $result[$i]['NAME'];
            unset($result[$i]['NAME']);
        }

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $result));
    }

    public function getallcibleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle=$request->query->get('motcle');

        $cibles=$em->getRepository('AgroBundle:Cible')->getAllCible($motcle);

        $count = count($cibles);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $cibles));
    }

    public function qtetotutlAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $unite=$request->query->get('unite');
        $dose =$request->query->get('dose');
        $bouillie=$request->query->get('bouillie');

        $sql = "SELECT FN_ITK_QTET('$unite', '$dose', 1, '$bouillie') AS result FROM dual";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return new JsonResponse($res);
    }

    public function ajoutintrantitkitlAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlid= (int)$request->request->get('id');
            $intrantid = (int)$request->request->get('intrantid');
            $rang = (int)$request->request->get('rang');
            $dose = $request->request->get('dose');
            $unite = $request->request->get('unite');
            $bouillie = $request->request->get('bouillie');
            $qtetot = $request->request->get('qtetot');
            $comment = $request->request->get('comment');

            $itkitlasize= sizeof($em->getRepository('AgroBundle:ITKITLA')->findBy(
                array('aRTICLEID' => $intrantid , 'iTKITLID' => $itkitlid )));
            if($itkitlasize == 0)
            {
                $itkitla = new ITKITLA();
                $itkitla->setITKITLID($itkitlid);
                $itkitla->setARTICLEID($intrantid);
                $itkitla->setRANG($rang );
                $itkitla->setQTE($dose);
                $itkitla->setUNITE($unite);
                $itkitla->setBOUILLIE($bouillie);
                $itkitla->setQTET($qtetot);
                $itkitla->setCOMMENTAIRE($comment);
                $itkitla->setPRIXUNITAIRE(0);
                $itkitla->setPRIXTOTAL(0);
                $itkitla->setQTEBC(0);

                $em->persist($itkitla);
                $em->flush();

                $itkitlaid = $itkitla->getId();

                $return = array('itkitlaid' => $itkitlaid);

            }
            else
            {
                $return = array('itkitlaid' => 0);
            }

        }
//        return $this->render('AgroBundle:Itkism:index.html.twig');
        $response = new Response(json_encode($return));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function supprimeritkitlaAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlaid =(int) $request->request->get('itkitlaid');

            $sql = "DELETE FROM ITKITLA WHERE ITKITLAID = $itkitlaid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('AgroBundle:Itkism:index.html.twig');
    }

    public function annuleritkitlAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $id = $request->request->get('id');

            $sql = "UPDATE ITKITL SET ETATINSTRUCTION = 'ANNULE' WHERE ITKITLID = $id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }

        return $this->render('AgroBundle:Itkitl:index.html.twig');
    }

    public function diffuseritkitlAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $id = $request->request->get('id');

        $sql = "UPDATE ITKITL SET ETATINSTRUCTION ='DIFFUSE' WHERE ITKITLID = $id";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $this->render('AgroBundle:Itkitl:index.html.twig');
    }

    public function viewitkitlAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else {
            $em = $this->getDoctrine()->getManager();

            $idinstruction = $request->query->get('id');

            $itkitl=$em->getRepository('AgroBundle:ITKITL')->find($idinstruction);
            $edit_form = $this->createForm(new ITKITLType($em), $itkitl);

            $fermeid = $itkitl->getFERMEID();
            $ferme=$em->getRepository('CultureBundle:Ferme')->find($fermeid)->getLIBELLE();

            $utlid=$itkitl->getUNITETLOCALISEID();
            $utldata=$em->getRepository('AgroBundle:UnitetLocalise')->find($utlid);
            $utl=$utldata->getLIBELLE();

            $cultureid=$utldata->getCULTUREID();
            $culture=$em->getRepository('CultureBundle:Culture')->find($cultureid);
            if($culture == null)
            {
                $culturelib='';
            }
            else
            {
                $culturelib=$culture->getLIBELLE();
            }

            $materielid=$itkitl->getMATERIELID();
            $appareilid=$itkitl->getAPPAREILID();
            $materiel='';
            $appareil='';
            if ($materielid!=null)
            {
                $materiel=$em->getRepository('TechniqueBundle:Materiel')->find($materielid)->getLIBELLE();
            }
            if($appareilid!=null)
            {
                $appareil=$em->getRepository('TechniqueBundle:Materiel')->find($appareilid)->getLIBELLE();
            }

            $chauffeurid=$itkitl->getCHAUFFEURID();
            $chauffeur='';
            if ($chauffeurid!=null)
            {
                $nom = $em->getRepository('TechniqueBundle:Personnel')->find($chauffeurid)->getNOM();
                $prenom = $em->getRepository('TechniqueBundle:Personnel')->find($chauffeurid)->getPRENOM();

                $chauffeur=$prenom.' '.$nom;
            }

            $itkitla=$em->getRepository('AgroBundle:ITKITLA')->getAllItkitlaByITKITLID($idinstruction);

            $itkitlepi=$em->getRepository('AgroBundle:ITKITLEPI')->getAllItkitlEpiByITKITLID($idinstruction);

            $itkitlcible=$em->getRepository('AgroBundle:ITKITLCIBLE')->getAllItkitlCibleByITKITLID($idinstruction);

            return $this->render('AgroBundle:Itkitl:detailsitkitl.html.twig' , array(
                'entity'=>$itkitl,
                'edit_form'=>$edit_form->createView(),
                'utl' => $utl,
                'culture' => $culturelib,
                'cultureid' => $cultureid,
                'ferme' => $ferme,
                'itkitl' => $itkitl,
                'materiel' => $materiel,
                'appareil' => $appareil,
                'chauffeur' => $chauffeur,
                'itkitlepi' => $itkitlepi,
                'itkitlcible' => $itkitlcible,
                'itkitla' => $itkitla
            ));
        }
    }

    public function imprimeritkitlAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $id = $request->query->get('id');

        $statut=$em->getRepository('AgroBundle:ITKITL')->find($id)->getETATINSTRUCTION();

        if($statut!='DIFFUSE')
        {
            $this->get('session')->getFlashBag()->add('notice', "Veuillez d'abord diffuser cette instruction !!");
            return $this->redirect($this->generateUrl('modifieritkitl', array('id' =>$id)));
        }
        else
        {
            $intrant=$em->getRepository('AgroBundle:ITKITLA')->findByITKITLID($id);

            if(sizeof($intrant)==0 )
            {
                $this->get('session')->getFlashBag()->add('notice', "Veuillez compléter les informations !!");
                return $this->redirect($this->generateUrl('modifieritkitl', array('id' =>$id)));
            }
            else {

                $date = new \DateTime();
                $dateformat = $date->format('d/m/Y');

                $itkitl = $em->getRepository('AgroBundle:ITKITL')->getItkitl($id)[0];

                $itkitlentity=$em->getRepository('AgroBundle:ITKITL')->find($id);
//                var_dump($itkitl); exit;

                $itkitla = $em->getRepository('AgroBundle:ITKITLA')->getAllItkitlaByITKITLID($id);
                $itkitlatype = [];
                foreach ($itkitla as $i)
                {
                    $articleid= $i['ARTICLEID'];
//                    var_dump($articleid);
                    $sql = "SELECT ARTICLEID FROM VW_PHYTOCULTURE WHERE ARTICLEID = $articleid";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                    $result = $stmt->fetchAll();

                    if (sizeof($result) == 0)
                    {
                        $itkitlatype[] = 'Heure';
                    }
                    else
                    {
                        $itkitlatype[] = 'Phyto';
                    }
                }
                $itkitlepi = $em->getRepository('AgroBundle:ITKITLEPI')->getAllItkitlEpiByITKITLID($id);

                $itkitlcible = $em->getRepository('AgroBundle:ITKITLCIBLE')->getAllItkitlCibleByITKITLID($id);

                $sql = "SELECT CIBLES FROM VW_ITKITL WHERE NOINST = $id";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $cibles = $result[0]['CIBLES'];
//                var_dump($cibles); exit;

                $html = $this->renderView('AgroBundle:ITKITL:imprimeritkitl.html.twig', array
                ('itkitl' =>$itkitl, 'itkitlentity' => $itkitlentity, 'date' => $dateformat, 'cibles' => $cibles, 'itkitla'
                => $itkitla, 'itkitlepi' => $itkitlepi, 'itkitlcible' => $itkitlcible, 'itkitlatype' => $itkitlatype));
                //on appelle le service html2pdf
                $html2pdf = $this->get('html2pdf_factory')->create('P', 'A4', 'fr', true, 'UTF-8', array(15, 10, 15, 10));
                //real : utilise la taille r�elle
                $html2pdf->pdf->SetDisplayMode('real');
                //writeHTML va tout simplement prendre la vue stocker dans la variable $html pour la convertir en format PDF
                $html2pdf->writeHTML($html);
                //Output envoit le document PDF au navigateur internet
                return new Response($html2pdf->Output('Instruction-semis.pdf'), 200, array('Content-Type' =>
                    'application/pdf'));
            }
        }
    }

}