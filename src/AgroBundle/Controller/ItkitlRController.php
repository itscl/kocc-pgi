<?php

namespace AgroBundle\Controller;

use AgroBundle\Entity\ITKISMEPI;
use AgroBundle\Entity\ITKISMPA;
use AgroBundle\Entity\ITKISMR;
use AgroBundle\Entity\ITKISMREPI;
use AgroBundle\Entity\ITKISMRPA;
use AgroBundle\Entity\ITKITLRA;
use AgroBundle\Entity\ITKITLRCIBLE;
use AgroBundle\Entity\ITKITLREPI;
use AgroBundle\Form\ITKISMRType;
use AgroBundle\Form\ITKISMType;
use AgroBundle\Form\ITKITLRType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

class ItkitlRController extends Controller
{
    public function indexAction()
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else {

            $em = $this->getDoctrine()->getManager();

            $fermes =$em->getRepository('CultureBundle:Ferme')->getFermesValide();

            return $this->render('AgroBundle:ItkitlR:generer.html.twig', array(
                'fermes' => $fermes
            ));
        }
    }

    public function getinstructiondiffuseutlAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');
        $fermeid = $request->query->get('fermeid');
        $page = $request->query->get('page');

        $result = $em->getRepository('AgroBundle:ITKITL')->getInstructionDiffuseLocalise($motcle, $fermeid);

        $count = count($result);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }
//
//    public function getcultureAction(Request $request)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $motcle = $request->query->get('motcle');
//
//        $page = $request->query->get('page');
//
//        $result_total = $em->getRepository('CultureBundle:Culture')->getCultureValide($motcle);
//
//        $result = $em->getRepository('CultureBundle:Culture')->getCultureValidePage($motcle, $page);
//
//        $count = count($result_total);
//
//        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
//        => $result));
//    }
//
//    public function getilotAction(Request $request)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $motcle = $request->query->get('motcle');
//
//        $page = $request->query->get('page');
//
//        $result_total = $em->getRepository('CultureBundle:Ilot')->getIlotValide($motcle);
//
//        $result = $em->getRepository('CultureBundle:Ilot')->getIlotValidePage($motcle, $page);
//
//        $count = count($result_total);
//
//        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
//        => $result));
//    }
//
    public function genererAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $user = $this->getUser()->getName();
            $date=new \DateTime();
            $date = $date->format('d-m-Y');

            $idinstruction = $request->request->get('idinstruction');

            $sql = "CALL sp_ITLRGenere('$idinstruction')";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $sql = "UPDATE ITKITL SET ETATINSTRUCTION ='REALISE' WHERE ITKITLID = $idinstruction ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $sql = "UPDATE ITKITLR SET USERMAJ = '$user', DATEREALISATION = '$date' WHERE ITKITLRID = $idinstruction ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            return $this->render('AgroBundle:ItkitlR:generer.html.twig');
        }
    }

    public function realiseeAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $user = $this->getUser()->getName();

            $id = $request->query->get('id');

            $itkitlr = $em->getRepository('AgroBundle:ITKITLR')->getItkitlr($id)[0];

            $itkitlrentity=$em->getRepository('AgroBundle:ITKITLR')->find($id);

            $utlid=$itkitlrentity->getUNITETLOCALISEID();
            $utldata=$em->getRepository('AgroBundle:UnitetLocalise')->find($utlid);

            $cultureid=$utldata->getCULTUREID();
            $culture=$em->getRepository('CultureBundle:Culture')->find($cultureid);
            if($culture == null)
            {
                $culturelib='';
            }
            else
            {
                $culturelib=$culture->getLIBELLE();
            }

            $itkitlra = $em->getRepository('AgroBundle:ITKITLRA')->getAllItkitlraByITKITLRID($id);

            $itkitlrepi = $em->getRepository('AgroBundle:ITKITLREPI')->getAllItkitlrEpiByITKITLRID($id);

            $itkitlrcible = $em->getRepository('AgroBundle:ITKITLRCIBLE')->getAllItkitlrCibleByITKITLRID($id);

            $edit_form = $this->createForm(new ITKITLRType($em), $itkitlrentity);

            return $this->render('AgroBundle:ItkitlR:realisee.html.twig',array(
                'entity' => $itkitlr,
                'edit_form' => $edit_form->createView(),
                'itkitlr' => $itkitlr,
                'itkitlrentity' => $itkitlrentity,
                'itkitlra' => $itkitlra,
                'itkitlrepi' => $itkitlrepi,
                'itkitlrcible' => $itkitlrcible,
                'culture' => $culturelib,
                'cultureid' => $cultureid
            ));
        }
    }

    public function updateitkitlrAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $username = $this->getUser()->getName();
            $date=new \DateTime();
            $date = $date->format('d-m-Y');

            $id = $request->request->get('id');

            $prescripteurid = (int)$request->request->get('prescripteurid');
            $applicateurid = (int)$request->request->get('applicateurid');
            $chauffeurid = (int)$request->request->get('chauffeurid');
            $modeapplication = $request->request->get('modeapplication');

            $datep = $request->request->get('datep');
            $datep = new \DateTime($datep);
            $datepformat = $datep->format('Y/m/d');
            $duree = (int)$request->request->get('duree');

            $datepfin = strtotime("+".($duree-1)." day", strtotime($datepformat));
            $datepfin=new \DateTime(date('Y-m-d',$datepfin));
            $datepfinformat = $datepfin->format('Y/m/d');

            $bouillie = $request->request->get('bouillie');
            $quantite = $request->request->get('quantite');

            $materielid = $request->request->get('materielid');
            $appareilid = $request->request->get('appareilid');

            $vent = $request->request->get('vent');
            $temperature = $request->request->get('temperature');
            $humidite = $request->request->get('humidite');
            $pluie = $request->request->get('pluie');


            $sql = "UPDATE ITKITLR SET PRESCRIPTEURID = $prescripteurid, APPLICATEURID = $applicateurid, CHAUFFEURID = $chauffeurid, MODEAPPLICATION = '$modeapplication', BOUILLIE = '$bouillie', QTELOT = '$quantite', MATERIELID =
$materielid, APPAREILID = $appareilid, DATEP = '$datepformat', DUREE = '$duree', DATEPFIN = 
'$datepfinformat', VENT = '$vent', TEMPERATURE = '$temperature', HUMIDITE = '$humidite', PLUIE = 
'$pluie', UPDATE_USER = '$username', UPDATE_DATE = '$date'  WHERE 
ITKITLRID = $id ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $hdebut = $request->request->get('hdebut');
            $hfin = $request->request->get('hfin');
            if($hdebut!='' && $hfin!='')
            {
                $datasdebut=explode(":",$hdebut);
                $heuredebut = new \DateTime();
                $heuredebut->setTime((int)$datasdebut[0], (int)$datasdebut[1]);
                $heuredebut=$heuredebut->format('Y/m/d H:m:s');
                $datasfin=explode(":",$hfin);
                $heurefin = new \DateTime();
                $heurefin->setTime((int)$datasfin[0], (int)$datasfin[1]);
                $heurefin=$heurefin->format('Y/m/d H:m:s');

                $sql = "UPDATE ITKITLR SET HEUREDEBUT = '$heuredebut', HEUREFIN = '$heurefin' WHERE ITKITLRID = $id";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
        }

        return $this->render('AgroBundle:Itkitl:index.html.twig');
    }

    public function ajoutintrantitkitlrAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlrid= (int)$request->request->get('id');
            $intrantid = (int)$request->request->get('intrantid');
            $rang = (int)$request->request->get('rang');
            $dose = $request->request->get('dose');
            $unite = $request->request->get('unite');
            $bouillie = $request->request->get('bouillie');
            $qtetot = $request->request->get('qtetot');
            $comment = $request->request->get('comment');

            $itkitlrasize= sizeof($em->getRepository('AgroBundle:ITKITLRA')->findBy(
                array('aRTICLEID' => $intrantid , 'iTKITLRID' => $itkitlrid )));
            if($itkitlrasize == 0)
            {
                $itkitlra = new ITKITLRA();
                $itkitlra->setITKITLRID($itkitlrid);
                $itkitlra->setARTICLEID($intrantid);
                $itkitlra->setRANG($rang );
                $itkitlra->setQTE($dose);
                $itkitlra->setUNITE($unite);
                $itkitlra->setBOUILLIE($bouillie);
                $itkitlra->setQTET($qtetot);
                $itkitlra->setCOMMENTAIRE($comment);
                $itkitlra->setPRIXUNITAIRE(0);
                $itkitlra->setPRIXTOTAL(0);

                $em->persist($itkitlra);
                $em->flush();

                $itkitlraid = $itkitlra->getId();

                $return = array('itkitlraid' => $itkitlraid);

            }
            else
            {
                $return = array('itkitlraid' => 0);
            }

        }
        $response = new Response(json_encode($return));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function supprimeritkitlraAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlraid =(int) $request->request->get('itkitlraid');

            $sql = "DELETE FROM ITKITLRA WHERE ITKITLRAID = $itkitlraid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('AgroBundle:Itkitl:index.html.twig');
    }

    public function updateepiitkitlrAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlrid = (int)$request->request->get('itkitlrid');
            $epiid =(int)$request->request->get('epiid');

            $itkitlrepidata = $em->getRepository('AgroBundle:ITKITLREPI')->findBy(
                array('iTKITLRID' => $itkitlrid , 'ePIID' => $epiid )                        // Offset
            );
            if(sizeof($itkitlrepidata)==0) {
                $itkitlrepi = new ITKITLREPI();
                $itkitlrepi->setEPIID($epiid);
                $itkitlrepi->setITKITLRID($itkitlrid);
                $em->persist($itkitlrepi);
                $em->flush();

                $return = array('exist' => 'false');
            }
            else
            {
                $return = array('exist' => 'true');
            }
            $response = new Response(json_encode($return));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    public function supprimerepiitkitlrAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlrid=(int)$request->request->get('itkitlrid');
            $epiid =(int) $request->request->get('epiid');

            $sql = "DELETE FROM ITKITLREPI WHERE ITKITLRID = $itkitlrid AND EPIID = $epiid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('AgroBundle:Itkitl:index.html.twig');
    }

    public function updatecibleitkitlrAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlrid=(int)$request->request->get('itkitlrid');
            $cibleid =(int) $request->request->get('cibleid');

            $itkitlrcibledata = $em->getRepository('AgroBundle:ITKITLRCIBLE')->findBy(
                array('iTKITLRID' => $itkitlrid , 'cIBLEID' => $cibleid )                        // Offset
            );
            if(sizeof($itkitlrcibledata)==0) {
                $itkitlrcible = new ITKITLRCIBLE();
                $itkitlrcible->setCIBLEID($cibleid);
                $itkitlrcible->setITKITLRID($itkitlrid);
                $itkitlrcible->setSEUILTRAITEMENT(0);
                $em->persist($itkitlrcible);
                $em->flush();

                $return = array('exist' => 'false');
            }
            else
            {
                $return = array('exist' => 'true');
            }
            $response = new Response(json_encode($return));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    public function supprimercibleitkitlrAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkitlrid=(int)$request->request->get('itkitlrid');
            $cibleid =(int) $request->request->get('cibleid');

            $sql = "DELETE FROM ITKITLRCIBLE WHERE ITKITLRID = $itkitlrid AND CIBLEID = $cibleid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('AgroBundle:Itkitl:index.html.twig');
    }

    public function viewitkitlrAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else {
            $em = $this->getDoctrine()->getManager();


            $id = $request->query->get('id');

            $itkitlrentity=$em->getRepository('AgroBundle:ITKITLR')->find($id);

            $itkitlr =$em->getRepository('AgroBundle:ITKITLR')->getItkitlr($id)[0];

            $itkitlra = $em->getRepository('AgroBundle:ITKITLRA')->getAllItkitlraByITKITLRID($id);

            $itkitlrepi = $em->getRepository('AgroBundle:ITKITLREPI')->getAllItkitlrEpiByITKITLRID($id);

            $itkitlrcible = $em->getRepository('AgroBundle:ITKITLRCIBLE')->getAllItkitlrCibleByITKITLRID($id);

            return $this->render('AgroBundle:Itkitlr:detailsitkitlr.html.twig' , array(
                'itkitlr' => $itkitlr,
                'itkitlrentity' => $itkitlrentity,
                'itkitlrepi' => $itkitlrepi,
                'itkitlrcible' => $itkitlrcible,
                'itkitlra' => $itkitlra
            ));
        }
    }
}
