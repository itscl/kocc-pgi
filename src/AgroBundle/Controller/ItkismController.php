<?php

namespace AgroBundle\Controller;

use AgroBundle\Entity\EPI;
use AgroBundle\Entity\ITKISM;
use AgroBundle\Entity\ITKISMA;
use AgroBundle\Entity\ITKISMEPI;
use AgroBundle\Entity\ITKISMLOT;
use AgroBundle\Entity\ITKISMPA;
use AgroBundle\Form\ITKISMType;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use \Doctrine\DBAL\Exception\NotNullConstraintViolationException;

class ItkismController extends Controller
{
    public function listeinstructionsemisAction()
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $response = new Response();
            $response->setMaxAge(300);

            if (!$response->isNotModified($this->getRequest()))
            {
                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();

                $username = $this->getUser();
                $userfermeid=$this->getUser()->getFermeid();
                $usersiteid=$this->getUser()->getSiteid();
                $roles=$this->getUser()->getRoles();

                if (in_array('ROLE_ADMIN', $roles, true) || in_array('ROLE_SUPER_ADMIN', $roles, true)) {
                    $itkism= $em->getRepository('AgroBundle:ITKISM')->getAllItkism();
                    $itkismr= $em->getRepository('AgroBundle:ITKISMR')->getAllItkismr();
                }
                else
                {
                    $itkismferm=array();
                    $itkism=array();
                    $itkismr=array();

                    if($usersiteid!=null)
                    {
                        $fermes=$em->getRepository('CultureBundle:Ferme')->findBySITEID($usersiteid);

                        foreach($fermes as $ferme)
                        {
                            $itkismferme= $em->getRepository('AgroBundle:ITKISM')->getAllItkismByFerme($ferme->getId());
                            foreach($itkismferme as $i)
                            {
                                $itkism[] = $i;
                            }

                            $itkismrferme= $em->getRepository('AgroBundle:ITKISMR')->getAllItkismrByFerme($ferme->getId());
                            foreach($itkismrferme as $i)
                            {
                                $itkismr[] = $i;
                            }
                        }
                    }
                    elseif($userfermeid!=null)
                    {
                        $itkismferme= $em->getRepository('AgroBundle:ITKISM')->getAllItkismByFerme($userfermeid);
                        foreach($itkismferme as $i)
                        {
                            $itkism[] = $i;
                        }

                        $itkismrferme= $em->getRepository('AgroBundle:ITKISMR')->getAllItkismrByFerme($userfermeid->getId());
                        foreach($itkismrferme as $i)
                        {
                            $itkismr[] = $i;
                        }
                    }
                    else
                    {
                        $itkism=[];
                        $itkismr=[];
                    }

                    for($i=0; $i<sizeof($itkism); $i++)
                    {
                        $ilotid=$itkism[$i]['ILOTID'];
                        $sql = "SELECT FN_ITK_PLANNING_PARCELLE($ilotid,'') AS result FROM dual";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();
                        $res = $stmt->fetchAll();
                        $result=$res[0]['RESULT'];
                        $itkism[$i]['PARCELLE']=$result;
                    }

                    for($i=0; $i<sizeof($itkismr); $i++)
                    {
                        $ilotid=$itkismr[$i]['ILOTID'];
                        $sql = "SELECT FN_ITK_PLANNING_PARCELLE($ilotid,'') AS result FROM dual";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();
                        $res = $stmt->fetchAll();
                        $result=$res[0]['RESULT'];
                        $itkismr[$i]['PARCELLE']=$result;
                    }
                }
            }

            if(sizeof($itkism)==0)
            {
                return $this->render('AgroBundle:Itkism:index.html.twig');
            }
            else
            {

//                var_dump($itkismr); exit;
                return $this->render('AgroBundle:Itkism:instructionsemis.html.twig' , array(
                    'itkism'=>$itkism,
                    'itkismr'=>$itkismr
                ));
            }
        }
    }

    public function ajouteritkismAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else {
            $em = $this->getDoctrine()->getManager();

            $date = new \DateTime();
            $dateformat = $date->format('d-m-Y');

            $itkism = new ITKISM();
            $form = $this->createForm(new ITKISMType($em), $itkism);

            return $this->render('AgroBundle:Itkism:ajouteritkism.html.twig' , array(
                'entity'=>$itkism,
                'form'=>$form->createView(),
                'date'=>$dateformat,
            ));
        }
    }

    public function modifieritkismAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $date = new \DateTime();
            $dateformat = $date->format('d-m-Y');

            $fermeid = $request->query->get('fermeid');

            $userfermeid = $this->getUser()->getFERMEID();
            $userdiffusion = $this->getUser()->getDIFFUSION();

            if($fermeid == $userfermeid && $userdiffusion == 1)
            {
                $diffusion = 1;
            }
            else
            {
                $diffusion = 0;
            }

            $itkism = new ITKISM();

            if($fermeid!=null)
            {
                $ccid = $request->query->get('opid');

                $itkismdata = $em->getRepository('AgroBundle:ITKISM')->findBy(
                    array('fERMEID' => $fermeid , 'cYCLECULTURALID' => $ccid )                        // Offset
                );
                if(sizeof($itkismdata)==0)
                {
                    $itkism->setFERMEID($fermeid);
                    $itkism->setCYCLECULTURALID($ccid);
                    $itkism->setDATECREATION($date);
                    $itkism->setETATINSTRUCTION('SAISI');
                    $em->persist($itkism);
                    $em->flush();

                    $ferme=$em->getRepository('CultureBundle:Ferme')->find($fermeid)->getLIBELLE();

                    $cc=$em->getRepository('CultureBundle:CycleCultural')->find($ccid);

                    $cvid=$cc->getCULTUREID();
                    $cv=$em->getRepository('CultureBundle:Culture')->find($cvid);

                    $fcid=$cv->getFAMILLECULTUREID();
                    $fc=$em->getRepository('CultureBundle:FamilleCulture')->find($fcid)->getLIBELLE();

                    $ti=$cc->getTYPEIMPLANTATION();

                    $ilotid=(int)$cc->getILOTID();
                    $ilot=$em->getRepository('CultureBundle:Ilot')->find($ilotid)->getLIBELLE();

                    $sql = "SELECT FN_ITK_PLANNING_PARCELLE($ilotid,'') AS result FROM dual";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                    $res = $stmt->fetchAll();
                    $parcelletot=$res[0]['RESULT'];

                    $sql = "SELECT FN_ITK_DAS($ccid) AS result FROM dual";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                    $result = $stmt->fetchAll();
                    $das=$result[0]['RESULT'];

                    $ilotid=(int)$cc->getILOTID();
                    $ilot=$em->getRepository('CultureBundle:Ilot')->find($ilotid)->getLIBELLE();

                    $dateprev=$cc->getDATEPREVISRECOLTE();

                    $surface=$em->getRepository('CultureBundle:Ilot')
                        ->getSurfaceTot($ilotid)[0]['SURFACE'];

                    $itkism->setSURFACE($surface);
                    $itkism->setSURFACETOT($surface);

                    $form = $this->createForm(new ITKISMType($em), $itkism);

                    $parcelles=$em->getRepository('CultureBundle:Ilot')
                        ->getAllParcelleIlot($ilotid);

                    for($i=0; $i<sizeof($parcelles); $i++)
                    {
                        $parcelle=$em->getRepository('CultureBundle:Parcelle')->findByLIBELLE($parcelles[$i]['PARCELLE']);
                        $typeirrigationid=$parcelle[0]->getTYPEIRRIGATIONID();
                        if($typeirrigationid==1)
                        {
                            $parcelles[$i]['TYPEIRRIGATIONID']='1';
                        }
                        else
                        {
                            $parcelles[$i]['TYPEIRRIGATIONID']='0';
                        }
                    }

//                $parcelles =[];
                    $itkismepi=[];
//
                    return $this->render('AgroBundle:Itkism:ajouteritkism1.html.twig' , array(
                        'itkism'=>$itkism,
                        'entity' => $itkism,
                        'form' => $form->createView(),
                        'cc' => $cc,
                        'parcelles' => $parcelles,
                        'itkismepi' => $itkismepi,
                        'fc' => $fc,
                        'cv' => $cv,
                        'cvid' => $cvid,
                        'ti' => $ti,
                        'dateprev' => $dateprev,
                        'das' => $das,
                        'parcellestot' => $parcelletot,
                        'ilot' => $ilot,
                        'ferme' => $ferme,
                        'surface' => $surface,
                        'diffusion' => $diffusion

                    ));
                }
                else
                {
                    $itkism=$itkismdata[0];
                    $this->get('session')->getFlashBag()->add('notice', "Il existe déja une instruction de semis avec
                     cette ferme et cette op. !");
                    return $this->render('AgroBundle:Itkism:ajouteritkism.html.twig');
                }

            }
            else
            {

                $idinstruction = $request->query->get('id');

                $itkism=$em->getRepository('AgroBundle:ITKISM')->find($idinstruction);
                $edit_form = $this->createForm(new ITKISMType($em), $itkism);

                $fermeid = $itkism->getFERMEID();
                $ferme=$em->getRepository('CultureBundle:Ferme')->find($fermeid)->getLIBELLE();

                $ccid=$itkism->getCYCLECULTURALID();
                $cc=$em->getRepository('CultureBundle:CycleCultural')->find($ccid);

                $cvid=$itkism->getCULTUREID();
                $cv=$em->getRepository('CultureBundle:Culture')->find($cvid);

                $fcid=$cv->getFAMILLECULTUREID();
                $fc=$em->getRepository('CultureBundle:FamilleCulture')->find($fcid)->getLIBELLE();

                $ti=$cc->getTYPEIMPLANTATION();

                $dateprev=$cc->getDATEPREVISRECOLTE();

                $surface=$itkism->getSURFACE();
                $surfacetot=$itkism->getSURFACETOT();
                $percentsurface = round((100*((int)str_replace(',', '.',$surface)))/((int)str_replace(',', '.',
                        $surfacetot)),2);

                $ilotid=(int)$cc->getILOTID();
                $ilot=$em->getRepository('CultureBundle:Ilot')->find($ilotid)->getLIBELLE();

                $sql = "SELECT FN_ITK_PLANNING_PARCELLE('$ilotid','') AS result FROM dual";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $res = $stmt->fetchAll();
                $parcelletot=$res[0]['RESULT'];

                $sql = "SELECT FN_ITK_DAS($ccid) AS result FROM dual";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $das=$result[0]['RESULT'];
//                var_dump($ccid);
//                var_dump($das); exit;

                $itkismpa=$em->getRepository('AgroBundle:ITKISMPA')->getAllItkismpaByITKISMID($idinstruction);

                $itkismlot=$em->getRepository('AgroBundle:ITKISMLOT')->getAllItkismlotByITKISMID($idinstruction);

                $itkisma=$em->getRepository('AgroBundle:ITKISMA')->getAllItkismaByITKISMID($idinstruction);
//                var_dump($itkisma); exit;
                if(sizeof($itkisma)==0)
                {
                    $itkisma['ARTICLEID']='';
                    $itkisma['ARTICLE']='';
                    $itkisma['QTE']='';
                    $itkisma['UNITE']='';
                    $itkisma['PRIXUNITAIRE']='';
                    $itkisma['QTETOT']='';
                    $itkisma['PRIXTOTAL']='';
                    $itkisma['COMMENTAIRE']='';
                }
                else
                {
                    $itkisma=$itkisma[0];
                }

                $itkismepi=$em->getRepository('AgroBundle:ITKISMEPI')->getAllItkismepiByITKISMID($idinstruction);

                $materielid=$itkism->getMATERIELID();
                $appareilid=$itkism->getAPPAREILID();
                $materiel='';
                $appareil='';
                if ($materielid!=null)
                {
                    $materiel=$em->getRepository('TechniqueBundle:Materiel')->find($materielid)->getLIBELLE();
                }
                if($appareilid!=null)
                {
                    $appareil=$em->getRepository('TechniqueBundle:Materiel')->find($appareilid)->getLIBELLE();
                }

                return $this->render('AgroBundle:Itkism:modifieritkism.html.twig' , array(
                    'itkism'=>$itkism,
                    'date'=>$dateformat,
                    'entity' => $itkism,
                    'edit_form' => $edit_form->createView(),
                    'ilot' => $ilot,
                    'ilotid' => $ilotid,
                    'parcelles' => $parcelletot,
                    'cv' => $cv,
                    'cc' => $cc,
                    'das' => $das,
                    'itkismpa' => $itkismpa,
                    'itkismlot' => $itkismlot,
                    'itkisma' => $itkisma,
                    'itkismepi' => $itkismepi,
                    'materiel' => $materiel,
                    'appareil' => $appareil,
                    'fc' => $fc,
                    'cvid' => $cvid,
                    'ti' => $ti,
                    'dateprev' => $dateprev,
                    'parcellestot' => $parcelletot,
                    'surface' => $surface,
                    'ferme' => $ferme,
                    'diffusion' => $diffusion
                ));
            }
        }
    }

    public function updateitkismAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $id = $request->request->get('id');

            $tmvegetal = $request->request->get('tmvegetal');
            $prescripteurid = (int)$request->request->get('prescripteurid');
            $modeapplication = $request->request->get('modeapplication');

            $surface = $request->request->get('surface');
            $surfacetot = $request->request->get('surfacetot');

            $cvid = (int)$request->request->get('cvid');
            $eligne = $request->request->get('eligne');
            $esemis = $request->request->get('esemis');
            $rang = (int)$request->request->get('rang');
            $datep = $request->request->get('datep');
            $datep = new \DateTime($datep);
            $datepformat = $datep->format('Y/m/d');
            $duree = (int)$request->request->get('duree');

            $datepfin = strtotime("+".$duree." day", strtotime($datepformat));
            $datepfin=new \DateTime(date('Y-m-d',$datepfin));
            $datepfinformat = $datepfin->format('Y/m/d');

            $surface = (float)str_replace(',', '.', $surface);
            $esemis = (float)str_replace(',', '.', $esemis);
            $eligne = (float)str_replace(',', '.', $eligne);

            $qtesemence = round((10000 / $eligne) * (10000 / $esemis) * $rang, 2);
            $qtesemencetot = round($qtesemence * $surface, 2);

            $surface = str_replace('.', ',', $surface);
            $esemis = str_replace('.', ',', $esemis);
            $eligne = str_replace('.', ',', $eligne);

            $sql = "UPDATE ITKISM SET TYPEMVEGETAL ='$tmvegetal', PRESCRIPTEURID = $prescripteurid, MODEAPPLICATION =
'$modeapplication', SURFACE = '$surface', SURFACETOT = '$surfacetot', CULTUREID = $cvid, ELIGNE = '$eligne', ESEMIS =
 '$esemis', RANGNB = '$rang', DATEP = '$datepformat', DUREE = '$duree', DATEPFIN = '$datepfinformat', QTESEM =
 '$qtesemence', QTESEMTOT = '$qtesemencetot' WHERE ITKISMID = $id ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $modeapplication = $request->request->get('modeapplication');

            if($modeapplication=='Plantation Mecanique' or $modeapplication=='Semis Mecanique')
            {
                $applicateurid = (int)$request->request->get('applicateurid');
                $sql = "UPDATE ITKISM SET APPLICATEURID = $applicateurid, EFFECTIF = null  WHERE ITKISMID = $id ";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
            elseif($modeapplication=='Plantation Manuelle' or $modeapplication=='Semis Manuel')
            {
                $effectif = $request->request->get('effectif');
                $sql = "UPDATE ITKISM SET EFFECTIF = '$effectif', APPLICATEURID = null, MATERIELID = null, APPAREILID = null
                WHERE ITKISMID = $id ";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
        }

        return $this->render('AgroBundle:Itkism:index.html.twig');
    }

    public function viewitkismAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $id = $request->query->get('id');

            $itkism=$em->getRepository('AgroBundle:ITKISM')->find($id);
            $edit_form = $this->createForm(new ITKISMType($em), $itkism);

            $fermeid = $itkism->getFERMEID();
            $ferme=$em->getRepository('CultureBundle:Ferme')->find($fermeid)->getLIBELLE();

            $ccid=$itkism->getCYCLECULTURALID();
            $cc=$em->getRepository('CultureBundle:CycleCultural')->find($ccid);

            $cvid=$itkism->getCULTUREID();
            $cv=$em->getRepository('CultureBundle:Culture')->find($cvid);

            $fcid=$cv->getFAMILLECULTUREID();
            $fc=$em->getRepository('CultureBundle:FamilleCulture')->find($fcid)->getLIBELLE();

            $ti=$cc->getTYPEIMPLANTATION();

            $dateprev=$cc->getDATEPREVISRECOLTE();

            $surface=$itkism->getSURFACE();
            $surfacetot=$itkism->getSURFACETOT();
            $percentsurface = round((100*((int)str_replace(',', '.',$surface)))/((int)str_replace(',', '.',
                    $surfacetot)),2);

            $ilotid=(int)$cc->getILOTID();
            $ilot=$em->getRepository('CultureBundle:Ilot')->find($ilotid)->getLIBELLE();

            $sql = "SELECT FN_ITK_PLANNING_PARCELLE($ilotid,'') AS result FROM dual";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->fetchAll();
            $parcelletot=$res[0]['RESULT'];

            $sql = "SELECT FN_ITK_DAS($ccid) AS result FROM dual";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $das=$result[0]['RESULT'];

            $itkismpa=$em->getRepository('AgroBundle:ITKISMPA')->getAllItkismpaByITKISMID($id);

            $itkismlot=$em->getRepository('AgroBundle:ITKISMLOT')->getAllItkismlotByITKISMID($id);

            $itkisma=$em->getRepository('AgroBundle:ITKISMA')->getAllItkismaByITKISMID($id);

            $itkismepi=$em->getRepository('AgroBundle:ITKISMEPI')->getAllItkismepiByITKISMID($id);

            $materielid=$itkism->getMATERIELID();
            $appareilid=$itkism->getAPPAREILID();
            $materiel='';
            $appareil='';
            if ($materielid!=null)
            {
                $materiel=$em->getRepository('TechniqueBundle:Materiel')->find($materielid)->getLIBELLE();
            }
            if($appareilid!=null)
            {
                $appareil=$em->getRepository('TechniqueBundle:Materiel')->find($appareilid)->getLIBELLE();
            }

            return $this->render('AgroBundle:Itkism:detailsitkism.html.twig' , array(
                'itkism'=>$itkism,
                'entity' => $itkism,
                'edit_form' => $edit_form->createView(),
                'ilot' => $ilot,
                'ilotid' => $ilotid,
                'parcelles' => $parcelletot,
                'cv' => $cv,
                'cc' => $cc,
                'das' => $das,
                'itkismpa' => $itkismpa,
                'itkismlot' => $itkismlot,
                'itkisma' => $itkisma,
                'itkismepi' => $itkismepi,
                'materiel' => $materiel,
                'appareil' => $appareil,
                'fc' => $fc,
                'cvid' => $cvid,
                'ti' => $ti,
                'dateprev' => $dateprev,
                'parcellestot' => $parcelletot,
                'surface' => $surface,
                'ferme' => $ferme
            ));

        }
    }

    public function modifierilotopAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $ilotid =(int) $request->request->get('ilotid');
            $ilot = $request->request->get('ilot');
            $ilotconfirm = $request->request->get('ilotconfirm');

            if ($ilot != null && $ilotconfirm != null)
            {
                if($ilot == $ilotconfirm)
                {
                    $sql = "CALL SP_ISMR_CHANGEILOT('$ilot', $ilotid)";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                }
            }
            return $this->render('AgroBundle:Itkism:index.html.twig');
        }
    }

    public function updateparcellesAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));
        }
        else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $date = new \DateTime();
            $dateformat = $date->format('Ymd');

            $itkismid=(int)$request->request->get('itkismid');

            $num = (int)$request->request->get('num');

            $surfaceutilisee=0;

            for($i=0; $i<$num; $i++)
            {
                $parcelleid=(int)$request->request->get('id'.$i);
                $surface=$request->request->get('surface'.$parcelleid);
                $checked=(string)$request->request->get('checked'.$parcelleid);

                $itkismpa = new ITKISMPA();
                $itkismpa->setITKISMID($itkismid);
                $itkismpa->setPARCELLEID($parcelleid);
                $itkismpa->setSURFACE($surface);
                if($checked=='false')
                {
                    $itkismpa->setPARCELLECHECK('0');
                }
                else
                {
                    $itkismpa->setPARCELLECHECK('-1');
                    $surface=str_replace(',', '.',$surface);
                    $surfaceutilisee=($surfaceutilisee) + ($surface);
                }

                $itkismpadata = sizeof($em->getRepository('AgroBundle:ITKISMPA')->findBy(
                    array('pARCELLEID' => $parcelleid , 'iTKISMID' => $itkismid )                        // Offset
                ));
                if($itkismpadata==0)
                {
                    $em->persist($itkismpa);
                    $em->flush();
                }
                else
                {
                    $surface=str_replace('.', ',',$surface);
                    $parcellecheck =$itkismpa->getPARCELLECHECK();
                    $sql = "UPDATE ITKISMPA SET PARCELLECHECK ='$parcellecheck', SURFACE = '$surface' WHERE ITKISMID = $itkismid AND
                    PARCELLEID = $parcelleid" ;
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                }
            }
            $surfaceutilisee=str_replace('.', ',',$surfaceutilisee);

            $sql = "UPDATE ITKISM SET SURFACE ='$surfaceutilisee' WHERE ITKISMID = $itkismid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }
        return $this->render('AgroBundle:Itkism:index.html.twig');
    }

    public function updatelotsemenceAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkismid=(int)$request->request->get('itkismid');
            $ls=$request->request->get('ls');
            $quantite=(int)$request->request->get('quantite');
            $unite=$request->request->get('unite');
            $lotno=$request->request->get('LOTNO');
            $libelle=$request->request->get('libelle');
            $semencelotid=(int)$request->request->get('semencelotid');

            $itkismlot = new ITKISMLOT();
            $itkismlot->setQTE($quantite);
            $itkismlot->setITKISMID($itkismid);
            $itkismlot->setLIBELLE($libelle);
            $itkismlot->setUNITE($unite);
            $itkismlot->setLOTNO($lotno);
            $itkismlot->setSEMENCELOTID($semencelotid);
            $itkismlotdata = sizeof($em->getRepository('AgroBundle:ITKISMLOT')->findByITKISMID($itkismid));

            if($itkismlotdata==0)
            {
                $em->persist($itkismlot);
                $em->flush();
            }
            else
            {
                $sql = "UPDATE ITKISMLOT SET QTE ='$quantite', LIBELLE = '$libelle', UNITE = '$unite', SEMENCELOTID
='$semencelotid', LOTNO = '$lotno' WHERE ITKISMID =$itkismid" ;
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
        }
        return $this->render('AgroBundle:Itkism:index.html.twig');
    }

    public function updateintrantAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkismid=(int)$request->request->get('itkismid');
            $intrantid=(int)$request->request->get('intrantid');
            $dose=(int)$request->request->get('dose');
            $prixunit=(int)$request->request->get('prixunit');
            $surface=$request->request->get('surface');
            $qtetot=(int)$request->request->get('qtetot');
            $prixtot=(int)$request->request->get('prixtot');
            $commentaire=$request->request->get('commentaire');
            $unite=$request->request->get('uniteint');

            $itkisma = new ITKISMA();
            $itkisma->setSURFACE($surface);
            $itkisma->setQTE($dose);
            $itkisma->setITKISMID($itkismid);
            $itkisma->setARTICLEID($intrantid);
            $itkisma->setPRIXUNITAIRE($prixunit);
            $itkisma->setPRIXTOTAL($prixtot);
            $itkisma->setQTET($qtetot);
            $itkisma->setRANG(1);
            $itkisma->setCOMMENTAIRE($commentaire);
            $itkisma->setUNITE($unite);

            $itkismadata = sizeof($em->getRepository('AgroBundle:ITKISMA')->findByITKISMID($itkismid));

            if($itkismadata==0)
            {
                $em->persist($itkisma);
                $em->flush();
            }
            else
            {
                $sql = "UPDATE ITKISMA SET SURFACE ='$surface', QTE = $dose, ARTICLEID = '$intrantid', PRIXUNITAIRE = '$prixunit',
 PRIXTOTAL = '$prixtot', QTET = '$qtetot', UNITE = '$unite', RANG = 1, COMMENTAIRE = '$commentaire' WHERE ITKISMID =
 $itkismid";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
        }
        return $this->render('AgroBundle:Itkism:index.html.twig');
    }

    public function updateepiAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkismid=(int)$request->request->get('itkismid');
            $materielid=(int)$request->request->get('materielid');
            $appareilid=(int)$request->request->get('appareilid');
            $type=$request->request->get('type');

            if($type=='mecanique')
            {
                $sql = "UPDATE ITKISM SET APPAREILID =$appareilid, MATERIELID = $materielid WHERE ITKISMID = $itkismid";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
            else if($type=='manuel')
            {
                $epiid =(int) $request->request->get('epiid');
                $itkismepi = new ITKISMEPI();
                $itkismepi->setEPIID($epiid);
                $itkismepi->setITKISMID($itkismid);
                $em->persist($itkismepi);
                $em->flush();
            }
        }
        return $this->render('AgroBundle:Itkism:index.html.twig');
    }

    public function supprimerepiitkismAction(Request $request)
    {

        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $itkismid=(int)$request->request->get('itkismid');
            $epiid =(int) $request->request->get('epiid');

            $sql = "DELETE FROM ITKISMEPI WHERE ITKISMID = $itkismid AND EPIID = $epiid";
            $stmt = $conn->prepare($sql);
            $stmt->execute();

        }
        return $this->render('AgroBundle:Itkism:index.html.twig');
    }

    //Recupérations des données

    public function getallopAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $ferme=$request->query->get('fermeid');
        $motcle=$request->query->get('motcle');
        $page=$request->query->get('page');

        $OP=$em->getRepository('CultureBundle:CycleCultural')->getAllOpFerme($ferme,$motcle);

        $count = count($OP);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $OP));
    }

    public function getopAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $op = $request->query->get('opid');

        $result = $em->getRepository('CultureBundle:CycleCultural')->getMyOP($op);

        return new JsonResponse($result);

    }

    public function getallculturevarieteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle=$request->query->get('motcle');

        $culturevarietes=$em->getRepository('CultureBundle:Culture')->getAllCultureVariete($motcle);

        $count = count($culturevarietes);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $culturevarietes));
    }

    public function getsurfacetotAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $ilotid=$request->query->get('ilotid');

        $result=$em->getRepository('CultureBundle:Ilot')->getSurfaceTot($ilotid);

        return new JsonResponse($result);
    }

    public function getfamillecultureAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $cvid=$request->query->get('cvid');

        $result=$em->getRepository('CultureBundle:FamilleCulture')->getFamilleCulture($cvid);

        return new JsonResponse($result);
    }

    public function getalllotsemenceAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $motcle=$request->query->get('motcle');

        $sql = "SELECT UNITETLOCALISEID, UNITELOCALISE, CULTUREVARIETE, UNITE FROM VW_SEMENCELOTGROUP# WHERE UNITELOCALISE LIKE
        '%$motcle%'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $res = $stmt->fetchAll();
        $result=[];

        for($i=0; $i<sizeof($res); $i++)
        {
            $result[$i]['id'] = $res[$i]['UNITETLOCALISEID'];
            $result[$i]['name'] = $res[$i]['UNITELOCALISE'];
            $result[$i]['CULTUREVARIETE'] = $res[$i]['CULTUREVARIETE'];
            $result[$i]['UNITE'] = $res[$i]['UNITE'];
        }

        $count = count($result);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $result));

    }

    public function getalllotsemenceimporteAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager('sqlserver');
        $conn = $em->getConnection();

        $motcle=$request->query->get('motcle');

        $sql="SELECT a.cbMarq as id, a.AR_Design as name, ast.AS_QteSto , u.U_Intitule, d.DE_Intitule as site, a.AR_Ref as code
                FROM F_ARTICLE a, F_CATALOGUE c, F_DEPOT d, P_UNITE u, F_ARTSTOCK ast
                WHERE a.AR_Design<>'NE PLUS UTILISER' AND ast.AR_Ref=a.AR_Ref AND a.CL_No2=c.CL_No AND c.CL_No=3 AND
                ast.DE_No=d.DE_No AND a.AR_UniteVen=u.cbMarq AND a.AR_Design LIKE '%$motcle%'  ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $count = count($result);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $result));

    }

    public function getdasAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $opid=$request->query->get('opid');

        $sql = "SELECT FN_ITK_DAS(?) AS result FROM dual";
        $params = array(
            $opid
        );
        $paramsTypes = array(
            \PDO::PARAM_STR
        );

        $stmt = $conn->executeQuery($sql, $params, $paramsTypes);

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return new JsonResponse($result);
    }

    public function getinstructiondiffuseopAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $opid=$request->query->get('opid');

        $result=$em->getRepository('AgroBundle:ITKISM')->getinstructiondiffuseop($opid);

        return new JsonResponse($result);
    }

    public function getallintrantAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle=$request->query->get('motcle');

        $intrants=$em->getRepository('TechniqueBundle:Article')->getAllIntrant($motcle);

        $count = count($intrants);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $intrants));
    }

    public function getallepiAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle=$request->query->get('motcle');

        $epi=$em->getRepository('AgroBundle:EPI')->getAllEpi($motcle);

        $count = count($epi);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $epi));
    }

    public function getallappareilAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle=$request->query->get('motcle');

        $appareils=$em->getRepository('TechniqueBundle:Materiel')->getAllAppareil($motcle);

        $count = count($appareils);

        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $appareils));
    }

    public function getallmaterielAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle=$request->query->get('motcle');

        $materiels=$em->getRepository('TechniqueBundle:Materiel')->getAllMaterielItkism($motcle);

        $count = count($materiels);
        return new JsonResponse(array("total_count" => $count,"incomplete_results" => false, "items" => $materiels));
    }

    public function diffuseritkismAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $id = $request->request->get('id');

        $sql = "UPDATE ITKISM SET ETATINSTRUCTION ='DIFFUSE' WHERE ITKISMID = $id";
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $this->render('AgroBundle:Itkism:index.html.twig');
    }

    public function imprimeritkismAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $id = $request->query->get('id');

        $statut=$em->getRepository('AgroBundle:ITKISM')->find($id)->getETATINSTRUCTION();

        if($statut!='DIFFUSE')
        {
            $this->get('session')->getFlashBag()->add('notice', "Veuillez d'abord diffuser cette instruction !!");
            return $this->redirect($this->generateUrl('modifieritkism', array('id' =>$id)));
        }
        else
        {
            $lot=$em->getRepository('AgroBundle:ITKISMLOT')->findByITKISMID($id);
            $intrant=$em->getRepository('AgroBundle:ITKISMA')->findByITKISMID($id);
            $parcelles=$em->getRepository('AgroBundle:ITKISMPA')->findByITKISMID($id);

            if(sizeof($lot)==0 or sizeof($intrant)==0 or sizeof($parcelles)==0)
            {
                $this->get('session')->getFlashBag()->add('notice', "Veuillez compléter les informations !!");
                return $this->redirect($this->generateUrl('modifieritkism', array('id' =>$id)));
            }
            else {

                $date = new \DateTime();
                $dateformat = $date->format('d/m/Y');

                $itkism=$em->getRepository('AgroBundle:ITKISM')->getItkism($id)[0];

                $ilotid=$itkism['ILOTID'];
                $sql = "SELECT FN_ITK_PLANNING_PARCELLE($ilotid,'') AS result FROM dual";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $res = $stmt->fetchAll();
                $parcelletot=$res[0]['RESULT'];

                $itkismpa = $em->getRepository('AgroBundle:ITKISMPA')->getAllItkismpaByITKISMID($id);
                $itkismlot = $em->getRepository('AgroBundle:ITKISMLOT')->getAllItkismlotByITKISMID($id)[0];
                $itkisma = $em->getRepository('AgroBundle:ITKISMA')->getAllItkismaByITKISMID($id)[0];
                $itkismepi = $em->getRepository('AgroBundle:ITKISMEPI')->getAllItkismepiByITKISMID($id);

                if($itkism['EFFECTIF']==null)
                {
                    $applicateurid=$itkism['APPLICATEURID'];
                    $applicateur=$em->getRepository('TechniqueBundle:Personnel')->find($applicateurid);
                    $applicateurname=$applicateur->getPRENOM().' '.$applicateur->getNOM();
                    $appareilid=$itkism['APPAREILID'];
                    $appareil=$em->getRepository('TechniqueBundle:Materiel')->find($appareilid)->getLIBELLE();
                    $materielid=$itkism['MATERIELID'];
                    $materiel=$em->getRepository('TechniqueBundle:Materiel')->find($materielid)->getLIBELLE();
                }
                else
                {
                    $applicateurname='';
                    $appareil='';
                    $materiel='';
                }


                $html = $this->renderView('AgroBundle:ITKISM:imprimeritkism.html.twig', array
                ('itkism' =>$itkism, 'parcelles' => $parcelletot, 'date' => $dateformat, 'itkismpa' => $itkismpa,
                    'itkismlot' => $itkismlot, 'itkisma' => $itkisma, 'itkismepi' => $itkismepi, 'applicateur' =>
                        $applicateurname, 'materiel' => $materiel, 'appareil' => $appareil));
                //on appelle le service html2pdf
                $html2pdf = $this->get('html2pdf_factory')->create('P', 'A4', 'fr', true, 'UTF-8', array(15, 10, 15, 10));
                //real : utilise la taille r�elle
                $html2pdf->pdf->SetDisplayMode('real');
                //writeHTML va tout simplement prendre la vue stocker dans la variable $html pour la convertir en format PDF
                $html2pdf->writeHTML($html);
                //Output envoit le document PDF au navigateur internet
                return new Response($html2pdf->Output('Instruction-semis.pdf'), 200, array('Content-Type' =>
                    'application/pdf'));
            }
        }
    }

    public function annuleritkismAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $id = $request->request->get('id');

            $sql = "UPDATE ITKISM SET ETATINSTRUCTION = 'ANNULE' WHERE ITKISMID = $id";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }

        return $this->redirect($this->generateUrl('instructionsemis'));
    }
}
