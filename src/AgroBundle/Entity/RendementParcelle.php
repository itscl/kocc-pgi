<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RendementParcelle
 */
class RendementParcelle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $sITE;

    /**
     * @var int
     */
    private $fERME;

    /**
     * @var string
     */
    private $pARCELLE;

    /**
     * @var string
     */
    private $cULTUREVARIETE;

    /**
     * @var string
     */
    private $dATESEMIS;

    /**
     * @var string
     */
    private $sURFACE;

    /**
     * @var string
     */
    private $rENDEMENTBRUT;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sITE
     *
     * @param integer $sITE
     * @return RendementParcelle
     */
    public function setSITE($sITE)
    {
        $this->sITE = $sITE;

        return $this;
    }

    /**
     * Get sITE
     *
     * @return integer 
     */
    public function getSITE()
    {
        return $this->sITE;
    }

    /**
     * Set fERME
     *
     * @param integer $fERME
     * @return RendementParcelle
     */
    public function setFERME($fERME)
    {
        $this->fERME = $fERME;

        return $this;
    }

    /**
     * Get fERME
     *
     * @return integer 
     */
    public function getFERME()
    {
        return $this->fERME;
    }

    /**
     * Set pARCELLE
     *
     * @param string $pARCELLE
     * @return RendementParcelle
     */
    public function setPARCELLE($pARCELLE)
    {
        $this->pARCELLE = $pARCELLE;

        return $this;
    }

    /**
     * Get pARCELLE
     *
     * @return string 
     */
    public function getPARCELLE()
    {
        return $this->pARCELLE;
    }

    /**
     * Set cULTUREVARIETE
     *
     * @param string $cULTUREVARIETE
     * @return RendementParcelle
     */
    public function setCULTUREVARIETE($cULTUREVARIETE)
    {
        $this->cULTUREVARIETE = $cULTUREVARIETE;

        return $this;
    }

    /**
     * Get cULTUREVARIETE
     *
     * @return string 
     */
    public function getCULTUREVARIETE()
    {
        return $this->cULTUREVARIETE;
    }

    /**
     * Set dATESEMIS
     *
     * @param string $dATESEMIS
     * @return RendementParcelle
     */
    public function setDATESEMIS($dATESEMIS)
    {
        $this->dATESEMIS = $dATESEMIS;

        return $this;
    }

    /**
     * Get dATESEMIS
     *
     * @return string
     */
    public function getDATESEMIS()
    {
        return $this->dATESEMIS;
    }

    /**
     * Set sURFACE
     *
     * @param string $sURFACE
     * @return RendementParcelle
     */
    public function setSURFACE($sURFACE)
    {
        $this->sURFACE = $sURFACE;

        return $this;
    }

    /**
     * Get sURFACE
     *
     * @return string
     */
    public function getSURFACE()
    {
        return $this->sURFACE;
    }

    /**
     * Set rENDEMENTBRUT
     *
     * @param string $rENDEMENTBRUT
     * @return RendementParcelle
     */
    public function setRENDEMENTBRUT($rENDEMENTBRUT)
    {
        $this->rENDEMENTBRUT = $rENDEMENTBRUT;

        return $this;
    }

    /**
     * Get rENDEMENTBRUT
     *
     * @return string
     */
    public function getRENDEMENTBRUT()
    {
        return $this->rENDEMENTBRUT;
    }
}
