<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKISMRA
 */
class ITKISMRA
{
    /**
     * @var integer
     */
    private $aRTICLEID;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var integer
     */
    private $iTKISMRID;

    /**
     * @var integer
     */
    private $oRIGINSITEID;

    /**
     * @var integer
     */
    private $pRIXTOTAL;

    /**
     * @var integer
     */
    private $pRIXUNITAIRE;

    /**
     * @var string
     */
    private $qTE;

    /**
     * @var string
     */
    private $qTET;

    /**
     * @var integer
     */
    private $rANG;

    /**
     * @var string
     */
    private $sURFACE;

    /**
     * @var string
     */
    private $uNITE;

    /**
     * @var string
     */
    private $uNITEPRIX;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set aRTICLEID
     *
     * @param integer $aRTICLEID
     * @return ITKISMRA
     */
    public function setARTICLEID($aRTICLEID)
    {
        $this->aRTICLEID = $aRTICLEID;

        return $this;
    }

    /**
     * Get aRTICLEID
     *
     * @return integer 
     */
    public function getARTICLEID()
    {
        return $this->aRTICLEID;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return ITKISMRA
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string 
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ITKISMRA
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set iTKISMRID
     *
     * @param integer $iTKISMRID
     * @return ITKISMRA
     */
    public function setITKISMRID($iTKISMRID)
    {
        $this->iTKISMRID = $iTKISMRID;

        return $this;
    }

    /**
     * Get iTKISMRID
     *
     * @return integer 
     */
    public function getITKISMRID()
    {
        return $this->iTKISMRID;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ITKISMRA
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set pRIXTOTAL
     *
     * @param integer $pRIXTOTAL
     * @return ITKISMRA
     */
    public function setPRIXTOTAL($pRIXTOTAL)
    {
        $this->pRIXTOTAL = $pRIXTOTAL;

        return $this;
    }

    /**
     * Get pRIXTOTAL
     *
     * @return integer 
     */
    public function getPRIXTOTAL()
    {
        return $this->pRIXTOTAL;
    }

    /**
     * Set pRIXUNITAIRE
     *
     * @param integer $pRIXUNITAIRE
     * @return ITKISMRA
     */
    public function setPRIXUNITAIRE($pRIXUNITAIRE)
    {
        $this->pRIXUNITAIRE = $pRIXUNITAIRE;

        return $this;
    }

    /**
     * Get pRIXUNITAIRE
     *
     * @return integer 
     */
    public function getPRIXUNITAIRE()
    {
        return $this->pRIXUNITAIRE;
    }

    /**
     * Set qTE
     *
     * @param string $qTE
     * @return ITKISMRA
     */
    public function setQTE($qTE)
    {
        $this->qTE = $qTE;

        return $this;
    }

    /**
     * Get qTE
     *
     * @return string 
     */
    public function getQTE()
    {
        return $this->qTE;
    }

    /**
     * Set qTET
     *
     * @param string $qTET
     * @return ITKISMRA
     */
    public function setQTET($qTET)
    {
        $this->qTET = $qTET;

        return $this;
    }

    /**
     * Get qTET
     *
     * @return string 
     */
    public function getQTET()
    {
        return $this->qTET;
    }

    /**
     * Set rANG
     *
     * @param integer $rANG
     * @return ITKISMRA
     */
    public function setRANG($rANG)
    {
        $this->rANG = $rANG;

        return $this;
    }

    /**
     * Get rANG
     *
     * @return integer 
     */
    public function getRANG()
    {
        return $this->rANG;
    }

    /**
     * Set sURFACE
     *
     * @param string $sURFACE
     * @return ITKISMRA
     */
    public function setSURFACE($sURFACE)
    {
        $this->sURFACE = $sURFACE;

        return $this;
    }

    /**
     * Get sURFACE
     *
     * @return string 
     */
    public function getSURFACE()
    {
        return $this->sURFACE;
    }

    /**
     * Set uNITE
     *
     * @param string $uNITE
     * @return ITKISMRA
     */
    public function setUNITE($uNITE)
    {
        $this->uNITE = $uNITE;

        return $this;
    }

    /**
     * Get uNITE
     *
     * @return string 
     */
    public function getUNITE()
    {
        return $this->uNITE;
    }

    /**
     * Set uNITEPRIX
     *
     * @param string $uNITEPRIX
     * @return ITKISMRA
     */
    public function setUNITEPRIX($uNITEPRIX)
    {
        $this->uNITEPRIX = $uNITEPRIX;

        return $this;
    }

    /**
     * Get uNITEPRIX
     *
     * @return string 
     */
    public function getUNITEPRIX()
    {
        return $this->uNITEPRIX;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKISMRA
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKISMRA
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
