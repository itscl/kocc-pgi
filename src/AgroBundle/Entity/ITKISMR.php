<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKISMR
 */
class ITKISMR
{
    /**
     * @var integer
     */
    private $aNNEESEM;

    /**
     * @var integer
     */
    private $aPPAREILID;

    /**
     * @var integer
     */
    private $aPPLICATEURID;

    /**
     * @var integer
     */
    private $cHAUFFEURID;

    /**
     * @var integer
     */
    private $cOMPTEURDEBUT;

    /**
     * @var integer
     */
    private $cOMPTEURFIN;

    /**
     * @var string
     */
    private $cOMPUTERMAJ;

    /**
     * @var integer
     */
    private $cTRDEB;

    /**
     * @var integer
     */
    private $cTRFIN;

    /**
     * @var integer
     */
    private $cULTUREID;

    /**
     * @var integer
     */
    private $cYCLECULTURALID;

    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var \DateTime
     */
    private $dATEMAJ;

    /**
     * @var \DateTime
     */
    private $dATEP;

    /**
     * @var \DateTime
     */
    private $dATEPFIN;

    /**
     * @var \DateTime
     */
    private $dATEREALISATION;

    /**
     * @var integer
     */
    private $dUREE;

    /**
     * @var string
     */
    private $eFFECTIF;

    /**
     * @var string
     */
    private $eLIGNE;

    /**
     * @var string
     */
    private $eSEMIS;

    /**
     * @var integer
     */
    private $fERMEID;

    /**
     * @var \DateTime
     */
    private $hEUREDEBUT;

    /**
     * @var \DateTime
     */
    private $hEUREFIN;

    /**
     * @var integer
     */
    private $iTKISMID;

    /**
     * @var integer
     */
    private $jOURABSOLU;

    /**
     * @var integer
     */
    private $lOTNO;

    /**
     * @var integer
     */
    private $mATERIELID;

    /**
     * @var string
     */
    private $mODEAPPLICATION;

    /**
     * @var integer
     */
    private $pERIODE;

    /**
     * @var integer
     */
    private $pRESCRIPTEURID;

    /**
     * @var string
     */
    private $qTESEM;

    /**
     * @var string
     */
    private $qTESEMTOT;

    /**
     * @var string
     */
    private $rANGNB;

    /**
     * @var integer
     */
    private $sGESTEID;

    /**
     * @var string
     */
    private $sURFACE;

    /**
     * @var string
     */
    private $sURFACETOT;

    /**
     * @var integer
     */
    private $tYPEINSTRUCTIONID;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var string
     */
    private $uSERMAJ;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set aNNEESEM
     *
     * @param integer $aNNEESEM
     * @return ITKISMR
     */
    public function setANNEESEM($aNNEESEM)
    {
        $this->aNNEESEM = $aNNEESEM;

        return $this;
    }

    /**
     * Get aNNEESEM
     *
     * @return integer 
     */
    public function getANNEESEM()
    {
        return $this->aNNEESEM;
    }

    /**
     * Set aPPAREILID
     *
     * @param integer $aPPAREILID
     * @return ITKISMR
     */
    public function setAPPAREILID($aPPAREILID)
    {
        $this->aPPAREILID = $aPPAREILID;

        return $this;
    }

    /**
     * Get aPPAREILID
     *
     * @return integer 
     */
    public function getAPPAREILID()
    {
        return $this->aPPAREILID;
    }

    /**
     * Set aPPLICATEURID
     *
     * @param integer $aPPLICATEURID
     * @return ITKISMR
     */
    public function setAPPLICATEURID($aPPLICATEURID)
    {
        $this->aPPLICATEURID = $aPPLICATEURID;

        return $this;
    }

    /**
     * Get aPPLICATEURID
     *
     * @return integer 
     */
    public function getAPPLICATEURID()
    {
        return $this->aPPLICATEURID;
    }

    /**
     * Set cHAUFFEURID
     *
     * @param integer $cHAUFFEURID
     * @return ITKISMR
     */
    public function setCHAUFFEURID($cHAUFFEURID)
    {
        $this->cHAUFFEURID = $cHAUFFEURID;

        return $this;
    }

    /**
     * Get cHAUFFEURID
     *
     * @return integer 
     */
    public function getCHAUFFEURID()
    {
        return $this->cHAUFFEURID;
    }

    /**
     * Set cOMPUTERMAJ
     *
     * @param string $cOMPUTERMAJ
     * @return ITKISMR
     */
    public function setCOMPUTERMAJ($cOMPUTERMAJ)
    {
        $this->cOMPUTERMAJ = $cOMPUTERMAJ;

        return $this;
    }

    /**
     * Get cOMPUTERMAJ
     *
     * @return string 
     */
    public function getCOMPUTERMAJ()
    {
        return $this->cOMPUTERMAJ;
    }

    /**
     * Set cTRDEB
     *
     * @param integer $cTRDEB
     * @return ITKISMR
     */
    public function setCTRDEB($cTRDEB)
    {
        $this->cTRDEB = $cTRDEB;

        return $this;
    }

    /**
     * Get cTRDEB
     *
     * @return integer 
     */
    public function getCTRDEB()
    {
        return $this->cTRDEB;
    }

    /**
     * Set cTRFIN
     *
     * @param integer $cTRFIN
     * @return ITKISMR
     */
    public function setCTRFIN($cTRFIN)
    {
        $this->cTRFIN = $cTRFIN;

        return $this;
    }

    /**
     * Get cTRFIN
     *
     * @return integer 
     */
    public function getCTRFIN()
    {
        return $this->cTRFIN;
    }

    /**
     * Set cULTUREID
     *
     * @param integer $cULTUREID
     * @return ITKISMR
     */
    public function setCULTUREID($cULTUREID)
    {
        $this->cULTUREID = $cULTUREID;

        return $this;
    }

    /**
     * Get cULTUREID
     *
     * @return integer 
     */
    public function getCULTUREID()
    {
        return $this->cULTUREID;
    }

    /**
     * Set cYCLECULTURALID
     *
     * @param integer $cYCLECULTURALID
     * @return ITKISMR
     */
    public function setCYCLECULTURALID($cYCLECULTURALID)
    {
        $this->cYCLECULTURALID = $cYCLECULTURALID;

        return $this;
    }

    /**
     * Get cYCLECULTURALID
     *
     * @return integer 
     */
    public function getCYCLECULTURALID()
    {
        return $this->cYCLECULTURALID;
    }

    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return ITKISMR
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set dATEMAJ
     *
     * @param \DateTime $dATEMAJ
     * @return ITKISMR
     */
    public function setDATEMAJ($dATEMAJ)
    {
        $this->dATEMAJ = $dATEMAJ;

        return $this;
    }

    /**
     * Get dATEMAJ
     *
     * @return \DateTime 
     */
    public function getDATEMAJ()
    {
        return $this->dATEMAJ;
    }

    /**
     * Set dATEP
     *
     * @param \DateTime $dATEP
     * @return ITKISMR
     */
    public function setDATEP($dATEP)
    {
        $this->dATEP = $dATEP;

        return $this;
    }

    /**
     * Get dATEP
     *
     * @return \DateTime 
     */
    public function getDATEP()
    {
        return $this->dATEP;
    }

    /**
     * Set dATEPFIN
     *
     * @param \DateTime $dATEPFIN
     * @return ITKISMR
     */
    public function setDATEPFIN($dATEPFIN)
    {
        $this->dATEPFIN = $dATEPFIN;

        return $this;
    }

    /**
     * Get dATEPFIN
     *
     * @return \DateTime 
     */
    public function getDATEPFIN()
    {
        return $this->dATEPFIN;
    }

    /**
     * Set dATEREALISATION
     *
     * @param \DateTime $dATEREALISATION
     * @return ITKISMR
     */
    public function setDATEREALISATION($dATEREALISATION)
    {
        $this->dATEREALISATION = $dATEREALISATION;

        return $this;
    }

    /**
     * Get dATEREALISATION
     *
     * @return \DateTime 
     */
    public function getDATEREALISATION()
    {
        return $this->dATEREALISATION;
    }

    /**
     * Set dUREE
     *
     * @param integer $dUREE
     * @return ITKISMR
     */
    public function setDUREE($dUREE)
    {
        $this->dUREE = $dUREE;

        return $this;
    }

    /**
     * Get dUREE
     *
     * @return integer 
     */
    public function getDUREE()
    {
        return $this->dUREE;
    }

    /**
     * Set eFFECTIF
     *
     * @param string $eFFECTIF
     * @return ITKISMR
     */
    public function setEFFECTIF($eFFECTIF)
    {
        $this->eFFECTIF = $eFFECTIF;

        return $this;
    }

    /**
     * Get eFFECTIF
     *
     * @return string 
     */
    public function getEFFECTIF()
    {
        return $this->eFFECTIF;
    }

    /**
     * Set eLIGNE
     *
     * @param string $eLIGNE
     * @return ITKISMR
     */
    public function setELIGNE($eLIGNE)
    {
        $this->eLIGNE = $eLIGNE;

        return $this;
    }

    /**
     * Get eLIGNE
     *
     * @return string 
     */
    public function getELIGNE()
    {
        return $this->eLIGNE;
    }

    /**
     * Set eSEMIS
     *
     * @param string $eSEMIS
     * @return ITKISMR
     */
    public function setESEMIS($eSEMIS)
    {
        $this->eSEMIS = $eSEMIS;

        return $this;
    }

    /**
     * Get eSEMIS
     *
     * @return string 
     */
    public function getESEMIS()
    {
        return $this->eSEMIS;
    }

    /**
     * Set fERMEID
     *
     * @param integer $fERMEID
     * @return ITKISMR
     */
    public function setFERMEID($fERMEID)
    {
        $this->fERMEID = $fERMEID;

        return $this;
    }

    /**
     * Get fERMEID
     *
     * @return integer 
     */
    public function getFERMEID()
    {
        return $this->fERMEID;
    }

    /**
     * Set hEUREDEBUT
     *
     * @param \DateTime $hEUREDEBUT
     * @return ITKISMR
     */
    public function setHEUREDEBUT($hEUREDEBUT)
    {
        $this->hEUREDEBUT = $hEUREDEBUT;

        return $this;
    }

    /**
     * Get hEUREDEBUT
     *
     * @return \DateTime 
     */
    public function getHEUREDEBUT()
    {
        return $this->hEUREDEBUT;
    }

    /**
     * Set hEUREFIN
     *
     * @param \DateTime $hEUREFIN
     * @return ITKISMR
     */
    public function setHEUREFIN($hEUREFIN)
    {
        $this->hEUREFIN = $hEUREFIN;

        return $this;
    }

    /**
     * Get hEUREFIN
     *
     * @return \DateTime 
     */
    public function getHEUREFIN()
    {
        return $this->hEUREFIN;
    }

    /**
     * Set iTKISMID
     *
     * @param integer $iTKISMID
     * @return ITKISMR
     */
    public function setITKISMID($iTKISMID)
    {
        $this->iTKISMID = $iTKISMID;

        return $this;
    }

    /**
     * Get iTKISMID
     *
     * @return integer 
     */
    public function getITKISMID()
    {
        return $this->iTKISMID;
    }

    /**
     * Set jOURABSOLU
     *
     * @param integer $jOURABSOLU
     * @return ITKISMR
     */
    public function setJOURABSOLU($jOURABSOLU)
    {
        $this->jOURABSOLU = $jOURABSOLU;

        return $this;
    }

    /**
     * Get jOURABSOLU
     *
     * @return integer 
     */
    public function getJOURABSOLU()
    {
        return $this->jOURABSOLU;
    }

    /**
     * Set lOTNO
     *
     * @param integer $lOTNO
     * @return ITKISMR
     */
    public function setLOTNO($lOTNO)
    {
        $this->lOTNO = $lOTNO;

        return $this;
    }

    /**
     * Get lOTNO
     *
     * @return integer 
     */
    public function getLOTNO()
    {
        return $this->lOTNO;
    }

    /**
     * Set mATERIELID
     *
     * @param integer $mATERIELID
     * @return ITKISMR
     */
    public function setMATERIELID($mATERIELID)
    {
        $this->mATERIELID = $mATERIELID;

        return $this;
    }

    /**
     * Get mATERIELID
     *
     * @return integer 
     */
    public function getMATERIELID()
    {
        return $this->mATERIELID;
    }

    /**
     * Set mODEAPPLICATION
     *
     * @param string $mODEAPPLICATION
     * @return ITKISMR
     */
    public function setMODEAPPLICATION($mODEAPPLICATION)
    {
        $this->mODEAPPLICATION = $mODEAPPLICATION;

        return $this;
    }

    /**
     * Get mODEAPPLICATION
     *
     * @return string 
     */
    public function getMODEAPPLICATION()
    {
        return $this->mODEAPPLICATION;
    }

    /**
     * Set pERIODE
     *
     * @param integer $pERIODE
     * @return ITKISMR
     */
    public function setPERIODE($pERIODE)
    {
        $this->pERIODE = $pERIODE;

        return $this;
    }

    /**
     * Get pERIODE
     *
     * @return integer 
     */
    public function getPERIODE()
    {
        return $this->pERIODE;
    }

    /**
     * Set pRESCRIPTEURID
     *
     * @param integer $pRESCRIPTEURID
     * @return ITKISMR
     */
    public function setPRESCRIPTEURID($pRESCRIPTEURID)
    {
        $this->pRESCRIPTEURID = $pRESCRIPTEURID;

        return $this;
    }

    /**
     * Get pRESCRIPTEURID
     *
     * @return integer 
     */
    public function getPRESCRIPTEURID()
    {
        return $this->pRESCRIPTEURID;
    }

    /**
     * Set qTESEM
     *
     * @param string $qTESEM
     * @return ITKISMR
     */
    public function setQTESEM($qTESEM)
    {
        $this->qTESEM = $qTESEM;

        return $this;
    }

    /**
     * Get qTESEM
     *
     * @return string 
     */
    public function getQTESEM()
    {
        return $this->qTESEM;
    }

    /**
     * Set qTESEMTOT
     *
     * @param string $qTESEMTOT
     * @return ITKISMR
     */
    public function setQTESEMTOT($qTESEMTOT)
    {
        $this->qTESEMTOT = $qTESEMTOT;

        return $this;
    }

    /**
     * Get qTESEMTOT
     *
     * @return string 
     */
    public function getQTESEMTOT()
    {
        return $this->qTESEMTOT;
    }

    /**
     * Set rANGNB
     *
     * @param string $rANGNB
     * @return ITKISMR
     */
    public function setRANGNB($rANGNB)
    {
        $this->rANGNB = $rANGNB;

        return $this;
    }

    /**
     * Get rANGNB
     *
     * @return string 
     */
    public function getRANGNB()
    {
        return $this->rANGNB;
    }

    /**
     * Set sGESTEID
     *
     * @param integer $sGESTEID
     * @return ITKISMR
     */
    public function setSGESTEID($sGESTEID)
    {
        $this->sGESTEID = $sGESTEID;

        return $this;
    }

    /**
     * Get sGESTEID
     *
     * @return integer 
     */
    public function getSGESTEID()
    {
        return $this->sGESTEID;
    }

    /**
     * Set sURFACE
     *
     * @param string $sURFACE
     * @return ITKISMR
     */
    public function setSURFACE($sURFACE)
    {
        $this->sURFACE = $sURFACE;

        return $this;
    }

    /**
     * Get sURFACE
     *
     * @return string 
     */
    public function getSURFACE()
    {
        return $this->sURFACE;
    }

    /**
     * Set sURFACETOT
     *
     * @param string $sURFACETOT
     * @return ITKISMR
     */
    public function setSURFACETOT($sURFACETOT)
    {
        $this->sURFACETOT = $sURFACETOT;

        return $this;
    }

    /**
     * Get sURFACETOT
     *
     * @return string 
     */
    public function getSURFACETOT()
    {
        return $this->sURFACETOT;
    }

    /**
     * Set tYPEINSTRUCTIONID
     *
     * @param integer $tYPEINSTRUCTIONID
     * @return ITKISMR
     */
    public function setTYPEINSTRUCTIONID($tYPEINSTRUCTIONID)
    {
        $this->tYPEINSTRUCTIONID = $tYPEINSTRUCTIONID;

        return $this;
    }

    /**
     * Get tYPEINSTRUCTIONID
     *
     * @return integer 
     */
    public function getTYPEINSTRUCTIONID()
    {
        return $this->tYPEINSTRUCTIONID;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKISMR
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKISMR
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set uSERMAJ
     *
     * @param string $uSERMAJ
     * @return ITKISMR
     */
    public function setUSERMAJ($uSERMAJ)
    {
        $this->uSERMAJ = $uSERMAJ;

        return $this;
    }

    /**
     * Get uSERMAJ
     *
     * @return string 
     */
    public function getUSERMAJ()
    {
        return $this->uSERMAJ;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCOMPTEURDEBUT()
    {
        return $this->cOMPTEURDEBUT;
    }

    /**
     * @param int $cOMPTEURDEBUT
     */
    public function setCOMPTEURDEBUT($cOMPTEURDEBUT)
    {
        $this->cOMPTEURDEBUT = $cOMPTEURDEBUT;
    }

    /**
     * @return int
     */
    public function getCOMPTEURFIN()
    {
        return $this->cOMPTEURFIN;
    }

    /**
     * @param int $cOMPTEURFIN
     */
    public function setCOMPTEURFIN($cOMPTEURFIN)
    {
        $this->cOMPTEURFIN = $cOMPTEURFIN;
    }

}
