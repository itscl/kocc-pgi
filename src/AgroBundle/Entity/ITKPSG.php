<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKPSG
 */
class ITKPSG
{
    /**
     * @var integer
     */
    private $aNNEESEM;

    /**
     * @var integer
     */
    private $aPPAREILID;

    /**
     * @var integer
     */
    private $aPPLICATEURID;

    /**
     * @var integer
     */
    private $bOUILLIE;

    /**
     * @var integer
     */
    private $cHAUFFEURID;

    /**
     * @var integer
     */
    private $cHEFEQUIPEID;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var string
     */
    private $cOMMENTAIREAUTRE;

    /**
     * @var string
     */
    private $cOMPUTERMAJ;

    /**
     * @var string
     */
    private $cONSIGNEAPPLICATION;

    /**
     * @var \DateTime
     */
    private $dAR;

    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var \DateTime
     */
    private $dATEMAJ;

    /**
     * @var \DateTime
     */
    private $dATEP;

    /**
     * @var \DateTime
     */
    private $dATEPFIN;

    /**
     * @var integer
     */
    private $dUREE;

    /**
     * @var string
     */
    private $eFFECTIF;

    /**
     * @var string
     */
    private $eTATINSTRUCTION;

    /**
     * @var \DateTime
     */
    private $hEUREDEBUT;

    /**
     * @var \DateTime
     */
    private $hEUREFIN;

    /**
     * @var string
     */
    private $hUMIDITE;

    /**
     * @var string
     */
    private $iTKSTADE;

    /**
     * @var string
     */
    private $mODEAPPLICATION;

    /**
     * @var string
     */
    private $pLUIE;

    /**
     * @var string
     */
    private $qUALITE;

    /**
     * @var string
     */
    private $sTATUS;

    /**
     * @var string
     */
    private $sURFACE;

    /**
     * @var string
     */
    private $sURFACETOT;

    /**
     * @var string
     */
    private $tEMPERATURE;

    /**
     * @var string
     */
    private $tYPEALERTE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var string
     */
    private $uSERMAJ;

    /**
     * @var string
     */
    private $vENT;

    /**
     * @var integer
     */
    private $iTKPID;

    /**
     * @var integer
     */
    private $iTKPSGOBSID;

    /**
     * @var integer
     */
    private $jOURABSOLU;

    /**
     * @var integer
     */
    private $mATERIELID;

    /**
     * @var integer
     */
    private $nBREPETITION;

    /**
     * @var integer
     */
    private $oBSERVATEURID;

    /**
     * @var integer
     */
    private $oRIGINSITEID;

    /**
     * @var integer
     */
    private $pERIODE;

    /**
     * @var integer
     */
    private $pLANOBSID;

    /**
     * @var integer
     */
    private $pRESCRIPTEURID;

    /**
     * @var integer
     */
    private $qTEARECOLTER;

    /**
     * @var integer
     */
    private $qTERECOLTEE;

    /**
     * @var integer
     */
    private $rDTBRUT;

    /**
     * @var integer
     */
    private $sGESTEID;

    /**
     * @var integer
     */
    private $tYPEINSTRUCTIONID;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set aNNEESEM
     *
     * @param integer $aNNEESEM
     * @return ITKPSG
     */
    public function setANNEESEM($aNNEESEM)
    {
        $this->aNNEESEM = $aNNEESEM;

        return $this;
    }

    /**
     * Get aNNEESEM
     *
     * @return integer 
     */
    public function getANNEESEM()
    {
        return $this->aNNEESEM;
    }

    /**
     * Set aPPAREILID
     *
     * @param integer $aPPAREILID
     * @return ITKPSG
     */
    public function setAPPAREILID($aPPAREILID)
    {
        $this->aPPAREILID = $aPPAREILID;

        return $this;
    }

    /**
     * Get aPPAREILID
     *
     * @return integer 
     */
    public function getAPPAREILID()
    {
        return $this->aPPAREILID;
    }

    /**
     * Set aPPLICATEURID
     *
     * @param integer $aPPLICATEURID
     * @return ITKPSG
     */
    public function setAPPLICATEURID($aPPLICATEURID)
    {
        $this->aPPLICATEURID = $aPPLICATEURID;

        return $this;
    }

    /**
     * Get aPPLICATEURID
     *
     * @return integer 
     */
    public function getAPPLICATEURID()
    {
        return $this->aPPLICATEURID;
    }

    /**
     * Set bOUILLIE
     *
     * @param integer $bOUILLIE
     * @return ITKPSG
     */
    public function setBOUILLIE($bOUILLIE)
    {
        $this->bOUILLIE = $bOUILLIE;

        return $this;
    }

    /**
     * Get bOUILLIE
     *
     * @return integer 
     */
    public function getBOUILLIE()
    {
        return $this->bOUILLIE;
    }

    /**
     * Set cHAUFFEURID
     *
     * @param integer $cHAUFFEURID
     * @return ITKPSG
     */
    public function setCHAUFFEURID($cHAUFFEURID)
    {
        $this->cHAUFFEURID = $cHAUFFEURID;

        return $this;
    }

    /**
     * Get cHAUFFEURID
     *
     * @return integer 
     */
    public function getCHAUFFEURID()
    {
        return $this->cHAUFFEURID;
    }

    /**
     * Set cHEFEQUIPEID
     *
     * @param integer $cHEFEQUIPEID
     * @return ITKPSG
     */
    public function setCHEFEQUIPEID($cHEFEQUIPEID)
    {
        $this->cHEFEQUIPEID = $cHEFEQUIPEID;

        return $this;
    }

    /**
     * Get cHEFEQUIPEID
     *
     * @return integer 
     */
    public function getCHEFEQUIPEID()
    {
        return $this->cHEFEQUIPEID;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return ITKPSG
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string 
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set cOMMENTAIREAUTRE
     *
     * @param string $cOMMENTAIREAUTRE
     * @return ITKPSG
     */
    public function setCOMMENTAIREAUTRE($cOMMENTAIREAUTRE)
    {
        $this->cOMMENTAIREAUTRE = $cOMMENTAIREAUTRE;

        return $this;
    }

    /**
     * Get cOMMENTAIREAUTRE
     *
     * @return string 
     */
    public function getCOMMENTAIREAUTRE()
    {
        return $this->cOMMENTAIREAUTRE;
    }

    /**
     * Set cOMPUTERMAJ
     *
     * @param string $cOMPUTERMAJ
     * @return ITKPSG
     */
    public function setCOMPUTERMAJ($cOMPUTERMAJ)
    {
        $this->cOMPUTERMAJ = $cOMPUTERMAJ;

        return $this;
    }

    /**
     * Get cOMPUTERMAJ
     *
     * @return string 
     */
    public function getCOMPUTERMAJ()
    {
        return $this->cOMPUTERMAJ;
    }

    /**
     * Set cONSIGNEAPPLICATION
     *
     * @param string $cONSIGNEAPPLICATION
     * @return ITKPSG
     */
    public function setCONSIGNEAPPLICATION($cONSIGNEAPPLICATION)
    {
        $this->cONSIGNEAPPLICATION = $cONSIGNEAPPLICATION;

        return $this;
    }

    /**
     * Get cONSIGNEAPPLICATION
     *
     * @return string 
     */
    public function getCONSIGNEAPPLICATION()
    {
        return $this->cONSIGNEAPPLICATION;
    }

    /**
     * Set dAR
     *
     * @param \DateTime $dAR
     * @return ITKPSG
     */
    public function setDAR($dAR)
    {
        $this->dAR = $dAR;

        return $this;
    }

    /**
     * Get dAR
     *
     * @return \DateTime 
     */
    public function getDAR()
    {
        return $this->dAR;
    }

    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return ITKPSG
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime 
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ITKPSG
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set dATEMAJ
     *
     * @param \DateTime $dATEMAJ
     * @return ITKPSG
     */
    public function setDATEMAJ($dATEMAJ)
    {
        $this->dATEMAJ = $dATEMAJ;

        return $this;
    }

    /**
     * Get dATEMAJ
     *
     * @return \DateTime 
     */
    public function getDATEMAJ()
    {
        return $this->dATEMAJ;
    }

    /**
     * Set dATEP
     *
     * @param \DateTime $dATEP
     * @return ITKPSG
     */
    public function setDATEP($dATEP)
    {
        $this->dATEP = $dATEP;

        return $this;
    }

    /**
     * Get dATEP
     *
     * @return \DateTime 
     */
    public function getDATEP()
    {
        return $this->dATEP;
    }

    /**
     * Set dATEPFIN
     *
     * @param \DateTime $dATEPFIN
     * @return ITKPSG
     */
    public function setDATEPFIN($dATEPFIN)
    {
        $this->dATEPFIN = $dATEPFIN;

        return $this;
    }

    /**
     * Get dATEPFIN
     *
     * @return \DateTime 
     */
    public function getDATEPFIN()
    {
        return $this->dATEPFIN;
    }

    /**
     * Set dUREE
     *
     * @param integer $dUREE
     * @return ITKPSG
     */
    public function setDUREE($dUREE)
    {
        $this->dUREE = $dUREE;

        return $this;
    }

    /**
     * Get dUREE
     *
     * @return integer 
     */
    public function getDUREE()
    {
        return $this->dUREE;
    }

    /**
     * Set eFFECTIF
     *
     * @param string $eFFECTIF
     * @return ITKPSG
     */
    public function setEFFECTIF($eFFECTIF)
    {
        $this->eFFECTIF = $eFFECTIF;

        return $this;
    }

    /**
     * Get eFFECTIF
     *
     * @return string 
     */
    public function getEFFECTIF()
    {
        return $this->eFFECTIF;
    }

    /**
     * Set eTATINSTRUCTION
     *
     * @param string $eTATINSTRUCTION
     * @return ITKPSG
     */
    public function setETATINSTRUCTION($eTATINSTRUCTION)
    {
        $this->eTATINSTRUCTION = $eTATINSTRUCTION;

        return $this;
    }

    /**
     * Get eTATINSTRUCTION
     *
     * @return string 
     */
    public function getETATINSTRUCTION()
    {
        return $this->eTATINSTRUCTION;
    }

    /**
     * Set hEUREDEBUT
     *
     * @param \DateTime $hEUREDEBUT
     * @return ITKPSG
     */
    public function setHEUREDEBUT($hEUREDEBUT)
    {
        $this->hEUREDEBUT = $hEUREDEBUT;

        return $this;
    }

    /**
     * Get hEUREDEBUT
     *
     * @return \DateTime 
     */
    public function getHEUREDEBUT()
    {
        return $this->hEUREDEBUT;
    }

    /**
     * Set hEUREFIN
     *
     * @param \DateTime $hEUREFIN
     * @return ITKPSG
     */
    public function setHEUREFIN($hEUREFIN)
    {
        $this->hEUREFIN = $hEUREFIN;

        return $this;
    }

    /**
     * Get hEUREFIN
     *
     * @return \DateTime 
     */
    public function getHEUREFIN()
    {
        return $this->hEUREFIN;
    }

    /**
     * Set hUMIDITE
     *
     * @param string $hUMIDITE
     * @return ITKPSG
     */
    public function setHUMIDITE($hUMIDITE)
    {
        $this->hUMIDITE = $hUMIDITE;

        return $this;
    }

    /**
     * Get hUMIDITE
     *
     * @return string 
     */
    public function getHUMIDITE()
    {
        return $this->hUMIDITE;
    }

    /**
     * Set iTKSTADE
     *
     * @param string $iTKSTADE
     * @return ITKPSG
     */
    public function setITKSTADE($iTKSTADE)
    {
        $this->iTKSTADE = $iTKSTADE;

        return $this;
    }

    /**
     * Get iTKSTADE
     *
     * @return string 
     */
    public function getITKSTADE()
    {
        return $this->iTKSTADE;
    }

    /**
     * Set mODEAPPLICATION
     *
     * @param string $mODEAPPLICATION
     * @return ITKPSG
     */
    public function setMODEAPPLICATION($mODEAPPLICATION)
    {
        $this->mODEAPPLICATION = $mODEAPPLICATION;

        return $this;
    }

    /**
     * Get mODEAPPLICATION
     *
     * @return string 
     */
    public function getMODEAPPLICATION()
    {
        return $this->mODEAPPLICATION;
    }

    /**
     * Set pLUIE
     *
     * @param string $pLUIE
     * @return ITKPSG
     */
    public function setPLUIE($pLUIE)
    {
        $this->pLUIE = $pLUIE;

        return $this;
    }

    /**
     * Get pLUIE
     *
     * @return string 
     */
    public function getPLUIE()
    {
        return $this->pLUIE;
    }

    /**
     * Set qUALITE
     *
     * @param string $qUALITE
     * @return ITKPSG
     */
    public function setQUALITE($qUALITE)
    {
        $this->qUALITE = $qUALITE;

        return $this;
    }

    /**
     * Get qUALITE
     *
     * @return string 
     */
    public function getQUALITE()
    {
        return $this->qUALITE;
    }

    /**
     * Set sTATUS
     *
     * @param string $sTATUS
     * @return ITKPSG
     */
    public function setSTATUS($sTATUS)
    {
        $this->sTATUS = $sTATUS;

        return $this;
    }

    /**
     * Get sTATUS
     *
     * @return string 
     */
    public function getSTATUS()
    {
        return $this->sTATUS;
    }

    /**
     * Set sURFACE
     *
     * @param string $sURFACE
     * @return ITKPSG
     */
    public function setSURFACE($sURFACE)
    {
        $this->sURFACE = $sURFACE;

        return $this;
    }

    /**
     * Get sURFACE
     *
     * @return string 
     */
    public function getSURFACE()
    {
        return $this->sURFACE;
    }

    /**
     * Set sURFACETOT
     *
     * @param string $sURFACETOT
     * @return ITKPSG
     */
    public function setSURFACETOT($sURFACETOT)
    {
        $this->sURFACETOT = $sURFACETOT;

        return $this;
    }

    /**
     * Get sURFACETOT
     *
     * @return string 
     */
    public function getSURFACETOT()
    {
        return $this->sURFACETOT;
    }

    /**
     * Set tEMPERATURE
     *
     * @param string $tEMPERATURE
     * @return ITKPSG
     */
    public function setTEMPERATURE($tEMPERATURE)
    {
        $this->tEMPERATURE = $tEMPERATURE;

        return $this;
    }

    /**
     * Get tEMPERATURE
     *
     * @return string 
     */
    public function getTEMPERATURE()
    {
        return $this->tEMPERATURE;
    }

    /**
     * Set tYPEALERTE
     *
     * @param string $tYPEALERTE
     * @return ITKPSG
     */
    public function setTYPEALERTE($tYPEALERTE)
    {
        $this->tYPEALERTE = $tYPEALERTE;

        return $this;
    }

    /**
     * Get tYPEALERTE
     *
     * @return string 
     */
    public function getTYPEALERTE()
    {
        return $this->tYPEALERTE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKPSG
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKPSG
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set uSERMAJ
     *
     * @param string $uSERMAJ
     * @return ITKPSG
     */
    public function setUSERMAJ($uSERMAJ)
    {
        $this->uSERMAJ = $uSERMAJ;

        return $this;
    }

    /**
     * Get uSERMAJ
     *
     * @return string 
     */
    public function getUSERMAJ()
    {
        return $this->uSERMAJ;
    }

    /**
     * Set vENT
     *
     * @param string $vENT
     * @return ITKPSG
     */
    public function setVENT($vENT)
    {
        $this->vENT = $vENT;

        return $this;
    }

    /**
     * Get vENT
     *
     * @return string 
     */
    public function getVENT()
    {
        return $this->vENT;
    }

    /**
     * Set iTKPID
     *
     * @param integer $iTKPID
     * @return ITKPSG
     */
    public function setITKPID($iTKPID)
    {
        $this->iTKPID = $iTKPID;

        return $this;
    }

    /**
     * Get iTKPID
     *
     * @return integer 
     */
    public function getITKPID()
    {
        return $this->iTKPID;
    }

    /**
     * Set iTKPSGOBSID
     *
     * @param integer $iTKPSGOBSID
     * @return ITKPSG
     */
    public function setITKPSGOBSID($iTKPSGOBSID)
    {
        $this->iTKPSGOBSID = $iTKPSGOBSID;

        return $this;
    }

    /**
     * Get iTKPSGOBSID
     *
     * @return integer 
     */
    public function getITKPSGOBSID()
    {
        return $this->iTKPSGOBSID;
    }

    /**
     * Set jOURABSOLU
     *
     * @param integer $jOURABSOLU
     * @return ITKPSG
     */
    public function setJOURABSOLU($jOURABSOLU)
    {
        $this->jOURABSOLU = $jOURABSOLU;

        return $this;
    }

    /**
     * Get jOURABSOLU
     *
     * @return integer 
     */
    public function getJOURABSOLU()
    {
        return $this->jOURABSOLU;
    }

    /**
     * Set mATERIELID
     *
     * @param integer $mATERIELID
     * @return ITKPSG
     */
    public function setMATERIELID($mATERIELID)
    {
        $this->mATERIELID = $mATERIELID;

        return $this;
    }

    /**
     * Get mATERIELID
     *
     * @return integer 
     */
    public function getMATERIELID()
    {
        return $this->mATERIELID;
    }

    /**
     * Set nBREPETITION
     *
     * @param integer $nBREPETITION
     * @return ITKPSG
     */
    public function setNBREPETITION($nBREPETITION)
    {
        $this->nBREPETITION = $nBREPETITION;

        return $this;
    }

    /**
     * Get nBREPETITION
     *
     * @return integer 
     */
    public function getNBREPETITION()
    {
        return $this->nBREPETITION;
    }

    /**
     * Set oBSERVATEURID
     *
     * @param integer $oBSERVATEURID
     * @return ITKPSG
     */
    public function setOBSERVATEURID($oBSERVATEURID)
    {
        $this->oBSERVATEURID = $oBSERVATEURID;

        return $this;
    }

    /**
     * Get oBSERVATEURID
     *
     * @return integer 
     */
    public function getOBSERVATEURID()
    {
        return $this->oBSERVATEURID;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ITKPSG
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set pERIODE
     *
     * @param integer $pERIODE
     * @return ITKPSG
     */
    public function setPERIODE($pERIODE)
    {
        $this->pERIODE = $pERIODE;

        return $this;
    }

    /**
     * Get pERIODE
     *
     * @return integer 
     */
    public function getPERIODE()
    {
        return $this->pERIODE;
    }

    /**
     * Set pLANOBSID
     *
     * @param integer $pLANOBSID
     * @return ITKPSG
     */
    public function setPLANOBSID($pLANOBSID)
    {
        $this->pLANOBSID = $pLANOBSID;

        return $this;
    }

    /**
     * Get pLANOBSID
     *
     * @return integer 
     */
    public function getPLANOBSID()
    {
        return $this->pLANOBSID;
    }

    /**
     * Set pRESCRIPTEURID
     *
     * @param integer $pRESCRIPTEURID
     * @return ITKPSG
     */
    public function setPRESCRIPTEURID($pRESCRIPTEURID)
    {
        $this->pRESCRIPTEURID = $pRESCRIPTEURID;

        return $this;
    }

    /**
     * Get pRESCRIPTEURID
     *
     * @return integer 
     */
    public function getPRESCRIPTEURID()
    {
        return $this->pRESCRIPTEURID;
    }

    /**
     * Set qTEARECOLTER
     *
     * @param integer $qTEARECOLTER
     * @return ITKPSG
     */
    public function setQTEARECOLTER($qTEARECOLTER)
    {
        $this->qTEARECOLTER = $qTEARECOLTER;

        return $this;
    }

    /**
     * Get qTEARECOLTER
     *
     * @return integer 
     */
    public function getQTEARECOLTER()
    {
        return $this->qTEARECOLTER;
    }

    /**
     * Set qTERECOLTEE
     *
     * @param integer $qTERECOLTEE
     * @return ITKPSG
     */
    public function setQTERECOLTEE($qTERECOLTEE)
    {
        $this->qTERECOLTEE = $qTERECOLTEE;

        return $this;
    }

    /**
     * Get qTERECOLTEE
     *
     * @return integer 
     */
    public function getQTERECOLTEE()
    {
        return $this->qTERECOLTEE;
    }

    /**
     * Set rDTBRUT
     *
     * @param integer $rDTBRUT
     * @return ITKPSG
     */
    public function setRDTBRUT($rDTBRUT)
    {
        $this->rDTBRUT = $rDTBRUT;

        return $this;
    }

    /**
     * Get rDTBRUT
     *
     * @return integer 
     */
    public function getRDTBRUT()
    {
        return $this->rDTBRUT;
    }

    /**
     * Set sGESTEID
     *
     * @param integer $sGESTEID
     * @return ITKPSG
     */
    public function setSGESTEID($sGESTEID)
    {
        $this->sGESTEID = $sGESTEID;

        return $this;
    }

    /**
     * Get sGESTEID
     *
     * @return integer 
     */
    public function getSGESTEID()
    {
        return $this->sGESTEID;
    }

    /**
     * Set tYPEINSTRUCTIONID
     *
     * @param integer $tYPEINSTRUCTIONID
     * @return ITKPSG
     */
    public function setTYPEINSTRUCTIONID($tYPEINSTRUCTIONID)
    {
        $this->tYPEINSTRUCTIONID = $tYPEINSTRUCTIONID;

        return $this;
    }

    /**
     * Get tYPEINSTRUCTIONID
     *
     * @return integer 
     */
    public function getTYPEINSTRUCTIONID()
    {
        return $this->tYPEINSTRUCTIONID;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
