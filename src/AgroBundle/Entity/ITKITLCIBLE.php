<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKITLCIBLE
 */
class ITKITLCIBLE
{
    /**
     * @var integer
     */
    private $cIBLEID;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var integer
     */
    private $iTKITLID;

    /**
     * @var integer
     */
    private $oRIGINSITEID;

    /**
     * @var integer
     */
    private $sEUILTRAITEMENT;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set cIBLEID
     *
     * @param integer $cIBLEID
     * @return ITKITLCIBLE
     */
    public function setCIBLEID($cIBLEID)
    {
        $this->cIBLEID = $cIBLEID;

        return $this;
    }

    /**
     * Get cIBLEID
     *
     * @return integer 
     */
    public function getCIBLEID()
    {
        return $this->cIBLEID;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ITKITLCIBLE
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set iTKITLID
     *
     * @param integer $iTKITLID
     * @return ITKITLCIBLE
     */
    public function setITKITLID($iTKITLID)
    {
        $this->iTKITLID = $iTKITLID;

        return $this;
    }

    /**
     * Get iTKITLID
     *
     * @return integer 
     */
    public function getITKITLID()
    {
        return $this->iTKITLID;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ITKITLCIBLE
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set sEUILTRAITEMENT
     *
     * @param integer $sEUILTRAITEMENT
     * @return ITKITLCIBLE
     */
    public function setSEUILTRAITEMENT($sEUILTRAITEMENT)
    {
        $this->sEUILTRAITEMENT = $sEUILTRAITEMENT;

        return $this;
    }

    /**
     * Get sEUILTRAITEMENT
     *
     * @return integer 
     */
    public function getSEUILTRAITEMENT()
    {
        return $this->sEUILTRAITEMENT;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKITLCIBLE
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKITLCIBLE
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
