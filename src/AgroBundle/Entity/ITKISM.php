<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKISM
 */
class ITKISM
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var int
     */
    private $fERMEID;

    /**
     * @var int
     */
    private $cOMPTEURDEBUT;

    /**
     * @var int
     */
    private $cOMPTEURFIN;

    /**
     * @var int
     */
    private $cYCLECULTURALID;

    /**
     * @var \DateTime
     */
    private $dATEP;

    /**
     * @var int
     */
    private $dUREE;

    /**
     * @var \DateTime
     */
    private $dATEPFIN;

    /**
     * @var int
     */
    private $cULTUREID;

    /**
     * @var int
     */
    private $pRESCRIPTEURID;

    /**
     * @var int
     */
    private $aPPLICATEURID;

    /**
     * @var int
     */
    private $mATERIELID;

    /**
     * @var int
     */
    private $aPPAREILID;

    /**
     * @var int
     */
    private $cHAUFFEURID;

    /**
     * @var string
     */
    private $mODEAPPLICATION;

    /**
     * @var string
     */
    private $sURFACE;

    /**
     * @var string
     */
    private $sURFACETOT;

    /**
     * @var \DateTime
     */
    private $hEUREDEBUT;

    /**
     * @var \DateTime
     */
    private $hEUREFIN;

    /**
     * @var string
     */
    private $eLIGNE;

    /**
     * @var string
     */
    private $eSEMIS;

    /**
     * @var string
     */
    private $qTESEM;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var string
     */
    private $eTATINSTRUCTION;

    /**
     * @var string
     */
    private $qTESEMTOT;

    /**
     * @var string
     */
    private $tYPEMVEGETAL;

    /**
     * @var int
     */
    private $pERIODE;

    /**
     * @var int
     */
    private $jOURABSOLU;

    /**
     * @var int
     */
    private $tYPEINSTRUCTIONID;

    /**
     * @var int
     */
    private $aNNEESEM;

    /**
     * @var string
     */
    private $tYPESEMENCE;

    /**
     * @var string
     */
    private $eFFECTIF;

    /**
     * @var string
     */
    private $rANGNB;

    /**
     * @var \DateTime
     */
    private $dATEMAJ;

    /**
     * @var string
     */
    private $uSERMAJ;

    /**
     * @var string
     */
    private $cOMPUTERMAJ;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return ITKISM
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime 
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set fERMEID
     *
     * @param integer $fERMEID
     * @return ITKISM
     */
    public function setFERMEID($fERMEID)
    {
        $this->fERMEID = $fERMEID;

        return $this;
    }

    /**
     * Get fERMEID
     *
     * @return integer 
     */
    public function getFERMEID()
    {
        return $this->fERMEID;
    }

    /**
     * Set cYCLECULTURALID
     *
     * @param integer $cYCLECULTURALID
     * @return ITKISM
     */
    public function setCYCLECULTURALID($cYCLECULTURALID)
    {
        $this->cYCLECULTURALID = $cYCLECULTURALID;

        return $this;
    }

    /**
     * Get cYCLECULTURALID
     *
     * @return integer 
     */
    public function getCYCLECULTURALID()
    {
        return $this->cYCLECULTURALID;
    }

    /**
     * Set dATEP
     *
     * @param \DateTime $dATEP
     * @return ITKISM
     */
    public function setDATEP($dATEP)
    {
        $this->dATEP = $dATEP;

        return $this;
    }

    /**
     * Get dATEP
     *
     * @return \DateTime 
     */
    public function getDATEP()
    {
        return $this->dATEP;
    }

    /**
     * Set dUREE
     *
     * @param integer $dUREE
     * @return ITKISM
     */
    public function setDUREE($dUREE)
    {
        $this->dUREE = $dUREE;

        return $this;
    }

    /**
     * Get dUREE
     *
     * @return integer 
     */
    public function getDUREE()
    {
        return $this->dUREE;
    }

    /**
     * Set dATEPFIN
     *
     * @param \DateTime $dATEPFIN
     * @return ITKISM
     */
    public function setDATEPFIN($dATEPFIN)
    {
        $this->dATEPFIN = $dATEPFIN;

        return $this;
    }

    /**
     * Get dATEPFIN
     *
     * @return \DateTime 
     */
    public function getDATEPFIN()
    {
        return $this->dATEPFIN;
    }

    /**
     * Set cULTUREID
     *
     * @param integer $cULTUREID
     * @return ITKISM
     */
    public function setCULTUREID($cULTUREID)
    {
        $this->cULTUREID = $cULTUREID;

        return $this;
    }

    /**
     * Get cULTUREID
     *
     * @return integer 
     */
    public function getCULTUREID()
    {
        return $this->cULTUREID;
    }

    /**
     * Set pRESCRIPTEURID
     *
     * @param integer $pRESCRIPTEURID
     * @return ITKISM
     */
    public function setPRESCRIPTEURID($pRESCRIPTEURID)
    {
        $this->pRESCRIPTEURID = $pRESCRIPTEURID;

        return $this;
    }

    /**
     * Get pRESCRIPTEURID
     *
     * @return integer 
     */
    public function getPRESCRIPTEURID()
    {
        return $this->pRESCRIPTEURID;
    }

    /**
     * Set aPPLICATEURID
     *
     * @param integer $aPPLICATEURID
     * @return ITKISM
     */
    public function setAPPLICATEURID($aPPLICATEURID)
    {
        $this->aPPLICATEURID = $aPPLICATEURID;

        return $this;
    }

    /**
     * Get aPPLICATEURID
     *
     * @return integer 
     */
    public function getAPPLICATEURID()
    {
        return $this->aPPLICATEURID;
    }

    /**
     * Set mATERIELID
     *
     * @param integer $mATERIELID
     * @return ITKISM
     */
    public function setMATERIELID($mATERIELID)
    {
        $this->mATERIELID = $mATERIELID;

        return $this;
    }

    /**
     * Get mATERIELID
     *
     * @return integer 
     */
    public function getMATERIELID()
    {
        return $this->mATERIELID;
    }

    /**
     * Set aPPAREILID
     *
     * @param integer $aPPAREILID
     * @return ITKISM
     */
    public function setAPPAREILID($aPPAREILID)
    {
        $this->aPPAREILID = $aPPAREILID;

        return $this;
    }

    /**
     * Get aPPAREILID
     *
     * @return integer 
     */
    public function getAPPAREILID()
    {
        return $this->aPPAREILID;
    }

    /**
     * Set cHAUFFEURID
     *
     * @param integer $cHAUFFEURID
     * @return ITKISM
     */
    public function setCHAUFFEURID($cHAUFFEURID)
    {
        $this->cHAUFFEURID = $cHAUFFEURID;

        return $this;
    }

    /**
     * Get cHAUFFEURID
     *
     * @return integer 
     */
    public function getCHAUFFEURID()
    {
        return $this->cHAUFFEURID;
    }

    /**
     * Set mODEAPPLICATION
     *
     * @param string $mODEAPPLICATION
     * @return ITKISM
     */
    public function setMODEAPPLICATION($mODEAPPLICATION)
    {
        $this->mODEAPPLICATION = $mODEAPPLICATION;

        return $this;
    }

    /**
     * Get mODEAPPLICATION
     *
     * @return string 
     */
    public function getMODEAPPLICATION()
    {
        return $this->mODEAPPLICATION;
    }

    /**
     * Set sURFACE
     *
     * @param string $sURFACE
     * @return ITKISM
     */
    public function setSURFACE($sURFACE)
    {
        $this->sURFACE = $sURFACE;

        return $this;
    }

    /**
     * Get sURFACE
     *
     * @return string 
     */
    public function getSURFACE()
    {
        return $this->sURFACE;
    }

    /**
     * Set sURFACETOT
     *
     * @param string $sURFACETOT
     * @return ITKISM
     */
    public function setSURFACETOT($sURFACETOT)
    {
        $this->sURFACETOT = $sURFACETOT;

        return $this;
    }

    /**
     * Get sURFACETOT
     *
     * @return string 
     */
    public function getSURFACETOT()
    {
        return $this->sURFACETOT;
    }

    /**
     * Set hEUREDEBUT
     *
     * @param \DateTime $hEUREDEBUT
     * @return ITKISM
     */
    public function setHEUREDEBUT($hEUREDEBUT)
    {
        $this->hEUREDEBUT = $hEUREDEBUT;

        return $this;
    }

    /**
     * Get hEUREDEBUT
     *
     * @return \DateTime 
     */
    public function getHEUREDEBUT()
    {
        return $this->hEUREDEBUT;
    }

    /**
     * Set hEUREFIN
     *
     * @param \DateTime $hEUREFIN
     * @return ITKISM
     */
    public function setHEUREFIN($hEUREFIN)
    {
        $this->hEUREFIN = $hEUREFIN;

        return $this;
    }

    /**
     * Get hEUREFIN
     *
     * @return \DateTime 
     */
    public function getHEUREFIN()
    {
        return $this->hEUREFIN;
    }

    /**
     * Set eLIGNE
     *
     * @param string $eLIGNE
     * @return ITKISM
     */
    public function setELIGNE($eLIGNE)
    {
        $this->eLIGNE = $eLIGNE;

        return $this;
    }

    /**
     * Get eLIGNE
     *
     * @return string 
     */
    public function getELIGNE()
    {
        return $this->eLIGNE;
    }

    /**
     * Set eSEMIS
     *
     * @param string $eSEMIS
     * @return ITKISM
     */
    public function setESEMIS($eSEMIS)
    {
        $this->eSEMIS = $eSEMIS;

        return $this;
    }

    /**
     * Get eSEMIS
     *
     * @return string 
     */
    public function getESEMIS()
    {
        return $this->eSEMIS;
    }

    /**
     * Set qTESEM
     *
     * @param string $qTESEM
     * @return ITKISM
     */
    public function setQTESEM($qTESEM)
    {
        $this->qTESEM = $qTESEM;

        return $this;
    }

    /**
     * Get qTESEM
     *
     * @return string 
     */
    public function getQTESEM()
    {
        return $this->qTESEM;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKISM
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKISM
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set eTATINSTRUCTION
     *
     * @param string $eTATINSTRUCTION
     * @return ITKISM
     */
    public function setETATINSTRUCTION($eTATINSTRUCTION)
    {
        $this->eTATINSTRUCTION = $eTATINSTRUCTION;

        return $this;
    }

    /**
     * Get eTATINSTRUCTION
     *
     * @return string 
     */
    public function getETATINSTRUCTION()
    {
        return $this->eTATINSTRUCTION;
    }

    /**
     * Set qTESEMTOT
     *
     * @param string $qTESEMTOT
     * @return ITKISM
     */
    public function setQTESEMTOT($qTESEMTOT)
    {
        $this->qTESEMTOT = $qTESEMTOT;

        return $this;
    }

    /**
     * Get qTESEMTOT
     *
     * @return string 
     */
    public function getQTESEMTOT()
    {
        return $this->qTESEMTOT;
    }

    /**
     * Set tYPEMVEGETAL
     *
     * @param string $tYPEMVEGETAL
     * @return ITKISM
     */
    public function setTYPEMVEGETAL($tYPEMVEGETAL)
    {
        $this->tYPEMVEGETAL = $tYPEMVEGETAL;

        return $this;
    }

    /**
     * Get tYPEMVEGETAL
     *
     * @return string 
     */
    public function getTYPEMVEGETAL()
    {
        return $this->tYPEMVEGETAL;
    }

    /**
     * Set pERIODE
     *
     * @param integer $pERIODE
     * @return ITKISM
     */
    public function setPERIODE($pERIODE)
    {
        $this->pERIODE = $pERIODE;

        return $this;
    }

    /**
     * Get pERIODE
     *
     * @return integer 
     */
    public function getPERIODE()
    {
        return $this->pERIODE;
    }

    /**
     * Set jOURABSOLU
     *
     * @param integer $jOURABSOLU
     * @return ITKISM
     */
    public function setJOURABSOLU($jOURABSOLU)
    {
        $this->jOURABSOLU = $jOURABSOLU;

        return $this;
    }

    /**
     * Get jOURABSOLU
     *
     * @return integer 
     */
    public function getJOURABSOLU()
    {
        return $this->jOURABSOLU;
    }

    /**
     * Set tYPEINSTRUCTIONID
     *
     * @param integer $tYPEINSTRUCTIONID
     * @return ITKISM
     */
    public function setTYPEINSTRUCTIONID($tYPEINSTRUCTIONID)
    {
        $this->tYPEINSTRUCTIONID = $tYPEINSTRUCTIONID;

        return $this;
    }

    /**
     * Get tYPEINSTRUCTIONID
     *
     * @return integer 
     */
    public function getTYPEINSTRUCTIONID()
    {
        return $this->tYPEINSTRUCTIONID;
    }

    /**
     * Set aNNEESEM
     *
     * @param integer $aNNEESEM
     * @return ITKISM
     */
    public function setANNEESEM($aNNEESEM)
    {
        $this->aNNEESEM = $aNNEESEM;

        return $this;
    }

    /**
     * Get aNNEESEM
     *
     * @return integer 
     */
    public function getANNEESEM()
    {
        return $this->aNNEESEM;
    }

    /**
     * Set tYPESEMENCE
     *
     * @param string $tYPESEMENCE
     * @return ITKISM
     */
    public function setTYPESEMENCE($tYPESEMENCE)
    {
        $this->tYPESEMENCE = $tYPESEMENCE;

        return $this;
    }

    /**
     * Get tYPESEMENCE
     *
     * @return string 
     */
    public function getTYPESEMENCE()
    {
        return $this->tYPESEMENCE;
    }

    /**
     * Set eFFECTIF
     *
     * @param string $eFFECTIF
     * @return ITKISM
     */
    public function setEFFECTIF($eFFECTIF)
    {
        $this->eFFECTIF = $eFFECTIF;

        return $this;
    }

    /**
     * Get eFFECTIF
     *
     * @return string 
     */
    public function getEFFECTIF()
    {
        return $this->eFFECTIF;
    }

    /**
     * Set rANGNB
     *
     * @param string $rANGNB
     * @return ITKISM
     */
    public function setRANGNB($rANGNB)
    {
        $this->rANGNB = $rANGNB;

        return $this;
    }

    /**
     * Get rANGNB
     *
     * @return string 
     */
    public function getRANGNB()
    {
        return $this->rANGNB;
    }

    /**
     * Set dATEMAJ
     *
     * @param \DateTime $dATEMAJ
     * @return ITKISM
     */
    public function setDATEMAJ($dATEMAJ)
    {
        $this->dATEMAJ = $dATEMAJ;

        return $this;
    }

    /**
     * Get dATEMAJ
     *
     * @return \DateTime 
     */
    public function getDATEMAJ()
    {
        return $this->dATEMAJ;
    }

    /**
     * Set uSERMAJ
     *
     * @param string $uSERMAJ
     * @return ITKISM
     */
    public function setUSERMAJ($uSERMAJ)
    {
        $this->uSERMAJ = $uSERMAJ;

        return $this;
    }

    /**
     * Get uSERMAJ
     *
     * @return string 
     */
    public function getUSERMAJ()
    {
        return $this->uSERMAJ;
    }

    /**
     * Set cOMPUTERMAJ
     *
     * @param string $cOMPUTERMAJ
     * @return ITKISM
     */
    public function setCOMPUTERMAJ($cOMPUTERMAJ)
    {
        $this->cOMPUTERMAJ = $cOMPUTERMAJ;

        return $this;
    }

    /**
     * Get cOMPUTERMAJ
     *
     * @return string 
     */
    public function getCOMPUTERMAJ()
    {
        return $this->cOMPUTERMAJ;
    }
    /**
     * @var integer
     */
    private $sGESTEID;


    /**
     * Set sGESTEID
     *
     * @param integer $sGESTEID
     * @return ITKISM
     */
    public function setSGESTEID($sGESTEID)
    {
        $this->sGESTEID = $sGESTEID;

        return $this;
    }

    /**
     * Get sGESTEID
     *
     * @return integer 
     */
    public function getSGESTEID()
    {
        return $this->sGESTEID;
    }

    /**
     * @return int
     */
    public function getCOMPTEURFIN()
    {
        return $this->cOMPTEURFIN;
    }

    /**
     * @param int $cOMPTEURFIN
     */
    public function setCOMPTEURFIN($cOMPTEURFIN)
    {
        $this->cOMPTEURFIN = $cOMPTEURFIN;
    }

    /**
     * @return int
     */
    public function getCOMPTEURDEBUT()
    {
        return $this->cOMPTEURDEBUT;
    }

    /**
     * @param int $cOMPTEURDEBUT
     */
    public function setCOMPTEURDEBUT($cOMPTEURDEBUT)
    {
        $this->cOMPTEURDEBUT = $cOMPTEURDEBUT;
    }

}
