<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKISMRLOT
 */
class ITKISMRLOT
{
    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var integer
     */
    private $iTKISMRID;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $lOTNO;

    /**
     * @var integer
     */
    private $oRIGINSITEID;

    /**
     * @var string
     */
    private $qTE;

    /**
     * @var integer
     */
    private $sEMENCELOTID;

    /**
     * @var string
     */
    private $uNITE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ITKISMRLOT
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set iTKISMRID
     *
     * @param integer $iTKISMRID
     * @return ITKISMRLOT
     */
    public function setITKISMRID($iTKISMRID)
    {
        $this->iTKISMRID = $iTKISMRID;

        return $this;
    }

    /**
     * Get iTKISMRID
     *
     * @return integer 
     */
    public function getITKISMRID()
    {
        return $this->iTKISMRID;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return ITKISMRLOT
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set lOTNO
     *
     * @param string $lOTNO
     * @return ITKISMRLOT
     */
    public function setLOTNO($lOTNO)
    {
        $this->lOTNO = $lOTNO;

        return $this;
    }

    /**
     * Get lOTNO
     *
     * @return string 
     */
    public function getLOTNO()
    {
        return $this->lOTNO;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ITKISMRLOT
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set qTE
     *
     * @param string $qTE
     * @return ITKISMRLOT
     */
    public function setQTE($qTE)
    {
        $this->qTE = $qTE;

        return $this;
    }

    /**
     * Get qTE
     *
     * @return string 
     */
    public function getQTE()
    {
        return $this->qTE;
    }

    /**
     * Set sEMENCELOTID
     *
     * @param integer $sEMENCELOTID
     * @return ITKISMRLOT
     */
    public function setSEMENCELOTID($sEMENCELOTID)
    {
        $this->sEMENCELOTID = $sEMENCELOTID;

        return $this;
    }

    /**
     * Get sEMENCELOTID
     *
     * @return integer 
     */
    public function getSEMENCELOTID()
    {
        return $this->sEMENCELOTID;
    }

    /**
     * Set uNITE
     *
     * @param string $uNITE
     * @return ITKISMRLOT
     */
    public function setUNITE($uNITE)
    {
        $this->uNITE = $uNITE;

        return $this;
    }

    /**
     * Get uNITE
     *
     * @return string 
     */
    public function getUNITE()
    {
        return $this->uNITE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKISMRLOT
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKISMRLOT
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
