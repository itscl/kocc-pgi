<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKISMRPA
 */
class ITKISMRPA
{
    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var integer
     */
    private $iTKISMRID;

    /**
     * @var integer
     */
    private $oRIGINSITEID;

    /**
     * @var string
     */
    private $pARCELLECHECK;

    /**
     * @var integer
     */
    private $pARCELLEID;

    /**
     * @var string
     */
    private $sURFACE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ITKISMRPA
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set iTKISMRID
     *
     * @param integer $iTKISMRID
     * @return ITKISMRPA
     */
    public function setITKISMRID($iTKISMRID)
    {
        $this->iTKISMRID = $iTKISMRID;

        return $this;
    }

    /**
     * Get iTKISMRID
     *
     * @return integer 
     */
    public function getITKISMRID()
    {
        return $this->iTKISMRID;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ITKISMRPA
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set pARCELLECHECK
     *
     * @param string $pARCELLECHECK
     * @return ITKISMRPA
     */
    public function setPARCELLECHECK($pARCELLECHECK)
    {
        $this->pARCELLECHECK = $pARCELLECHECK;

        return $this;
    }

    /**
     * Get pARCELLECHECK
     *
     * @return string 
     */
    public function getPARCELLECHECK()
    {
        return $this->pARCELLECHECK;
    }

    /**
     * Set pARCELLEID
     *
     * @param integer $pARCELLEID
     * @return ITKISMRPA
     */
    public function setPARCELLEID($pARCELLEID)
    {
        $this->pARCELLEID = $pARCELLEID;

        return $this;
    }

    /**
     * Get pARCELLEID
     *
     * @return integer 
     */
    public function getPARCELLEID()
    {
        return $this->pARCELLEID;
    }

    /**
     * Set sURFACE
     *
     * @param string $sURFACE
     * @return ITKISMRPA
     */
    public function setSURFACE($sURFACE)
    {
        $this->sURFACE = $sURFACE;

        return $this;
    }

    /**
     * Get sURFACE
     *
     * @return string 
     */
    public function getSURFACE()
    {
        return $this->sURFACE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKISMRPA
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKISMRPA
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
