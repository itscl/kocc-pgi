<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKITLR
 */
class ITKITLR
{
    /**
     * @var integer
     */
    private $aPPAREILID;

    /**
     * @var integer
     */
    private $aPPLICATEURID;

    /**
     * @var integer
     */
    private $bOUILLIE;

    /**
     * @var integer
     */
    private $cHAUFFEURID;

    /**
     * @var integer
     */
    private $cTRDEB;

    /**
     * @var integer
     */
    private $cTRFIN;

    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var \DateTime
     */
    private $dATEP;

    /**
     * @var \DateTime
     */
    private $dATEPFIN;

    /**
     * @var \DateTime
     */
    private $dATEREALISATION;

    /**
     * @var integer
     */
    private $dUREE;

    /**
     * @var integer
     */
    private $fERMEID;

    /**
     * @var \DateTime
     */
    private $hEUREDEBUT;

    /**
     * @var \DateTime
     */
    private $hEUREFIN;

    /**
     * @var string
     */
    private $hUMIDITE;

    /**
     * @var integer
     */
    private $iTKITLID;

    /**
     * @var string
     */
    private $lOTNO;

    /**
     * @var integer
     */
    private $mATERIELID;

    /**
     * @var string
     */
    private $mODEAPPLICATION;

    /**
     * @var string
     */
    private $pLUIE;

    /**
     * @var integer
     */
    private $pRESCRIPTEURID;

    /**
     * @var string
     */
    private $qTELOT;

    /**
     * @var string
     */
    private $tEMPERATURE;

    /**
     * @var string
     */
    private $uNITELOT;

    /**
     * @var integer
     */
    private $uNITETLOCALISEID;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var string
     */
    private $uSERMAJ;

    /**
     * @var string
     */
    private $vENT;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set aPPAREILID
     *
     * @param integer $aPPAREILID
     * @return ITKITLR
     */
    public function setAPPAREILID($aPPAREILID)
    {
        $this->aPPAREILID = $aPPAREILID;

        return $this;
    }

    /**
     * Get aPPAREILID
     *
     * @return integer 
     */
    public function getAPPAREILID()
    {
        return $this->aPPAREILID;
    }

    /**
     * Set aPPLICATEURID
     *
     * @param integer $aPPLICATEURID
     * @return ITKITLR
     */
    public function setAPPLICATEURID($aPPLICATEURID)
    {
        $this->aPPLICATEURID = $aPPLICATEURID;

        return $this;
    }

    /**
     * Get aPPLICATEURID
     *
     * @return integer 
     */
    public function getAPPLICATEURID()
    {
        return $this->aPPLICATEURID;
    }

    /**
     * Set bOUILLIE
     *
     * @param integer $bOUILLIE
     * @return ITKITLR
     */
    public function setBOUILLIE($bOUILLIE)
    {
        $this->bOUILLIE = $bOUILLIE;

        return $this;
    }

    /**
     * Get bOUILLIE
     *
     * @return integer 
     */
    public function getBOUILLIE()
    {
        return $this->bOUILLIE;
    }

    /**
     * Set cHAUFFEURID
     *
     * @param integer $cHAUFFEURID
     * @return ITKITLR
     */
    public function setCHAUFFEURID($cHAUFFEURID)
    {
        $this->cHAUFFEURID = $cHAUFFEURID;

        return $this;
    }

    /**
     * Get cHAUFFEURID
     *
     * @return integer 
     */
    public function getCHAUFFEURID()
    {
        return $this->cHAUFFEURID;
    }

    /**
     * Set cTRDEB
     *
     * @param integer $cTRDEB
     * @return ITKITLR
     */
    public function setCTRDEB($cTRDEB)
    {
        $this->cTRDEB = $cTRDEB;

        return $this;
    }

    /**
     * Get cTRDEB
     *
     * @return integer 
     */
    public function getCTRDEB()
    {
        return $this->cTRDEB;
    }

    /**
     * Set cTRFIN
     *
     * @param integer $cTRFIN
     * @return ITKITLR
     */
    public function setCTRFIN($cTRFIN)
    {
        $this->cTRFIN = $cTRFIN;

        return $this;
    }

    /**
     * Get cTRFIN
     *
     * @return integer 
     */
    public function getCTRFIN()
    {
        return $this->cTRFIN;
    }

    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return ITKITLR
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime 
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set dATEP
     *
     * @param \DateTime $dATEP
     * @return ITKITLR
     */
    public function setDATEP($dATEP)
    {
        $this->dATEP = $dATEP;

        return $this;
    }

    /**
     * Get dATEP
     *
     * @return \DateTime 
     */
    public function getDATEP()
    {
        return $this->dATEP;
    }

    /**
     * Set dATEPFIN
     *
     * @param \DateTime $dATEPFIN
     * @return ITKITLR
     */
    public function setDATEPFIN($dATEPFIN)
    {
        $this->dATEPFIN = $dATEPFIN;

        return $this;
    }

    /**
     * Get dATEPFIN
     *
     * @return \DateTime 
     */
    public function getDATEPFIN()
    {
        return $this->dATEPFIN;
    }

    /**
     * Set dATEREALISATION
     *
     * @param \DateTime $dATEREALISATION
     * @return ITKITLR
     */
    public function setDATEREALISATION($dATEREALISATION)
    {
        $this->dATEREALISATION = $dATEREALISATION;

        return $this;
    }

    /**
     * Get dATEREALISATION
     *
     * @return \DateTime 
     */
    public function getDATEREALISATION()
    {
        return $this->dATEREALISATION;
    }

    /**
     * Set dUREE
     *
     * @param integer $dUREE
     * @return ITKITLR
     */
    public function setDUREE($dUREE)
    {
        $this->dUREE = $dUREE;

        return $this;
    }

    /**
     * Get dUREE
     *
     * @return integer 
     */
    public function getDUREE()
    {
        return $this->dUREE;
    }

    /**
     * Set fERMEID
     *
     * @param integer $fERMEID
     * @return ITKITLR
     */
    public function setFERMEID($fERMEID)
    {
        $this->fERMEID = $fERMEID;

        return $this;
    }

    /**
     * Get fERMEID
     *
     * @return integer 
     */
    public function getFERMEID()
    {
        return $this->fERMEID;
    }

    /**
     * Set hEUREDEBUT
     *
     * @param \DateTime $hEUREDEBUT
     * @return ITKITLR
     */
    public function setHEUREDEBUT($hEUREDEBUT)
    {
        $this->hEUREDEBUT = $hEUREDEBUT;

        return $this;
    }

    /**
     * Get hEUREDEBUT
     *
     * @return \DateTime 
     */
    public function getHEUREDEBUT()
    {
        return $this->hEUREDEBUT;
    }

    /**
     * Set hEUREFIN
     *
     * @param \DateTime $hEUREFIN
     * @return ITKITLR
     */
    public function setHEUREFIN($hEUREFIN)
    {
        $this->hEUREFIN = $hEUREFIN;

        return $this;
    }

    /**
     * Get hEUREFIN
     *
     * @return \DateTime 
     */
    public function getHEUREFIN()
    {
        return $this->hEUREFIN;
    }

    /**
     * Set hUMIDITE
     *
     * @param string $hUMIDITE
     * @return ITKITLR
     */
    public function setHUMIDITE($hUMIDITE)
    {
        $this->hUMIDITE = $hUMIDITE;

        return $this;
    }

    /**
     * Get hUMIDITE
     *
     * @return string 
     */
    public function getHUMIDITE()
    {
        return $this->hUMIDITE;
    }

    /**
     * Set iTKITLID
     *
     * @param integer $iTKITLID
     * @return ITKITLR
     */
    public function setITKITLID($iTKITLID)
    {
        $this->iTKITLID = $iTKITLID;

        return $this;
    }

    /**
     * Get iTKITLID
     *
     * @return integer 
     */
    public function getITKITLID()
    {
        return $this->iTKITLID;
    }

    /**
     * Set lOTNO
     *
     * @param string $lOTNO
     * @return ITKITLR
     */
    public function setLOTNO($lOTNO)
    {
        $this->lOTNO = $lOTNO;

        return $this;
    }

    /**
     * Get lOTNO
     *
     * @return string 
     */
    public function getLOTNO()
    {
        return $this->lOTNO;
    }

    /**
     * Set mATERIELID
     *
     * @param integer $mATERIELID
     * @return ITKITLR
     */
    public function setMATERIELID($mATERIELID)
    {
        $this->mATERIELID = $mATERIELID;

        return $this;
    }

    /**
     * Get mATERIELID
     *
     * @return integer 
     */
    public function getMATERIELID()
    {
        return $this->mATERIELID;
    }

    /**
     * Set mODEAPPLICATION
     *
     * @param string $mODEAPPLICATION
     * @return ITKITLR
     */
    public function setMODEAPPLICATION($mODEAPPLICATION)
    {
        $this->mODEAPPLICATION = $mODEAPPLICATION;

        return $this;
    }

    /**
     * Get mODEAPPLICATION
     *
     * @return string 
     */
    public function getMODEAPPLICATION()
    {
        return $this->mODEAPPLICATION;
    }

    /**
     * Set pLUIE
     *
     * @param string $pLUIE
     * @return ITKITLR
     */
    public function setPLUIE($pLUIE)
    {
        $this->pLUIE = $pLUIE;

        return $this;
    }

    /**
     * Get pLUIE
     *
     * @return string 
     */
    public function getPLUIE()
    {
        return $this->pLUIE;
    }

    /**
     * Set pRESCRIPTEURID
     *
     * @param integer $pRESCRIPTEURID
     * @return ITKITLR
     */
    public function setPRESCRIPTEURID($pRESCRIPTEURID)
    {
        $this->pRESCRIPTEURID = $pRESCRIPTEURID;

        return $this;
    }

    /**
     * Get pRESCRIPTEURID
     *
     * @return integer 
     */
    public function getPRESCRIPTEURID()
    {
        return $this->pRESCRIPTEURID;
    }

    /**
     * Set qTELOT
     *
     * @param string $qTELOT
     * @return ITKITLR
     */
    public function setQTELOT($qTELOT)
    {
        $this->qTELOT = $qTELOT;

        return $this;
    }

    /**
     * Get qTELOT
     *
     * @return string 
     */
    public function getQTELOT()
    {
        return $this->qTELOT;
    }

    /**
     * Set tEMPERATURE
     *
     * @param string $tEMPERATURE
     * @return ITKITLR
     */
    public function setTEMPERATURE($tEMPERATURE)
    {
        $this->tEMPERATURE = $tEMPERATURE;

        return $this;
    }

    /**
     * Get tEMPERATURE
     *
     * @return string 
     */
    public function getTEMPERATURE()
    {
        return $this->tEMPERATURE;
    }

    /**
     * Set uNITELOT
     *
     * @param string $uNITELOT
     * @return ITKITLR
     */
    public function setUNITELOT($uNITELOT)
    {
        $this->uNITELOT = $uNITELOT;

        return $this;
    }

    /**
     * Get uNITELOT
     *
     * @return string 
     */
    public function getUNITELOT()
    {
        return $this->uNITELOT;
    }

    /**
     * Set uNITETLOCALISEID
     *
     * @param integer $uNITETLOCALISEID
     * @return ITKITLR
     */
    public function setUNITETLOCALISEID($uNITETLOCALISEID)
    {
        $this->uNITETLOCALISEID = $uNITETLOCALISEID;

        return $this;
    }

    /**
     * Get uNITETLOCALISEID
     *
     * @return integer 
     */
    public function getUNITETLOCALISEID()
    {
        return $this->uNITETLOCALISEID;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKITLR
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKITLR
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set vENT
     *
     * @param string $vENT
     * @return ITKITLR
     */
    public function setVENT($vENT)
    {
        $this->vENT = $vENT;

        return $this;
    }

    /**
     * Get vENT
     *
     * @return string 
     */
    public function getVENT()
    {
        return $this->vENT;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUSERMAJ()
    {
        return $this->uSERMAJ;
    }

    /**
     * @param string $uSERMAJ
     */
    public function setUSERMAJ($uSERMAJ)
    {
        $this->uSERMAJ = $uSERMAJ;
    }
}
