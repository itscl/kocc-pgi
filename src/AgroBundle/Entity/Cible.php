<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cible
 */
class Cible
{
    /**
     * @var string
     */
    private $cIBLECATEGORIE;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $eSPECES;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $pHYTOCATEGORIE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set cIBLECATEGORIE
     *
     * @param string $cIBLECATEGORIE
     * @return Cible
     */
    public function setCIBLECATEGORIE($cIBLECATEGORIE)
    {
        $this->cIBLECATEGORIE = $cIBLECATEGORIE;

        return $this;
    }

    /**
     * Get cIBLECATEGORIE
     *
     * @return string 
     */
    public function getCIBLECATEGORIE()
    {
        return $this->cIBLECATEGORIE;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return Cible
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string 
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return Cible
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set eSPECES
     *
     * @param string $eSPECES
     * @return Cible
     */
    public function setESPECES($eSPECES)
    {
        $this->eSPECES = $eSPECES;

        return $this;
    }

    /**
     * Get eSPECES
     *
     * @return string 
     */
    public function getESPECES()
    {
        return $this->eSPECES;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return Cible
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set pHYTOCATEGORIE
     *
     * @param string $pHYTOCATEGORIE
     * @return Cible
     */
    public function setPHYTOCATEGORIE($pHYTOCATEGORIE)
    {
        $this->pHYTOCATEGORIE = $pHYTOCATEGORIE;

        return $this;
    }

    /**
     * Get pHYTOCATEGORIE
     *
     * @return string 
     */
    public function getPHYTOCATEGORIE()
    {
        return $this->pHYTOCATEGORIE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return Cible
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return Cible
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
