<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKISMEPI
 */
class ITKISMEPI
{
    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var integer
     */
    private $ePIID;

    /**
     * @var integer
     */
    private $iTKISMID;

    /**
     * @var integer
     */
    private $oRIGINSITEID;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ITKISMEPI
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set EPIID
     *
     * @param integer $ePIID
     * @return ITKISMEPI
     */
    public function setEPIID($ePIID)
    {
        $this->ePIID = $ePIID;

        return $this;
    }

    /**
     * Get EPIID
     *
     * @return integer
     */
    public function getEPIID()
    {
        return $this->ePIID;
    }

    /**
     * Set ORIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ITKISMEPI
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get ORIGINSITEID
     *
     * @return integer
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set UPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKISMEPI
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get UPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set UPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKISMEPI
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get UPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getITKISMID()
    {
        return $this->iTKISMID;
    }

    /**
     * @param int $iTKISMID
     */
    public function setITKISMID($iTKISMID)
    {
        $this->iTKISMID = $iTKISMID;
    }

}
