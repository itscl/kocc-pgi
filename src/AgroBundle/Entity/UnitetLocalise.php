<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UnitetLocalise
 */
class UnitetLocalise
{
    /**
     * @var integer
     */
    private $cULTUREID;

    /**
     * @var integer
     */
    private $cULTUREVARIETEID;

    /**
     * @var \DateTime
     */
    private $dATEFIN;

    /**
     * @var integer
     */
    private $fERMEID;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $tYPEMVEGETAL;

    /**
     * @var string
     */
    private $uNITE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set cULTUREID
     *
     * @param integer $cULTUREID
     * @return UnitetLocalise
     */
    public function setCULTUREID($cULTUREID)
    {
        $this->cULTUREID = $cULTUREID;

        return $this;
    }

    /**
     * Get cULTUREID
     *
     * @return integer 
     */
    public function getCULTUREID()
    {
        return $this->cULTUREID;
    }

    /**
     * Set cULTUREVARIETEID
     *
     * @param integer $cULTUREVARIETEID
     * @return UnitetLocalise
     */
    public function setCULTUREVARIETEID($cULTUREVARIETEID)
    {
        $this->cULTUREVARIETEID = $cULTUREVARIETEID;

        return $this;
    }

    /**
     * Get cULTUREVARIETEID
     *
     * @return integer 
     */
    public function getCULTUREVARIETEID()
    {
        return $this->cULTUREVARIETEID;
    }

    /**
     * Set dATEFIN
     *
     * @param \DateTime $dATEFIN
     * @return UnitetLocalise
     */
    public function setDATEFIN($dATEFIN)
    {
        $this->dATEFIN = $dATEFIN;

        return $this;
    }

    /**
     * Get dATEFIN
     *
     * @return \DateTime 
     */
    public function getDATEFIN()
    {
        return $this->dATEFIN;
    }

    /**
     * Set fERMEID
     *
     * @param integer $fERMEID
     * @return UnitetLocalise
     */
    public function setFERMEID($fERMEID)
    {
        $this->fERMEID = $fERMEID;

        return $this;
    }

    /**
     * Get fERMEID
     *
     * @return integer 
     */
    public function getFERMEID()
    {
        return $this->fERMEID;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return UnitetLocalise
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set tYPEMVEGETAL
     *
     * @param string $tYPEMVEGETAL
     * @return UnitetLocalise
     */
    public function setTYPEMVEGETAL($tYPEMVEGETAL)
    {
        $this->tYPEMVEGETAL = $tYPEMVEGETAL;

        return $this;
    }

    /**
     * Get tYPEMVEGETAL
     *
     * @return string 
     */
    public function getTYPEMVEGETAL()
    {
        return $this->tYPEMVEGETAL;
    }

    /**
     * Set uNITE
     *
     * @param string $uNITE
     * @return UnitetLocalise
     */
    public function setUNITE($uNITE)
    {
        $this->uNITE = $uNITE;

        return $this;
    }

    /**
     * Get uNITE
     *
     * @return string 
     */
    public function getUNITE()
    {
        return $this->uNITE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return UnitetLocalise
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
