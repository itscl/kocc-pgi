<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKITLRA
 */
class ITKITLRA
{
    /**
     * @var integer
     */
    private $aRTICLEID;

    /**
     * @var string
     */
    private $bOUILLIE;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var integer
     */
    private $iTKITLRID;

    /**
     * @var integer
     */
    private $oRIGINSITEID;

    /**
     * @var integer
     */
    private $pRIXTOTAL;

    /**
     * @var integer
     */
    private $pRIXUNITAIRE;

    /**
     * @var string
     */
    private $qTE;

    /**
     * @var string
     */
    private $qTET;

    /**
     * @var integer
     */
    private $rANG;

    /**
     * @var string
     */
    private $uNITE;

    /**
     * @var string
     */
    private $uNITEPRIX;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set aRTICLEID
     *
     * @param integer $aRTICLEID
     * @return ITKITLRA
     */
    public function setARTICLEID($aRTICLEID)
    {
        $this->aRTICLEID = $aRTICLEID;

        return $this;
    }

    /**
     * Get aRTICLEID
     *
     * @return integer 
     */
    public function getARTICLEID()
    {
        return $this->aRTICLEID;
    }

    /**
     * Set bOUILLIE
     *
     * @param string $bOUILLIE
     * @return ITKITLRA
     */
    public function setBOUILLIE($bOUILLIE)
    {
        $this->bOUILLIE = $bOUILLIE;

        return $this;
    }

    /**
     * Get bOUILLIE
     *
     * @return string 
     */
    public function getBOUILLIE()
    {
        return $this->bOUILLIE;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return ITKITLRA
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string 
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ITKITLRA
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set iTKITLRID
     *
     * @param integer $iTKITLRID
     * @return ITKITLRA
     */
    public function setITKITLRID($iTKITLRID)
    {
        $this->iTKITLRID = $iTKITLRID;

        return $this;
    }

    /**
     * Get iTKITLRID
     *
     * @return integer 
     */
    public function getITKITLRID()
    {
        return $this->iTKITLRID;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ITKITLRA
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set pRIXTOTAL
     *
     * @param integer $pRIXTOTAL
     * @return ITKITLRA
     */
    public function setPRIXTOTAL($pRIXTOTAL)
    {
        $this->pRIXTOTAL = $pRIXTOTAL;

        return $this;
    }

    /**
     * Get pRIXTOTAL
     *
     * @return integer 
     */
    public function getPRIXTOTAL()
    {
        return $this->pRIXTOTAL;
    }

    /**
     * Set pRIXUNITAIRE
     *
     * @param integer $pRIXUNITAIRE
     * @return ITKITLRA
     */
    public function setPRIXUNITAIRE($pRIXUNITAIRE)
    {
        $this->pRIXUNITAIRE = $pRIXUNITAIRE;

        return $this;
    }

    /**
     * Get pRIXUNITAIRE
     *
     * @return integer 
     */
    public function getPRIXUNITAIRE()
    {
        return $this->pRIXUNITAIRE;
    }

    /**
     * Set qTE
     *
     * @param string $qTE
     * @return ITKITLRA
     */
    public function setQTE($qTE)
    {
        $this->qTE = $qTE;

        return $this;
    }

    /**
     * Get qTE
     *
     * @return string 
     */
    public function getQTE()
    {
        return $this->qTE;
    }

    /**
     * Set qTET
     *
     * @param string $qTET
     * @return ITKITLRA
     */
    public function setQTET($qTET)
    {
        $this->qTET = $qTET;

        return $this;
    }

    /**
     * Get qTET
     *
     * @return string 
     */
    public function getQTET()
    {
        return $this->qTET;
    }

    /**
     * Set rANG
     *
     * @param integer $rANG
     * @return ITKITLRA
     */
    public function setRANG($rANG)
    {
        $this->rANG = $rANG;

        return $this;
    }

    /**
     * Get rANG
     *
     * @return integer 
     */
    public function getRANG()
    {
        return $this->rANG;
    }

    /**
     * Set uNITE
     *
     * @param string $uNITE
     * @return ITKITLRA
     */
    public function setUNITE($uNITE)
    {
        $this->uNITE = $uNITE;

        return $this;
    }

    /**
     * Get uNITE
     *
     * @return string 
     */
    public function getUNITE()
    {
        return $this->uNITE;
    }

    /**
     * Set uNITEPRIX
     *
     * @param string $uNITEPRIX
     * @return ITKITLRA
     */
    public function setUNITEPRIX($uNITEPRIX)
    {
        $this->uNITEPRIX = $uNITEPRIX;

        return $this;
    }

    /**
     * Get uNITEPRIX
     *
     * @return string 
     */
    public function getUNITEPRIX()
    {
        return $this->uNITEPRIX;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKITLRA
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKITLRA
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
