<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKITL
 */
class ITKITL
{
    /**
     * @var integer
     */
    private $aPPAREILID;

    /**
     * @var integer
     */
    private $aPPLICATEURID;

    /**
     * @var integer
     */
    private $bOUILLIE;

    /**
     * @var integer
     */
    private $cHAUFFEURID;

    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var \DateTime
     */
    private $dATEP;

    /**
     * @var \DateTime
     */
    private $dATEPFIN;

    /**
     * @var integer
     */
    private $dUREE;

    /**
     * @var string
     */
    private $eTATINSTRUCTION;

    /**
     * @var integer
     */
    private $fERMEID;

    /**
     * @var \DateTime
     */
    private $hEUREDEBUT;

    /**
     * @var \DateTime
     */
    private $hEUREFIN;

    /**
     * @var string
     */
    private $hUMIDITE;

    /**
     * @var string
     */
    private $lOTNO;

    /**
     * @var integer
     */
    private $mATERIELID;

    /**
     * @var string
     */
    private $mODEAPPLICATION;

    /**
     * @var string
     */
    private $pLUIE;

    /**
     * @var integer
     */
    private $pRESCRIPTEURID;

    /**
     * @var string
     */
    private $qTELOT;

    /**
     * @var string
     */
    private $tEMPERATURE;

    /**
     * @var string
     */
    private $tRAITEMENT;

    /**
     * @var string
     */
    private $uNITELOT;

    /**
     * @var integer
     */
    private $uNITETLOCALISEID;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var string
     */
    private $vENT;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set aPPAREILID
     *
     * @param integer $aPPAREILID
     * @return ITKITL
     */
    public function setAPPAREILID($aPPAREILID)
    {
        $this->aPPAREILID = $aPPAREILID;

        return $this;
    }

    /**
     * Get aPPAREILID
     *
     * @return integer 
     */
    public function getAPPAREILID()
    {
        return $this->aPPAREILID;
    }

    /**
     * Set aPPLICATEURID
     *
     * @param integer $aPPLICATEURID
     * @return ITKITL
     */
    public function setAPPLICATEURID($aPPLICATEURID)
    {
        $this->aPPLICATEURID = $aPPLICATEURID;

        return $this;
    }

    /**
     * Get aPPLICATEURID
     *
     * @return integer 
     */
    public function getAPPLICATEURID()
    {
        return $this->aPPLICATEURID;
    }

    /**
     * Set bOUILLIE
     *
     * @param integer $bOUILLIE
     * @return ITKITL
     */
    public function setBOUILLIE($bOUILLIE)
    {
        $this->bOUILLIE = $bOUILLIE;

        return $this;
    }

    /**
     * Get bOUILLIE
     *
     * @return integer 
     */
    public function getBOUILLIE()
    {
        return $this->bOUILLIE;
    }

    /**
     * Set cHAUFFEURID
     *
     * @param integer $cHAUFFEURID
     * @return ITKITL
     */
    public function setCHAUFFEURID($cHAUFFEURID)
    {
        $this->cHAUFFEURID = $cHAUFFEURID;

        return $this;
    }

    /**
     * Get cHAUFFEURID
     *
     * @return integer 
     */
    public function getCHAUFFEURID()
    {
        return $this->cHAUFFEURID;
    }

    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return ITKITL
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime 
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set dATEP
     *
     * @param \DateTime $dATEP
     * @return ITKITL
     */
    public function setDATEP($dATEP)
    {
        $this->dATEP = $dATEP;

        return $this;
    }

    /**
     * Get dATEP
     *
     * @return \DateTime 
     */
    public function getDATEP()
    {
        return $this->dATEP;
    }

    /**
     * Set dATEPFIN
     *
     * @param \DateTime $dATEPFIN
     * @return ITKITL
     */
    public function setDATEPFIN($dATEPFIN)
    {
        $this->dATEPFIN = $dATEPFIN;

        return $this;
    }

    /**
     * Get dATEPFIN
     *
     * @return \DateTime 
     */
    public function getDATEPFIN()
    {
        return $this->dATEPFIN;
    }

    /**
     * Set dUREE
     *
     * @param integer $dUREE
     * @return ITKITL
     */
    public function setDUREE($dUREE)
    {
        $this->dUREE = $dUREE;

        return $this;
    }

    /**
     * Get dUREE
     *
     * @return integer 
     */
    public function getDUREE()
    {
        return $this->dUREE;
    }

    /**
     * Set eTATINSTRUCTION
     *
     * @param string $eTATINSTRUCTION
     * @return ITKITL
     */
    public function setETATINSTRUCTION($eTATINSTRUCTION)
    {
        $this->eTATINSTRUCTION = $eTATINSTRUCTION;

        return $this;
    }

    /**
     * Get eTATINSTRUCTION
     *
     * @return string 
     */
    public function getETATINSTRUCTION()
    {
        return $this->eTATINSTRUCTION;
    }

    /**
     * Set fERMEID
     *
     * @param integer $fERMEID
     * @return ITKITL
     */
    public function setFERMEID($fERMEID)
    {
        $this->fERMEID = $fERMEID;

        return $this;
    }

    /**
     * Get fERMEID
     *
     * @return integer 
     */
    public function getFERMEID()
    {
        return $this->fERMEID;
    }

    /**
     * Set hEUREDEBUT
     *
     * @param \DateTime $hEUREDEBUT
     * @return ITKITL
     */
    public function setHEUREDEBUT($hEUREDEBUT)
    {
        $this->hEUREDEBUT = $hEUREDEBUT;

        return $this;
    }

    /**
     * Get hEUREDEBUT
     *
     * @return \DateTime 
     */
    public function getHEUREDEBUT()
    {
        return $this->hEUREDEBUT;
    }

    /**
     * Set hEUREFIN
     *
     * @param \DateTime $hEUREFIN
     * @return ITKITL
     */
    public function setHEUREFIN($hEUREFIN)
    {
        $this->hEUREFIN = $hEUREFIN;

        return $this;
    }

    /**
     * Get hEUREFIN
     *
     * @return \DateTime 
     */
    public function getHEUREFIN()
    {
        return $this->hEUREFIN;
    }

    /**
     * Set hUMIDITE
     *
     * @param string $hUMIDITE
     * @return ITKITL
     */
    public function setHUMIDITE($hUMIDITE)
    {
        $this->hUMIDITE = $hUMIDITE;

        return $this;
    }

    /**
     * Get hUMIDITE
     *
     * @return string 
     */
    public function getHUMIDITE()
    {
        return $this->hUMIDITE;
    }

    /**
     * Set lOTNO
     *
     * @param string $lOTNO
     * @return ITKITL
     */
    public function setLOTNO($lOTNO)
    {
        $this->lOTNO = $lOTNO;

        return $this;
    }

    /**
     * Get lOTNO
     *
     * @return string 
     */
    public function getLOTNO()
    {
        return $this->lOTNO;
    }

    /**
     * Set mATERIELID
     *
     * @param integer $mATERIELID
     * @return ITKITL
     */
    public function setMATERIELID($mATERIELID)
    {
        $this->mATERIELID = $mATERIELID;

        return $this;
    }

    /**
     * Get mATERIELID
     *
     * @return integer 
     */
    public function getMATERIELID()
    {
        return $this->mATERIELID;
    }

    /**
     * Set mODEAPPLICATION
     *
     * @param string $mODEAPPLICATION
     * @return ITKITL
     */
    public function setMODEAPPLICATION($mODEAPPLICATION)
    {
        $this->mODEAPPLICATION = $mODEAPPLICATION;

        return $this;
    }

    /**
     * Get mODEAPPLICATION
     *
     * @return string 
     */
    public function getMODEAPPLICATION()
    {
        return $this->mODEAPPLICATION;
    }

    /**
     * Set pLUIE
     *
     * @param string $pLUIE
     * @return ITKITL
     */
    public function setPLUIE($pLUIE)
    {
        $this->pLUIE = $pLUIE;

        return $this;
    }

    /**
     * Get pLUIE
     *
     * @return string 
     */
    public function getPLUIE()
    {
        return $this->pLUIE;
    }

    /**
     * Set pRESCRIPTEURID
     *
     * @param integer $pRESCRIPTEURID
     * @return ITKITL
     */
    public function setPRESCRIPTEURID($pRESCRIPTEURID)
    {
        $this->pRESCRIPTEURID = $pRESCRIPTEURID;

        return $this;
    }

    /**
     * Get pRESCRIPTEURID
     *
     * @return integer 
     */
    public function getPRESCRIPTEURID()
    {
        return $this->pRESCRIPTEURID;
    }

    /**
     * Set qTELOT
     *
     * @param string $qTELOT
     * @return ITKITL
     */
    public function setQTELOT($qTELOT)
    {
        $this->qTELOT = $qTELOT;

        return $this;
    }

    /**
     * Get qTELOT
     *
     * @return string 
     */
    public function getQTELOT()
    {
        return $this->qTELOT;
    }

    /**
     * Set tEMPERATURE
     *
     * @param string $tEMPERATURE
     * @return ITKITL
     */
    public function setTEMPERATURE($tEMPERATURE)
    {
        $this->tEMPERATURE = $tEMPERATURE;

        return $this;
    }

    /**
     * Get tEMPERATURE
     *
     * @return string 
     */
    public function getTEMPERATURE()
    {
        return $this->tEMPERATURE;
    }

    /**
     * Set tRAITEMENT
     *
     * @param string $tRAITEMENT
     * @return ITKITL
     */
    public function setTRAITEMENT($tRAITEMENT)
    {
        $this->tRAITEMENT = $tRAITEMENT;

        return $this;
    }

    /**
     * Get tRAITEMENT
     *
     * @return string 
     */
    public function getTRAITEMENT()
    {
        return $this->tRAITEMENT;
    }

    /**
     * Set uNITELOT
     *
     * @param string $uNITELOT
     * @return ITKITL
     */
    public function setUNITELOT($uNITELOT)
    {
        $this->uNITELOT = $uNITELOT;

        return $this;
    }

    /**
     * Get uNITELOT
     *
     * @return string 
     */
    public function getUNITELOT()
    {
        return $this->uNITELOT;
    }

    /**
     * Set uNITETLOCALISEID
     *
     * @param integer $uNITETLOCALISEID
     * @return ITKITL
     */
    public function setUNITETLOCALISEID($uNITETLOCALISEID)
    {
        $this->uNITETLOCALISEID = $uNITETLOCALISEID;

        return $this;
    }

    /**
     * Get uNITETLOCALISEID
     *
     * @return integer 
     */
    public function getUNITETLOCALISEID()
    {
        return $this->uNITETLOCALISEID;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKITL
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKITL
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set vENT
     *
     * @param string $vENT
     * @return ITKITL
     */
    public function setVENT($vENT)
    {
        $this->vENT = $vENT;

        return $this;
    }

    /**
     * Get vENT
     *
     * @return string 
     */
    public function getVENT()
    {
        return $this->vENT;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
