<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKISMLOTDETAIL
 */
class ITKISMLOTDETAIL
{
    /**
     * @var string
     */
    private $lOTID;

    /**
     * @var string
     */
    private $qTE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set lOTID
     *
     * @param string $lOTID
     * @return ITKISMLOTDETAIL
     */
    public function setLOTID($lOTID)
    {
        $this->lOTID = $lOTID;

        return $this;
    }

    /**
     * Get lOTID
     *
     * @return string 
     */
    public function getLOTID()
    {
        return $this->lOTID;
    }

    /**
     * Set qTE
     *
     * @param string $qTE
     * @return ITKISMLOTDETAIL
     */
    public function setQTE($qTE)
    {
        $this->qTE = $qTE;

        return $this;
    }

    /**
     * Get qTE
     *
     * @return string 
     */
    public function getQTE()
    {
        return $this->qTE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKISMLOTDETAIL
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKISMLOTDETAIL
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
