<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SemenceLot
 */
class SemenceLot
{
    /**
     * @var \DateTime
     */
    private $dATECREATION;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $lOTNO;

    /**
     * @var integer
     */
    private $nOINST;

    /**
     * @var string
     */
    private $qTELOT;

    /**
     * @var string
     */
    private $qTERESTANT;

    /**
     * @var string
     */
    private $uNITELOT;

    /**
     * @var string
     */
    private $uPDATE_DATE;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set dATECREATION
     *
     * @param \DateTime $dATECREATION
     * @return SEMENCELOT
     */
    public function setDATECREATION($dATECREATION)
    {
        $this->dATECREATION = $dATECREATION;

        return $this;
    }

    /**
     * Get dATECREATION
     *
     * @return \DateTime 
     */
    public function getDATECREATION()
    {
        return $this->dATECREATION;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return SEMENCELOT
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set lOTNO
     *
     * @param string $lOTNO
     * @return SEMENCELOT
     */
    public function setLOTNO($lOTNO)
    {
        $this->lOTNO = $lOTNO;

        return $this;
    }

    /**
     * Get lOTNO
     *
     * @return string 
     */
    public function getLOTNO()
    {
        return $this->lOTNO;
    }

    /**
     * Set nOINST
     *
     * @param integer $nOINST
     * @return SEMENCELOT
     */
    public function setNOINST($nOINST)
    {
        $this->nOINST = $nOINST;

        return $this;
    }

    /**
     * Get nOINST
     *
     * @return integer 
     */
    public function getNOINST()
    {
        return $this->nOINST;
    }

    /**
     * Set qTELOT
     *
     * @param string $qTELOT
     * @return SEMENCELOT
     */
    public function setQTELOT($qTELOT)
    {
        $this->qTELOT = $qTELOT;

        return $this;
    }

    /**
     * Get qTELOT
     *
     * @return string 
     */
    public function getQTELOT()
    {
        return $this->qTELOT;
    }

    /**
     * Set qTERESTANT
     *
     * @param string $qTERESTANT
     * @return SEMENCELOT
     */
    public function setQTERESTANT($qTERESTANT)
    {
        $this->qTERESTANT = $qTERESTANT;

        return $this;
    }

    /**
     * Get qTERESTANT
     *
     * @return string 
     */
    public function getQTERESTANT()
    {
        return $this->qTERESTANT;
    }

    /**
     * Set uNITELOT
     *
     * @param string $uNITELOT
     * @return SEMENCELOT
     */
    public function setUNITELOT($uNITELOT)
    {
        $this->uNITELOT = $uNITELOT;

        return $this;
    }

    /**
     * Get uNITELOT
     *
     * @return string 
     */
    public function getUNITELOT()
    {
        return $this->uNITELOT;
    }

    /**
     * Set uPDATE_DATE
     *
     * @param string $uPDATEDATE
     * @return SEMENCELOT
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATE_DATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATE_DATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATE_DATE;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string
     */
    private $uPDATEDATE;


}
