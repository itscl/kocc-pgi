<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKP
 */
class ITKP
{
    /**
     * @var string
     */
    private $cODE;

    /**
     * @var integer
     */
    private $cYCLECULTURALID;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var integer
     */
    private $iTKID;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var integer
     */
    private $oRIGINSITEID;

    /**
     * @var integer
     */
    private $rANG;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set cODE
     *
     * @param string $cODE
     * @return ITKP
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string 
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set cYCLECULTURALID
     *
     * @param integer $cYCLECULTURALID
     * @return ITKP
     */
    public function setCYCLECULTURALID($cYCLECULTURALID)
    {
        $this->cYCLECULTURALID = $cYCLECULTURALID;

        return $this;
    }

    /**
     * Get cYCLECULTURALID
     *
     * @return integer 
     */
    public function getCYCLECULTURALID()
    {
        return $this->cYCLECULTURALID;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ITKP
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set iTKID
     *
     * @param integer $iTKID
     * @return ITKP
     */
    public function setITKID($iTKID)
    {
        $this->iTKID = $iTKID;

        return $this;
    }

    /**
     * Get iTKID
     *
     * @return integer 
     */
    public function getITKID()
    {
        return $this->iTKID;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return ITKP
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ITKP
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set rANG
     *
     * @param integer $rANG
     * @return ITKP
     */
    public function setRANG($rANG)
    {
        $this->rANG = $rANG;

        return $this;
    }

    /**
     * Get rANG
     *
     * @return integer 
     */
    public function getRANG()
    {
        return $this->rANG;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKP
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKP
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
