<?php

namespace AgroBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKITLREPI
 */
class ITKITLREPI
{
    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var integer
     */
    private $ePIID;

    /**
     * @var integer
     */
    private $iTKITLRID;

    /**
     * @var integer
     */
    private $oRIGINSITEID;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ITKITLREPI
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set ePIID
     *
     * @param integer $ePIID
     * @return ITKITLREPI
     */
    public function setEPIID($ePIID)
    {
        $this->ePIID = $ePIID;

        return $this;
    }

    /**
     * Get ePIID
     *
     * @return integer 
     */
    public function getEPIID()
    {
        return $this->ePIID;
    }

    /**
     * Set iTKITLRID
     *
     * @param integer $iTKITLRID
     * @return ITKITLREPI
     */
    public function setITKITLRID($iTKITLRID)
    {
        $this->iTKITLRID = $iTKITLRID;

        return $this;
    }

    /**
     * Get iTKITLRID
     *
     * @return integer 
     */
    public function getITKITLRID()
    {
        return $this->iTKITLRID;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ITKITLREPI
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKITLREPI
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKITLREPI
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
