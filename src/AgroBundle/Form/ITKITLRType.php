<?php

namespace AgroBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ITKITLRType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $prescripteurs=array();
        $applicateurs=array();
        $chauffeurs=array();
        $modeapplications=array();

        //liste des prescripteurs
        $query=$this->em->getRepository('TechniqueBundle:Personnel')->getAllPrescripteur();

        foreach($query as $ref)
        {
            $prescripteurs[$ref['PID']]=$ref['PRENOM'].' '.$ref['NOM'];
        }

        //liste des applicateurs
        $query=$this->em->getRepository('TechniqueBundle:Personnel')->getAllApplicateur();

        foreach($query as $ref)
        {
            $applicateurs[$ref['PID']]=$ref['PRENOM'].' '.$ref['NOM'];
        }

        //liste des chauffeurs
        $query=$this->em->getRepository('TechniqueBundle:Personnel')->getAllChauffeur();

        foreach($query as $ref)
        {
            $chauffeurs[$ref['PID']]=$ref['PRENOM'].' '.$ref['NOM'];
        }

        //liste des modes d'application
        $query=$this->em->getRepository('TechniqueBundle:Lov')->getAllModeApp();

        foreach($query as $ref)
        {
            $modeapplications[$ref['LIBELLE']]=$ref['LIBELLE'];
        }

        $builder
            ->add('dATEP', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy  H:m:s',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'data-date-format' => 'DD-MM-YYYY  H:m:s'
                ]
            ))
            ->add('dUREE', 'text', array(
                'required' => true
            ))
            ->add('dATEPFIN', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'data-date-format' => 'DD-MM-YYYY'
                ]
            ))
            ->add('pRESCRIPTEURID', 'choice', array(
                'choices' => $prescripteurs,  'required' => false,
                'attr' => [
                    'class' => 'form-control selectpicker'
                ]
            ))
            ->add('aPPLICATEURID', 'choice', array(
                'choices' => $applicateurs,  'required' => false,
                'attr' => [
                    'class' => 'form-control selectpicker'
                ]
            ))
            ->add('cHAUFFEURID', 'choice', array(
                'choices' => $chauffeurs,  'required' => false,
                'attr' => [
                    'class' => 'form-control selectpicker'
                ]
            ))
            ->add('mODEAPPLICATION', 'choice', array(
                'choices' => $modeapplications,  'required' => false,
                'attr' => [
                    'class' => 'form-control selectpicker'
                ]
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AgroBundle\Entity\ITKITLR'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'agrobundle_itkitlr';
    }


}
