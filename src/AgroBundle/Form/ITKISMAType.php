<?php

namespace AgroBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ITKISMAType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('aRTICLEID')->add('cOMMENTAIRE')->add('dATEINVALIDE')->add('oRIGINSITEID')->add('pRIXTOTAL')->add('pRIXUNITAIRE')->add('qTE')->add('qTET')->add('rANG')->add('sURFACE')->add('uNITE')->add('uNITEPRIX')->add('uPDATEDATE')->add('uPDATEUSER');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AgroBundle\Entity\ITKISMA'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'agrobundle_itkisma';
    }


}
