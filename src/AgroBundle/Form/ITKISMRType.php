<?php

namespace AgroBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ITKISMRType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fermes = array();
        $prescripteurs=array();
        $opfermes=array();
        $appicateurs=array();
        $modeapplications=array();
        $culturevarietes=array();
        $typemvegetals=array();
        $nbrang=[1, 2];

        //liste des fermes
        $query=$this->em->getRepository('CultureBundle:Ferme')
            ->createQueryBuilder('fe')
            ->orderBy('fe.lIBELLE');

        foreach($query->getQuery()->getResult() as $ref)
        {
            $fermes[$ref->getId()]=$ref->getLIBELLE();
        }

        //liste des prescripteurs
        $query=$this->em->getRepository('TechniqueBundle:Personnel')->getAllPrescripteur();

        foreach($query as $ref)
        {
            $prescripteurs[$ref['PID']]=$ref['PRENOM'].' '.$ref['NOM'];
        }

        //liste des applicateurs
        $query=$this->em->getRepository('TechniqueBundle:Personnel')->getAllApplicateur();

        foreach($query as $ref)
        {
            $appicateurs[$ref['PID']]=$ref['PRENOM'].' '.$ref['NOM'];
        }

        //liste des modes d'application
        $query=$this->em->getRepository('TechniqueBundle:Lov')->getAllModeApplication();

        foreach($query as $ref)
        {
            $modeapplications[$ref['LIBELLE']]=$ref['LIBELLE'];
        }

        $builder
            ->add('fERMEID', 'choice', array(
                'choices' => $fermes,
                "empty_value" => ''
            ))
            ->add('cYCLECULTURALID','hidden')
            ->add('cULTUREID','hidden')
            ->add('dATEP', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy  H:m:s',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'data-date-format' => 'DD-MM-YYYY  H:m:s'
                ]
            ))
            ->add('dUREE', 'text', array(
                'required' => true
            ))
            ->add('dATEPFIN', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'data-date-format' => 'DD-MM-YYYY'
                ]
            ))
            ->add('rANGNB', 'choice', array(
                'choices' => $nbrang
            ))
            ->add('eLIGNE', 'text', array(
                'required' => true
            ))
            ->add('eSEMIS', 'text', array(
                'required' => true
            ))
            ->add('pRESCRIPTEURID', 'choice', array(
                'choices' => $prescripteurs,  'required' => false,
                'attr' => [
                    'class' => 'form-control selectpicker'
                ]
            ))
            ->add('mODEAPPLICATION', 'choice', array(
                'choices' => $modeapplications,  'required' => false,
                'attr' => [
                    'class' => 'form-control selectpicker'
                ]
            ))
            ->add('aPPLICATEURID', 'choice', array(
                'choices' => $appicateurs,  'required' => false,
                'attr' => [
                    'class' => 'form-control selectpicker'
                ]
            ))
            ->add('eFFECTIF', 'hidden')
            ->add('sURFACE')
            ->add('sURFACETOT')
            ->add('qTESEM')
            ->add('qTESEMTOT');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AgroBundle\Entity\ITKISMR'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'agrobundle_itkismr';
    }

}
