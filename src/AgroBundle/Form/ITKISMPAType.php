<?php

namespace AgroBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ITKISMPAType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dATEINVALIDE')->add('iTKISMID')->add('oRIGINSITEID')->add('pARCELLECHECK')->add('pARCELLEID')->add('sURFACE')->add('uPDATEDATE')->add('uPDATEUSER');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AgroBundle\Entity\ITKISMPA'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'agrobundle_itkismpa';
    }


}
