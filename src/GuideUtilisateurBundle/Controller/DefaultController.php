<?php

namespace GuideUtilisateurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('GuideUtilisateurBundle:Default:index.html.twig');
    }
}
