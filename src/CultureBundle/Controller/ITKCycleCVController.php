<?php

namespace CultureBundle\Controller;

use CultureBundle\Entity\ITKCycleCV;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class ITKCycleCVController extends Controller
{
    public function ajoutitkcyclecvAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $semainerecolte = $request->query->get('semainerecolte');
        $rndtbrut = $request->query->get('rndtbrut');
        $culture = $request->query->get('culture');

        $itkcyclecv = $em->getRepository('CultureBundle:ITKCycleCV')->findBy(array('sEMAINERECOLTE' =>
            $semainerecolte, 'cULTUREID' => $culture));

        if(!$itkcyclecv)
        {
            $entity = new ITKCycleCV();

            if($semainerecolte !='' && $rndtbrut !='' && $culture !='')
            {
                $entity->setRDTBRUT($rndtbrut);
                $entity->setSEMAINERECOLTE($semainerecolte);
                $entity->setCULTUREID($culture);

                $em->persist($entity);
                $em->flush();
            }
            if($semainerecolte !='' && $culture !='')
            {
                $entity->setSEMAINERECOLTE($semainerecolte);
                $entity->setCULTUREID($culture);

                $em->persist($entity);
                $em->flush();
            }
        }

        var_dump($semainerecolte);
        var_dump($rndtbrut);
        var_dump($culture);

        return $this->render('CultureBundle:ITKCycleCV:ajax.html.twig');
    }

    public function modifieritkcyclecvAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $id = $request->query->get('id');
        $semainerecolte = $request->query->get('semainerecolte');
        $rndtbrut = $request->query->get('rndtbrut');

        $itkcyclecv = $em->getRepository('CultureBundle:ITKCycleCV')->find($id);

        $itkcyclecv->setRDTBRUT($rndtbrut);
        $itkcyclecv->setSEMAINERECOLTE($semainerecolte);

        $em->flush();

        var_dump($id);
        var_dump($semainerecolte);
        var_dump($rndtbrut);

        return $this->render('CultureBundle:ITKCycleCV:ajax.html.twig');
    }
}
