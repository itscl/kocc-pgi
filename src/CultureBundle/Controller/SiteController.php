<?php

namespace CultureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use CultureBundle\Entity\Site;
use Symfony\Component\HttpFoundation\Response;
use CultureBundle\Form\SiteType;

class SiteController extends Controller
{
    public function siteAction()
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            if (!$response->isNotModified($this->getRequest()))
            {
                $em = $this->getDoctrine()->getManager();
                $sites = $em->getRepository('CultureBundle:Site')->findAll();

                $response->setContent($this->renderView('CultureBundle:Site:site.html.twig', array(
                    'sites' => $sites
                )));

                return $response;
            }
        }
    }

    public function modifiersiteAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $site = $em->getRepository('CultureBundle:Site')->find($id);

            $edit_form = $this->createForm(new SiteType($em), $site);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    try
                    {
                        $em->flush();

                        $sites = $em->getRepository('CultureBundle:Site')->findAll();
                        $this->get('session')->getFlashBag()->add('noticeok', "Le site a bien été modifié !");

                        return $this->render('CultureBundle:Site:site.html.twig', array(
                            'sites' => $sites,
                        ));
                    }
                    catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Cette donnée ne peut pas être ajoutée car elle existe déja dans la base");
                    }
                }
            }

            return $this->render('CultureBundle:Site:modifiersite.html.twig', array(
                'site' => $site,
                'edit_form' => $edit_form->createView()
            ));
        }
    }

    public function ajoutersiteAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $entity = new Site();

            $form = $this->createForm(new SiteType($em), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    try
                    {
                        $em->persist($entity);
                        $em->flush();
                        $sites = $em->getRepository('CultureBundle:Site')->findAll();
                        $this->get('session')->getFlashBag()->add('noticeok', "Le site a bien été ajouté !");

                        return $this->render('CultureBundle:Site:site.html.twig', array(
                            'sites' => $sites,
                        ));
                    }
                    catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Cette donnée ne peut pas être ajoutée car elle existe déja dans la base");
                    }
                }
            }

            return $this->render('CultureBundle:Site:ajoutersite.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
        }
    }

    public function supprimersiteAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $site = $em->getRepository('CultureBundle:Site')->find($id);

            try
            {
                $em->remove($site);
                $em->flush();
                $this->get('session')->getFlashBag()->add('noticeok', "Le site a bien été supprimé !");

            } catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Impossible de supprimer ce site car des fermes y sont attachés");
            }
            catch(\Doctrine\ORM\ORMInvalidArgumentException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Cette donnée n'existe plus dans la base");
            }

            $sites = $em->getRepository('CultureBundle:Site')->findAll();
            return $this->render('CultureBundle:Site:site.html.twig', array(
                'sites' => $sites,
            ));
        }
    }

    public function getSiteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');

        $result_total = $em->getRepository('CultureBundle:Site')->getSiteValide($motcle);

        $result = $em->getRepository('CultureBundle:Site')->getSitePage($motcle, $page);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }
}