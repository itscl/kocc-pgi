<?php

namespace CultureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CultureVarieteController extends Controller
{
    public function getculturevarieteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');

        $result_total = $em->getRepository('CultureBundle:CultureVariete')->getCultureVarieteValide($motcle);

        $result = $em->getRepository('CultureBundle:CultureVariete')->getCultureVarieteValidePage($motcle, $page);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }
}
