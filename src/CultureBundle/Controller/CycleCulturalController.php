<?php

namespace CultureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CycleCulturalController extends Controller
{
    public function getcycleculturalAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');

        $fermeid = $request->query->get('fermeid');

        $site = $request->query->get('site');

        $result_total = $em->getRepository('CultureBundle:CycleCultural')->getCycleculturalValide($motcle, $fermeid, $site);

        $result = $em->getRepository('CultureBundle:CycleCultural')->getCycleculturalValidePage($motcle, $page, $fermeid, $site);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }

    public function gettypeirrigationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $result_total = $em->getRepository('CultureBundle:TypeIrrigation')->getTypeIrrigationValide($motcle);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result_total));
    }

    public function getgesteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $result_total = $em->getRepository('CultureBundle:GesteMo')->getGesteValide($motcle);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result_total));
    }
}
