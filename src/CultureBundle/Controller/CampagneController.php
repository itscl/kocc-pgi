<?php

namespace CultureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use CultureBundle\Form\CampagneType;
use CultureBundle\Entity\Campagne;
use \Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use \Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;

class CampagneController extends Controller
{
    public function campagneAction()
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            if (!$response->isNotModified($this->getRequest()))
            {
                $em = $this->getDoctrine()->getManager();

                $campagnes = $em->getRepository('CultureBundle:Campagne')->findAll();

                $response->setContent($this->renderView('CultureBundle:Campagne:campagne.html.twig', array(
                    'campagnes' => $campagnes
                )));

                return $response;
            }
        }
    }

    public function modifiercampagneAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $campagne = $em->getRepository('CultureBundle:Campagne')->find($id);

            $edit_form = $this->createForm(new CampagneType($em), $campagne);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    try
                    {
                        $em->flush();

                        $campagnes = $em->getRepository('CultureBundle:Campagne')->findAll();
                        $this->get('session')->getFlashBag()->add('noticeok', "La campagne a bien été modifiée !");
                        return $this->render('CultureBundle:Campagne:campagne.html.twig', array(
                            'campagnes' => $campagnes,
                        ));
                    }
                    catch(UniqueConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Cette donnée ne peut pas être ajoutée car elle existe déja dans la base");
                    }

                }
            }

            return $this->render('CultureBundle:Campagne:modifiercampagne.html.twig', array(
                'campagne' => $campagne,
                'edit_form' => $edit_form->createView()
            ));
        }
    }

    public function ajoutercampagneAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $entity = new Campagne();

            $form = $this->createForm(new CampagneType($em), $entity);


            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    try
                    {
                        $em->persist($entity);
                        $em->flush();

                        $campagnes = $em->getRepository('CultureBundle:Campagne')->findAll();
                        $this->get('session')->getFlashBag()->add('noticeok', "La campagne a bien été ajoutée !");
                        return $this->render('CultureBundle:Campagne:campagne.html.twig', array(
                            'campagnes' => $campagnes,
                        ));
                    }
                    catch(UniqueConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Cette donnée ne peut pas être ajoutée car elle existe déja dans la base");
                    }
                }
            }

            return $this->render('CultureBundle:Campagne:ajoutercampagne.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
        }
    }

    public function supprimercampagneAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $campagne = $em->getRepository('CultureBundle:Campagne')->find($id);

            try
            {
                $em->remove($campagne);
                $em->flush();
                $this->get('session')->getFlashBag()->add('noticeok', "La campagne a bien été supprimée !");

            } catch (ForeignKeyConstraintViolationException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Impossible de supprimer cette campagne car des ilots y sont attachés");
            }
            catch(\Doctrine\ORM\ORMInvalidArgumentException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Cette donnée n'existe plus dans la base");
            }

            $campagnes = $em->getRepository('CultureBundle:Campagne')->findAll();

            return $this->render('CultureBundle:Campagne:campagne.html.twig', array(
                'campagnes' => $campagnes,
            ));
        }
    }
}