<?php

namespace CultureBundle\Controller;

use CultureBundle\Entity\Culture;
use CultureBundle\Form\CultureType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CultureController extends Controller
{
    public function cultureAction()
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $culture = $em->getRepository('CultureBundle:Culture')->getAllCulture();



            return $this->render('CultureBundle:Culture:culture.html.twig', array(
                'culture' => $culture
            ));
        }
    }

    public function ajoutcultureAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $entity = new Culture();

            $form = $this->createForm(new CultureType($em), $entity);

            $user = $this->getUser()->getName();

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    $entity->setUPDATEUSER($user);
                    $em->persist($entity);
                    $em->flush();
                }
            }

            return $this->render('CultureBundle:culture:ajoutculture.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
        }
    }

    public function modifiercultureAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $id = $request->query->get('id');
            $em = $this->getDoctrine()->getManager();

            $culture = $em->getRepository('CultureBundle:Culture')->find($id);

            $itkcyclecv = $em->getRepository('CultureBundle:ITKCycleCV')->findBy(array('cULTUREID' => $id), array('sEMAINERECOLTE' => 'ASC'));

            $repartitionrendemants = $em->getRepository('CultureBundle:RepartitionRendement')->findBy(array('cULTUREID' => $id), array('nUMRECOLTE' => 'ASC'));

            //var_dump($repartitionrendemants); exit;

            $culturevarieteid = $culture->getCULTUREVARIETEID();
            $culturevarieteligneid = $culture->getCULTUREVARIETELIGNEID();
            $famillecultureid = $culture->getFAMILLECULTUREID();

            $culturevariete = $em->getRepository('CultureBundle:CultureVariete')->find($culturevarieteid);
            $culturevarieteligne = $em->getRepository('CultureBundle:CultureVarieteLigne')->find($culturevarieteligneid);
            $familleculture = $em->getRepository('CultureBundle:FamilleCulture')->find($famillecultureid);

            $edit_form = $this->createForm(new CultureType($em), $culture);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $em->flush();
                }
            }

            return $this->render('CultureBundle:culture:modifierculture.html.twig',array(
                'culture' => $culture,
                'edit_form' => $edit_form->createView(),
                'culturevariete' => $culturevariete,
                'culturevarieteligne' => $culturevarieteligne,
                'familleculture' => $familleculture,
                'itkcyclecv' => $itkcyclecv,
                'repartitionrendemants' => $repartitionrendemants
            ));
        }
    }

    public function getcultureAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');

        $result_total = $em->getRepository('CultureBundle:Culture')->getCultureValide($motcle);

        $result = $em->getRepository('CultureBundle:Culture')->getCultureValidePage($motcle, $page);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }
}
