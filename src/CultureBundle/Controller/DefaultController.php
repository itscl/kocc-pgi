<?php

namespace CultureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CultureBundle:Default:index.html.twig');
    }
}
