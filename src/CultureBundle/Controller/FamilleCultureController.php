<?php

namespace CultureBundle\Controller;

use CultureBundle\Entity\FamilleCulture;
use CultureBundle\Entity\ITKMO;
use CultureBundle\Entity\ItMO;
use CultureBundle\Form\FamilleCultureType;
use CultureBundle\Form\ItMOType;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FamilleCultureController extends Controller
{
    public function famillecultureAction()
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $familleculture = $em->getRepository('CultureBundle:FamilleCulture')->findAll();

            return $this->render('CultureBundle:FamilleCulture:familleculture.html.twig', array(
                'familleculture' => $familleculture
            ));
        }
    }

    public function ajoutfamillecultureAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $entity = new FamilleCulture();

            $form = $this->createForm(new FamilleCultureType(), $entity);

            $user = $this->getUser()->getName();

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    $entity->setUPDATEUSER($user);
                    $em->persist($entity);
                    $em->flush();

                    return $this->redirect($this->generateUrl('itk' , array('famillecultureid'=> $entity->getId())));
                }
            }

            return $this->render('CultureBundle:FamilleCulture:ajoutfamilleculture.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
        }
    }

    public function modifierfamillecultureAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $familleculture = $em->getRepository('CultureBundle:FamilleCulture')->find($id);

            $edit_form = $this->createForm(new FamilleCultureType(), $familleculture);

            $user  = $this->getUser()->getName();

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    $familleculture->setUPDATEDATE(new \DateTime());
                    $familleculture->setUPDATEUSER($user);
                    $em->flush();
                }
            }

            $allitmoByFamille = $em->getRepository('CultureBundle:ItMO')->getAllitmoByFamilleCulture($id);

            $entity = new ItMO();
            $entityitkmo1 = new ITKMO();
            $entityitkmo2 = new ITKMO();
            $entityitkmo3 = new ITKMO();
            $entityitkmo4= new ITKMO();

            $form = $this->createForm(new ItMOType($em), $entity);

            if ($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    $newlibelle = $form->get('lIBELLE')->getData();
                    //$itmoByFamille = $em->getRepository('CultureBundle:ItMO')->getitmoByFamilleCulture($id,
                    //$newlibelle);

                    try
                    {
                        $entity->setUSERUPDATE($user);
                        $entity->setDATEUPDATE(new \DateTime());
                        $entity->setFAMILLECULTUREID($id);

                        //var_dump($form->getData()); exit;

                        $em->persist($entity);
                        $em->flush();

                        $entityitkmo1->setITMOID($entity->getId());
                        $entityitkmo1->setUPDATEDATE(new \DateTime());
                        $entityitkmo1->setUPDATEUSER($user);
                        $entityitkmo1->setRANG(-10);
                        $entityitkmo1->setCODE('PRE');
                        $entityitkmo1->setLIBELLE('PREPARATION SOL');
                        $entityitkmo1->setFAMILLECULTUREID($id);

                        $entityitkmo2->setITMOID($entity->getId());
                        $entityitkmo2->setUPDATEDATE(new \DateTime());
                        $entityitkmo2->setUPDATEUSER($user);
                        $entityitkmo2->setRANG(0);
                        $entityitkmo2->setCODE('SEM');
                        $entityitkmo2->setLIBELLE('SEMIS');
                        $entityitkmo2->setFAMILLECULTUREID($id);

                        $entityitkmo3->setITMOID($entity->getId());
                        $entityitkmo3->setUPDATEDATE(new \DateTime());
                        $entityitkmo3->setUPDATEUSER($user);
                        $entityitkmo3->setRANG(10);
                        $entityitkmo3->setCODE('CON');
                        $entityitkmo3->setLIBELLE('CONDUITE CULTURALE');
                        $entityitkmo3->setFAMILLECULTUREID($id);

                        $entityitkmo4->setITMOID($entity->getId());
                        $entityitkmo4->setUPDATEDATE(new \DateTime());
                        $entityitkmo4->setUPDATEUSER($user);
                        $entityitkmo4->setRANG(20);
                        $entityitkmo4->setCODE('REC');
                        $entityitkmo4->setLIBELLE('RECOLTE');
                        $entityitkmo4->setFAMILLECULTUREID($id);

                        $em->persist($entityitkmo1);
                        $em->flush();

                        $em->persist($entityitkmo2);
                        $em->flush();

                        $em->persist($entityitkmo3);
                        $em->flush();

                        $em->persist($entityitkmo4);
                        $em->flush();

                        return $this->redirect($this->generateUrl('modifierfamilleculture', array('id' => $id)));
                    }
                    catch(NotNullConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Veuillez renseigner tous les champs obligatoire !!");
                    }
                }
            }

            $itkmo = $em->getRepository('CultureBundle:ITKMO')->findBy(array('fAMILLECULTUREID' => $id), array('rANG' => 'ASC'));
            $itkmos = $em->getRepository('CultureBundle:ITKMOS')->getitmosByFamilleCulture($id);
            $gestemo = $em->getRepository('CultureBundle:GesteMo')->findAll();
            $itkmog = $em->getRepository('CultureBundle:ITKMOG')->getitmogByFamilleCulture($id);
            $unitemo = $em->getRepository('TechniqueBundle:Lov')->lovunitemo();
            $culture = $em->getRepository('CultureBundle:Culture')->findBy(array('fAMILLECULTUREID' => $id, 'dATEINVALIDE' => NULL));
            $itkmocv = $em->getRepository('CultureBundle:ITKMOCV')->getitmocvByFamilleCulture($id);
            $itkmocvp = $em->getRepository('CultureBundle:ITKMOCV')->getitmocvByFamilleCultureAndPasage($id);
            //var_dump($itkmocv); exit;
            //var_dump($itkmocvp);
            //exit;
            $typeirrigation= $em->getRepository('CultureBundle:TypeIrrigation')->findAll();

            return $this->render('CultureBundle:FamilleCulture:modifierfamilleculture.html.twig',array(
                'familleculture' => $familleculture,
                'edit_form' => $edit_form->createView(),
                'entity' => $entity,
                'form' => $form->createView(),
                'itmoByFamille' => $allitmoByFamille,
                'itkmo' => $itkmo,
                'itkmos' => $itkmos,
                'gestemo' => $gestemo,
                'itkmog' => $itkmog,
                'unitemo' => $unitemo,
                'culture' => $culture,
                'itkmocv' => $itkmocv,
                'typeirrigation' => $typeirrigation,
                'itkmocvp' => $itkmocvp

            ));
        }
    }

    public function getfamillecultureAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');

        $result_total = $em->getRepository('CultureBundle:FamilleCulture')->getFamilleCultureValide($motcle);

        $result = $em->getRepository('CultureBundle:FamilleCulture')->getFamilleCultureValidePage($motcle, $page);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }

}
