<?php

namespace CultureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use CultureBundle\Entity\Ferme;
use Symfony\Component\HttpFoundation\Response;
use CultureBundle\Form\FermeType;

class FermeController extends Controller
{
    public function fermeAction()
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            if (!$response->isNotModified($this->getRequest()))
            {
                $em = $this->getDoctrine()->getManager();
                $fermes = $em->getRepository('CultureBundle:Ferme')->getAllFerme();

                $response->setContent($this->renderView('CultureBundle:Ferme:ferme.html.twig', array(
                    'fermes' => $fermes
                )));

                return $response;
            }
        }
    }

    public function modifierfermeAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $ferme = $em->getRepository('CultureBundle:Ferme')->find($id);

            $edit_form = $this->createForm(new FermeType($em), $ferme);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    try
                    {
                        $em->flush();

                        $fermes = $em->getRepository('CultureBundle:Ferme')->getAllFerme();
                        $this->get('session')->getFlashBag()->add('noticeok', "La ferme a bien été modifiée !");

                        return $this->render('CultureBundle:Ferme:ferme.html.twig', array(
                            'fermes' => $fermes,
                        ));
                    }
                    catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Cette donnée ne peut pas être ajoutée car elle existe déja dans la base");
                    }
                }
            }

            return $this->render('CultureBundle:Ferme:modifierferme.html.twig', array(
                'ferme' => $ferme,
                'edit_form' => $edit_form->createView()
            ));
        }
    }

    public function ajouterfermeAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $entity = new Ferme();

            $form = $this->createForm(new FermeType($em), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    try
                    {
                        $em->persist($entity);
                        $em->flush();

                        $fermes = $em->getRepository('CultureBundle:Ferme')->getAllFerme();
                        $this->get('session')->getFlashBag()->add('noticeok', "La ferme a bien été ajoutée !");

                        return $this->render('CultureBundle:Ferme:ferme.html.twig', array(
                            'fermes' => $fermes,
                        ));
                    }
                    catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Cette donnée ne peut pas être ajoutée car elle existe déja dans la base");
                    }
                }
            }

            return $this->render('CultureBundle:Ferme:ajouterferme.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
        }
    }

    public function supprimerfermeAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $ferme = $em->getRepository('CultureBundle:Ferme')->find($id);

            try
            {
                $em->remove($ferme);
                $em->flush();
                $this->get('session')->getFlashBag()->add('noticeok', "La ferme a bien été supprimée !");

            } catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Impossible de supprimer cette ferme car des données y sont attachés");
            }
            catch(\Doctrine\ORM\ORMInvalidArgumentException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Cette donnée n'existe plus dans la base");
            }

            $fermes = $em->getRepository('CultureBundle:Ferme')->getAllFerme();

            return $this->render('CultureBundle:Ferme:ferme.html.twig', array(
                'fermes' => $fermes,
            ));
        }
    }

    public function getfermeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');

        $siteid = $request->query->get('siteid');

        $result_total = $em->getRepository('CultureBundle:Ferme')->getFermeValide($motcle, $siteid);

        $result = $em->getRepository('CultureBundle:Ferme')->getFermePage($motcle, $page, $siteid);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }
}
