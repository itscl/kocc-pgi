<?php

namespace CultureBundle\Controller;

use CultureBundle\Entity\ITKMOCV;
use CultureBundle\Entity\ITKMOG;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ItkmogController extends Controller
{
    public function ajoutitkmogAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $itkmosid = $request->query->get('itkmosid');
        $gesteid = $request->query->get('gesteid');

        $itkmog = $em->getRepository('CultureBundle:ITKMOG')->findBy(array('iTKMOSID' => $itkmosid, 'gESTEID' => $gesteid));

        if(!$itkmog)
        {
            if ($gesteid != null) {

                $entity = new ITKMOG();
                $entity->setITKMOSID($itkmosid);
                $entity->setGESTEID($gesteid);

                $em->persist($entity);
                $em->flush();
            }
        }
        else
        {
            $this->get('session')->getFlashBag()->add('notice1', "Ce geste existe déjà pour cette periode ");
        }

        return $this->render('CultureBundle:Itkmog:ajax.html.twig');
    }

    public function ajoutitkmocvAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();


        $itkmogid = $request->query->get('itkmogid');
        $cultureid = $request->query->get('cultureid');

        $itkmocv = $em->getRepository('CultureBundle:ITKMOCV')->findBy(array('iTKMOGID' => $itkmogid, 'cULTUREID' => $cultureid));

        //var_dump($itkmocv); exit;

        if(!$itkmocv)
        {
            if($cultureid !=null)
            {

                $entity = new ITKMOCV();
                $entity->setCULTUREID($cultureid);
                $entity->setITKMOGID($itkmogid);

                $em->persist($entity);


                var_dump($entity);// exit;

                //$itkmocv1 = $em->getRepository('CultureBundle:ITKMOCV')->find($value);

                if($entity)
                {
                    $query = "select seq_mo.NEXTVAL as val from DUAL";
                    $stmt = $conn->prepare($query);
                    $stmt->execute();
                    $result = $stmt->fetchAll();

                    var_dump($result); //exit;
                }

                $em->flush();

                //$conn->insert('ITKMOCV', array('ITKMOGID' => $itkmogid, 'CULTUREID' => $cultureid));
            }
        }
        else
        {
            $this->get('session')->getFlashBag()->add('notice1', "Cette culture variéte existe déjà pour ce geste ");
        }


        return $this->render('CultureBundle:Itkmog:ajax.html.twig');
    }

    public function modifieritkmocvAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $itkmocvid = $request->query->get('itkmocvid');
        $quantite = $request->query->get('quantite');
        $unite = $request->query->get('unite');
        $passage = $request->query->get('passage');

        $itkmocv = $em->getRepository('CultureBundle:ITKMOCV')->find($itkmocvid);

        var_dump($itkmocvid);
        var_dump($quantite);
        var_dump($itkmocv);

        $itkmocv->setQUANTITE($quantite);
        $itkmocv->setUNITE($unite);

        if($passage != '' && $passage != -1)
        {
            $getidpassage = $em->getRepository('CultureBundle:RepartitionRendement')->findBy(array('nUMRECOLTE' => $passage, 'cULTUREID' => $itkmocv->getCULTUREID() ));

            foreach($getidpassage as $rs)
            {
                var_dump($rs->getId());
                $itkmocv->setREPARTITIONREDEMENTID($rs->getId());
            }

            //
        }
        else
            if($passage == -1)
            {
                $itkmocv->setREPARTITIONREDEMENTID(-1);
            }
       // exit;
        $em->flush();


        return $this->render('CultureBundle:Itkmog:ajax.html.twig');
    }

    public function supprimeritkmogAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $itkmogid = $request->query->get('itkmogid');
        $quantite = $request->query->get('quantite');

        $itkmog = $em->getRepository('CultureBundle:ITKMOG')->find($itkmogid);

        $em->remove($itkmog);
        $em->flush();

        return $this->render('CultureBundle:Itkmog:ajax.html.twig');

    }

    public function supprimeritkmocvAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $famillecultureid = $request->query->get('famillecultureid');
        $itkmocvid = $request->query->get('itkmocvid');

        $itkmocv = $em->getRepository('CultureBundle:ITKMOCV')->find($itkmocvid);


        $em->remove($itkmocv);
        $em->flush();

        return $this->redirect($this->generateUrl('modifierfamilleculture', array('id' => $famillecultureid)));
    }

    public function reloadgesteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->query->get('id');
        $itkmosid = $request->query->get('itkmosid');
        $gesteid = $request->query->get('gesteid');

        $itkmog = $em->getRepository('CultureBundle:ITKMOG')->getitmogByitkmosid($itkmosid, $gesteid);

        foreach($itkmog as $rs)
        {
            $iTMOID = $rs['iTMOID'];
        }

        //var_dump($itkmog); exit;

        $culture = $em->getRepository('CultureBundle:Culture')->findBy(array('fAMILLECULTUREID' => $id, 'dATEINVALIDE' => NULL));
        $itkmocv = $em->getRepository('CultureBundle:ITKMOCV')->getitmocvByFamilleCulture($id);
        $gestemo = $em->getRepository('CultureBundle:GesteMo')->findAll();
        $unitemo = $em->getRepository('TechniqueBundle:Lov')->lovunitemo();


        return $this->render('CultureBundle:Itkmog:reloadgeste.html.twig', array(
            'itkmog' => $itkmog,
            'culture' => $culture,
            'itkmocv' => $itkmocv,
            'itkmosid' => $itkmosid,
            'gestemo' => $gestemo,
            'id' => $id,
            'unitemo' => $unitemo,
            'iTMOID' => $iTMOID

        ));
    }

    public function reloadcvAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->query->get('id');
        $itkmogid = $request->query->get('itkmogid');
        $cultureid = $request->query->get('cultureid');

        $itkmocv = $em->getRepository('CultureBundle:ITKMOCV')->getitmocvByitkmog($itkmogid, $cultureid);

        $itkmocvp = $em->getRepository('CultureBundle:ITKMOCV')->getitmocvByFamilleCultureAndPasage($id);

        $itkmog = $em->getRepository('CultureBundle:ITKMOG')->getitmog($itkmogid);

        $unitemo = $em->getRepository('TechniqueBundle:Lov')->lovunitemo();

        $culture = $em->getRepository('CultureBundle:Culture')->findBy(array('fAMILLECULTUREID' => $id, 'dATEINVALIDE' => NULL));

        return $this->render('CultureBundle:Itkmog:reloadcv.html.twig', array(
            'itkmocv' => $itkmocv,
            'itkmog' => $itkmog,
            'id' => $id,
            'unitemo' => $unitemo,
            'itkmogid' => $itkmogid,
            'culture' => $culture,
            'itkmocvp' => $itkmocvp
        ));
    }

    public function supprimeritkmogesteAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $famillecultureid = $request->query->get('famillecultureid');
            $itkmogid = $request->query->get('itkmogid');

            $itkmog = $em->getRepository('CultureBundle:ITKMOG')->find($itkmogid);

            $em->remove($itkmog);
            $em->flush();

            //var_dump($itkmos); exit;

            return $this->redirect($this->generateUrl('modifierfamilleculture', array('id' => $famillecultureid)));
        }
    }

    public function dupliqueritkmogAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $itkmogid = $request->request->get('itkmogid');
        $periode = $request->request->get('itkmos');
        $itmoid = $request->request->get('itmoid');

        var_dump($itkmogid);
        var_dump($periode);
        var_dump($itmoid);

        $itkmog = $em->getRepository('CultureBundle:ITKMOG')->find($itkmogid);
        $itkmosid = $itkmog->getITKMOSID();
        $gesteid = $itkmog->getGESTEID();
        /*$itkmosid = $itkmog->getITKMOSID();*/

        $itkmos = $em->getRepository('CultureBundle:ITKMOS')->find($itkmosid);

        $itkmosperieode = $em->getRepository('CultureBundle:ITKMOS')->getPeriode($itmoid, $periode);

/*
        var_dump($itkmog);
        var_dump($itkmosid);
        var_dump($itkmos);
        var_dump($itkmosperieode);
        exit;*/

        if($itkmosperieode) {
            foreach ($itkmosperieode as $rs) {

                $id = $rs['id'];

                $entityitkmog = new ITKMOG();

                $entityitkmog->setITKMOSID($id);
                $entityitkmog->setGESTEID($gesteid);

                $em->persist($entityitkmog);
                $em->flush();

                //exit;

                $itkmocv = $em->getRepository('CultureBundle:ITKMOCV')->findByiTKMOGID($itkmogid);
               // $itkmocv1 = $em->getRepository('CultureBundle:ITKMOCV')->findByiTKMOGID($itkmogid);

                //var_dump($itkmocv); exit;

                if($itkmocv) {
                    foreach ($itkmocv as $rsitkmocv) {

                        $itkmoscvid = $rsitkmocv->getId();
                        $quantite = $rsitkmocv->getQUANTITE();
                        $unite = $rsitkmocv->getUNITE();
                        $cultureid = $rsitkmocv->getCULTUREID();

                        $entityitkmocv = new ITKMOCV();

                        $entityitkmocv->setITKMOGID($entityitkmog->getId());
                        $entityitkmocv->setQUANTITE($quantite);
                        $entityitkmocv->setUNITE($unite);
                        $entityitkmocv->setCULTUREID($cultureid);

                        $em->persist($entityitkmocv);
                        var_dump($entityitkmocv);
                        if ($entityitkmocv) {
                            $query = "select seq_mo.NEXTVAL as val from DUAL";
                            $stmt = $conn->prepare($query);
                            $stmt->execute();
                            $result = $stmt->fetchAll();

                            var_dump($result); //exit;
                        }
                        $em->flush();
                    }
                }
            }
        }
        else
        {
            $this->get('session')->getFlashBag()->add('notice1', "La semaine ".$itkmos." n'est pas encore crée");
        }

        return $this->render('CultureBundle:Itkmog:ajax.html.twig');
    }


}
