<?php

namespace CultureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CultureVarieteLigneController extends Controller
{
    public function getvarieteligneAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');

        $culturevarieteid =  $request->query->get('culturevarieteid');

        $result_total = $em->getRepository('CultureBundle:CultureVarieteLigne')->getVarieteLigneV($motcle, $culturevarieteid);

        $result = $em->getRepository('CultureBundle:CultureVarieteLigne')->getVarieteLigneVPage($motcle, $culturevarieteid, $page);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }
}
