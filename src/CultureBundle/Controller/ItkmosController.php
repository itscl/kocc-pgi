<?php

namespace CultureBundle\Controller;

use CultureBundle\Entity\ITKMOCV;
use CultureBundle\Entity\ITKMOG;
use CultureBundle\Entity\ITKMOS;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ItkmosController extends Controller
{

    public function ajoutitkmosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $iditkmo = $request->query->get('iditkmo');
        $semaine = $request->query->get('semaine');
        $famillecultureid = $request->query->get('famillecultureid');

        $periodeexist = $em->getRepository('CultureBundle:ITKMOS')->periodeExist($famillecultureid, $semaine );

        if(!$periodeexist)
        {
            //$conn->insert('ITKMOS', array('ITKMOID' => $iditkmo, 'PERIODE' => $semaine));
            $entity = new ITKMOS();

            $entity->setITKMOID($iditkmo);
            $entity->setPERIODE($semaine);

            $em->persist($entity);
            $em->flush();
        }
        else
        {
            foreach($periodeexist as $rs)
            {
                $libelle = $rs['lIBELLE'];
            }
            $this->get('session')->getFlashBag()->add('notice2', "Cette semaine existe déjà. Veuillez choisir une autre semaine !");
        }


        return $this->render('CultureBundle:Itkmos:ajax.html.twig');
    }

    public function reloadAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->query->get('id');
        $itkmoid = $request->query->get('itkmoid');
        $semaine = $request->query->get('semaine');

        $itkmo = $em->getRepository('CultureBundle:ITKMO')->findBy(array('fAMILLECULTUREID' => $id));

        $itkmos = $em->getRepository('CultureBundle:ITKMOS')->periodeExist1($itkmoid, $semaine );

        $gestemo = $em->getRepository('CultureBundle:GesteMo')->findAll();
        $itkmog = $em->getRepository('CultureBundle:ITKMOG')->getitmogByFamilleCulture($id);
        $itkmocv = $em->getRepository('CultureBundle:ITKMOCV')->getitmocvByFamilleCulture($id);
        $culture = $em->getRepository('CultureBundle:Culture')->findBy(array('fAMILLECULTUREID' => $id, 'dATEINVALIDE' => NULL));
        $unitemo = $em->getRepository('TechniqueBundle:Lov')->lovunitemo();

        //var_dump($itkmos);

        return $this->render('CultureBundle:Itkmos:reload.html.twig', array(
            'itkmo' => $itkmo,
            'itkmos' => $itkmos,
            'id' => $id,
            'gestemo' => $gestemo,
            'itkmog' => $itkmog,
            'itkmocv' => $itkmocv,
            'culture' => $culture,
            'unitemo' => $unitemo
        ));
    }

    public function supprimeritkmosAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $famillecultureid = $request->query->get('famillecultureid');
            $itkmosid = $request->query->get('itkmosid');

            $itkmos = $em->getRepository('CultureBundle:ITKMOS')->find($itkmosid);

            $em->remove($itkmos);
            $em->flush();

            //var_dump($itkmos); exit;

            return $this->redirect($this->generateUrl('modifierfamilleculture', array('id' => $famillecultureid)));
        }
    }

    public function getterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->query->get('id');
        $itkmoid = $request->query->get('itkmoid');
        $semaine = $request->query->get('semaine');

        $itkmo = $em->getRepository('CultureBundle:ITKMO')->findBy(array('fAMILLECULTUREID' => $id));

        $itkmos = $em->getRepository('CultureBundle:ITKMOS')->periodeExist($itkmoid, $semaine );

        $gestemo = $em->getRepository('CultureBundle:GesteMo')->findAll();

        return $this->render('CultureBundle:Itkmos:getter.html.twig', array(
            'itkmo' => $itkmo,
            'itkmos' => $itkmos,
            'id' => $id,
            'gestemo' => $gestemo
        ));
    }

    public function dupliqueritkmosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $itkmosid = $request->request->get('itkmosid');
        $itkmoid = $request->request->get('itkmoid');
        $newlibelle = $request->request->get('newlibelle');
        $famillecultureid = $request->request->get('famillecultureid');

        echo "itkmos";
        var_dump($itkmosid);
        var_dump("itk mo "+$itkmoid);
        var_dump($newlibelle);
        var_dump($famillecultureid);
        //exit;

        //$itkmos = $em->getRepository('CultureBundle:ITKMOS')->find($itkmosid);

        $periodeexist = $em->getRepository('CultureBundle:ITKMOS')->periodeExist($famillecultureid, $newlibelle, $itkmoid);

       //var_dump($periodeexist); exit;

        if(!$periodeexist)
        {
            //$conn->insert('ITKMOS', array('ITKMOID' => $iditkmo, 'PERIODE' => $semaine));
            $entityitkmos = new ITKMOS();

            $entityitkmos->setITKMOID($itkmoid);
            $entityitkmos->setPERIODE($newlibelle);

            $em->persist($entityitkmos);
            $em->flush();

            $itkmog = $em->getRepository('CultureBundle:ITKMOG')->findByiTKMOSID($itkmosid);

            if($itkmog)
            {
                foreach($itkmog as $rsitkmog)
                {
                    $itkmogid = $rsitkmog->getId();
                    $gesteid = $rsitkmog->getGESTEID();

                    $entityitkmog = new ITKMOG();

                    $entityitkmog->setITKMOSID($entityitkmos->getId());
                    $entityitkmog->setGESTEID($gesteid);

                    $em->persist($entityitkmog);
                    $em->flush();

                    $itkmocv = $em->getRepository('CultureBundle:ITKMOCV')->findByiTKMOGID($itkmogid);

                    if($itkmocv)
                    {
                        foreach($itkmocv as $rsitkmocv)
                        {
                            $itkmoscvid = $rsitkmocv->getId();
                            $quantite = $rsitkmocv->getQUANTITE();
                            $unite = $rsitkmocv->getUNITE();
                            $cultureid = $rsitkmocv->getCULTUREID();

                            $entityitkmocv = new ITKMOCV();

                            $entityitkmocv->setITKMOGID($entityitkmog->getId());
                            $entityitkmocv->setQUANTITE($quantite);
                            $entityitkmocv->setUNITE($unite);
                            $entityitkmocv->setCULTUREID($cultureid);

                            $em->persist($entityitkmocv);
                            var_dump($entityitkmocv);
                            if($entityitkmocv)
                            {
                                $query = "select seq_mo.NEXTVAL as val from DUAL";
                                $stmt = $conn->prepare($query);
                                $stmt->execute();
                                $result = $stmt->fetchAll();

                                var_dump($result); //exit;
                            }
                            $em->flush();
                        }
                    }
                }
            }
        }
        else
        {
            foreach($periodeexist as $rs)
            {
                $id = $rs['id'];
                $libelle = $rs['lIBELLE'];

            }

            $itkmog1 = $em->getRepository('CultureBundle:ITKMOG')->findByiTKMOSID($itkmosid);
            $itkmog = $em->getRepository('CultureBundle:ITKMOG')->findByiTKMOSID($id);

            if($itkmog)
            {
                foreach($itkmog1 as $rsitkmog1)
                {
                    $itkmogid = $rsitkmog1->getId();
                    $gesteid = $rsitkmog1->getGESTEID();

                    foreach($itkmog as $rsitkmog)
                    {
                        $itkmosid = $rsitkmog->getITKMOSID();

                        $entityitkmog = new ITKMOG();

                        $entityitkmog->setITKMOSID($itkmosid);
                        $entityitkmog->setGESTEID($gesteid);

                        $em->persist($entityitkmog);
                        if($entityitkmog)
                        {
                            $query = "select seq_mo.NEXTVAL as val from DUAL";
                            $stmt = $conn->prepare($query);
                            $stmt->execute();
                            $result = $stmt->fetchAll();

                            var_dump($result); //exit;
                        }
                        $em->flush();

                        $itkmocv = $em->getRepository('CultureBundle:ITKMOCV')->findByiTKMOGID($itkmogid);

                        if($itkmocv)
                        {
                            foreach($itkmocv as $rsitkmocv)
                            {
                                $itkmoscvid = $rsitkmocv->getId();
                                $quantite = $rsitkmocv->getQUANTITE();
                                $unite = $rsitkmocv->getUNITE();
                                $cultureid = $rsitkmocv->getCULTUREID();

                                $entityitkmocv = new ITKMOCV();

                                $entityitkmocv->setITKMOGID($entityitkmog->getId());
                                $entityitkmocv->setQUANTITE($quantite);
                                $entityitkmocv->setUNITE($unite);
                                $entityitkmocv->setCULTUREID($cultureid);

                                $em->persist($entityitkmocv);

                                var_dump($entityitkmocv);

                                if($entityitkmocv)
                                {
                                    $query = "select seq_mo.NEXTVAL as val from DUAL";
                                    $stmt = $conn->prepare($query);
                                    $stmt->execute();
                                    $result = $stmt->fetchAll();

                                    var_dump($result); //exit;
                                }

                                $em->flush();

                            }
                        }
                    }
                }


            }
            //$this->get('session')->getFlashBag()->add('notice2', "Cette semaine existe déjà. Veuillez choisir une autre semaine !");
        }


        //var_dump($itkmos);
       // exit;

        return $this->render('CultureBundle:Itkmos:ajax.html.twig');
    }

}
