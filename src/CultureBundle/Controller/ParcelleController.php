<?php

namespace CultureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use CultureBundle\Entity\Parcelle;
use CultureBundle\Form\ParcelleType;
use Symfony\Component\HttpFoundation\Response;

class ParcelleController extends Controller
{
    public function parcelleAction()
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            if (!$response->isNotModified($this->getRequest()))
            {
                $em = $this->getDoctrine()->getManager();
                $parcelles = $em->getRepository('CultureBundle:Parcelle')->getAllParcelle();

                $response->setContent($this->renderView('CultureBundle:Parcelle:parcelle.html.twig', array(
                    'parcelles' => $parcelles
                )));

                return $response;
            }
        }
    }

    public function modifierparcelleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $parcelle = $em->getRepository('CultureBundle:Parcelle')->find($id);

            $edit_form = $this->createForm(new ParcelleType($em), $parcelle);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    try
                    {
                        $em->flush();

                        $parcelles = $em->getRepository('CultureBundle:Parcelle')->getAllParcelle();
                        $this->get('session')->getFlashBag()->add('noticeok', "La parcelle a bien été modifiée !");

                        return $this->render('CultureBundle:Parcelle:parcelle.html.twig', array(
                            'parcelles' => $parcelles,
                        ));
                    }
                    catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Cette donnée ne peut pas être ajoutée car elle existe déja dans la base");
                    }
                }
            }

            return $this->render('CultureBundle:Parcelle:modifierparcelle.html.twig', array(
                'parcelle' => $parcelle,
                'edit_form' => $edit_form->createView()
            ));
        }
    }

    public function ajouterparcelleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $entity = new Parcelle();

            $form = $this->createForm(new ParcelleType($em), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    try
                    {
                        $em->persist($entity);
                        $em->flush();

                        $parcelles = $em->getRepository('CultureBundle:Parcelle')->getAllParcelle();
                        $this->get('session')->getFlashBag()->add('noticeok', "La parcelle a bien été ajoutée !");

                        return $this->render('CultureBundle:Parcelle:parcelle.html.twig', array(
                            'parcelles' => $parcelles,
                        ));
                    }
                    catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Cette donnée ne peut pas être ajoutée car elle existe déja dans la base");
                    }
                }
            }

            return $this->render('CultureBundle:Parcelle:ajouterparcelle.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
        }
    }


    public function supprimerparcelleAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $parcelle = $em->getRepository('CultureBundle:Parcelle')->find($id);

            try
            {
                $em->remove($parcelle);
                $em->flush();
                $this->get('session')->getFlashBag()->add('noticeok', "La parcelle a bien été supprimée !");

            } catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Impossible de supprimer cette parcelle car des données y sont attachés");
            }
            catch(\Doctrine\ORM\ORMInvalidArgumentException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Cette donnée n'existe plus dans la base");
            }

            $parcelles= $em->getRepository('CultureBundle:Parcelle')->getAllParcelle();

            return $this->render('CultureBundle:Parcelle:parcelle.html.twig', array(
                'parcelles' => $parcelles,
            ));
        }
    }}