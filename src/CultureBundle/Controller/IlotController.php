<?php

namespace CultureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use CultureBundle\Entity\Ilot;
use CultureBundle\Form\IlotType;
use Symfony\Component\HttpFoundation\Response;

class IlotController extends Controller
{
    public function ilotAction()
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $response = new Response();
            $response->setMaxAge(300);

            if (!$response->isNotModified($this->getRequest()))
            {
                $em = $this->getDoctrine()->getManager();

                $ilots = $em->getRepository('CultureBundle:Ilot')->getAllIlot();

                $response->setContent($this->renderView('CultureBundle:ilot:ilot.html.twig', array(
                    'ilots' => $ilots
                )));

                return $response;
            }
        }
    }

    public function modifierilotAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $ilot = $em->getRepository('CultureBundle:Ilot')->find($id);

            $edit_form = $this->createForm(new IlotType($em), $ilot);

            if($request->isMethod('post'))
            {
                $edit_form->handleRequest($request);

                if($edit_form->isValid())
                {
                    try
                    {
                        $em->flush();

                        $ilots = $em->getRepository('CultureBundle:Ilot')->getAllIlot();
                        $this->get('session')->getFlashBag()->add('noticeok', "L'ilot a bien été modifié !");

                        return $this->render('CultureBundle:Ilot:ilot.html.twig', array(
                            'ilots' => $ilots,
                        ));
                    }
                    catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Cette donnée ne peut pas être ajoutée car elle existe déja dans la base");
                    }
                }
            }

            return $this->render('CultureBundle:Ilot:modifierilot.html.twig', array(
                'ilot' => $ilot,
                'edit_form' => $edit_form->createView()
            ));
        }
    }

    public function ajouterilotAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $entity = new Ilot();

            $form = $this->createForm(new IlotType($em), $entity);

            if($request->isMethod('post'))
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    try
                    {
                        $em->persist($entity);
                        $em->flush();

                        $ilots = $em->getRepository('CultureBundle:Ilot')->getAllIlot();
                        $this->get('session')->getFlashBag()->add('noticeok', "L'ilot a bien été modifié !");

                        return $this->render('CultureBundle:Ilot:ilot.html.twig', array(
                            'ilots' => $ilots,
                        ));
                    }
                    catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e)
                    {
                        $this->get('session')->getFlashBag()->add('notice', "Cette donnée ne peut pas être ajoutée car elle existe déja dans la base");
                    }
                }
            }

            return $this->render('CultureBundle:Ilot:ajouterilot.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
        }
    }

    public function supprimerilotAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');

        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $id = $request->query->get('id');

            $ilot = $em->getRepository('CultureBundle:Ilot')->find($id);

            try
            {
                $em->remove($ilot);
                $em->flush();
                $this->get('session')->getFlashBag()->add('noticeok', "L'ilot a bien été supprimé !");

            } catch (\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Impossible de supprimer cet ilot car des données y sont attachés");
            }
            catch(\Doctrine\ORM\ORMInvalidArgumentException $e)
            {
                $this->get('session')->getFlashBag()->add('notice', "Cette donnée n'existe plus dans la base");
            }

            $ilots= $em->getRepository('CultureBundle:Ilot')->getAllIlot();

            return $this->render('CultureBundle:Ilot:ilot.html.twig', array(
                'ilots' => $ilots,
            ));
        }
    }

    public function getilotfermeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $motcle = $request->query->get('motcle');

        $page = $request->query->get('page');

        $fermeid = $request->query->get('fermeid');

        $result_total = $em->getRepository('CultureBundle:Ilot')->getIlotAllFerme($motcle, $fermeid);

        $result = $em->getRepository('CultureBundle:Ilot')->getIlotFerme($motcle, $page, $fermeid);

        $count = count($result_total);

        return new JsonResponse(array("count" => $count,"incomplete_results" => false, "items"
        => $result));
    }
}