<?php

namespace CultureBundle\Controller;

use CultureBundle\Entity\RepartitionRendement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RepartitionRendementController extends Controller
{
    public function ajoutAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $new_numrecolte = $request->query->get('new_numrecolte');
        $new_pourcentage = $request->query->get('new_pourcentage');
        $culture = $request->query->get('culture');

        var_dump($new_numrecolte);
        var_dump($new_pourcentage);
        //var_dump($culture);
       // exit;
        $repartitionrend = $em->getRepository('CultureBundle:RepartitionRendement')->findBy(array('cULTUREID' => $culture, 'nUMRECOLTE' => $new_numrecolte));

        $user = $this->getUser()->getName();

        $entity = new RepartitionRendement();

        $entity->setNUMRECOLTE($new_numrecolte);
        $entity->setPOURCENTREND($new_pourcentage);
        $entity->setCULTUREID($culture);
        $entity->setDATEUPDATE(new \DateTime());
        $entity->setUSERUPDATE($user);

        if(!$repartitionrend)
        {
            $em->persist($entity);
            $em->flush();
        }

        return $this->render('CultureBundle:RepartitionRendement:ajax.html.twig');
    }

    public function modifierAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->query->get('id');
        $numrecolte = $request->query->get('numrecolte');
        $pourcentrend = $request->query->get('pourcentrend');
        //$culture = $request->query->get('culture');

        $user = $this->getUser()->getName();


        $repartitionrend = $em->getRepository('CultureBundle:RepartitionRendement')->find($id);

        $repartitionrend->setNUMRECOLTE($numrecolte);
        $repartitionrend->setPOURCENTREND($pourcentrend);
        $repartitionrend->setDATEUPDATE(new \DateTime());
        $repartitionrend->setUSERUPDATE($user);


        $em->flush();

        var_dump($numrecolte);
        var_dump($pourcentrend);

        return $this->render('CultureBundle:RepartitionRendement:ajax.html.twig');
    }

    public function supprimerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->query->get('id');

        var_dump($id);

        $repartitionrend = $em->getRepository('CultureBundle:RepartitionRendement')->find($id);

        var_dump($repartitionrend);

        $em->remove($repartitionrend);
        $em->flush();

        return $this->render('CultureBundle:RepartitionRendement:ajax.html.twig');

    }
}
