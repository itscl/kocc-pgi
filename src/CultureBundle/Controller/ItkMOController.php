<?php

namespace CultureBundle\Controller;

use CultureBundle\Entity\Culture;
use CultureBundle\Entity\ITKMO;
use CultureBundle\Entity\ITKMOCV;
use CultureBundle\Entity\ITKMOG;
use CultureBundle\Entity\ITKMOS;
use CultureBundle\Entity\ItMO;
use CultureBundle\Form\CultureType;
use CultureBundle\Form\ItMOType;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ItkMOController extends Controller
{
    public function dupliqueritkmoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $itmoid = $request->request->get('itmoid');
        $newlibelle = $request->request->get('newlibelleitmo');
        $typeirrigation = $request->request->get('typeirrigation');

        var_dump($itmoid);
        var_dump($newlibelle);
        var_dump($typeirrigation);
        //exit;
        $itmo = $em->getRepository('CultureBundle:ItMO')->find($itmoid);

        $famillecultureid = $itmo->getFAMILLECULTUREID();

        //$itmolibelleexiste = $em->getRepository('CultureBundle:ItMO')->findBy(array('fAMILLECULTUREID' =>
        //$famillecultureid, 'lIBELLE' => strtoupper($newlibelle)));

        $itmotypeirriexiste = $em->getRepository('CultureBundle:ItMO')->findBy(array('fAMILLECULTUREID' => $famillecultureid, 'tYPEIRRIGATIONID' => $typeirrigation));

        if(!$itmotypeirriexiste)
        {

            $user = $this->getUser()->getName();

            $date = new \DateTime();
            $date = $date->format('d-m-y');

            $itkmo = $em->getRepository('CultureBundle:ITKMO')->findByiTMOID($itmoid);

            $entityitmo = new ItMO();


            $entityitmo->setFAMILLECULTUREID($famillecultureid);
            $entityitmo->setTYPEIRRIGATIONID($typeirrigation);
            $entityitmo->setLIBELLE($newlibelle);
            $entityitmo->setUSERUPDATE($user);
            $entityitmo->setDATEUPDATE(new \DateTime());

            $em->persist($entityitmo);
            $em->flush();

            foreach($itkmo as $rsitkmo)
            {
                $itkmoid = $rsitkmo->getId();
                $code = $rsitkmo->getCODE();
                $rang = $rsitkmo->getRANG();
                $libelle = $rsitkmo->getLIBELLE();

                $entityitkmo = new ITKMO();

                $entityitkmo->setITMOID($entityitmo->getId());
                $entityitkmo->setLIBELLE($libelle);
                $entityitkmo->setCODE($code);
                $entityitkmo->setFAMILLECULTUREID($famillecultureid);
                $entityitkmo->setRANG($rang);
                $entityitkmo->setUPDATEUSER($user);
                $entityitkmo->setUPDATEDATE(new \DateTime());

                $em->persist($entityitkmo);
                $em->flush();

                $itkmos = $em->getRepository('CultureBundle:ITKMOS')->findByiTKMOID($itkmoid);

                foreach($itkmos as $rsitkmos)
                {
                    $itkmosid = $rsitkmos->getId();
                    $periode = $rsitkmos->getPERIODE();

                    $entityitkmos = new ITKMOS();

                    $entityitkmos->setITKMOID($entityitkmo->getId());
                    $entityitkmos->setPERIODE($periode);

                    $em->persist($entityitkmos);
                    $em->flush();

                    $itkmog = $em->getRepository('CultureBundle:ITKMOG')->findByiTKMOSID($itkmosid);

                    foreach($itkmog as $rsitkmog)
                    {
                        $itkmogid = $rsitkmog->getId();
                        $gesteid = $rsitkmog->getGESTEID();

                        $entityitkmog = new ITKMOG();

                        $entityitkmog->setITKMOSID($entityitkmos->getId());
                        $entityitkmog->setGESTEID($gesteid);

                        $em->persist($entityitkmog);
                        $em->flush();

                        $itkmocv = $em->getRepository('CultureBundle:ITKMOCV')->findByiTKMOGID($itkmogid);

                        foreach($itkmocv as $rsitkmocv)
                        {
                            $itkmoscvid = $rsitkmocv->getId();
                            $quantite = $rsitkmocv->getQUANTITE();
                            $unite = $rsitkmocv->getUNITE();
                            $cultureid = $rsitkmocv->getCULTUREID();
                            $pourcentrend = $rsitkmocv->getREPARTITIONREDEMENTID();

                            $entityitkmocv = new ITKMOCV();

                            $entityitkmocv->setITKMOGID($entityitkmog->getId());
                            $entityitkmocv->setQUANTITE($quantite);
                            $entityitkmocv->setUNITE($unite);
                            $entityitkmocv->setCULTUREID($cultureid);
                            $entityitkmocv->setREPARTITIONREDEMENTID($pourcentrend);

                            $em->persist($entityitkmocv);
                            $em->flush();
                        }
                    }
                }
            }
        }
        else
        {
            $this->get('session')->getFlashBag()->add('notice1', "Le type d'irrigation existe déjà !");
        }

        return $this->render('CultureBundle:ItkMO:ajax.html.twig');
    }

    public function modifieritmoAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $famillecultureid = $request->query->get('famillecultureid');
            $itmoid = $request->query->get('itmoid');
            $libelle = $request->query->get('libelle');

            var_dump($itmoid);
            var_dump($libelle);
            //exit;

            $itmo = $em->getRepository('CultureBundle:ItMO')->find($itmoid);

            $itmo->setLIBELLE($libelle);

            $em->flush();

            return $this->render('CultureBundle:ItkMO:ajax.html.twig');
        }
    }

    public function supprimeritmoAction(Request $request)
    {
        $authorization = $this->get('security.authorization_checker');
        if (!$authorization->isGranted('IS_AUTHENTICATED_FULLY'))
        {

            return $this->redirect($this->generateUrl('fos_user_security_logout'));

        } else
        {
            $em = $this->getDoctrine()->getManager();

            $famillecultureid = $request->query->get('famillecultureid');
            $itmoid = $request->query->get('itmoid');

            $itmo = $em->getRepository('CultureBundle:ItMO')->find($itmoid);

            $em->remove($itmo);
            $em->flush();

            //var_dump($itkmos); exit;

            return $this->redirect($this->generateUrl('modifierfamilleculture', array('id' => $famillecultureid)));
        }
    }
}


