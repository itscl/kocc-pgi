<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RepartitionRendement
 */
class RepartitionRendement
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $nUMRECOLTE;

    /**
     * @var float
     */
    private $pOURCENTREND;

    /**
     * @var \DateTime
     */
    private $dATEUPDATE;

    /**
     * @var string
     */
    private $uSERUPDATE;

    /**
     * @var int
     */
    private $cULTUREID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nUMRECOLTE
     *
     * @param integer $nUMRECOLTE
     * @return RepartitionRendement
     */
    public function setNUMRECOLTE($nUMRECOLTE)
    {
        $this->nUMRECOLTE = $nUMRECOLTE;

        return $this;
    }

    /**
     * Get nUMRECOLTE
     *
     * @return integer 
     */
    public function getNUMRECOLTE()
    {
        return $this->nUMRECOLTE;
    }

    /**
     * Set pOURCENTREND
     *
     * @param float $pOURCENTREND
     * @return RepartitionRendement
     */
    public function setPOURCENTREND($pOURCENTREND)
    {
        $this->pOURCENTREND = $pOURCENTREND;

        return $this;
    }

    /**
     * Get pOURCENTREND
     *
     * @return float 
     */
    public function getPOURCENTREND()
    {
        return $this->pOURCENTREND;
    }

    /**
     * Set dATEUPDATE
     *
     * @param \DateTime $dATEUPDATE
     * @return RepartitionRendement
     */
    public function setDATEUPDATE($dATEUPDATE)
    {
        $this->dATEUPDATE = $dATEUPDATE;

        return $this;
    }

    /**
     * Get dATEUPDATE
     *
     * @return \DateTime 
     */
    public function getDATEUPDATE()
    {
        return $this->dATEUPDATE;
    }

    /**
     * Set uSERUPDATE
     *
     * @param string $uSERUPDATE
     * @return RepartitionRendement
     */
    public function setUSERUPDATE($uSERUPDATE)
    {
        $this->uSERUPDATE = $uSERUPDATE;

        return $this;
    }

    /**
     * Get uSERUPDATE
     *
     * @return string 
     */
    public function getUSERUPDATE()
    {
        return $this->uSERUPDATE;
    }

    /**
     * Set cULTUREID
     *
     * @param integer $cULTUREID
     * @return RepartitionRendement
     */
    public function setCULTUREID($cULTUREID)
    {
        $this->cULTUREID = $cULTUREID;

        return $this;
    }

    /**
     * Get cULTUREID
     *
     * @return integer
     */
    public function getCULTUREID()
    {
        return $this->cULTUREID;
    }
}
