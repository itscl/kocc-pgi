<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKCycle
 */
class ITKCycle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $fAMILLECULTUREID;

    /**
     * @var int
     */
    private $sEMAINESEMIS;

    /**
     * @var int
     */
    private $dUREECYCLE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var int
     */
    private $rDTBRUT;

    /**
     * @var int
     */
    private $pRDTNET;

    /**
     * @var int
     */
    private $iTPHYTOID;

    /**
     * @var int
     */
    private $iTFERTIID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fAMILLECULTUREID
     *
     * @param integer $fAMILLECULTUREID
     * @return ITKCycle
     */
    public function setFAMILLECULTUREID($fAMILLECULTUREID)
    {
        $this->fAMILLECULTUREID = $fAMILLECULTUREID;

        return $this;
    }

    /**
     * Get fAMILLECULTUREID
     *
     * @return integer 
     */
    public function getFAMILLECULTUREID()
    {
        return $this->fAMILLECULTUREID;
    }

    /**
     * Set sEMAINESEMIS
     *
     * @param integer $sEMAINESEMIS
     * @return ITKCycle
     */
    public function setSEMAINESEMIS($sEMAINESEMIS)
    {
        $this->sEMAINESEMIS = $sEMAINESEMIS;

        return $this;
    }

    /**
     * Get sEMAINESEMIS
     *
     * @return integer 
     */
    public function getSEMAINESEMIS()
    {
        return $this->sEMAINESEMIS;
    }

    /**
     * Set dUREECYCLE
     *
     * @param integer $dUREECYCLE
     * @return ITKCycle
     */
    public function setDUREECYCLE($dUREECYCLE)
    {
        $this->dUREECYCLE = $dUREECYCLE;

        return $this;
    }

    /**
     * Get dUREECYCLE
     *
     * @return integer 
     */
    public function getDUREECYCLE()
    {
        return $this->dUREECYCLE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ITKCycle
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ITKCycle
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKCycle
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ITKCycle
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set rDTBRUT
     *
     * @param integer $rDTBRUT
     * @return ITKCycle
     */
    public function setRDTBRUT($rDTBRUT)
    {
        $this->rDTBRUT = $rDTBRUT;

        return $this;
    }

    /**
     * Get rDTBRUT
     *
     * @return integer 
     */
    public function getRDTBRUT()
    {
        return $this->rDTBRUT;
    }

    /**
     * Set pRDTNET
     *
     * @param integer $pRDTNET
     * @return ITKCycle
     */
    public function setPRDTNET($pRDTNET)
    {
        $this->pRDTNET = $pRDTNET;

        return $this;
    }

    /**
     * Get pRDTNET
     *
     * @return integer 
     */
    public function getPRDTNET()
    {
        return $this->pRDTNET;
    }

    /**
     * Set iTPHYTOID
     *
     * @param integer $iTPHYTOID
     * @return ITKCycle
     */
    public function setITPHYTOID($iTPHYTOID)
    {
        $this->iTPHYTOID = $iTPHYTOID;

        return $this;
    }

    /**
     * Get iTPHYTOID
     *
     * @return integer 
     */
    public function getITPHYTOID()
    {
        return $this->iTPHYTOID;
    }

    /**
     * Set iTFERTIID
     *
     * @param integer $iTFERTIID
     * @return ITKCycle
     */
    public function setITFERTIID($iTFERTIID)
    {
        $this->iTFERTIID = $iTFERTIID;

        return $this;
    }

    /**
     * Get iTFERTIID
     *
     * @return integer 
     */
    public function getITFERTIID()
    {
        return $this->iTFERTIID;
    }
}
