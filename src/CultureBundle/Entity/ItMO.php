<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ItMO
 */
class ItMO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $fAMILLECULTUREID;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var int
     */
    private $tYPEIRRIGATIONID;

    /**
     * @var string
     */
    private $uSERUPDATE;

    /**
     * @var \DateTime
     */
    private $dATEUPDATE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fAMILLECULTUREID
     *
     * @param integer $fAMILLECULTUREID
     * @return ItMO
     */
    public function setFAMILLECULTUREID($fAMILLECULTUREID)
    {
        $this->fAMILLECULTUREID = $fAMILLECULTUREID;

        return $this;
    }

    /**
     * Get fAMILLECULTUREID
     *
     * @return integer 
     */
    public function getFAMILLECULTUREID()
    {
        return $this->fAMILLECULTUREID;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return ItMO
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set tYPEIRRIGATIONID
     *
     * @param integer $tYPEIRRIGATIONID
     * @return ItMO
     */
    public function setTYPEIRRIGATIONID($tYPEIRRIGATIONID)
    {
        $this->tYPEIRRIGATIONID = $tYPEIRRIGATIONID;

        return $this;
    }

    /**
     * Get tYPEIRRIGATIONID
     *
     * @return integer 
     */
    public function getTYPEIRRIGATIONID()
    {
        return $this->tYPEIRRIGATIONID;
    }

    /**
     * Set uSERUPDATE
     *
     * @param string $uSERUPDATE
     * @return ItMO
     */
    public function setUSERUPDATE($uSERUPDATE)
    {
        $this->uSERUPDATE = $uSERUPDATE;

        return $this;
    }

    /**
     * Get uSERUPDATE
     *
     * @return string 
     */
    public function getUSERUPDATE()
    {
        return $this->uSERUPDATE;
    }

    /**
     * Set dATEUPDATE
     *
     * @param \DateTime $dATEUPDATE
     * @return ItMO
     */
    public function setDATEUPDATE($dATEUPDATE)
    {
        $this->dATEUPDATE = $dATEUPDATE;

        return $this;
    }

    /**
     * Get dATEUPDATE
     *
     * @return \DateTime 
     */
    public function getDATEUPDATE()
    {
        return $this->dATEUPDATE;
    }
}
