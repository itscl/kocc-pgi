<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CultureVariete
 */
class CultureVariete
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $pRIXVENTE;

    /**
     * @var string
     */
    private $uNITEVENTE;

    /**
     * @var string
     */
    private $lIBELLEUP;

    /**
     * @var string
     */
    private $eXTRACTIONUP;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var int
     */
    private $eCARTEMENTLIGNE;

    /**
     * @var int
     */
    private $eCARTEMENTSEMIS;

    /**
     * @var int
     */
    private $dENSITESEMIS;

    /**
     * @var int
     */
    private $pOSCOLOR;

    /**
     * @var string
     */
    private $pOSEXTRACTION;

    /**
     * @var string
     */
    private $cODEARTICLE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return CultureVariete
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string 
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return CultureVariete
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return CultureVariete
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return CultureVariete
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return CultureVariete
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set pRIXVENTE
     *
     * @param integer $pRIXVENTE
     * @return CultureVariete
     */
    public function setPRIXVENTE($pRIXVENTE)
    {
        $this->pRIXVENTE = $pRIXVENTE;

        return $this;
    }

    /**
     * Get pRIXVENTE
     *
     * @return integer 
     */
    public function getPRIXVENTE()
    {
        return $this->pRIXVENTE;
    }

    /**
     * Set uNITEVENTE
     *
     * @param string $uNITEVENTE
     * @return CultureVariete
     */
    public function setUNITEVENTE($uNITEVENTE)
    {
        $this->uNITEVENTE = $uNITEVENTE;

        return $this;
    }

    /**
     * Get uNITEVENTE
     *
     * @return string 
     */
    public function getUNITEVENTE()
    {
        return $this->uNITEVENTE;
    }

    /**
     * Set lIBELLEUP
     *
     * @param string $lIBELLEUP
     * @return CultureVariete
     */
    public function setLIBELLEUP($lIBELLEUP)
    {
        $this->lIBELLEUP = $lIBELLEUP;

        return $this;
    }

    /**
     * Get lIBELLEUP
     *
     * @return string 
     */
    public function getLIBELLEUP()
    {
        return $this->lIBELLEUP;
    }

    /**
     * Set eXTRACTIONUP
     *
     * @param string $eXTRACTIONUP
     * @return CultureVariete
     */
    public function setEXTRACTIONUP($eXTRACTIONUP)
    {
        $this->eXTRACTIONUP = $eXTRACTIONUP;

        return $this;
    }

    /**
     * Get eXTRACTIONUP
     *
     * @return string 
     */
    public function getEXTRACTIONUP()
    {
        return $this->eXTRACTIONUP;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return CultureVariete
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set eCARTEMENTLIGNE
     *
     * @param integer $eCARTEMENTLIGNE
     * @return CultureVariete
     */
    public function setECARTEMENTLIGNE($eCARTEMENTLIGNE)
    {
        $this->eCARTEMENTLIGNE = $eCARTEMENTLIGNE;

        return $this;
    }

    /**
     * Get eCARTEMENTLIGNE
     *
     * @return integer 
     */
    public function getECARTEMENTLIGNE()
    {
        return $this->eCARTEMENTLIGNE;
    }

    /**
     * Set eCARTEMENTSEMIS
     *
     * @param integer $eCARTEMENTSEMIS
     * @return CultureVariete
     */
    public function setECARTEMENTSEMIS($eCARTEMENTSEMIS)
    {
        $this->eCARTEMENTSEMIS = $eCARTEMENTSEMIS;

        return $this;
    }

    /**
     * Get eCARTEMENTSEMIS
     *
     * @return integer 
     */
    public function getECARTEMENTSEMIS()
    {
        return $this->eCARTEMENTSEMIS;
    }

    /**
     * Set dENSITESEMIS
     *
     * @param integer $dENSITESEMIS
     * @return CultureVariete
     */
    public function setDENSITESEMIS($dENSITESEMIS)
    {
        $this->dENSITESEMIS = $dENSITESEMIS;

        return $this;
    }

    /**
     * Get dENSITESEMIS
     *
     * @return integer 
     */
    public function getDENSITESEMIS()
    {
        return $this->dENSITESEMIS;
    }

    /**
     * Set pOSCOLOR
     *
     * @param integer $pOSCOLOR
     * @return CultureVariete
     */
    public function setPOSCOLOR($pOSCOLOR)
    {
        $this->pOSCOLOR = $pOSCOLOR;

        return $this;
    }

    /**
     * Get pOSCOLOR
     *
     * @return integer 
     */
    public function getPOSCOLOR()
    {
        return $this->pOSCOLOR;
    }

    /**
     * Set pOSEXTRACTION
     *
     * @param string $pOSEXTRACTION
     * @return CultureVariete
     */
    public function setPOSEXTRACTION($pOSEXTRACTION)
    {
        $this->pOSEXTRACTION = $pOSEXTRACTION;

        return $this;
    }

    /**
     * Get pOSEXTRACTION
     *
     * @return string 
     */
    public function getPOSEXTRACTION()
    {
        return $this->pOSEXTRACTION;
    }

    /**
     * Set cODEARTICLE
     *
     * @param string $cODEARTICLE
     * @return CultureVariete
     */
    public function setCODEARTICLE($cODEARTICLE)
    {
        $this->cODEARTICLE = $cODEARTICLE;

        return $this;
    }

    /**
     * Get cODEARTICLE
     *
     * @return string 
     */
    public function getCODEARTICLE()
    {
        return $this->cODEARTICLE;
    }
}
