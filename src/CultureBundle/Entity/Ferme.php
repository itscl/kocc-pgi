<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ferme
 */
class Ferme
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var int
     */
    private $sITEID;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var string
     */
    private $jOURSEMAINEPOINTAGE;

    /**
     * @var int
     */
    private $cHEFFERMEID;

    /**
     * @var int
     */
    private $pRESCRIPTEURID;

    /**
     * @var int
     */
    private $rESPONSAPPLIID;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return Ferme
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return Ferme
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set sITEID
     *
     * @param integer $sITEID
     * @return Ferme
     */
    public function setSITEID($sITEID)
    {
        $this->sITEID = $sITEID;

        return $this;
    }

    /**
     * Get sITEID
     *
     * @return integer
     */
    public function getSITEID()
    {
        return $this->sITEID;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return Ferme
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return Ferme
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return Ferme
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return Ferme
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set jOURSEMAINEPOINTAGE
     *
     * @param string $jOURSEMAINEPOINTAGE
     * @return Ferme
     */
    public function setJOURSEMAINEPOINTAGE($jOURSEMAINEPOINTAGE)
    {
        $this->jOURSEMAINEPOINTAGE = $jOURSEMAINEPOINTAGE;

        return $this;
    }

    /**
     * Get jOURSEMAINEPOINTAGE
     *
     * @return string
     */
    public function getJOURSEMAINEPOINTAGE()
    {
        return $this->jOURSEMAINEPOINTAGE;
    }

    /**
     * Set cHEFFERMEID
     *
     * @param integer $cHEFFERMEID
     * @return Ferme
     */
    public function setCHEFFERMEID($cHEFFERMEID)
    {
        $this->cHEFFERMEID = $cHEFFERMEID;

        return $this;
    }

    /**
     * Get cHEFFERMEID
     *
     * @return integer
     */
    public function getCHEFFERMEID()
    {
        return $this->cHEFFERMEID;
    }

    /**
     * Set pRESCRIPTEURID
     *
     * @param integer $pRESCRIPTEURID
     * @return Ferme
     */
    public function setPRESCRIPTEURID($pRESCRIPTEURID)
    {
        $this->pRESCRIPTEURID = $pRESCRIPTEURID;

        return $this;
    }

    /**
     * Get pRESCRIPTEURID
     *
     * @return integer
     */
    public function getPRESCRIPTEURID()
    {
        return $this->pRESCRIPTEURID;
    }

    /**
     * Set rESPONSAPPLIID
     *
     * @param integer $rESPONSAPPLIID
     * @return Ferme
     */
    public function setRESPONSAPPLIID($rESPONSAPPLIID)
    {
        $this->rESPONSAPPLIID = $rESPONSAPPLIID;

        return $this;
    }

    /**
     * Get rESPONSAPPLIID
     *
     * @return integer
     */
    public function getRESPONSAPPLIID()
    {
        return $this->rESPONSAPPLIID;
    }
}
