<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CycleCultural
 */
class CycleCultural
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $iLOTID;

    /**
     * @var int
     */
    private $cYCLENO;

    /**
     * @var int
     */
    private $cULTUREID;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var \DateTime
     */
    private $dATEDEBUTSEMIS;

    /**
     * @var \DateTime
     */
    private $dATEFINSEMIS;

    /**
     * @var \DateTime
     */
    private $dATEDEBUTRECOLTE;

    /**
     * @var \DateTime
     */
    private $dATEFINRECOLTE;

    /**
     * @var string
     */
    private $sTATUS;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var string
     */
    private $pOIDSCOEF;

    /**
     * @var \DateTime
     */
    private $dATEDERNIERERECOLTE;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var \DateTime
     */
    private $dATEFINCYCLE;

    /**
     * @var string
     */
    private $tYPEIMPLANTATION;

    /**
     * @var int
     */
    private $tYPEIRRIGATIONID;

    /**
     * @var int
     */
    private $eCARTEMENTLIGNE;

    /**
     * @var int
     */
    private $eCARTEMENTSEMIS;

    /**
     * @var int
     */
    private $dENSITESEMIS;

    /**
     * @var \DateTime
     */
    private $dATEPREVISRECOLTE;

    /**
     * @var int
     */
    private $dUREECYCLE;

    /**
     * @var \DateTime
     */
    private $dATEGENEREITK;

    /**
     * @var string
     */
    private $cOMMENTAIREITK;

    /**
     * @var string
     */
    private $iTKTRAITEMENT;

    /**
     * @var \DateTime
     */
    private $dATEPREVISRECOLTEA;

    /**
     * @var string
     */
    private $iTKAASEM;

    /**
     * @var string
     */
    private $dATEPREVISRECOLTEACOMM;

    /**
     * @var string
     */
    private $tYPEOP;

    /**
     * @var string
     */
    private $lOTSEMENCENO;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iLOTID
     *
     * @param integer $iLOTID
     * @return CycleCultural
     */
    public function setILOTID($iLOTID)
    {
        $this->iLOTID = $iLOTID;

        return $this;
    }

    /**
     * Get iLOTID
     *
     * @return integer
     */
    public function getILOTID()
    {
        return $this->iLOTID;
    }

    /**
     * Set cYCLENO
     *
     * @param integer $cYCLENO
     * @return CycleCultural
     */
    public function setCYCLENO($cYCLENO)
    {
        $this->cYCLENO = $cYCLENO;

        return $this;
    }

    /**
     * Get cYCLENO
     *
     * @return integer
     */
    public function getCYCLENO()
    {
        return $this->cYCLENO;
    }

    /**
     * Set cULTUREID
     *
     * @param integer $cULTUREID
     * @return CycleCultural
     */
    public function setCULTUREID($cULTUREID)
    {
        $this->cULTUREID = $cULTUREID;

        return $this;
    }

    /**
     * Get cULTUREID
     *
     * @return integer
     */
    public function getCULTUREID()
    {
        return $this->cULTUREID;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return CycleCultural
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set dATEDEBUTSEMIS
     *
     * @param \DateTime $dATEDEBUTSEMIS
     * @return CycleCultural
     */
    public function setDATEDEBUTSEMIS($dATEDEBUTSEMIS)
    {
        $this->dATEDEBUTSEMIS = $dATEDEBUTSEMIS;

        return $this;
    }

    /**
     * Get dATEDEBUTSEMIS
     *
     * @return \DateTime
     */
    public function getDATEDEBUTSEMIS()
    {
        return $this->dATEDEBUTSEMIS;
    }

    /**
     * Set dATEFINSEMIS
     *
     * @param \DateTime $dATEFINSEMIS
     * @return CycleCultural
     */
    public function setDATEFINSEMIS($dATEFINSEMIS)
    {
        $this->dATEFINSEMIS = $dATEFINSEMIS;

        return $this;
    }

    /**
     * Get dATEFINSEMIS
     *
     * @return \DateTime
     */
    public function getDATEFINSEMIS()
    {
        return $this->dATEFINSEMIS;
    }

    /**
     * Set dATEDEBUTRECOLTE
     *
     * @param \DateTime $dATEDEBUTRECOLTE
     * @return CycleCultural
     */
    public function setDATEDEBUTRECOLTE($dATEDEBUTRECOLTE)
    {
        $this->dATEDEBUTRECOLTE = $dATEDEBUTRECOLTE;

        return $this;
    }

    /**
     * Get dATEDEBUTRECOLTE
     *
     * @return \DateTime
     */
    public function getDATEDEBUTRECOLTE()
    {
        return $this->dATEDEBUTRECOLTE;
    }

    /**
     * Set dATEFINRECOLTE
     *
     * @param \DateTime $dATEFINRECOLTE
     * @return CycleCultural
     */
    public function setDATEFINRECOLTE($dATEFINRECOLTE)
    {
        $this->dATEFINRECOLTE = $dATEFINRECOLTE;

        return $this;
    }

    /**
     * Get dATEFINRECOLTE
     *
     * @return \DateTime
     */
    public function getDATEFINRECOLTE()
    {
        return $this->dATEFINRECOLTE;
    }

    /**
     * Set sTATUS
     *
     * @param string $sTATUS
     * @return CycleCultural
     */
    public function setSTATUS($sTATUS)
    {
        $this->sTATUS = $sTATUS;

        return $this;
    }

    /**
     * Get sTATUS
     *
     * @return string
     */
    public function getSTATUS()
    {
        return $this->sTATUS;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return CycleCultural
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return CycleCultural
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set pOIDSCOEF
     *
     * @param string $pOIDSCOEF
     * @return CycleCultural
     */
    public function setPOIDSCOEF($pOIDSCOEF)
    {
        $this->pOIDSCOEF = $pOIDSCOEF;

        return $this;
    }

    /**
     * Get pOIDSCOEF
     *
     * @return string
     */
    public function getPOIDSCOEF()
    {
        return $this->pOIDSCOEF;
    }

    /**
     * Set dATEDERNIERERECOLTE
     *
     * @param \DateTime $dATEDERNIERERECOLTE
     * @return CycleCultural
     */
    public function setDATEDERNIERERECOLTE($dATEDERNIERERECOLTE)
    {
        $this->dATEDERNIERERECOLTE = $dATEDERNIERERECOLTE;

        return $this;
    }

    /**
     * Get dATEDERNIERERECOLTE
     *
     * @return \DateTime
     */
    public function getDATEDERNIERERECOLTE()
    {
        return $this->dATEDERNIERERECOLTE;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return CycleCultural
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set dATEFINCYCLE
     *
     * @param \DateTime $dATEFINCYCLE
     * @return CycleCultural
     */
    public function setDATEFINCYCLE($dATEFINCYCLE)
    {
        $this->dATEFINCYCLE = $dATEFINCYCLE;

        return $this;
    }

    /**
     * Get dATEFINCYCLE
     *
     * @return \DateTime
     */
    public function getDATEFINCYCLE()
    {
        return $this->dATEFINCYCLE;
    }

    /**
     * Set tYPEIMPLANTATION
     *
     * @param string $tYPEIMPLANTATION
     * @return CycleCultural
     */
    public function setTYPEIMPLANTATION($tYPEIMPLANTATION)
    {
        $this->tYPEIMPLANTATION = $tYPEIMPLANTATION;

        return $this;
    }

    /**
     * Get tYPEIMPLANTATION
     *
     * @return string
     */
    public function getTYPEIMPLANTATION()
    {
        return $this->tYPEIMPLANTATION;
    }

    /**
     * Set tYPEIRRIGATIONID
     *
     * @param integer $tYPEIRRIGATIONID
     * @return CycleCultural
     */
    public function setTYPEIRRIGATIONID($tYPEIRRIGATIONID)
    {
        $this->tYPEIRRIGATIONID = $tYPEIRRIGATIONID;

        return $this;
    }

    /**
     * Get tYPEIRRIGATIONID
     *
     * @return integer
     */
    public function getTYPEIRRIGATIONID()
    {
        return $this->tYPEIRRIGATIONID;
    }

    /**
     * Set eCARTEMENTLIGNE
     *
     * @param integer $eCARTEMENTLIGNE
     * @return CycleCultural
     */
    public function setECARTEMENTLIGNE($eCARTEMENTLIGNE)
    {
        $this->eCARTEMENTLIGNE = $eCARTEMENTLIGNE;

        return $this;
    }

    /**
     * Get eCARTEMENTLIGNE
     *
     * @return integer
     */
    public function getECARTEMENTLIGNE()
    {
        return $this->eCARTEMENTLIGNE;
    }

    /**
     * Set eCARTEMENTSEMIS
     *
     * @param integer $eCARTEMENTSEMIS
     * @return CycleCultural
     */
    public function setECARTEMENTSEMIS($eCARTEMENTSEMIS)
    {
        $this->eCARTEMENTSEMIS = $eCARTEMENTSEMIS;

        return $this;
    }

    /**
     * Get eCARTEMENTSEMIS
     *
     * @return integer
     */
    public function getECARTEMENTSEMIS()
    {
        return $this->eCARTEMENTSEMIS;
    }

    /**
     * Set dENSITESEMIS
     *
     * @param integer $dENSITESEMIS
     * @return CycleCultural
     */
    public function setDENSITESEMIS($dENSITESEMIS)
    {
        $this->dENSITESEMIS = $dENSITESEMIS;

        return $this;
    }

    /**
     * Get dENSITESEMIS
     *
     * @return integer
     */
    public function getDENSITESEMIS()
    {
        return $this->dENSITESEMIS;
    }

    /**
     * Set dATEPREVISRECOLTE
     *
     * @param \DateTime $dATEPREVISRECOLTE
     * @return CycleCultural
     */
    public function setDATEPREVISRECOLTE($dATEPREVISRECOLTE)
    {
        $this->dATEPREVISRECOLTE = $dATEPREVISRECOLTE;

        return $this;
    }

    /**
     * Get dATEPREVISRECOLTE
     *
     * @return \DateTime
     */
    public function getDATEPREVISRECOLTE()
    {
        return $this->dATEPREVISRECOLTE;
    }

    /**
     * Set dUREECYCLE
     *
     * @param integer $dUREECYCLE
     * @return CycleCultural
     */
    public function setDUREECYCLE($dUREECYCLE)
    {
        $this->dUREECYCLE = $dUREECYCLE;

        return $this;
    }

    /**
     * Get dUREECYCLE
     *
     * @return integer
     */
    public function getDUREECYCLE()
    {
        return $this->dUREECYCLE;
    }

    /**
     * Set dATEGENEREITK
     *
     * @param \DateTime $dATEGENEREITK
     * @return CycleCultural
     */
    public function setDATEGENEREITK($dATEGENEREITK)
    {
        $this->dATEGENEREITK = $dATEGENEREITK;

        return $this;
    }

    /**
     * Get dATEGENEREITK
     *
     * @return \DateTime
     */
    public function getDATEGENEREITK()
    {
        return $this->dATEGENEREITK;
    }

    /**
     * Set cOMMENTAIREITK
     *
     * @param string $cOMMENTAIREITK
     * @return CycleCultural
     */
    public function setCOMMENTAIREITK($cOMMENTAIREITK)
    {
        $this->cOMMENTAIREITK = $cOMMENTAIREITK;

        return $this;
    }

    /**
     * Get cOMMENTAIREITK
     *
     * @return string
     */
    public function getCOMMENTAIREITK()
    {
        return $this->cOMMENTAIREITK;
    }

    /**
     * Set iTKTRAITEMENT
     *
     * @param string $iTKTRAITEMENT
     * @return CycleCultural
     */
    public function setITKTRAITEMENT($iTKTRAITEMENT)
    {
        $this->iTKTRAITEMENT = $iTKTRAITEMENT;

        return $this;
    }

    /**
     * Get iTKTRAITEMENT
     *
     * @return string
     */
    public function getITKTRAITEMENT()
    {
        return $this->iTKTRAITEMENT;
    }

    /**
     * Set dATEPREVISRECOLTEA
     *
     * @param \DateTime $dATEPREVISRECOLTEA
     * @return CycleCultural
     */
    public function setDATEPREVISRECOLTEA($dATEPREVISRECOLTEA)
    {
        $this->dATEPREVISRECOLTEA = $dATEPREVISRECOLTEA;

        return $this;
    }

    /**
     * Get dATEPREVISRECOLTEA
     *
     * @return \DateTime
     */
    public function getDATEPREVISRECOLTEA()
    {
        return $this->dATEPREVISRECOLTEA;
    }

    /**
     * Set iTKAASEM
     *
     * @param string $iTKAASEM
     * @return CycleCultural
     */
    public function setITKAASEM($iTKAASEM)
    {
        $this->iTKAASEM = $iTKAASEM;

        return $this;
    }

    /**
     * Get iTKAASEM
     *
     * @return string
     */
    public function getITKAASEM()
    {
        return $this->iTKAASEM;
    }

    /**
     * Set dATEPREVISRECOLTEACOMM
     *
     * @param string $dATEPREVISRECOLTEACOMM
     * @return CycleCultural
     */
    public function setDATEPREVISRECOLTEACOMM($dATEPREVISRECOLTEACOMM)
    {
        $this->dATEPREVISRECOLTEACOMM = $dATEPREVISRECOLTEACOMM;

        return $this;
    }

    /**
     * Get dATEPREVISRECOLTEACOMM
     *
     * @return string
     */
    public function getDATEPREVISRECOLTEACOMM()
    {
        return $this->dATEPREVISRECOLTEACOMM;
    }

    /**
     * Set tYPEOP
     *
     * @param string $tYPEOP
     * @return CycleCultural
     */
    public function setTYPEOP($tYPEOP)
    {
        $this->tYPEOP = $tYPEOP;

        return $this;
    }

    /**
     * Get tYPEOP
     *
     * @return string
     */
    public function getTYPEOP()
    {
        return $this->tYPEOP;
    }

    /**
     * Set lOTSEMENCENO
     *
     * @param string $lOTSEMENCENO
     * @return CycleCultural
     */
    public function setLOTSEMENCENO($lOTSEMENCENO)
    {
        $this->lOTSEMENCENO = $lOTSEMENCENO;

        return $this;
    }

    /**
     * Get lOTSEMENCENO
     *
     * @return string
     */
    public function getLOTSEMENCENO()
    {
        return $this->lOTSEMENCENO;
    }
}
