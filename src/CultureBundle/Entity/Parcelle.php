<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parcelle
 */
class Parcelle
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $fERMEID;

    /**
     * @var int
     */
    private $tYPEIRRIGATIONID;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $sURFACE;

    /**
     * @var string
     */
    private $sURFACEBRUT;

    /**
     * @var string
     */
    private $tYPEDESOL;

    /**
     * @var string
     */
    private $lONGITUDE;

    /**
     * @var string
     */
    private $lATITUDE;

    /**
     * @var string
     */
    private $rANG;

    /**
     * @var string
     */
    private $tILT;

    /**
     * @var string
     */
    private $hEADING;

    /**
     * @var string
     */
    private $iCONE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fERMEID
     *
     * @param integer $fERMEID
     * @return Parcelle
     */
    public function setFERMEID($fERMEID)
    {
        $this->fERMEID = $fERMEID;

        return $this;
    }

    /**
     * Get fERMEID
     *
     * @return integer 
     */
    public function getFERMEID()
    {
        return $this->fERMEID;
    }

    /**
     * Set tYPEIRRIGATIONID
     *
     * @param integer $tYPEIRRIGATIONID
     * @return Parcelle
     */
    public function setTYPEIRRIGATIONID($tYPEIRRIGATIONID)
    {
        $this->tYPEIRRIGATIONID = $tYPEIRRIGATIONID;

        return $this;
    }

    /**
     * Get tYPEIRRIGATIONID
     *
     * @return integer 
     */
    public function getTYPEIRRIGATIONID()
    {
        return $this->tYPEIRRIGATIONID;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return Parcelle
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set sURFACE
     *
     * @param string $sURFACE
     * @return Parcelle
     */
    public function setSURFACE($sURFACE)
    {
        $this->sURFACE = $sURFACE;

        return $this;
    }

    /**
     * Get sURFACE
     *
     * @return string 
     */
    public function getSURFACE()
    {
        return $this->sURFACE;
    }

    /**
     * Set sURFACEBRUT
     *
     * @param string $sURFACEBRUT
     * @return Parcelle
     */
    public function setSURFACEBRUT($sURFACEBRUT)
    {
        $this->sURFACEBRUT = $sURFACEBRUT;

        return $this;
    }

    /**
     * Get sURFACEBRUT
     *
     * @return string 
     */
    public function getSURFACEBRUT()
    {
        return $this->sURFACEBRUT;
    }

    /**
     * Set tYPEDESOL
     *
     * @param string $tYPEDESOL
     * @return Parcelle
     */
    public function setTYPEDESOL($tYPEDESOL)
    {
        $this->tYPEDESOL = $tYPEDESOL;

        return $this;
    }

    /**
     * Get tYPEDESOL
     *
     * @return string 
     */
    public function getTYPEDESOL()
    {
        return $this->tYPEDESOL;
    }

    /**
     * Set lONGITUDE
     *
     * @param string $lONGITUDE
     * @return Parcelle
     */
    public function setLONGITUDE($lONGITUDE)
    {
        $this->lONGITUDE = $lONGITUDE;

        return $this;
    }

    /**
     * Get lONGITUDE
     *
     * @return string 
     */
    public function getLONGITUDE()
    {
        return $this->lONGITUDE;
    }

    /**
     * Set lATITUDE
     *
     * @param string $lATITUDE
     * @return Parcelle
     */
    public function setLATITUDE($lATITUDE)
    {
        $this->lATITUDE = $lATITUDE;

        return $this;
    }

    /**
     * Get lATITUDE
     *
     * @return string 
     */
    public function getLATITUDE()
    {
        return $this->lATITUDE;
    }

    /**
     * Set rANG
     *
     * @param string $rANG
     * @return Parcelle
     */
    public function setRANG($rANG)
    {
        $this->rANG = $rANG;

        return $this;
    }

    /**
     * Get rANG
     *
     * @return string 
     */
    public function getRANG()
    {
        return $this->rANG;
    }

    /**
     * Set tILT
     *
     * @param string $tILT
     * @return Parcelle
     */
    public function setTILT($tILT)
    {
        $this->tILT = $tILT;

        return $this;
    }

    /**
     * Get tILT
     *
     * @return string 
     */
    public function getTILT()
    {
        return $this->tILT;
    }

    /**
     * Set hEADING
     *
     * @param string $hEADING
     * @return Parcelle
     */
    public function setHEADING($hEADING)
    {
        $this->hEADING = $hEADING;

        return $this;
    }

    /**
     * Get hEADING
     *
     * @return string 
     */
    public function getHEADING()
    {
        return $this->hEADING;
    }

    /**
     * Set iCONE
     *
     * @param string $iCONE
     * @return Parcelle
     */
    public function setICONE($iCONE)
    {
        $this->iCONE = $iCONE;

        return $this;
    }

    /**
     * Get iCONE
     *
     * @return string 
     */
    public function getICONE()
    {
        return $this->iCONE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return Parcelle
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return Parcelle
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return Parcelle
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return Parcelle
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }
}
