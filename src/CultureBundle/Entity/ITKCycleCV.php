<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKCycleCV
 */
class ITKCycleCV
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $sEMAINERECOLTE;

    /**
     * @var float
     */
    private $rDTBRUT;

    /**
     * @var int
     */
    private $cULTUREID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sEMAINERECOLTE
     *
     * @param integer $sEMAINERECOLTE
     * @return ITKCycleCV
     */
    public function setSEMAINERECOLTE($sEMAINERECOLTE)
    {
        $this->sEMAINERECOLTE = $sEMAINERECOLTE;

        return $this;
    }

    /**
     * Get sEMAINERECOLTE
     *
     * @return integer 
     */
    public function getSEMAINERECOLTE()
    {
        return $this->sEMAINERECOLTE;
    }

    /**
     * Set rDTBRUT
     *
     * @param float $rDTBRUT
     * @return ITKCycleCV
     */
    public function setRDTBRUT($rDTBRUT)
    {
        $this->rDTBRUT = $rDTBRUT;

        return $this;
    }

    /**
     * Get rDTBRUT
     *
     * @return float 
     */
    public function getRDTBRUT()
    {
        return $this->rDTBRUT;
    }

    /**
     * Set cULTUREID
     *
     * @param integer $cULTUREID
     * @return ITKCycleCV
     */
    public function setCULTUREID($cULTUREID)
    {
        $this->cULTUREID = $cULTUREID;

        return $this;
    }

    /**
     * Get cULTUREID
     *
     * @return integer 
     */
    public function getCULTUREID()
    {
        return $this->cULTUREID;
    }
}
