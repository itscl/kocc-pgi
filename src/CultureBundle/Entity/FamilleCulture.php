<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FamilleCulture
 */
class FamilleCulture
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var string
     */
    private $dATEINVALIDE;

    /**
     * @var \DateTime
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var string
     */
    private $dUREECYCLEMAX;

    /**
     * @var int
     */
    private $eCARTEMENTLIGNE;

    /**
     * @var int
     */
    private $eCARTEMENTSEMIS;

    /**
     * @var int
     */
    private $nBREPETITIONOAP;

    /**
     * @var string
     */
    private $cRITEREOAP;

    /**
     * @var string
     */
    private $cAPACITERECOLTE;

    /**
     * @var int
     */
    private $pMARCHEL;

    /**
     * @var int
     */
    private $cAPACITETC;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return FamilleCulture
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return FamilleCulture
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string 
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return FamilleCulture
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return FamilleCulture
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return FamilleCulture
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return FamilleCulture
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set dUREECYCLEMAX
     *
     * @param string $dUREECYCLEMAX
     * @return FamilleCulture
     */
    public function setDUREECYCLEMAX($dUREECYCLEMAX)
    {
        $this->dUREECYCLEMAX = $dUREECYCLEMAX;

        return $this;
    }

    /**
     * Get dUREECYCLEMAX
     *
     * @return string 
     */
    public function getDUREECYCLEMAX()
    {
        return $this->dUREECYCLEMAX;
    }

    /**
     * Set eCARTEMENTLIGNE
     *
     * @param integer $eCARTEMENTLIGNE
     * @return FamilleCulture
     */
    public function setECARTEMENTLIGNE($eCARTEMENTLIGNE)
    {
        $this->eCARTEMENTLIGNE = $eCARTEMENTLIGNE;

        return $this;
    }

    /**
     * Get eCARTEMENTLIGNE
     *
     * @return integer 
     */
    public function getECARTEMENTLIGNE()
    {
        return $this->eCARTEMENTLIGNE;
    }

    /**
     * Set eCARTEMENTSEMIS
     *
     * @param integer $eCARTEMENTSEMIS
     * @return FamilleCulture
     */
    public function setECARTEMENTSEMIS($eCARTEMENTSEMIS)
    {
        $this->eCARTEMENTSEMIS = $eCARTEMENTSEMIS;

        return $this;
    }

    /**
     * Get eCARTEMENTSEMIS
     *
     * @return integer 
     */
    public function getECARTEMENTSEMIS()
    {
        return $this->eCARTEMENTSEMIS;
    }

    /**
     * Set nBREPETITIONOAP
     *
     * @param integer $nBREPETITIONOAP
     * @return FamilleCulture
     */
    public function setNBREPETITIONOAP($nBREPETITIONOAP)
    {
        $this->nBREPETITIONOAP = $nBREPETITIONOAP;

        return $this;
    }

    /**
     * Get nBREPETITIONOAP
     *
     * @return integer 
     */
    public function getNBREPETITIONOAP()
    {
        return $this->nBREPETITIONOAP;
    }

    /**
     * Set cRITEREOAP
     *
     * @param string $cRITEREOAP
     * @return FamilleCulture
     */
    public function setCRITEREOAP($cRITEREOAP)
    {
        $this->cRITEREOAP = $cRITEREOAP;

        return $this;
    }

    /**
     * Get cRITEREOAP
     *
     * @return string 
     */
    public function getCRITEREOAP()
    {
        return $this->cRITEREOAP;
    }

    /**
     * Set cAPACITERECOLTE
     *
     * @param string $cAPACITERECOLTE
     * @return FamilleCulture
     */
    public function setCAPACITERECOLTE($cAPACITERECOLTE)
    {
        $this->cAPACITERECOLTE = $cAPACITERECOLTE;

        return $this;
    }

    /**
     * Get cAPACITERECOLTE
     *
     * @return string 
     */
    public function getCAPACITERECOLTE()
    {
        return $this->cAPACITERECOLTE;
    }

    /**
     * Set pMARCHEL
     *
     * @param integer $pMARCHEL
     * @return FamilleCulture
     */
    public function setPMARCHEL($pMARCHEL)
    {
        $this->pMARCHEL = $pMARCHEL;

        return $this;
    }

    /**
     * Get pMARCHEL
     *
     * @return integer 
     */
    public function getPMARCHEL()
    {
        return $this->pMARCHEL;
    }

    /**
     * Set cAPACITETC
     *
     * @param integer $cAPACITETC
     * @return FamilleCulture
     */
    public function setCAPACITETC($cAPACITETC)
    {
        $this->cAPACITETC = $cAPACITETC;

        return $this;
    }

    /**
     * Get cAPACITETC
     *
     * @return integer 
     */
    public function getCAPACITETC()
    {
        return $this->cAPACITETC;
    }
}
