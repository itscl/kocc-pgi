<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Culture
 */
class Culture
{
    /**
     * @var string
     */
    private $cODEARTICLE;

    /**
     * @var string
     */
    private $cODEVARIETE;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var integer
     */
    private $dENSITESEMIS;

    /**
     * @var integer
     */
    private $eCARTEMENTLIGNE;

    /**
     * @var integer
     */
    private $eCARTEMENTSEMIS;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var integer
     */
    private $oRIGINSITEID;

    /**
     * @var string
     */
    private $pHOTO;

    /**
     * @var integer
     */
    private $sAISIEPOIDSCOEF;

    /**
     * @var integer
     */
    private $sTOCKHUMIDITE;

    /**
     * @var integer
     */
    private $sTOCKNBJ_CONSERVATION;

    /**
     * @var integer
     */
    private $sTOCKSECURITE;

    /**
     * @var integer
     */
    private $sTOCKTEMPERATURE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $fAMILLECULTUREID;

    /**
     * @var integer
     */
    private $cULTUREVARIETEID;

    /**
     * @var integer
     */
    private $cULTUREVARIETELIGNEID;

    /**
     * @var integer
     */
    private $fAMILLERID;

    /**
     * @var integer
     */
    private $kGHOMJR;

    /**
     * Set cODEARTICLE
     *
     * @param string $cODEARTICLE
     * @return CULTURE
     */
    public function setCODEARTICLE($cODEARTICLE)
    {
        $this->cODEARTICLE = $cODEARTICLE;

        return $this;
    }

    /**
     * Get cODEARTICLE
     *
     * @return string 
     */
    public function getCODEARTICLE()
    {
        return $this->cODEARTICLE;
    }

    /**
     * Set cODEVARIETE
     *
     * @param string $cODEVARIETE
     * @return CULTURE
     */
    public function setCODEVARIETE($cODEVARIETE)
    {
        $this->cODEVARIETE = $cODEVARIETE;

        return $this;
    }

    /**
     * Get cODEVARIETE
     *
     * @return string 
     */
    public function getCODEVARIETE()
    {
        return $this->cODEVARIETE;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return CULTURE
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string 
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return CULTURE
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set dENSITESEMIS
     *
     * @param integer $dENSITESEMIS
     * @return CULTURE
     */
    public function setDENSITESEMIS($dENSITESEMIS)
    {
        $this->dENSITESEMIS = $dENSITESEMIS;

        return $this;
    }

    /**
     * Get dENSITESEMIS
     *
     * @return integer 
     */
    public function getDENSITESEMIS()
    {
        return $this->dENSITESEMIS;
    }

    /**
     * Set eCARTEMENTLIGNE
     *
     * @param integer $eCARTEMENTLIGNE
     * @return CULTURE
     */
    public function setECARTEMENTLIGNE($eCARTEMENTLIGNE)
    {
        $this->eCARTEMENTLIGNE = $eCARTEMENTLIGNE;

        return $this;
    }

    /**
     * Get eCARTEMENTLIGNE
     *
     * @return integer 
     */
    public function getECARTEMENTLIGNE()
    {
        return $this->eCARTEMENTLIGNE;
    }

    /**
     * Set eCARTEMENTSEMIS
     *
     * @param integer $eCARTEMENTSEMIS
     * @return CULTURE
     */
    public function setECARTEMENTSEMIS($eCARTEMENTSEMIS)
    {
        $this->eCARTEMENTSEMIS = $eCARTEMENTSEMIS;

        return $this;
    }

    /**
     * Get eCARTEMENTSEMIS
     *
     * @return integer 
     */
    public function getECARTEMENTSEMIS()
    {
        return $this->eCARTEMENTSEMIS;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return CULTURE
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return CULTURE
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set pHOTO
     *
     * @param string $pHOTO
     * @return CULTURE
     */
    public function setPHOTO($pHOTO)
    {
        $this->pHOTO = $pHOTO;

        return $this;
    }

    /**
     * Get pHOTO
     *
     * @return string 
     */
    public function getPHOTO()
    {
        return $this->pHOTO;
    }

    /**
     * Set sAISIEPOIDSCOEF
     *
     * @param integer $sAISIEPOIDSCOEF
     * @return CULTURE
     */
    public function setSAISIEPOIDSCOEF($sAISIEPOIDSCOEF)
    {
        $this->sAISIEPOIDSCOEF = $sAISIEPOIDSCOEF;

        return $this;
    }

    /**
     * Get sAISIEPOIDSCOEF
     *
     * @return integer 
     */
    public function getSAISIEPOIDSCOEF()
    {
        return $this->sAISIEPOIDSCOEF;
    }

    /**
     * Set sTOCKHUMIDITE
     *
     * @param integer $sTOCKHUMIDITE
     * @return CULTURE
     */
    public function setSTOCKHUMIDITE($sTOCKHUMIDITE)
    {
        $this->sTOCKHUMIDITE = $sTOCKHUMIDITE;

        return $this;
    }

    /**
     * Get sTOCKHUMIDITE
     *
     * @return integer 
     */
    public function getSTOCKHUMIDITE()
    {
        return $this->sTOCKHUMIDITE;
    }

    /**
     * Set sTOCKNBJ_CONSERVATION
     *
     * @param integer $sTOCKNBJCONSERVATION
     * @return CULTURE
     */
    public function setSTOCKNBJCONSERVATION($sTOCKNBJCONSERVATION)
    {
        $this->sTOCKNBJ_CONSERVATION = $sTOCKNBJCONSERVATION;

        return $this;
    }

    /**
     * Get sTOCKNBJ_CONSERVATION
     *
     * @return integer 
     */
    public function getSTOCKNBJCONSERVATION()
    {
        return $this->sTOCKNBJ_CONSERVATION;
    }

    /**
     * Set sTOCKSECURITE
     *
     * @param integer $sTOCKSECURITE
     * @return CULTURE
     */
    public function setSTOCKSECURITE($sTOCKSECURITE)
    {
        $this->sTOCKSECURITE = $sTOCKSECURITE;

        return $this;
    }

    /**
     * Get sTOCKSECURITE
     *
     * @return integer 
     */
    public function getSTOCKSECURITE()
    {
        return $this->sTOCKSECURITE;
    }

    /**
     * Set sTOCKTEMPERATURE
     *
     * @param integer $sTOCKTEMPERATURE
     * @return CULTURE
     */
    public function setSTOCKTEMPERATURE($sTOCKTEMPERATURE)
    {
        $this->sTOCKTEMPERATURE = $sTOCKTEMPERATURE;

        return $this;
    }

    /**
     * Get sTOCKTEMPERATURE
     *
     * @return integer 
     */
    public function getSTOCKTEMPERATURE()
    {
        return $this->sTOCKTEMPERATURE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return CULTURE
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return CULTURE
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fAMILLECULTUREID
     *
     * @param integer $fAMILLECULTUREID
     * @return CULTURE
     */
    public function setFAMILLECULTUREID($fAMILLECULTUREID)
    {
        $this->fAMILLECULTUREID = $fAMILLECULTUREID;

        return $this;
    }

    /**
     * Get fAMILLECULTUREID
     *
     * @return integer
     */
    public function getFAMILLECULTUREID()
    {
        return $this->fAMILLECULTUREID;
    }

    /**
     * Set cULTUREVARIETEID
     *
     * @param integer $cULTUREVARIETEID
     * @return CULTURE
     */
    public function setCULTUREVARIETEID($cULTUREVARIETEID)
    {
        $this->cULTUREVARIETEID = $cULTUREVARIETEID;

        return $this;
    }

    /**
     * Get cULTUREVARIETEID
     *
     * @return integer
     */
    public function getCULTUREVARIETEID()
    {
        return $this->cULTUREVARIETEID;
    }

    /**
     * Set cULTUREVARIETELIGNEID
     *
     * @param integer $cULTUREVARIETELIGNEID
     * @return CULTURE
     */
    public function setCULTUREVARIETELIGNEID($cULTUREVARIETELIGNEID)
    {
        $this->cULTUREVARIETELIGNEID = $cULTUREVARIETELIGNEID;

        return $this;
    }

    /**
     * Get cULTUREVARIETELIGNEID
     *
     * @return integer
     */
    public function getCULTUREVARIETELIGNEID()
    {
        return $this->cULTUREVARIETELIGNEID;
    }

    /**
     * Set fAMILLERID
     *
     * @param integer $fAMILLERID
     * @return CULTURE
     */
    public function setFAMILLERID($fAMILLERID)
    {
        $this->fAMILLERID = $fAMILLERID;

        return $this;
    }

    /**
     * Get fAMILLERID
     *
     * @return integer
     */
    public function getFAMILLERID()
    {
        return $this->fAMILLERID;
    }

    /**
     * Set kGHOMJR
     *
     * @param integer $kGHOMJR
     * @return CULTURE
     */
    public function setKGHOMJR($kGHOMJR)
    {
        $this->kGHOMJR = $kGHOMJR;

        return $this;
    }

    /**
     * Get kGHOMJR
     *
     * @return integer
     */
    public function getKGHOMJR()
    {
        return $this->kGHOMJR;
    }
}
