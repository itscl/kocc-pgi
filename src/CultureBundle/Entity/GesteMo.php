<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GesteMo
 */
class GesteMo
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var string
     */
    private $uSERUPDATE;

    /**
     * @var \DateTime
     */
    private $dATEUPDATE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return GesteMo
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string 
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return GesteMo
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set uSERUPDATE
     *
     * @param string $uSERUPDATE
     * @return GesteMo
     */
    public function setUSERUPDATE($uSERUPDATE)
    {
        $this->uSERUPDATE = $uSERUPDATE;

        return $this;
    }

    /**
     * Get uSERUPDATE
     *
     * @return string 
     */
    public function getUSERUPDATE()
    {
        return $this->uSERUPDATE;
    }

    /**
     * Set dATEUPDATE
     *
     * @param \DateTime $dATEUPDATE
     * @return GesteMo
     */
    public function setDATEUPDATE($dATEUPDATE)
    {
        $this->dATEUPDATE = $dATEUPDATE;

        return $this;
    }

    /**
     * Get dATEUPDATE
     *
     * @return \DateTime 
     */
    public function getDATEUPDATE()
    {
        return $this->dATEUPDATE;
    }
}
