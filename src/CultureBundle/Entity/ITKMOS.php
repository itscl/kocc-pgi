<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKMOS
 */
class ITKMOS
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $iTKMOID;

    /**
     * @var int
     */
    private $pERIODE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iTKMOID
     *
     * @param integer $iTKMOID
     * @return ITKMOS
     */
    public function setITKMOID($iTKMOID)
    {
        $this->iTKMOID = $iTKMOID;

        return $this;
    }

    /**
     * Get iTKMOID
     *
     * @return integer 
     */
    public function getITKMOID()
    {
        return $this->iTKMOID;
    }

    /**
     * Set pERIODE
     *
     * @param integer $pERIODE
     * @return ITKMOS
     */
    public function setPERIODE($pERIODE)
    {
        $this->pERIODE = $pERIODE;

        return $this;
    }

    /**
     * Get pERIODE
     *
     * @return integer 
     */
    public function getPERIODE()
    {
        return $this->pERIODE;
    }
}
