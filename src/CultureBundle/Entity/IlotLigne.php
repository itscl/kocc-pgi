<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IlotLigne
 */
class IlotLigne
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $iLOTID;

    /**
     * @var string
     */
    private $sURFACE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var int
     */
    private $nOINST;

    /**
     * @var \DateTime
     */
    private $dATESEMIS;

    /**
     * @var int
     */
    private $pARCELLEID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iLOTID
     *
     * @param integer $iLOTID
     * @return IlotLigne
     */
    public function setILOTID($iLOTID)
    {
        $this->iLOTID = $iLOTID;

        return $this;
    }

    /**
     * Get iLOTID
     *
     * @return integer 
     */
    public function getILOTID()
    {
        return $this->iLOTID;
    }

    /**
     * Set sURFACE
     *
     * @param string $sURFACE
     * @return IlotLigne
     */
    public function setSURFACE($sURFACE)
    {
        $this->sURFACE = $sURFACE;

        return $this;
    }

    /**
     * Get sURFACE
     *
     * @return string 
     */
    public function getSURFACE()
    {
        return $this->sURFACE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return IlotLigne
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return IlotLigne
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return IlotLigne
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set nOINST
     *
     * @param integer $nOINST
     * @return IlotLigne
     */
    public function setNOINST($nOINST)
    {
        $this->nOINST = $nOINST;

        return $this;
    }

    /**
     * Get nOINST
     *
     * @return integer 
     */
    public function getNOINST()
    {
        return $this->nOINST;
    }

    /**
     * Set dATESEMIS
     *
     * @param \DateTime $dATESEMIS
     * @return IlotLigne
     */
    public function setDATESEMIS($dATESEMIS)
    {
        $this->dATESEMIS = $dATESEMIS;

        return $this;
    }

    /**
     * Get dATESEMIS
     *
     * @return \DateTime 
     */
    public function getDATESEMIS()
    {
        return $this->dATESEMIS;
    }

    /**
     * Set pARCELLEID
     *
     * @param integer $pARCELLEID
     * @return IlotLigne
     */
    public function setPARCELLEID($pARCELLEID)
    {
        $this->pARCELLEID = $pARCELLEID;

        return $this;
    }

    /**
     * Get pARCELLEID
     *
     * @return integer
     */
    public function getPARCELLEID()
    {
        return $this->pARCELLEID;
    }
}
