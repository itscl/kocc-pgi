<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKMOCV
 */
class ITKMOCV
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $iTKMOGID;

    /**
     * @var float
     */
    private $qUANTITE;

    /**
     * @var string
     */
    private $uNITE;

    /**
     * @var int
     */
    private $cULTUREID;

    /**
     * @var int
     */
    private $rEPARTITIONREDEMENTID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iTKMOGID
     *
     * @param integer $iTKMOGID
     * @return ITKMOCV
     */
    public function setITKMOGID($iTKMOGID)
    {
        $this->iTKMOGID = $iTKMOGID;

        return $this;
    }

    /**
     * Get iTKMOGID
     *
     * @return integer 
     */
    public function getITKMOGID()
    {
        return $this->iTKMOGID;
    }

    /**
     * Set qUANTITE
     *
     * @param float $qUANTITE
     * @return ITKMOCV
     */
    public function setQUANTITE($qUANTITE)
    {
        $this->qUANTITE = $qUANTITE;

        return $this;
    }

    /**
     * Get qUANTITE
     *
     * @return float 
     */
    public function getQUANTITE()
    {
        return $this->qUANTITE;
    }

    /**
     * Set uNITE
     *
     * @param string $uNITE
     * @return ITKMOCV
     */
    public function setUNITE($uNITE)
    {
        $this->uNITE = $uNITE;

        return $this;
    }

    /**
     * Get uNITE
     *
     * @return string 
     */
    public function getUNITE()
    {
        return $this->uNITE;
    }

    /**
     * Set cULTUREID
     *
     * @param integer $CULTUREID
     * @return ITKMOCV
     */
    public function setCULTUREID($CULTUREID)
    {
        $this->cULTUREID = $CULTUREID;

        return $this;
    }

    /**
     * Get cULTUREID
     *
     * @return integer
     */
    public function getCULTUREID()
    {
        return $this->cULTUREID;
    }

    /**
     * Set rEPARTITIONREDEMENTID
     *
     * @param integer $rEPARTITIONREDEMENTID
     * @return ITKMOCV
     */
    public function setREPARTITIONREDEMENTID($rEPARTITIONREDEMENTID)
    {
        $this->rEPARTITIONREDEMENTID = $rEPARTITIONREDEMENTID;

        return $this;
    }

    /**
     * Get rEPARTITIONREDEMENTID
     *
     * @return integer
     */
    public function getREPARTITIONREDEMENTID()
    {
        return $this->rEPARTITIONREDEMENTID;
    }
}
