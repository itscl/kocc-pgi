<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CultureVarieteLigne
 */
class CultureVarieteLigne
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $cULTUREVARIETEID;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cULTUREVARIETEID
     *
     * @param integer $cULTUREVARIETEID
     * @return CultureVarieteLigne
     */
    public function setCULTUREVARIETEID($cULTUREVARIETEID)
    {
        $this->cULTUREVARIETEID = $cULTUREVARIETEID;

        return $this;
    }

    /**
     * Get cULTUREVARIETEID
     *
     * @return integer 
     */
    public function getCULTUREVARIETEID()
    {
        return $this->cULTUREVARIETEID;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return CultureVarieteLigne
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return CultureVarieteLigne
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return CultureVarieteLigne
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return CultureVarieteLigne
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return CultureVarieteLigne
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }
}
