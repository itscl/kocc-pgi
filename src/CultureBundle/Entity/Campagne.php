<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Campagne
 */
class Campagne
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var \DateTime
     */
    private $dATEDEBUT;

    /**
     * @var \DateTime
     */
    private $dATEFIN;

    /**
     * @var string
     */
    private $eNCOURS;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var \DateTime
     */
    private $dATECLOTURE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return Campagne
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set dATEDEBUT
     *
     * @param \DateTime $dATEDEBUT
     * @return Campagne
     */
    public function setDATEDEBUT($dATEDEBUT)
    {
        $this->dATEDEBUT = $dATEDEBUT;

        return $this;
    }

    /**
     * Get dATEDEBUT
     *
     * @return \DateTime 
     */
    public function getDATEDEBUT()
    {
        return $this->dATEDEBUT;
    }

    /**
     * Set dATEFIN
     *
     * @param \DateTime $dATEFIN
     * @return Campagne
     */
    public function setDATEFIN($dATEFIN)
    {
        $this->dATEFIN = $dATEFIN;

        return $this;
    }

    /**
     * Get dATEFIN
     *
     * @return \DateTime 
     */
    public function getDATEFIN()
    {
        return $this->dATEFIN;
    }

    /**
     * Set eNCOURS
     *
     * @param string $eNCOURS
     * @return Campagne
     */
    public function setENCOURS($eNCOURS)
    {
        $this->eNCOURS = $eNCOURS;

        return $this;
    }

    /**
     * Get eNCOURS
     *
     * @return string 
     */
    public function getENCOURS()
    {
        return $this->eNCOURS;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return Campagne
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return Campagne
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return Campagne
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set dATECLOTURE
     *
     * @param \DateTime $dATECLOTURE
     * @return Campagne
     */
    public function setDATECLOTURE($dATECLOTURE)
    {
        $this->dATECLOTURE = $dATECLOTURE;

        return $this;
    }

    /**
     * Get dATECLOTURE
     *
     * @return \DateTime 
     */
    public function getDATECLOTURE()
    {
        return $this->dATECLOTURE;
    }
}
