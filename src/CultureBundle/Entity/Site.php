<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Site
 */
class Site
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var string
     */
    private $sITEDEFAULT;

    /**
     * @var string
     */
    private $sITETYPE;

    /**
     * @var string
     */
    private $sITEIPADRESS;

    /**
     * @var int
     */
    private $dENO;

    /**
     * @var int
     */
    private $cHEFSITEID;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return Site
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return Site
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return Site
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return Site
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return Site
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set sITEDEFAULT
     *
     * @param string $sITEDEFAULT
     * @return Site
     */
    public function setSITEDEFAULT($sITEDEFAULT)
    {
        $this->sITEDEFAULT = $sITEDEFAULT;

        return $this;
    }

    /**
     * Get sITEDEFAULT
     *
     * @return string
     */
    public function getSITEDEFAULT()
    {
        return $this->sITEDEFAULT;
    }

    /**
     * Set sITETYPE
     *
     * @param string $sITETYPE
     * @return Site
     */
    public function setSITETYPE($sITETYPE)
    {
        $this->sITETYPE = $sITETYPE;

        return $this;
    }

    /**
     * Get sITETYPE
     *
     * @return string
     */
    public function getSITETYPE()
    {
        return $this->sITETYPE;
    }

    /**
     * Set sITEIPADRESS
     *
     * @param string $sITEIPADRESS
     * @return Site
     */
    public function setSITEIPADRESS($sITEIPADRESS)
    {
        $this->sITEIPADRESS = $sITEIPADRESS;

        return $this;
    }

    /**
     * Get sITEIPADRESS
     *
     * @return string
     */
    public function getSITEIPADRESS()
    {
        return $this->sITEIPADRESS;
    }

    /**
     * Set dENO
     *
     * @param integer $dENO
     * @return Site
     */
    public function setDENO($dENO)
    {
        $this->dENO = $dENO;

        return $this;
    }

    /**
     * Get dENO
     *
     * @return integer
     */
    public function getDENO()
    {
        return $this->dENO;
    }

    /**
     * @return int
     */
    public function getCHEFSITEID()
    {
        return $this->cHEFSITEID;
    }

    /**
     * @param int $cHEFSITEID
     */
    public function setCHEFSITEID($cHEFSITEID)
    {
        $this->cHEFSITEID = $cHEFSITEID;
    }



}
