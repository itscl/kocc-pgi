<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKMOG
 */
class ITKMOG
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $iTKMOSID;

    /**
     * @var int
     */
    private $gESTEID;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iTKMOSID
     *
     * @param integer $iTKMOSID
     * @return ITKMOG
     */
    public function setITKMOSID($iTKMOSID)
    {
        $this->iTKMOSID = $iTKMOSID;

        return $this;
    }

    /**
     * Get iTKMOSID
     *
     * @return integer 
     */
    public function getITKMOSID()
    {
        return $this->iTKMOSID;
    }

    /**
     * Set gESTEID
     *
     * @param integer $gESTEID
     * @return ITKMOG
     */
    public function setGESTEID($gESTEID)
    {
        $this->gESTEID = $gESTEID;

        return $this;
    }

    /**
     * Get gESTEID
     *
     * @return integer 
     */
    public function getGESTEID()
    {
        return $this->gESTEID;
    }
}
