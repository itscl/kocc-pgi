<?php

namespace CultureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ITKMO
 */
class ITKMO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $iTMOID;

    /**
     * @var int
     */
    private $rANG;

    /**
     * @var string
     */
    private $cODE;

    /**
     * @var string
     */
    private $lIBELLE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var \DateTime
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $fAMILLECULTUREID;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iTMOID
     *
     * @param integer $iTMOID
     * @return ITKMO
     */
    public function setITMOID($iTMOID)
    {
        $this->iTMOID = $iTMOID;

        return $this;
    }

    /**
     * Get iTMOID
     *
     * @return integer 
     */
    public function getITMOID()
    {
        return $this->iTMOID;
    }

    /**
     * Set rANG
     *
     * @param integer $rANG
     * @return ITKMO
     */
    public function setRANG($rANG)
    {
        $this->rANG = $rANG;

        return $this;
    }

    /**
     * Get rANG
     *
     * @return integer 
     */
    public function getRANG()
    {
        return $this->rANG;
    }

    /**
     * Set cODE
     *
     * @param string $cODE
     * @return ITKMO
     */
    public function setCODE($cODE)
    {
        $this->cODE = $cODE;

        return $this;
    }

    /**
     * Get cODE
     *
     * @return string 
     */
    public function getCODE()
    {
        return $this->cODE;
    }

    /**
     * Set lIBELLE
     *
     * @param string $lIBELLE
     * @return ITKMO
     */
    public function setLIBELLE($lIBELLE)
    {
        $this->lIBELLE = $lIBELLE;

        return $this;
    }

    /**
     * Get lIBELLE
     *
     * @return string 
     */
    public function getLIBELLE()
    {
        return $this->lIBELLE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ITKMO
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param \DateTime $uPDATEDATE
     * @return ITKMO
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return \DateTime 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ITKMO
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set fAMILLECULTUREID
     *
     * @param integer $fAMILLECULTUREID
     * @return ITKMO
     */
    public function setFAMILLECULTUREID($fAMILLECULTUREID)
    {
        $this->fAMILLECULTUREID = $fAMILLECULTUREID;

        return $this;
    }

    /**
     * Get fAMILLECULTUREID
     *
     * @return integer
     */
    public function getFAMILLECULTUREID()
    {
        return $this->fAMILLECULTUREID;
    }
}
