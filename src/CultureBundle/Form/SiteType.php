<?php

namespace CultureBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //$builder->add('cODE')->add('lIBELLE')->add('dATEINVALIDE')->add('uPDATEDATE')->add('uPDATEUSER')->add('sITEDEFAULT')->add('sITETYPE')->add('sITEIPADRESS')->add('dENO');
        $builder
            ->add('cODE')
            ->add('lIBELLE')
            ->add('dATEINVALIDE', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'DD-MM-YYYY'
                ]
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CultureBundle\Entity\Site'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'culturebundle_site';
    }


}
