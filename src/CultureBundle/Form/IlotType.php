<?php

namespace CultureBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;

class IlotType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $campagnes = array();
        $fermes = array();

        $query=$this->em->getRepository('CultureBundle:Campagne')
            ->createQueryBuilder('c')
            ->orderBy('c.lIBELLE');

        foreach($query->getQuery()->getResult() as $ref){
            $campagnes[$ref->getId()]=$ref->getLIBELLE();
        }

        $query=$this->em->getRepository('CultureBundle:Ferme')
            ->createQueryBuilder('f')
            ->orderBy('f.lIBELLE');

        foreach($query->getQuery()->getResult() as $ref){
            $fermes[$ref->getId()]=$ref->getLIBELLE();
        }

//        $builder->add('cAMPAGNEID')->add('fERMEID')->add('lIBELLE')->add('uPDATEDATE')->add('uPDATEUSER')->add('sURFACE')->add('oRIGINSITEID')->add('dATEINVALIDE');
        $builder
            ->add('lIBELLE')
            ->add('cAMPAGNEID', 'choice', array(
                'choices' => $campagnes,  'required' => false
            ))
            ->add('fERMEID', 'choice', array(
                'choices' => $fermes,  'required' => false
            ))
            ->add('sURFACE')
            ->add('dATEINVALIDE', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'DD-MM-YYYY'
                ]
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CultureBundle\Entity\Ilot'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'culturebundle_ilot';
    }


}
