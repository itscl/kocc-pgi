<?php

namespace CultureBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;

class ParcelleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fermes = array();
        $typeirrigations = array();


        $query=$this->em->getRepository('CultureBundle:Ferme')
            ->createQueryBuilder('f')
            ->orderBy('f.lIBELLE');

        foreach($query->getQuery()->getResult() as $ref){
            $fermes[$ref->getId()]=$ref->getLIBELLE();
        }

        $query=$this->em->getRepository('CultureBundle:TypeIrrigation')
            ->createQueryBuilder('t')
            ->orderBy('t.lIBELLE');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $typeirrigations[$ref->getId()]=$ref->getLIBELLE();
        }
        $builder
            ->add('lIBELLE')
            ->add('fERMEID', 'choice', array(
                'choices' => $fermes,  'required' => false
            ))
            ->add('tYPEIRRIGATIONID', 'choice', array(
                'choices' => $typeirrigations,  'required' => false
            ))
            ->add('sURFACE');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CultureBundle\Entity\Parcelle'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'culturebundle_parcelle';
    }


}
