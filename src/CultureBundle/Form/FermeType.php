<?php

namespace CultureBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;

class FermeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sites = array();
        $cheffermes = array();

        $query=$this->em->getRepository('CultureBundle:Site')
            ->createQueryBuilder('s')
            ->orderBy('s.lIBELLE');

        foreach($query->getQuery()->getResult() as $ref){
            $sites[$ref->getId()]=$ref->getLIBELLE();
        }

        $query=$this->em->getRepository('TechniqueBundle:Personnel')
            ->createQueryBuilder('p')
            ->orderBy('p.nOM, p.pRENOM');

        //on cr�e la liste de choix
        foreach($query->getQuery()->getResult() as $ref){
            $cheffermes[$ref->getId()]=$ref->getNOM().' '.$ref->getPRENOM();
        }

        $builder
            ->add('lIBELLE')
            ->add('cODE')
            ->add('sITEID', 'choice', array(
                'choices' => $sites,  'required' => false,
                'attr' => [
                    'class' => 'form-control selectpicker'
                ]
            ))
            ->add('cHEFFERMEID', 'choice', array(
                'choices' => $cheffermes,  'required' => false
            ))
            ->add('dATEINVALIDE', 'date', array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'DD-MM-YYYY'
                ]
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CultureBundle\Entity\Ferme'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'culturebundle_ferme';
    }


}
