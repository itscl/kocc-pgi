<?php

namespace CultureBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CultureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $famille = array();

        $resfamille=$this->em->getRepository('CultureBundle:FamilleR')->findAll();

        //on cr�e la liste de choix
        foreach($resfamille as $ref){
            $famille[$ref->getId()]=$ref->getLIBELLE();
        }

        $builder
            ->add('cODEARTICLE')
            ->add('lIBELLE')
            ->add('sAISIEPOIDSCOEF','choice', array(
                'expanded' => false,
                'choices' => array(
                    '0' => '0',
                    '-1' => '-1'
            )))
            ->add('fAMILLECULTUREID', 'hidden')
            ->add('cULTUREVARIETEID', 'hidden')
            ->add('cULTUREVARIETELIGNEID', 'hidden')
            ->add('cODEVARIETE')
            ->add('kGHOMJR')
            ->add('fAMILLERID','choice', array(
                'choices' => $famille,
                'empty_value' => " -- Choisir une famille recolte -- ",
                'required' => true
            ))    ;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $culture = $event->getData();
            $form = $event->getForm();
            if($culture && $culture->getId() != null) {
                $form->add('dATEINVALIDE', DateType::class, array(
                    'widget' => 'single_text',
                ))
                    ->add('sAISIEPOIDSCOEF', 'hidden');
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CultureBundle\Entity\Culture'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'culturebundle_culture';
    }


}
