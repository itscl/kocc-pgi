<?php

namespace CultureBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FamilleCultureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lIBELLE')
            ->add('cOMMENTAIRE', TextareaType::class)
            ->add('eCARTEMENTLIGNE')
            ->add('eCARTEMENTSEMIS')
            ->add('dUREECYCLEMAX')
        ;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $familleculture = $event->getData();
            $form = $event->getForm();
            if($familleculture && $familleculture->getId() != null) {
                $form->add('dATEINVALIDE', DateType::class, array(
                    'widget' => 'single_text',
                ));
            }

        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CultureBundle\Entity\FamilleCulture'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'culturebundle_familleculture';
    }


}
