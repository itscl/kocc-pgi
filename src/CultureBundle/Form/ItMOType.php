<?php

namespace CultureBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItMOType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $typeirrigation = array();

        $restypeirrigation=$this->em->getRepository('CultureBundle:TypeIrrigation')->findAll();

        //on cr�e la liste de choix
        foreach($restypeirrigation as $ref){
            $typeirrigation[$ref->getId()]=$ref->getLIBELLE();
        }

        $builder
            ->add('lIBELLE', 'text'/*, array(
                'constraints' => array(
                    new UniqueEntity(array(
                        'message' => 'This value should be blank.'
                    ))
                ))*/
            )
            ->add('tYPEIRRIGATIONID','choice', array(
                'choices' => $typeirrigation,
                'empty_value' => " -- Choisir un type d'irrigation -- ",
                'required' => true
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CultureBundle\Entity\ItMO'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'culturebundle_itmo';
    }


}
