<?php

namespace ReceptionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReceptionR
 */
class ReceptionR
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dATERECEPTION;

    /**
     * @var int
     */
    private $cYCLECULTURALID;

    /**
     * @var string
     */
    private $lOTNO;

    /**
     * @var int
     */
    private $vEHICULEID;

    /**
     * @var int
     */
    private $pALETTEID;

    /**
     * @var int
     */
    private $pALETTENB;

    /**
     * @var int
     */
    private $pALETTEPOIDS;

    /**
     * @var int
     */
    private $eMBALLAGEID;

    /**
     * @var int
     */
    private $eMBALLAGENB;

    /**
     * @var int
     */
    private $eMBALLAGEPOIDS;

    /**
     * @var float
     */
    private $pOIDBRUT;

    /**
     * @var float
     */
    private $pOIDSPACKAGING;

    /**
     * @var float
     */
    private $pOIDNET;

    /**
     * @var string
     */
    private $cOMMENTAIRE;

    /**
     * @var \DateTime
     */
    private $dATEINVALIDE;

    /**
     * @var string
     */
    private $uPDATEDATE;

    /**
     * @var string
     */
    private $uPDATEUSER;

    /**
     * @var int
     */
    private $oRIGINSITEID;

    /**
     * @var int
     */
    private $rECEPTIONLVID;

    /**
     * @var int
     */
    private $rECEPTIONLVDETAILID;

    /**
     * @var string
     */
    private $cODEARTICLE;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dATERECEPTION
     *
     * @param \DateTime $dATERECEPTION
     * @return ReceptionR
     */
    public function setDATERECEPTION($dATERECEPTION)
    {
        $this->dATERECEPTION = $dATERECEPTION;

        return $this;
    }

    /**
     * Get dATERECEPTION
     *
     * @return \DateTime 
     */
    public function getDATERECEPTION()
    {
        return $this->dATERECEPTION;
    }

    /**
     * Set cYCLECULTURALID
     *
     * @param integer $cYCLECULTURALID
     * @return ReceptionR
     */
    public function setCYCLECULTURALID($cYCLECULTURALID)
    {
        $this->cYCLECULTURALID = $cYCLECULTURALID;

        return $this;
    }

    /**
     * Get cYCLECULTURALID
     *
     * @return integer 
     */
    public function getCYCLECULTURALID()
    {
        return $this->cYCLECULTURALID;
    }

    /**
     * Set lOTNO
     *
     * @param string $lOTNO
     * @return ReceptionR
     */
    public function setLOTNO($lOTNO)
    {
        $this->lOTNO = $lOTNO;

        return $this;
    }

    /**
     * Get lOTNO
     *
     * @return string 
     */
    public function getLOTNO()
    {
        return $this->lOTNO;
    }

    /**
     * Set vEHICULEID
     *
     * @param integer $vEHICULEID
     * @return ReceptionR
     */
    public function setVEHICULEID($vEHICULEID)
    {
        $this->vEHICULEID = $vEHICULEID;

        return $this;
    }

    /**
     * Get vEHICULEID
     *
     * @return integer 
     */
    public function getVEHICULEID()
    {
        return $this->vEHICULEID;
    }

    /**
     * Set pALETTEID
     *
     * @param integer $pALETTEID
     * @return ReceptionR
     */
    public function setPALETTEID($pALETTEID)
    {
        $this->pALETTEID = $pALETTEID;

        return $this;
    }

    /**
     * Get pALETTEID
     *
     * @return integer 
     */
    public function getPALETTEID()
    {
        return $this->pALETTEID;
    }

    /**
     * Set pALETTENB
     *
     * @param integer $pALETTENB
     * @return ReceptionR
     */
    public function setPALETTENB($pALETTENB)
    {
        $this->pALETTENB = $pALETTENB;

        return $this;
    }

    /**
     * Get pALETTENB
     *
     * @return integer 
     */
    public function getPALETTENB()
    {
        return $this->pALETTENB;
    }

    /**
     * Set pALETTEPOIDS
     *
     * @param integer $pALETTEPOIDS
     * @return ReceptionR
     */
    public function setPALETTEPOIDS($pALETTEPOIDS)
    {
        $this->pALETTEPOIDS = $pALETTEPOIDS;

        return $this;
    }

    /**
     * Get pALETTEPOIDS
     *
     * @return integer 
     */
    public function getPALETTEPOIDS()
    {
        return $this->pALETTEPOIDS;
    }

    /**
     * Set eMBALLAGEID
     *
     * @param integer $eMBALLAGEID
     * @return ReceptionR
     */
    public function setEMBALLAGEID($eMBALLAGEID)
    {
        $this->eMBALLAGEID = $eMBALLAGEID;

        return $this;
    }

    /**
     * Get eMBALLAGEID
     *
     * @return integer 
     */
    public function getEMBALLAGEID()
    {
        return $this->eMBALLAGEID;
    }

    /**
     * Set eMBALLAGENB
     *
     * @param integer $eMBALLAGENB
     * @return ReceptionR
     */
    public function setEMBALLAGENB($eMBALLAGENB)
    {
        $this->eMBALLAGENB = $eMBALLAGENB;

        return $this;
    }

    /**
     * Get eMBALLAGENB
     *
     * @return integer 
     */
    public function getEMBALLAGENB()
    {
        return $this->eMBALLAGENB;
    }

    /**
     * Set eMBALLAGEPOIDS
     *
     * @param integer $eMBALLAGEPOIDS
     * @return ReceptionR
     */
    public function setEMBALLAGEPOIDS($eMBALLAGEPOIDS)
    {
        $this->eMBALLAGEPOIDS = $eMBALLAGEPOIDS;

        return $this;
    }

    /**
     * Get eMBALLAGEPOIDS
     *
     * @return integer 
     */
    public function getEMBALLAGEPOIDS()
    {
        return $this->eMBALLAGEPOIDS;
    }

    /**
     * Set pOIDBRUT
     *
     * @param float $pOIDBRUT
     * @return ReceptionR
     */
    public function setPOIDBRUT($pOIDBRUT)
    {
        $this->pOIDBRUT = $pOIDBRUT;

        return $this;
    }

    /**
     * Get pOIDBRUT
     *
     * @return float 
     */
    public function getPOIDBRUT()
    {
        return $this->pOIDBRUT;
    }

    /**
     * Set pOIDSPACKAGING
     *
     * @param float $pOIDSPACKAGING
     * @return ReceptionR
     */
    public function setPOIDSPACKAGING($pOIDSPACKAGING)
    {
        $this->pOIDSPACKAGING = $pOIDSPACKAGING;

        return $this;
    }

    /**
     * Get pOIDSPACKAGING
     *
     * @return float 
     */
    public function getPOIDSPACKAGING()
    {
        return $this->pOIDSPACKAGING;
    }

    /**
     * Set pOIDNET
     *
     * @param float $pOIDNET
     * @return ReceptionR
     */
    public function setPOIDNET($pOIDNET)
    {
        $this->pOIDNET = $pOIDNET;

        return $this;
    }

    /**
     * Get pOIDNET
     *
     * @return float 
     */
    public function getPOIDNET()
    {
        return $this->pOIDNET;
    }

    /**
     * Set cOMMENTAIRE
     *
     * @param string $cOMMENTAIRE
     * @return ReceptionR
     */
    public function setCOMMENTAIRE($cOMMENTAIRE)
    {
        $this->cOMMENTAIRE = $cOMMENTAIRE;

        return $this;
    }

    /**
     * Get cOMMENTAIRE
     *
     * @return string 
     */
    public function getCOMMENTAIRE()
    {
        return $this->cOMMENTAIRE;
    }

    /**
     * Set dATEINVALIDE
     *
     * @param \DateTime $dATEINVALIDE
     * @return ReceptionR
     */
    public function setDATEINVALIDE($dATEINVALIDE)
    {
        $this->dATEINVALIDE = $dATEINVALIDE;

        return $this;
    }

    /**
     * Get dATEINVALIDE
     *
     * @return \DateTime 
     */
    public function getDATEINVALIDE()
    {
        return $this->dATEINVALIDE;
    }

    /**
     * Set uPDATEDATE
     *
     * @param string $uPDATEDATE
     * @return ReceptionR
     */
    public function setUPDATEDATE($uPDATEDATE)
    {
        $this->uPDATEDATE = $uPDATEDATE;

        return $this;
    }

    /**
     * Get uPDATEDATE
     *
     * @return string 
     */
    public function getUPDATEDATE()
    {
        return $this->uPDATEDATE;
    }

    /**
     * Set uPDATEUSER
     *
     * @param string $uPDATEUSER
     * @return ReceptionR
     */
    public function setUPDATEUSER($uPDATEUSER)
    {
        $this->uPDATEUSER = $uPDATEUSER;

        return $this;
    }

    /**
     * Get uPDATEUSER
     *
     * @return string 
     */
    public function getUPDATEUSER()
    {
        return $this->uPDATEUSER;
    }

    /**
     * Set oRIGINSITEID
     *
     * @param integer $oRIGINSITEID
     * @return ReceptionR
     */
    public function setORIGINSITEID($oRIGINSITEID)
    {
        $this->oRIGINSITEID = $oRIGINSITEID;

        return $this;
    }

    /**
     * Get oRIGINSITEID
     *
     * @return integer 
     */
    public function getORIGINSITEID()
    {
        return $this->oRIGINSITEID;
    }

    /**
     * Set rECEPTIONLVID
     *
     * @param integer $rECEPTIONLVID
     * @return ReceptionR
     */
    public function setRECEPTIONLVID($rECEPTIONLVID)
    {
        $this->rECEPTIONLVID = $rECEPTIONLVID;

        return $this;
    }

    /**
     * Get rECEPTIONLVID
     *
     * @return integer 
     */
    public function getRECEPTIONLVID()
    {
        return $this->rECEPTIONLVID;
    }

    /**
     * Set rECEPTIONLVDETAILID
     *
     * @param integer $rECEPTIONLVDETAILID
     * @return ReceptionR
     */
    public function setRECEPTIONLVDETAILID($rECEPTIONLVDETAILID)
    {
        $this->rECEPTIONLVDETAILID = $rECEPTIONLVDETAILID;

        return $this;
    }

    /**
     * Get rECEPTIONLVDETAILID
     *
     * @return integer 
     */
    public function getRECEPTIONLVDETAILID()
    {
        return $this->rECEPTIONLVDETAILID;
    }

    /**
     * Set cODEARTICLE
     *
     * @param string $cODEARTICLE
     * @return ReceptionR
     */
    public function setCODEARTICLE($cODEARTICLE)
    {
        $this->cODEARTICLE = $cODEARTICLE;

        return $this;
    }

    /**
     * Get cODEARTICLE
     *
     * @return string 
     */
    public function getCODEARTICLE()
    {
        return $this->cODEARTICLE;
    }
}
