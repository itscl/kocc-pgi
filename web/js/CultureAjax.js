
$(function(){

    $('#datedebutcampagne').datetimepicker();
    $('#datefincampagne').datetimepicker();
    $('#culturebundle_campagne_dATEDEBUT').datetimepicker(
    );
    $('#culturebundle_campagne_dATEFIN').datetimepicker(
    );

    $('#dfin').datetimepicker();
});

$(function(){

    //--Bootstrap Date Range Picker--
    $('#dateinvalide').datetimepicker();
    $('#culturebundle_ferme_dATEINVALIDE').datetimepicker(
    );
});

$(function(){

    $('#dateinvalide').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY'
    });
    $('#culturebundle_site_dATEINVALIDE').datetimepicker(
    );
});

$(function(){
    var ecartementligne = $("#culturebundle_familleculture_eCARTEMENTLIGNE");
    var ecartementesemis = $("#culturebundle_familleculture_eCARTEMENTSEMIS");
    var densite = $("#densite");
    var densite1 = $("#deniste1");
    var ecartementligne1 = $("#ecartementligne");
    var ecartementesemis1 = $("#ecartementsemis");

    var densiteval1 = (10000 / $(ecartementligne1).val()) * (10000 / $(ecartementesemis1).val());

    var densiteval = (10000 / $(ecartementligne).val()) * (10000 / $(ecartementesemis).val());
    $(densite1).val(densiteval1.toFixed());

    $(densite).val(densiteval.toFixed());

    $(ecartementligne).change(function(){
        var densiteval = (10000 / $(ecartementligne).val()) * (10000 / $(ecartementesemis).val());
        $(densite).val(densiteval.toFixed(2))
    });

    $(ecartementesemis).change(function(){
        var densiteval = (10000 / $(ecartementligne).val()) * (10000 / $(ecartementesemis).val());
        $(densite).val(densiteval.toFixed())
    });
});

function ajoutitmoligne(idgeste, iditmo)
{
    var gesteid = idgeste;
    var itmoid = iditmo;
    var periode = $("#periode"+iditmo).val();
    var qte = $("#qte"+idgeste+iditmo).val();

    var datas = "periode="+periode+"&gesteid="+gesteid+ "&qte="+qte+ "&itmoid="+itmoid;
    $.ajax({
        type: "POST",
        url: Routing.generate('ajoutitmoligne'),
        data: datas,
        dataType: "html"
    }).done(function (msg) {
        //location.reload(true);
    }).fail(function () {
        alert("error");
    });
}

function dupliquer(str)
{
    var itmoid = str;
    var newlibelleitmo = $("#newlibelleitmo"+str).val();
    var typeirrigation = $("#typeirrigation"+str).val();
    var oldtypeirrigation = $("#oldtypeirrigation"+str).val();
    var oldlibelle = $("#oldlibelle"+str).val();

    if(newlibelleitmo != '' && typeirrigation !='')
    {
        if(typeirrigation != oldtypeirrigation)
        {
           // alert(typeirrigation +" "+ oldtypeirrigation)
            var datas = "newlibelleitmo="+newlibelleitmo+"&typeirrigation="+typeirrigation+"&itmoid="+str;
            $.ajax({
                type: "POST",
                url: Routing.generate('dupliqueritkmo'),
                data: datas,
                dataType: "html"
            }).done(function (msg) {
                location.reload(true);
            }).fail(function () {

            });
        }
        else
        {
            //alert(typeirrigation +" "+ oldtypeirrigation)
            $("#modal-danger1").modal();
        }
    }
    else
    {
        $("#modal-danger").modal();
    }
}

function dupliquerperiode(str)
{
    var itkmosid = str;
    var itkmoid = $("#itkmoid"+str).val();
    var newlibelle = $("#newlibelle"+str).val();
    var oldlibelle = $("#oldlibelle"+str).val();
    var famillecultureid = $("#famillecultureid").val();

    //alert(itkmoid);

    if(newlibelle != '')
    {
        var datas = "newlibelle="+newlibelle+"&itkmoid="+itkmoid+"&itkmosid="+str+"&famillecultureid="+famillecultureid;
        $.ajax({
            type: "POST",
            url: Routing.generate('dupliqueritkmos'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            location.reload(true);
        }).fail(function () {

        });
    }
    else
    {
        $("#modal-danger").modal();
    }
}

function dupliquergeste(str)
{
    var itkmogid = str;
    var itkmos = $("#itkmos"+str).val();
    var itmoid = $("#itmoid"+str).val();

    if(itkmos != '')
    {
        var datas = "itkmogid="+str+"&itkmos="+itkmos+"&itmoid="+itmoid;
        $.ajax({
            type: "POST",
            url: Routing.generate('dupliqueritkmog'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            location.reload(true);
        }).fail(function () {
            $("#modal-danger2").modal();
        });
    }
    else
    {
        $("#modal-danger").modal();
    }
}

function modifieritmo(str)
{
    var itmoid = str;
    var famillecultureid = $("#famillecultureid").val();
    var libelle = $("#libelle"+str).val();

    if(libelle != '')
    {
        var datas = "itmoid="+str+"&famillecultureid="+famillecultureid+"&libelle="+libelle;
        $.ajax({
            type: "GET",
            url: Routing.generate('modifieritmo'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            location.reload(true);
        }).fail(function () {

        });
    }
    else
    {
        $("#modal-danger").modal();
    }
}

/*$(function() {
    $.contextMenu({
        selector: '.context-menu-one1',
        items: {
            "delete": {
                name: "Supprimer",
                icon: "delete",
                callback: function(key, options) {
                    var m = $(this).text();

                    console.log(m);
                    var datas = "itmoligne="+ m.trim();

                    bootbox.confirm({
                        message: "Vous êtes sûr de vouloir supprimer cette période ?",
                        buttons:
                        {
                            confirm:
                            {
                                label: 'OK',
                                className: 'btn-primary'
                            },
                            cancel:
                            {
                                label: 'Annuler',
                                className: 'btn-default'
                            }
                        },
                        callback: function (result)
                        {
                            if(result==true)
                            {
                                $.ajax({
                                    type: "POST",
                                    url: Routing.generate('supprimeritmoligne'),
                                    data: datas,
                                    dataType: "html"
                                }).done(function (msg) {
                                    location.reload(true);
                                }).fail(function () {
                                    $("#modal-danger").modal();
                                });
                            }
                        }
                    });

                }
            },
            "sep1": "---------",
            "quit": {name: "Quitter", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
});*/

function reload(str, str2, str3)
{
    /* if (str1=="") {
     document.getElementById("txtHint").innerHTML="";
     return;
     }*/
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("periode"+str2).innerHTML=this.responseText;
        }
    };
    xmlhttp.open("GET",Routing.generate('reload')+"?id="+str+"&itkmoid="+str2+"&semaine="+str3,true);
    xmlhttp.send();
}

function reloadgeste(str, str2, str3)
{
    /* if (str1=="") {
     document.getElementById("txtHint").innerHTML="";
     return;
     }*/
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("addgeste"+str).innerHTML=this.responseText;
        }
    };
    xmlhttp.open("GET",Routing.generate('reloadgeste')+"?itkmosid="+str+"&gesteid="+str2+"&id="+str3,true);
    xmlhttp.send();
}



function ajoutitkmos(str)
{
    var id = str;
    var semaine = $("#semaine"+str);
   // var periode = $("#periode"+str);

    var famillecultureid = $("#famillecultureid").val();

    var datas = "iditkmo="+str+ "&semaine="+$(semaine).val()+"&famillecultureid="+famillecultureid;

    $.ajax({
        url: Routing.generate('ajoutitkmos'),
        type: "GET",
        data: datas,
    }).done(function (msg) {
        reload(famillecultureid, str, $(semaine).val());
        /*var periode = $(semaine).val();
        var elem = document.getElementById('nestable'+str);
        var elem1 = $('#second-choice').load(Routing.generate('getter'));
        console.log(elem1);
        elem.innerHTML=elem.innerHTML+ ' <li class="dd-item dd2-item" data-id="{{ rsitkmo.id }}" id="periode{{ rsitkmo.id }}"> ' +
            '<div class="dd-handle dd2-handle"> ' +
            '<a href=""><i class="normal-icon fa fa-trash red"></i></a> ' +
            '<i class="drag-icon fa fa-arrows-alt "></i> </div> <div class="dd2-content">'+periode+'</div><ol class="dd-list"><li class="dd-item" data-id="17"> <div class="dd-handle">' +
            '<select id="mySelect"></select></div></li></ol></li> ';*/
        //parent.location.reload();
    }).fail(function () {

    });
}


/*
$(document).ready(function () {
    $(function ($) {
        $('.dd').nestable().nestable('collapseAll');
        $('.dd-handle a').on('mousedown', function (e) {
            e.stopPropagation();
        });
    });
});
*/

function ajoutitkmog(str1)
{
    var famillecultureid = $("#famillecultureid").val();
    var itkmosid = str1;
    var gesteid = $("#geste"+str1).val();

    var datas = "itkmosid="+str1+ "&gesteid="+gesteid;

    if(gesteid != '') {
        $.ajax({
            url: Routing.generate('ajoutitkmog'),
            type: "GET",
            data: datas,
            success: function (data) {
                reloadgeste(str1, gesteid, famillecultureid);
                //parent.location.reload();
            }
        });
    }
}


/*function ajoutitkmog(str1, str2, str3)
{
    var itkmosid = str1;
    var gesteid = str2;
    var cultureid = str3;
    var quantite = $("#quantite"+str1+str2+str3);

    var datas = "itkmosid="+str1+ "&gesteid="+str2+ "&cultureid="+str3+ "&quantite="+$(quantite).val();

        $.ajax({
            url: Routing.generate('ajoutitkmog'),
            type: "GET",
            data: datas,
            success: function (data)
            {

            }
        });
}*/

function updateitmo(str)
{
    var itmoid = str;
    var libelle = $("#libelle"+str).val();

    var datas = "itmoid="+str+ "&libelle="+libelle;

        $.ajax({
            url: Routing.generate('modifieritmo'),
            type: "GET",
            data: datas,
        }).done(function (msg) {
            location.reload(true);
        }).fail(function () {
            alert("error");
            location.reload(true);
        });

}

function updateitkmocv(str)
{
    var itkmocvid = str;
    var quantite = $("#quantite"+str).val();
    var unite = $("#unite"+str).val();

    var datas = "itkmocvid="+str+ "&quantite="+quantite+ "&unite="+unite;

    if(quantite != '' || unite != '') {
        $.ajax({
            url: Routing.generate('updateitkmocv'),
            type: "GET",
            data: datas,
            success: function (data) {

                //parent.location.reload();
            }
        });
    }
else
    {
        $.ajax({
            url: Routing.generate('supprimeritkmog'),
            type: "GET",
            data: datas,
            success: function (data)
            {
                alert("ok")
            }
        });
    }
}

function reloadcv(str, str2, str3)
{
    /* if (str1=="") {
     document.getElementById("txtHint").innerHTML="";
     return;
     }*/
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("addcv"+str).innerHTML=this.responseText;
        }
    };
    xmlhttp.open("GET",Routing.generate('reloadcv')+"?itkmogid="+str+"&cultureid="+str2+"&id="+str3,true);
    xmlhttp.send();
}

function ajoutitkmocv(str1)
{
    var famillecultureid = $("#famillecultureid").val();
    var itkmogid = str1;
    var cultureid = $("#cultureid"+str1).val();

    var datas = "itkmogid="+str1+ "&cultureid="+cultureid;

    if(cultureid != '') {
        $.ajax({
            url: Routing.generate('ajoutitkmocv'),
            type: "GET",
            data: datas,
            success: function (data) {
                reloadcv(str1,cultureid,famillecultureid);
            }
        });
    }
}
$(document).ready(function() {
    $('#btn_click').on('click', function() {
        var url = Routing.generate('reload',{iditkmo:str, semaine:$(semaine).val(), famillecultureid:famillecultureid});
        $('#widget').load(url);
    });
});

function ajoutitkcyclecv(int)
{
    var semainerecolte = $("#semainerecolte"+int).val();
    var rndtbrut = $("#rndtbrut"+int).val();
    var culture = $("#culture").val();


    if(semainerecolte != '' && rndtbrut != '')
    {
        var datas = "semainerecolte="+semainerecolte+"&rndtbrut="+rndtbrut+"&culture="+culture;
        $.ajax({
            type: "GET",
            url: Routing.generate('ajoutitkcyclecv'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            //location.reload(true);
        }).fail(function () {

        });
    }
    else
    {
        Notify('Un des champs est vide ! ', 'top-right', '5000', 'danger', 'fa-bolt', true);
        //$("#modal-danger").modal();
    }
}

function ajoutitkcyclecv1()
{
    var semainerecolte = $("#semainerecolte").val();
    var rndtbrut = $("#rndtbrut").val();
    var culture = $("#culture").val();


    if(semainerecolte != '')
    {
        var datas = "semainerecolte="+semainerecolte+"&rndtbrut="+rndtbrut+"&culture="+culture;
        $.ajax({
            type: "GET",
            url: Routing.generate('ajoutitkcyclecv'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {

            var datatable = $("#dataTable");

            //addRow(datatable);

            //var table = document.getElementById(tableID);

            //location.reload(true);
        }).fail(function () {

        });
    }
    else
    {
        Notify('Un des champs est vide ! ', 'top-right', '5000', 'danger', 'fa-bolt', true);
        //$("#modal-danger").modal();
    }
}

function addRow()
{
    //add a row to the rows collection and get a reference to the newly added row
    var newRow = document.getElementById("dataTable").insertRow(-1);

    var oCell = newRow.insertCell(0);
    oCell.innerHTML = "<input type='text' class='form-control' id='semainerecolte'>";

    oCell = newRow.insertCell(1);
    oCell.innerHTML = "<input type='text' class='form-control' id='rndtbrut' onchange='ajoutitkcyclecv1();'>";
}

function modifieritkcyclecv(str)
{
    var id = str;
    var semainerecolte = $("#semainerecolte"+str).val();
    var rndtbrut = $("#rndtbrut"+str).val();

    if(rndtbrut != '')
    {
        var datas = "id="+str+"&rndtbrut="+rndtbrut+"&semainerecolte="+semainerecolte;
        $.ajax({
            type: "GET",
            url: Routing.generate('modifieritkcyclecv'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            //location.reload(true);
        }).fail(function () {

        });
    }
    else
    {
        Notify('Un des champs est vide ! ', 'top-right', '5000', 'danger', 'fa-bolt', true);
        //$("#modal-danger").modal();
    }
}

function ajoutrepartitionrendement()
{
    var numsemaine = $("#numsemaine").val();
    var pourcentrend = $("#pourcentrend").val();
    var culture = $("#culture").val();

    if(numsemaine != '')
    {
        var datas = "numsemaine="+numsemaine+"&pourcentrend="+pourcentrend+"&culture="+culture;

        $.ajax({
            type: "GET",
            url: Routing.generate('ajout_repartitionrendement'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {

            /*var datatable = $("#dataTable");*/
        }).fail(function () {

        });
    }
    else
    {
        Notify('Un des champs est vide ! ', 'top-right', '5000', 'danger', 'fa-bolt', true);
        //$("#modal-danger").modal();
    }
}


function modifierrepartitionrendement(str)
{
    var id = str;
    var numsemaine = $("#numsemaine"+str).val();
    var pourcentrend = $("#pourcentrend"+str).val();

    if(pourcentrend != '')
    {
        var datas = "id="+str+"&pourcentrend="+pourcentrend+"&numsemaine="+numsemaine;
        $.ajax({
            type: "GET",
            url: Routing.generate('modifier_repartitionrendement'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            //location.reload(true);
        }).fail(function () {

        });
    }
    else
    {
        Notify('Un des champs est vide ! ', 'top-right', '5000', 'danger', 'fa-bolt', true);
        //$("#modal-danger").modal();
    }
}


function edit_row(no)
{
    document.getElementById("edit_button"+no).style.display="none";
    document.getElementById("save_button"+no).style.display="block";

    var numrecolte=document.getElementById("numrecolte_row"+no);
    var pourcentage=document.getElementById("pourcentage_row"+no);


    var numrecolte_data=numrecolte.innerHTML;
    var pourcentage_data=pourcentage.innerHTML;

    numrecolte.innerHTML="<input type='text' id='numrecolte_text"+no+"' value='"+numrecolte_data+"'>";
    pourcentage.innerHTML="<input type='text' id='pourcentage_text"+no+"' value='"+pourcentage_data+"'>";
}

function add_row()
{
    var new_numrecolte=document.getElementById("new_numrecolte").value;
    var new_pourcentage=document.getElementById("new_pourcentage").value;
    var culture=document.getElementById("culture").value;

    var datas = "new_numrecolte="+new_numrecolte+"&new_pourcentage="+new_pourcentage+"&culture="+culture;

    $.ajax({
        type: "GET",
        url: Routing.generate('ajout_repartitionrendement'),
        data: datas,
        dataType: "html",
        success: function (data) {
            // Je charge les données dans box
            //alert(data)
        },

        // La fonction à appeler si la requête n'a pas abouti
        error: function() {
            // J'affiche un message d'erreur
            box.html("Désolé, aucun résultat trouvé.");
        }

    })

    var table=document.getElementById("data_table");
    //var table_len=$("#id").val();
    var table_len=(table.rows.length)-1;
    var row = table.insertRow(table_len).outerHTML="<tr id='row"+table_len+"'><td id='numrecolte_row"+table_len+"'><input type='number' class='form-control' value='"+new_numrecolte+"'/></td><td id='pourcentage_row"+table_len+"'><input type='number' class='form-control' value='"+new_pourcentage+"'/></td></tr>";

    document.getElementById("new_numrecolte").value="";
    document.getElementById("new_pourcentage").value="";
}

function save_row(no)
{
    var numrecolte_val=document.getElementById("numrecolte_text"+no).value;
    var pourcentage_val=document.getElementById("pourcentage_text"+no).value;
    //var culture_val=document.getElementById("culture_text").value;

    var datas = "id="+no+"&pourcentrend="+pourcentage_val+"&numrecolte="+numrecolte_val;
    $.ajax({
        type: "GET",
        url: Routing.generate('modifier_repartitionrendement'),
        data: datas,
        dataType: "html"
    }).done(function (msg) {
        //location.reload(true);
    }).fail(function () {

    });

}

function delete_row(no)
{
    var datas = "id="+no;

    $.ajax({
        type: "GET",
        url: Routing.generate('supprimer_repartitionrendement'),
        data: datas,
        dataType: "html"
    }).done(function (msg) {
        //location.reload(true);
    }).fail(function () {

    });

    document.getElementById("row"+no+"").outerHTML="";
}

function updatepassageitkmocv(str)
{
    var itkmocvid = str;
    var quantite = $("#quantite"+str).val();
    var unite = $("#unite"+str).val();
    var passage = $("#passage"+str).val();

    var datas = "itkmocvid="+str+ "&quantite="+quantite+ "&unite="+unite+"&passage="+passage;

    if(quantite != '' || unite != '') {
        $.ajax({
            url: Routing.generate('updateitkmocv'),
            type: "GET",
            data: datas,
            success: function (data) {
                //parent.location.reload();
            }
        });
    }
}

