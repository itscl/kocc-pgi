/**
 * Created by akdaho on 16/12/2017.
 */

$(document).ready(function()
{
    var options = {
        chart: {
            type: 'column',
            renderTo: 'containersemresp',
            width: 600
        },
        title: {
            text: 'Main d\'oeuvre par site de Decembre à '+moment(new Date()).weekday(13).format('WWYY')
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: null
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
        },
        series: [{
            name: 'Main d\'oeuvre',
            colorByPoint: true
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 200
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    }
    $.getJSON(Routing.generate('chartgrsite'),function(json) {
        //options.series[0].data = json['data'];
        options.series[0] = json[0];
        options.series[1] = json[1];
        console.log(json[0]);
        chart = new Highcharts.Chart(options);
    });
});

$(document).ready( function () {
    var options = {
        chart: {
            type: 'pie',
            renderTo: 'container',
        },

        title: {
            text: 'Main d\'oeuvre gloable de Decembre à '+moment(new Date()).weekday(13).format('WWYY')
    },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                size: 180,
                colors :['#11a9cc', '#ffce55'],
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                }
            }
        },
        series: [{}]
    };

    var url = Routing.generate('itmoglobalcharte');// "http://url-to-your-remote-server/jsonp.php?callback=?";
    $.getJSON(url,  function(data) {
        options.series[0].data = data;
        var chart = new Highcharts.Chart(options);
    });

});

$(document).ready(function()
{
    var siteid = 'DIAMA';
    var semaine = $("#semaine").val();

    var options = {
        chart: {
            renderTo: 'containersemcur',
            type: 'column',
            width: 150
        },
        title: {
            text: null
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical',
            enabled: false,
            x: -10,
            y: 20,
        },

        xAxis: {
            categories: ['MO'],
            labels: {
                x: -10
            }
        },

        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Quantite'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.1f} %',
                }
            },
            bar: {
                minPointLength: 10,

            }
        },
        series: [],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 200
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    }
    $.getJSON(Routing.generate('itmositesemchart'),{semaine:semaine,site:siteid}, function(json) {
        options.series[0] = json[0];
        options.series[1] = json[1];
        chart = new Highcharts.Chart(options);
    });
});


$(document).ready(function()
{
    var siteid = 'NGALAM';
    var semaine = $("#semaine").val();

    var options = {
        chart: {
            renderTo: 'containersemcurn',
            type: 'column',
            width: 150
        },
        title: {
            text: null
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical',
            enabled: false,
            x: -10,
            y: 20,
        },

        xAxis: {
            categories: ['MO'],
            labels: {
                x: -10
            }
        },

        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Quantite'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.1f} %',
                }
            },
            bar: {
                minPointLength: 10,

            }
        },
        series: [],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 200
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    };
    $.getJSON(Routing.generate('itmositesemchart'),{semaine:semaine,site:siteid}, function(json) {
        options.series[0] = json[0];
        options.series[1] = json[1];
        chart = new Highcharts.Chart(options);
    });
});


$(document).ready(function()
{
    var siteid = 'SAVOIGNE';
    var semaine = $("#semaine").val();

    var options = {
        chart: {
            renderTo: 'containersemcurs',
            type: 'column',
            width: 150
        },
        title: {
            text: null
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical',
            enabled: false,
            x: -10,
            y: 20,
        },

        xAxis: {
            categories: ['MO'],
            labels: {
                x: -10
            }
        },

        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Quantite'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.1f} %',
                }
            },
            bar: {
                minPointLength: 10,

            }
        },
        series: [],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 200
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    }
    $.getJSON(Routing.generate('itmositesemchart'),{semaine:semaine,site:siteid}, function(json) {
        options.series[0] = json[0];
        options.series[1] = json[1];
        chart = new Highcharts.Chart(options);
    });
});

/**/

/**/

$(document).ready(function()
{
    var siteid = 'DIAMA';
    var semaine = $("#semaineprec").val();

    var options = {
        chart: {
            renderTo: 'containersemprecd',
            type: 'column',
            width: 150

        },
        title: {
            text: null
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical',
            enabled: false,
            x: -10,
            y: 20,
        },

        xAxis: {
            categories: ['MO'],
            labels: {
                x: -10
            }
        },

        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Quantite'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.1f} %',
                }
            },
            bar: {
                minPointLength: 10,

            }
        },
        series: [],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 200
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    }
    $.getJSON(Routing.generate('itmositesemchart'),{semaine:semaine,site:siteid}, function(json) {
        options.series[0] = json[0];
        options.series[1] = json[1];
        chart = new Highcharts.Chart(options);
    });
});

$(document).ready(function()
{
    var siteid = 'DIAMA';
    var semaine = $("#semainesuiv").val();

    var options = {
        chart: {
            renderTo: 'containersemsuivd',
            type: 'column',
            width: 150
        },
        title: {
            text: null
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical',
            enabled: false,
            x: -10,
            y: 20,
        },

        xAxis: {
            categories: ['MO'],
            labels: {
                x: -10
            }
        },

        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Quantite'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.1f} %',
                }
            },
            bar: {
                minPointLength: 10,

            }
        },
        series: [],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 200
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    }
    $.getJSON(Routing.generate('itmositesemchart'),{semaine:semaine,site:siteid}, function(json) {
        options.series[0] = json[0];
        options.series[1] = json[1];
        chart = new Highcharts.Chart(options);
    });
});

$(document).ready(function()
{
    var siteid = 'NGALAM';
    var semaine = $("#semaineprec").val();

    var options = {
        chart: {
            renderTo: 'containersemprecn',
            type: 'column',
            width: 150
        },
        title: {
            text: null
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical',
            enabled: false,
            x: -10,
            y: 20,
        },

        xAxis: {
            categories: ['MO'],
            labels: {
                x: -10
            }
        },

        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Quantite'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.1f} %',
                }
            },
            bar: {
                minPointLength: 10,

            }
        },
        series: [],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 200
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    }
    $.getJSON(Routing.generate('itmositesemchart'),{semaine:semaine,site:siteid}, function(json) {
        options.series[0] = json[0];
        options.series[1] = json[1];
        chart = new Highcharts.Chart(options);
    });
});

$(document).ready(function()
{
    var siteid = 'SAVOIGNE';
    var semaine = $("#semaineprec").val();

    var options = {
        chart: {
            renderTo: 'containersemprecs',
            type: 'column',
            width: 150
        },
        title: {
            text: null
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical',
            enabled: false,
            x: -10,
            y: 20,
        },

        xAxis: {
            categories: ['MO'],
            labels: {
                x: -10
            }
        },

        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Quantite'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.1f} %',
                }
            },
            bar: {
                minPointLength: 10,

            }
        },
        series: [],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 200
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    }
    $.getJSON(Routing.generate('itmositesemchart'),{semaine:semaine,site:siteid}, function(json) {
        options.series[0] = json[0];
        options.series[1] = json[1];
        chart = new Highcharts.Chart(options);
    });
});


$(document).ready(function()
{
    var siteid = 'NGALAM';
    var semaine = $("#semainesuiv").val();

    var options = {
        chart: {
            renderTo: 'containersemsuivn',
            type: 'column',
            width: 150
        },
        title: {
            text: null
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical',
            enabled: false,
            x: -10,
            y: 20,
        },

        xAxis: {
            categories: ['MO'],
            labels: {
                x: -10
            }
        },

        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Quantite'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.1f} %',
                }
            },
            bar: {
                minPointLength: 10,

            }
        },
        series: [],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 200
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    }
    $.getJSON(Routing.generate('itmositesemchart'),{semaine:semaine,site:siteid}, function(json) {
        options.series[0] = json[0];
        options.series[1] = json[1];
        chart = new Highcharts.Chart(options);
    });
});

$(document).ready(function()
{
    var siteid = 'SAVOIGNE';
    var semaine = $("#semainesuiv").val();

    var options = {
        chart: {
            renderTo: 'containersemsuivs',
            type: 'column',
            width: 150
        },
        title: {
            text: null
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical',
            enabled: false,
            x: -10,
            y: 20,
        },

        xAxis: {
            categories: ['MO'],
            labels: {
                x: -10
            }
        },

        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Quantite'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.1f} %',
                }
            },
            bar: {
                minPointLength: 10,

            }
        },
        series: [],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 200
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    }
    $.getJSON(Routing.generate('itmositesemchart'),{semaine:semaine,site:siteid}, function(json) {
        options.series[0] = json[0];
        options.series[1] = json[1];
        chart = new Highcharts.Chart(options);
    });
});

/*


$(document).ready(function()
{
    var siteid = 3;
    var semaine = $("#semaineprec").val();

    var options = {
        chart: {
            renderTo: 'containersemprecc3',
            type: 'column'
        },
        title: {
            text: null
        },

        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical',
            enabled: false,
            x: -10,
            y: 20,
        },

        xAxis: {
            categories: ['MO'],
            labels: {
                x: -10
            }
        },

        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Quantite'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.1f} %',
                }
            },
            bar: {
                minPointLength: 10,

            }
        },
        series: [],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 200
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal'
                    },
                    yAxis: {
                        labels: {
                            align: 'left',
                            x: 0,
                            y: -5
                        },
                        title: {
                            text: null
                        }
                    },
                    subtitle: {
                        text: null
                    },
                    credits: {
                        enabled: false
                    }
                }
            }]
        }
    }
    $.getJSON(Routing.generate('itmositesemchart'),{semaine:semaine,site:siteid}, function(json) {
        options.series[0] = json[0];
        options.series[1] = json[1];
        chart = new Highcharts.Chart(options);
    });
});





*/

/*
*/
/*$(document).ready(function() {

    var options = {
        chart: {
            renderTo: 'containersemresp',
            type: 'bar',
            // marginRight: 130,
            //marginBottom: 25
        },
        title: {
            text: '',
        },
        xAxis: [{
            categories: ["BALDE Harouna", "NDOUR Ousmane", "GAYE Mamadou", "BALDE Harouna", "WADE Abdou", "DIEYE Ouleye", "DIEDHIOU Asselile", "GAYE Mamadou", "NDOUR Ousmane", "SECK Modou", "FALL Malick"]
        }, {
            categories: [
                '8/5/2013 8:59 PM',
                '8/5/2013 12:59 PM',
                '8/5/2013 2:59 PM',
                '8/5/2013 6:59 PM',
                '8/5/2013 12:59 AM',
                '8/5/2013 3:59 PM',
                '8/5/2013 8:23 PM',
                '8/5/2013 8:19 PM'],
            opposite: true,
            title: {
                text: 'Last vote cast at'
            }
        }],
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Votes'
            }
        },

        plotOptions: {},


        series: [{
            dataLabels: {
                color: '#FFFFFF',
                enabled: true,
                inside: true
            },
            showInLegend: false,
            name: 'Votes',
            xAxis: 1,
            pointWidth:25,
            groupPadding:0.5
        }],
    }

    $.getJSON(Routing.generate('itmostrespchart'), function(json) {
       // options.xAxis.categories = json[0]['data'];
        console.log(json[0]['data']);
        console.log(json[0]['data']);
        options.series[0] = json[1];
        options.series[1] = json[2];
        chart = new Highcharts.Chart(options);
    });



});*/

/*
$(document).ready(function () {
    $('#containersemresp').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        plotOptions:{},
        xAxis: [{
            categories: ['Injustice: Gods Among Us ★', 'Tekken Tag Tournament 2 ★', 'Super Smash Bros. Melee ★', 'Persona 4 Arena', 'King of Fighters XIII', 'Dead or Alive 5 Ultimate', 'Street Fighter X Tekken Ver. 2013', 'Soulcalibur V'],
        }, {
            categories: [
                '8/5/2013 8:59 PM',
                '8/5/2013 12:59 PM',
                '8/5/2013 2:59 PM',
                '8/5/2013 6:59 PM',
                '8/5/2013 12:59 AM',
                '8/5/2013 3:59 PM',
                '8/5/2013 8:23 PM',
                '8/5/2013 8:19 PM'],
            opposite: true,
            title: {
                text: 'Last vote cast at'
            }
        }],
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Votes'
            }
        },
        series: [{
            data: [1426,823,462,305,181,148,127,115],
            showInLegend: false,
            name: 'Votes',
            xAxis: 1,
            pointWidth:25,
            groupPadding:0.5
        }, {
            data: [20, 50, 0, 0, 5, 0, 0, 0],
        }]
    });
});*/
