/**
 * Created by akdaho on 09/01/2017.
 */
function getroles(str) {
    var id = str;

    var datas = $('.checkrole:checked').serialize()+ "&id="+str;

    $.ajax({
        url: Routing.generate('ajouterrole'),
        type: "POST",
        data: datas,
        success: function (data) {
            location.reload(true);
        }
    });
}

function deleteroles(str) {
    var id = str;
    var datas = $('.checkrole:checked').serialize()+ "&id="+str;

    $.ajax({
        url: Routing.generate('supprimerrole'),
        type: "POST",
        data: datas,
        success: function (data) {
            location.reload(true);
        }
    });
}