/* ************************************************************ Site ************************************ */
$(document).ready(function () {

    var site = $("#site");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(site).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getSite'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});

$(document).ready(function () {

    var site = $("#site1");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(site).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getSite'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});
/* ************************************************************ Fin Site ************************************ */

/* ************************************************************ Ferme ************************************ */
$(document).ready(function () {

    var ferme = $("#ferme");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(ferme).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getferme'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term,
                    siteid: $("#site").val()
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});

$(document).ready(function () {

    var ferme = $("#ferme1");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(ferme).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getferme'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term,
                    siteid: $("#site").val()
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});
/* ************************************************************ Fin Ferme ************************************ */

/* ************************************************************ Irrigation ************************************ */
$(document).ready(function () {

    var typeirrigation = $("#typeirrigation");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(typeirrigation).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('gettypeirrigation'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term,
                    //siteid: $("#site").val()
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});
/* ************************************************************ Fin Irrigation ************************************ */

/* ************************************************************ Cycle culturale ************************************ */
$(document).ready(function () {

    var cyclecultural = ("#cyclecultural");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(cyclecultural).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getcyclecultural'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term, // search term
                    fermeid: $("#ferme").val(), // search term
                    siteid: $("#site").val(), // search term
                    page: 10 // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });
});

$(document).ready(function () {

    var cyclecultural = ("#cyclecultural1");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(cyclecultural).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getcyclecultural'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term, // search term
                    fermeid: $("#ferme").val(), // search term
                    siteid: $("#site").val(), // search term
                    page: 10 // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });
});

$(document).ready(function () {

    var cyclecultural = ("#cyclecultural2");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(cyclecultural).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getcyclecultural'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term, // search term
                    fermeid: $("#ferme").val(), // search term
                    siteid: $("#site").val(), // search term
                    page: 10 // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });
});
/* ************************************************************ Fin Cycle culturale ************************************ */

/* ************************************************************ Geste ************************************ */
$(document).ready(function () {

    var geste = ("#geste");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(geste).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getgeste'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },

            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });
});
/* ************************************************************ Fin Geste ************************************ */

/* ************************************************************ Culture ************************************ */
$(document).ready(function () {

    var culture = ("#culture");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(culture).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getculture'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});

$(document).ready(function () {

    var culture = ("#culture1");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(culture).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getculture'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});
/* ************************************************************ Fin Culture ************************************ */

/* ************************************************************ Ilot ************************************ */
$(document).ready(function () {

    var ilot = ("#ilot");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(ilot).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getilotferme'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term, // search term
                    fermeid: $("#ferme").val()
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});

$(document).ready(function () {

    var ilot1 = ("#ilot1");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(ilot1).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getilotferme'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term, // search term
                    fermeid: $("#ferme1").val()
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});
/* ************************************************************ Fin Ilot ************************************ */

/* ************************************************************ Famille culture ************************************ */
$(document).ready(function () {

    var familleculture = ("#familleculture");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(familleculture).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getfamilleculture'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        initSelection: function (element, callback) {
            var data = {id: $("#culturebundle_culture_fAMILLECULTUREID").val(), name:  $("#namefamilleculture").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });

    /* $(culture).change(function(){
     var id = $(culture).select2('data').id;
     var famillec = $(culture).select2('data').type;

     $("#agrobundle_itkismr_cULTUREID").val(id);

     $("#familleculture").val(famillec);

     $("#modal-warning1").modal();

     });*/

});

$(document).ready(function () {

    var familleculture = ("#familleculture1");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(familleculture).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getfamilleculture'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        initSelection: function (element, callback) {
            var data = {id: $("#culturebundle_culture_fAMILLECULTUREID").val(), name:  $("#namefamilleculture").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });

    /* $(culture).change(function(){
     var id = $(culture).select2('data').id;
     var famillec = $(culture).select2('data').type;

     $("#agrobundle_itkismr_cULTUREID").val(id);

     $("#familleculture").val(famillec);

     $("#modal-warning1").modal();

     });*/

});
/* ************************************************************ fin Famille cultural ************************************ */


$(document).ready( function () {

    var table = $('#example').DataTable({
        "columnDefs": [
            {
                "targets": [ 7,10,14 ],
                "visible": false,
                "searchable": false
            }
        ],
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            var total = api
                .column( 17 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            if (api.column(17, {search:'applied'}).data().length>0) {
                total_search = api
                    .column( 17, {search:'applied'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    } );
            } else {
                total_search = 0;
            };
            var pageTotal = api
                .column( 17, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            $( api.column( 17 ).footer() ).html(
                '<b>' +pageTotal.toFixed(0)+'</b>' +' /<br><b style="color: #607D8B"> '+ total_search.toFixed(0) +'</b> '+' /<br><b style="color: #607D8B"> '+ total.toFixed(0) +'</b> '
            );
        },
        "iDisplayLength": 10,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
        //"scrollX": true,
        'rowsGroup': [0, 1, 2, 3, 4, 5, 6, 7, 8,9,10,11, 12]
    });

    $('#cyclecultural').change(function () {
        var name = $('#cyclecultural').select2('data').name;
        table.columns(0).search( name ).draw();
    } );
    $('#ilot').change(function () {
        var name = $('#ilot').select2('data').name;
        table.columns(2).search( name ).draw();
    } );
    $('#site').change(function () {
        var name = $('#site').select2('data').name;
        table.columns(3).search( name ).draw();
    } );
    $('#ferme').change(function () {
        var name = $('#ferme').select2('data').name;
        table.columns(4).search( name ).draw();
    } );
    $('#familleculture').change(function () {
        var name = $('#familleculture').select2('data').name;
        table.columns(5).search( name ).draw();
    } );
    $('#culture').change(function () {
        var name = $('#culture').select2('data').name;
        table.columns(6).search( name ).draw();
    } );
    $('#geste').change(function () {
        var name = $('#geste').select2('data').name;
        table.columns(13).search( name ).draw();
    } );



});

$(document).ready( function () {

    var table = $('#example').dataTable();

    $('#debutsemaine').change(function(e){
        e.preventDefault();
        var startDate = $('#debutsemaine').val(),
            endDate = $('#finsemaine').val();

        if(startDate != null && endDate != null)
        {
            filterByDate(12, startDate, endDate); // We call our filter function

            table.fnDraw(); // Manually redraw the table after filtering
        }
    });

    $('#finsemaine').change(function(e){
        e.preventDefault();
        var startDate = $('#debutsemaine').val(),
            endDate = $('#finsemaine').val();

        if(startDate != null && endDate != null)
        {
            filterByDate(12, startDate, endDate); // We call our filter function

            table.fnDraw(); // Manually redraw the table after filtering
        }
    });

    $('#debutmois').change(function(e){
        e.preventDefault();
        var startDate = $('#debutmois').val(),
            endDate = $('#finmois').val();

        if(startDate != null && endDate != null)
        {
            filterByDate(11, startDate, endDate); // We call our filter function

            table.fnDraw(); // Manually redraw the table after filtering
        }
    });


    $('#finmois').change(function(e){
        e.preventDefault();
        var startDate = $('#debutmois').val(),
            endDate = $('#finmois').val();

        if(startDate != null && endDate != null)
        {
            filterByDate(11, startDate, endDate); // We call our filter function

            table.fnDraw(); // Manually redraw the table after filtering
        }
    });

    $('#clearFilter').on('click', function(e){
        location.reload();
    });


    var filterByDate = function(column, startDate, endDate) {
        // Custom filter syntax requires pushing the new filter to the global filter array
        $.fn.dataTableExt.afnFiltering.push(
            function( oSettings, aData, iDataIndex ) {
                var rowDate = aData[column],
                    start = startDate,
                    end = endDate;

                // If our date from the row is between the start and end
                if (start <= rowDate && rowDate <= end) {
                    return true;
                } else if (rowDate >= start && end === '' && start !== ''){
                    return true;
                } else return !!(rowDate <= end && start === '' && end !== '');
            }
        );
    };

});

$(document).ready(function() {

    var tableop = $('#tableop').DataTable({
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            var total = api
                .column( 10 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            if (api.column(10, {search:'applied'}).data().length>0) {
                total_search = api
                    .column( 10, {search:'applied'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    } );
            } else {
                total_search = 0;
            };

            var pageTotal = api
                .column( 10, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            $( api.column( 10 ).footer() ).html(
                '<b>' +pageTotal.toFixed(0)+'</b>' +' /<br>  <b style="color: #B71C1C"> '+ total_search.toFixed(0) +'</b> ' +' / <br> <b style="color: #607D8B"> '+ total.toFixed(0) +'</b> '
            );
        },
        "iDisplayLength": 15,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });

    $('#cyclecultural1').change(function () {
        var name = $('#cyclecultural1').select2('data').name;
        tableop.columns(0).search( name ).draw();
    } );
    $('#ilot1').change(function () {
        var name = $('#ilot1').select2('data').name;
        tableop.columns(2).search( name ).draw();
    } );
    $('#site1').change(function () {
        var name = $('#site1').select2('data').name;
        tableop.columns(3).search( name ).draw();
    } );
    $('#ferme1').change(function () {
        var name = $('#ferme1').select2('data').name;
        tableop.columns(4).search( name ).draw();
    } );
    $('#familleculture1').change(function () {
        var name = $('#familleculture1').select2('data').name;
        tableop.columns(5).search( name ).draw();
    } );

    $('#culture1').change(function () {
        var name = $('#culture1').select2('data').name;
        tableop.columns(6).search( name ).draw();
    } );
    $('#clearFilter1').on('click', function(e){
        location.reload();
    });
} );

$(document).ready(function() {
    $('#btnitkmoop').on('click', function(e){
        var op = $('#cyclecultural2').val();
        var rendement = $('#rendement').val();

        if(op != '' && rendement != '') {
            $('#table_itkmoop').DataTable({
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // converting to interger to find total
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
                    // Total over all pages
                    var total = api
                        .column( 5 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    var pageTotal = api
                        .column( 5, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                    $( api.column( 5 ).footer() ).html(
                        '<b>' +Number((pageTotal).toFixed(2))+'</b>' +' (<b style="color: #607D8B"> '+ Number((total).toFixed(2)) +'</b> totale) '
                    );
                },
                "ajax": {
                    "url":Routing.generate('itkmoop'),
                    "data": {
                        "op": op,
                        "rendement": rendement
                    }
                },
                "columns": [
                    { "data": "MOIS" },
                    { "data": "SEMAINE" },
                    { "data": "GESTE" },
                    { "data": "QUANTITE" },
                    { "data": "UNITE" },
                    { "data": "QUANTITET" }
                ],
                'rowsGroup': [0, 1],
            });
        }
        else
        {
            Notify('Un des champs est vide ! ', 'top-right', '5000', 'danger', 'fa-bolt', true);
        }
    } );
} );

$(document).ready(function() {
    $('#tableopgeste').DataTable({
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            var geste1 = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var geste2 = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var geste3 = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var geste4 = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var geste5 = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var geste6 = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var geste7 = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var geste8 = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var geste9 = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 0 ).footer() ).html('<b>Total</b>');
            $( api.column( 1 ).footer() ).html('<b style="color: #607D8B">'+Number((geste1).toFixed(2))+'</b>');
            $( api.column( 2 ).footer() ).html('<b style="color: #607D8B">'+Number((geste2).toFixed(2))+'</b>');
            $( api.column( 3 ).footer() ).html('<b style="color: #607D8B">'+Number((geste3).toFixed(2))+'</b>');
            $( api.column( 4 ).footer() ).html('<b style="color: #607D8B">'+Number((geste4).toFixed(2))+'</b>');
            $( api.column( 5 ).footer() ).html('<b style="color: #607D8B">'+Number((geste5).toFixed(2))+'</b>');
            $( api.column( 6 ).footer() ).html('<b style="color: #607D8B">'+Number((geste6).toFixed(2))+'</b>');
            $( api.column( 7 ).footer() ).html('<b style="color: #607D8B">'+Number((geste7).toFixed(2))+'</b>');
            $( api.column( 8 ).footer() ).html('<b style="color: #607D8B">'+Number((geste8).toFixed(2))+'</b>');
            $( api.column( 9 ).footer() ).html('<b style="color: #607D8B">'+Number((geste9).toFixed(2))+'</b>');
        }
    })
});

$(document).ready(function() {

    var semaine = $("#semaine");
    var ferme = $("#ferme2");

    $(semaine).change(function(){
        var semaineval = $(semaine).val();
        var fermeval = $(ferme).val();
        window.location.href = Routing.generate('phebdomadaire', {semaine:semaineval,ferme:fermeval})
    });

    $(ferme).change(function(){
        var fermeval = $(ferme).val();
        var semaineval = $(semaine).val();
        window.location.href = Routing.generate('phebdomadaire', {semaine:semaineval,ferme:fermeval})
    });

});