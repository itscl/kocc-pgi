$(function()
{
    $('#datep').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY H:m:s'
    });
    $('#datepfin').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY'
    });
    $('#agrobundle_itkism_dATEP').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY H:m:s'
    });
    $('#agrobundle_itkismr_dATEP').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY H:m:s'
    });

    $('#agrobundle_itkism_dATEPFIN').datetimepicker();

    $('#agrobundle_itkismr_dATEPFIN').datetimepicker();

    $('#agrobundle_itkismr_hEUREDEBUT').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY H:m:s'
    });

    //--Bootstrap Time Picker--
    var hdebut=$('#timepickerdebut');
    var hfin= $('#timepickerfin')
    /*$(hdebut).timepicker();*/
    hdebut.val('');
    //$(hfin).timepicker();
    hfin.val('');

    $("#btnaffiformilotsaisi").click(function(){
        document.getElementById("oldIlot").value=$("#il").val();
        document.getElementById("ilotconfirm").value='';
        $("#myModal").modal();
    });

    $("#annulerinstruction").click(function(){
        $("#modal-dangeropdiffuse").modal('hide');
        location.reload(true);
    });

    $("#realiserinstruction").click(function(){
        $("#modal-dangeropdiffuse").modal('hide');
        var id =document.getElementById('diffuse').value;
        //window.location.href = Routing.generate('itkismr_creer');
        location.href = Routing.generate('itkismr_creer');
    });

    $('#WiredWizard').wizard();
});


$("#agrobundle_itkism_eLigne").change(function()
{
    $("#agrobundle_itkism_eSEMIS").val('');
});

//$("#agrobundle_itkism_dATEP").change(function()
//{
//    var datep =$("#agrobundle_itkism_dATEP");
//    var data=datep.val();
//    var b = data.split(' ');
//    var dmy = b[0].split('-');
//    var hms = b[2].split(':');
//    data=dmy[2]+'-'+dmy[1]+'-'+dmy[0];
//    var date = new Date(data);
//    date.setDate(date.getDate()+parseInt(duree.val()));
//
//    var datedas=$("#das").val();
//
//    if(datedas!='')
//    {
//        var date1 = new Date(datedas);
//
//        var timedas = date1.getTime();
//
//        var timesemis = date.getTime();
//
//        if(timedas>timesemis)
//        {
//            $("#modal-dangerdas").modal();
//            datep.val('');
//        }
//    }
//});


$("#agrobundle_itkism_eSEMIS").change(function()
{
    var qtesem = $("#agrobundle_itkism_qTESEM");
    var qtesemtot = $("#agrobundle_itkism_qTESEMTOT");
    var surface=$("#surface").val();
    var eligne = $("#agrobundle_itkism_eLIGNE").val();
    var esemis = $("#agrobundle_itkism_eSEMIS").val();
    var rang = $("#agrobundle_itkism_rANGNB");
    if(surface.indexOf(',')!=-1)
    {
        surface=surface.replace(",", ".");
    }
    if(eligne.indexOf(',')!=-1)
    {
        eligne=eligne.replace(",", ".");
    }
    if(esemis.indexOf(',')!=-1)
    {
        esemis=esemis.replace(",", ".");
    }
    var qtesemdata = ((10000/eligne)*(10000/esemis)*(parseInt(rang.val())+1)).toFixed(2);
    var qtesemtotdata = (qtesemdata*surface).toFixed(2);

    qtesemdata=qtesemdata.toString().replace('.',',');
    qtesemtotdata=qtesemtotdata.toString().replace('.',',');
    qtesem.val(qtesemdata);
    qtesemtot.val(qtesemtotdata);
});

$("#agrobundle_itkismr_eSEMIS").change(function()
{
    var qtesem = $("#agrobundle_itkismr_qTESEM");
    var qtesemtot = $("#agrobundle_itkismr_qTESEMTOT");
    var surface=$("#surface").val();
    var eligne = $("#agrobundle_itkismr_eLIGNE").val();
    var esemis = $("#agrobundle_itkismr_eSEMIS").val();
    var rang = $("#agrobundle_itkismr_rANGNB");
    if(surface.indexOf(',')!=-1)
    {
        surface=surface.replace(",", ".");
    }
    if(eligne.indexOf(',')!=-1)
    {
        eligne=eligne.replace(",", ".");
    }
    if(esemis.indexOf(',')!=-1)
    {
        esemis=esemis.replace(",", ".");
    }
    var qtesemdata = ((10000/eligne)*(10000/esemis)*(parseInt(rang.val())+1)).toFixed(2);
    var qtesemtotdata = (qtesemdata*surface).toFixed(2);

    qtesem.val(qtesemdata);
    qtesemtot.val(qtesemtotdata);
});

$("#agrobundle_itkism_mODEAPPLICATION").change(function()
{
    var operateur = document.getElementById("agrobundle_itkism_aPPLICATEURID");
    var effectif = document.getElementById("effectif");
    var effectiform = document.getElementById("agrobundle_itkism_eFFECTIF");
    var modeapplication = $("#agrobundle_itkism_mODEAPPLICATION");
    var operateurlabel = document.getElementById("operateurlabel");
    var effectiflabel = document.getElementById("effectiflabel");

    if(modeapplication.val()=='Plantation Mecanique' || modeapplication.val()=='Semis Mecanique')
    {
        operateurlabel.style.color="red";
        effectiflabel.style.color="black";
        operateur.disabled = false;
        effectif.value="" ;
        effectif.blur();
        effectif.disabled = true;
        document.getElementById('meca').style.display='block';
    }
    else if(modeapplication.val()=='Plantation Manuelle' || modeapplication.val()=='Semis Manuel')
    {
        operateurlabel.style.color="black";
        effectiflabel.style.color="red";
        effectif.disabled = false;
        operateur.value = "";
        effectif.focus();
        effectif.select();
        operateur.disabled = true;
        document.getElementById('meca').style.display='none';
    }
});

$("#dose").change(function()
{
    var surface=$("#surface").val();
    if(surface.indexOf(',')!=-1)
    {
        surface=surface.replace(",", ".");
    }
    var qtetot=(parseFloat(surface)*parseFloat($("#dose").val())).toFixed(2);
    var prixtot=parseInt(qtetot*$("#prixunit").val());
    qtetot=qtetot.toString().replace('.',',');
    $("#qtetot").val(qtetot);
    $("#prixtot").val(prixtot);


});


$("#effectif").change(function()
{
    $("#agrobundle_itkism_eFFECTIF").val($("#effectif").val());
});

$("#surface").change(function()
{
    $("#agrobundle_itkism_sURFACE").val($("#surface").val());
});

$("#surfacetot").change(function()
{
    $("#agrobundle_itkism_sURFACETOT").val($("#surfacetot").val());
});

$("#qtesem").change(function()
{
    $("#agrobundle_itkism_qTESEM").val($("#qtesem").val());
});

$("#qtesemtot").change(function()
{
    $("#agrobundle_itkism_qTESEMTOT").val($("#qtesemtot").val());
});

$("#checkedlot").change(function()
{
    var checked =document.getElementById('checkedlot').checked;
    if(checked==true)
    {
        select2lotsemenceimporte();
        $("#code_cv").text('Code article : ');
    }
    else
    {
        select2lotsemence();
        $("#code_cv").text('Culture variété : ');
    }
});

function getop(str)
{
    var datas = "opid="+str;
    var fc =  $("#fc");
    var cv =  $("#cuva");
    var ti =  $("#ti");
    var il =  $("#il");
    var rp =  $("#rp");
    var das =  $("#das");
    var dar =  $("#dar");
    var ddr =  $("#ddr");
    var cvhidden = $("#cvhidden");
    var ilothidden = $("#ilothidden");
    $.ajax({
        url:  Routing.generate('getop'),
        type: "GET",
        data: datas,
        context: this,
        error: function () {},
        dataType: 'text',
        success : function (data) {
            var json_obj = $.parseJSON(data);
            fc.val(json_obj[0].FAMILLECULTURE);
            il.val(json_obj[0].ILOT);
            ilothidden.val(json_obj[0].ILOT);
            ti.val(json_obj[0].TYPEIMPLANTATION);
            cv.val(json_obj[0].CULTUREVARIETE);

            document.getElementById("ilotid").value = json_obj[0].ILOTID;

            getsurfacetot(json_obj[0].ILOTID);

            select2CultureVariete(json_obj[0].CULTUREVARIETE);

            if(json_obj[0].DATEPREVISRECOLTE!=null)
            {
                var date=new Date(json_obj[0].DATEPREVISRECOLTE.date);

                if(date.getMonth()<9)
                {
                    if(date.getDate()<9)
                    {
                        rp.val('0'+date.getDate()+'-0'+(date.getMonth()+1)+'-'+date.getFullYear());
                    }
                    else
                    {
                        rp.val(date.getDate()+'-0'+(date.getMonth()+1)+'-'+date.getFullYear());
                    }
                }
                else
                {
                    if(date.getDate()<9)
                    {
                        rp.val('0'+date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear());
                    }
                    else
                    {
                        rp.val(date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear());
                    }
                }
            }
        }
    });
}

function getsurfacetot(str)
{
    var datas = "ilotid="+str;
    var surface=$("#surface");
    var surfacetot=$("#surfacetot");
    var surfacetotform = $("#agrobundle_itkism_sURFACETOT");
    var surfaceform=$("#agrobundle_itkism_sURFACE");
    var percentsurface = $("#percentsurface");
    $.ajax({
        url:  Routing.generate('getsurfacetot'),
        type: "GET",
        data: datas,
        context: this,
        error: function () {},
        dataType: 'text',
        success : function (data) {
            var json_obj = $.parseJSON(data);
            var surfacetotale=0;
            for(var i=0; i<json_obj.length; i++)
            {
                var surfaceparcelle=json_obj[i]['SURFACE'].replace(",", ".");
                surfacetotale=surfacetotale+parseFloat(surfaceparcelle);
            }
            surface.val(surfacetotale);
            surfacetot.val(surfacetotale);
            if(surfaceform.val()!=null)
            {
                surfaceform.val(surfacetotale);
                surfacetotform.val(surfacetotale);
            }
            percentsurface.val('100%');
        }
    });
}

function getfamilleculture(str)
{
    var datas = "cvid="+str;
    var fc=$("#fc");
    $.ajax({
        url:  Routing.generate('getfamilleculture'),
        type: "GET",
        data: datas,
        context: this,
        error: function () {},
        dataType: 'text',
        success : function (data) {
            var json_obj = $.parseJSON(data);
            fc.val(json_obj[0].FAMILLECULTURE);
            document.getElementById("ilot").value = '-'+json_obj[0].codevariete;
        }
    });
}

function getdas(str)
{
    var datas = "opid="+str;
    var das =  $("#das");
    var dar =  $("#dar");
    var ddr =  $("#ddr");
    $.ajax({
        url:  Routing.generate('getdas'),
        type: "GET",
        data: datas,
        context: this,
        error: function () {},
        dataType: 'json',
        success : function (data) {
            das.val(data[0]["RESULT"]);
        }
    });
}

function showitkismr(str1) {
    /* if (str1=="") {
     document.getElementById("txtHint").innerHTML="";
     return;
     }*/
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("divetape1").innerHTML=this.responseText;
        }
    }
    xmlhttp.open("GET",Routing.generate('itkismr_creer')+"?id="+str1,true);
    xmlhttp.send();
}

function genererITKISMR()
{

    var elem = $('#instructionid').select2('data');
    if(elem!==null)
    {
        bootbox.confirm({
            message: "Etes vous sûr de vouloir réaliser cette instruction ?",
            buttons: {
                confirm: {
                    label: 'Ok',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-default'
                }
            },
            callback: function (result)
            {
                if (result === true) {
                    if(elem.name === 'SEMIS')
                    {
                        var datas = "idinstruction="+elem.id;
                        $.ajax({
                            type: "POST",
                            url: Routing.generate('generer'),
                            data: datas,
                            dataType: "html"
                        }).done(function (msg) {
                            window.location.href = Routing.generate('modifier_itkismr', {id:elem.id})
                        }).fail(function () {
                            alert("error");
                        });
                    }
                    else
                    {
                        alert(elem.name+'  A venir');
                    }
                }
            }
        });
    }
}

function updateilotop(str)
{
    var id = str;
    var ilot = $("#ilot").val();
    var ilotconfirm = $("#ilotconfirm").val();

    var datas ="ilotid="+str+"&ilot="+ilot +"&ilotconfirm="+ilotconfirm ;

    if(ilot != null && ilotconfirm != null)
    {
        if(ilot == ilotconfirm)
        {
            $.ajax({
                type: "POST",
                url: Routing.generate('modifierilotop'),
                data: datas,
                dataType: "html"
            }).done(function (msg) {
                location.reload(true);
            }).fail(function () {
                alert("error");
            });
        }
        else
        {
            $("#modal-danger").modal();
        }
    }
}

function updateilotopsaisi(str)
{
    var id = str;
    var ilot = $("#ilot").val();
    var ilotconfirm = $("#ilotconfirm").val();

    var datas ="ilotid="+str+"&ilot="+ilot +"&ilotconfirm="+ilotconfirm ;

    if(ilot != null && ilotconfirm != null)
    {
        if(ilot === ilotconfirm)
        {
            $.ajax({
                type: "POST",
                url: Routing.generate('modifierilotopsaisi'),
                data: datas,
                dataType: "html"
            }).done(function (msg) {
                document.getElementById("il").value = ilot;
                $('#myModal').modal('hide');
                $("#modal-success1").modal();
            }).fail(function () {
                alert("error");
            });
        }
        else
        {
            $("#modal-danger").modal();
        }
    }
}

function getinstructiondiffuseop(str)
{
    var datas = "opid="+str;
    $.ajax({
        url:  Routing.generate('getinstructiondiffuseop'),
        type: "GET",
        data: datas,
        context: this,
        error: function () {},
        dataType: 'text',
        success : function (data) {
            var json_obj = $.parseJSON(data);
            var opdiffuse = json_obj[0]["id"];
            if(opdiffuse!=null)
            {
                document.getElementById('diffuse').value=opdiffuse;
                $("#modal-dangeropdiffuse").modal();
            }
        }
    });
}

function updatesurfaceitkismr(str)
{

    var surface=$("#surface");
    var qtesem = $("#agrobundle_itkismr_qTESEM").val();

    var qtesemtot = $("#agrobundle_itkismr_qTESEMTOT");

    var parcelles = document.getElementById('parcellesitkismr').value;
    parcelles = parcelles.split('[')[1];
    parcelles = parcelles.split(']')[0];
    parcelles = parcelles.split(',');
    var surfacetot=0;

    for (var i = 0; i < parcelles.length; i++) {
        var surfaceparcelle = document.getElementById("surface" + parcelles[i]).value;
        var checked = document.getElementById("checked" + parcelles[i]).checked;
        if(checked==true)
        {
            if(surfaceparcelle.indexOf(',')!=-1)
            {
                surfaceparcelle=surfaceparcelle.replace(',','.');
                surfacetot=parseFloat(surfacetot)+parseFloat(surfaceparcelle);
            }
            else
            {
                surfacetot=surfacetot+parseFloat(surfaceparcelle)
            }
        }
    }

    if(qtesem.indexOf(',')!=-1)
    {
        qtesem.replace(',','.');
    }

    var qtesemtotdata=((surfacetot*parseFloat(qtesem)).toFixed(2)).toString().replace('.',',');
    qtesemtot.val(qtesemtotdata);

    var qtetot=surfacetot*parseFloat($("#dose").val());
    $("#qtetot").val(qtetot);
    var prixtot=qtetot*$("#prixunit").val();
    $("#prixtot").val(prixtot);

    surfacetot=surfacetot.toString().replace('.',',');
    $("#surfaceint").val(surfacetot);
    document.getElementById('surface').value=surfacetot;
}

function updatesurfaceitkism(str)
{
    var surface=$("#surface");
    var qtesem = $("#agrobundle_itkism_qTESEM").val();

    var qtesemtot = $("#agrobundle_itkism_qTESEMTOT");

    var parcelles = document.getElementById('parcellesitkism').value;
    parcelles = parcelles.split('[')[1];
    parcelles = parcelles.split(']')[0];
    parcelles = parcelles.split(',');
    var surfacetot=0;

    for (var i = 0; i < parcelles.length; i++) {
        var surfaceparcelle = document.getElementById("surface" + parcelles[i]).value;
        var checked = document.getElementById("checked" + parcelles[i]).checked;
        console.log('a'+checked+'a');
        if(checked==true)
        {
            if(surfaceparcelle.indexOf(',')!=-1)
            {
                surfaceparcelle=surfaceparcelle.replace(',','.');
                surfacetot=parseFloat(surfacetot)+parseFloat(surfaceparcelle);
            }
            else
            {
                surfacetot=surfacetot+parseInt(surfaceparcelle)
            }
        }
    }

    if(qtesem.indexOf(',')!=-1)
    {
        qtesem.replace(',','.');
    }

    var qtesemtotdata=((surfacetot*parseFloat(qtesem)).toFixed(2)).toString().replace('.',',');
    qtesemtot.val(qtesemtotdata);

    var qtetot=surfacetot*parseFloat($("#dose").val());
    $("#qtetot").val(qtetot);
    var prixtot=qtetot*$("#prixunit").val();
    $("#prixtot").val(prixtot);

    var surfacetotitkism = $("#surfacetot").val();
    surfacetotitkism = surfacetotitkism.replace(',','.');
    var percentsurface = (surfacetot*100)/parseFloat(surfacetotitkism);
    surfacetot=surfacetot.toString().replace('.',',');
    $("#surfaceint").val(surfacetot);

    $("#percentsurface").val(percentsurface);
    document.getElementById('surface').value=surfacetot;
}

function updatelotsemence(str)
{
    var ls=$("#ls");
    var quantite=$("#quantite").val();
    var unite=$("#unite").val();
    var cvsem=$("#cvsem").val();
    var libelle=$("#libelle").val();
    var semencelotid=$("#semencelotid").val();
    var datas="itkismid="+str+"&quantite="+quantite+"&unite="+unite+"&LOTNO="+cvsem+"&libelle="+libelle+"&semencelotid="+semencelotid;
    if(quantite==='' || $(ls).select2('data')===null)
    {
        $("#modal-warning-itkism").modal();
    }
    else
    {
        $.ajax
        ({
            type: "POST",
            url: Routing.generate('updatelotsemence'),
            data: datas,
            dataType: "html",
            success: function ()
            {
                location.href = Routing.generate('ajouterintrant', {
                    'id': str
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                alert("Error: " + errorThrown);
            }
        });
    }
}

function updateintrant(str)
{
    var dose=$("#dose").val();
    var prixunit=$("#prixunit").val();
    var surfaceint=$("#surface").val();
    var qtetot=$("#qtetot").val();
    var prixtot=$("#prixtot").val();
    var commentaire =$("#comment").val();
    var unite =$("#uniteint").val();
    var intrantid=$("#intrant");
    if($(intrantid).val()=="")
    {
        intrantid=$("#intrantid");
    }
    var datas = "itkismid="+str+"&intrantid="+$(intrantid).val()+"&unite="+unite+"&dose="+dose+"&prixunit="+prixunit+"&surfaceint="+surfaceint+"&qtetot="+qtetot+"&prixtot="+prixtot+"&commentaire="+commentaire;

    if(dose=='' || $(intrantid).select2('data')==null)
    {
        $("#modal-warning-itkism").modal();
    }
    else
    {
        $.ajax
        ({
            type: "POST",
            url: Routing.generate('updateintrant'),
            data: datas,
            dataType: "html",
            success: function ()
            {
                location.href = Routing.generate('ajouterepisemis', {
                    'id': str
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                alert("Error: " + errorThrown);
            }
        });
    }
}

function updateepi(str)
{
    var effectif=document.getElementById('effectif').value;
    var datas="itkismid="+str;
    var cdebut=$("#cdebut").val();
    var cfin=$("#cfin").val();
    var materielidepi=$("#materielepi");
    if($(materielidepi).val()=="")
    {
        materielidepi=$("#materielidepi");
    }
    var appareilid=$("#appareil");
    if($(appareilid).val()=="")
    {
        appareilid=$("#appareilid");
    }
    datas = datas+"&type=mecanique"+"&materielid="+$(materielidepi).val()+"&appareilid="+$(appareilid).val()+"&cdebut="+cdebut+"&cfin="+cfin+"&epi=epi";

    if(cdebut=='' || cfin=='' || $(materielidepi).select2('data')==null || $(appareilid).select2('data')==null)
    {
        $("#modal-warning-itkism").modal();
    }
    else
    {
        $.ajax
        ({
            type: "POST",
            url: Routing.generate('updateepi'),
            data: datas,
            dataType: "html",
            success: function ()
            {
                location.href = Routing.generate('instructionsemis');
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                alert("Error: " + errorThrown);
            }
        });
    }
}

function updateepimanuel(str)
{
    var datas="itkismid="+str;
    var epiid=$("#epi");

    if($(epiid).select2('data').id==undefined)
    {
        $("#modal-warning-itkism").modal();
    }
    else
    {
        var epi = $(epiid).select2('data').name;
        datas = datas+"&type=manuel"+"&epiid="+$(epiid).select2('data').id;
        $.ajax
        ({
            type: "POST",
            url: Routing.generate('updateepi'),
            data: datas,
            dataType: "html",
            success: function ()
            {
                var elem = document.getElementById('tableepi');
                elem.innerHTML=elem.innerHTML+ '<tr id='+$(epiid).select2('data').id+'>'+' <td>'+epi+'</td> <td><button type="button"' +
                    ' class="btn btn-danger btn-sm"  onclick=" supprimerepiitkism('+$(epiid).select2('data').id+','+str+ ');'+'"'+' >'+'<i' +
                    ' class="fa fa-trash"'+'></i>'+'Supprimer'+'</button></td></tr>';
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                $("#modal-danger-epi").modal();
            }
        });
    }
}

function supprimerepiitkism(epiid, itkismid)
{
    var datas = "itkismid="+itkismid+"&epiid="+epiid;

    $.ajax
    ({
        type: "POST",
        url: Routing.generate('supprimerepiitkism'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            var elem = document.getElementById(epiid);
            elem.remove();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error: " + errorThrown);
        }
    });
}

function realiseritkismr()
{

    bootbox.confirm({
        message: "Voulez vous enregistrer ces données ?",
        buttons: {
            confirm: {
                label: 'Ok',
                className: 'btn-primary'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result) {
            if (result === true) {
                var id = $("#itkismrid").val();
                //var cv = $("#agrobundle_itkismr_cULTUREID").val();
                var cv = $("#cv").select2('data');
                var cvid='';
                if(cv === undefined)
                {
                    cvid = $("#cvid").val();
                }
                else
                {
                    cvid = cv.id;
                }
                //var surface = $("#agrobundle_itkismr_sURFACE").val();
                var surface = $("#surface").val();
                var eligne = $("#agrobundle_itkismr_eLIGNE").val();
                var esemis = $("#agrobundle_itkismr_eSEMIS").val();
                var rang = $("#agrobundle_itkismr_rANGNB").val();
                var qtesem = $("#agrobundle_itkismr_qTESEM").val();
                var qtesemtot = $("#agrobundle_itkismr_qTESEMTOT").val();
                var datep = $("#agrobundle_itkismr_dATEP").val();
                var datepfin = $("#agrobundle_itkismr_dATEPFIN").val();
                var duree = $("#agrobundle_itkismr_dUREE").val();

                var datas = "id="+id+"&cvid="+cvid+"&surface="+surface+"&eligne="+eligne+"&esemis="+esemis+"&rang="+rang+"&qtesem="+qtesem+"&qtesemtot="+qtesemtot+"&datep="+datep+"&datepfin="+datepfin+"&duree="+duree;

                var parcelles = document.getElementById('parcellesitkismr').value;
                parcelles = parcelles.split('[')[1];
                parcelles = parcelles.split(']')[0];
                parcelles = parcelles.split(',');

                for (var i = 0; i < parcelles.length; i++) {
                    var surfaceparcelle = document.getElementById("surface" + parcelles[i]).value;
                    var checked = document.getElementById("checked" + parcelles[i]).checked;
                    datas = datas + "&id" + i + "=" + parcelles[i] + "&surface" + parcelles[i] + "=" + surfaceparcelle + "&checked" + parcelles[i] + "=" + checked ;
                }

                //var ls=$("#ls").select2('data');
                var quantite=$("#quantite").val();
                var unite=$("#unite").val();
                var cvsem=$("#cvsem").val();
                var libelle=$("#libelle").val();
                var semencelotid=$("#semencelotid").val();

                datas=datas+"&quantite="+quantite+"&unite="+unite+"&LOTNO="+cvsem+"&libelle="+libelle+"&semencelotid="+semencelotid;

                var dose=$("#dose").val();
                var prixunit=$("#prixunit").val();
                var surfaceint=$("#surfaceint").val();
                var qtetot=$("#qtetot").val();
                var prixtot=$("#prixtot").val();
                var uniteint=$("#uniteint").val();
                var commentaire =$("#comment").val();
                var intrantid=$("#intrant").select2('data');

                datas = datas+"&uniteint="+uniteint+"&dose="+dose+"&prixunit="+prixunit+"&surfaceint="+surfaceint+"&qtetot="+qtetot+"&prixtot="+prixtot+"&commentaire="+commentaire;

                if(eligne =='' || esemis=='' || datep=='' || datepfin=='' || duree=='' || dose=='' || quantite=='' || intrantid==undefined || intrantid.name=='' || modeapplication=='')
                {
                    $("#modal-warning-itkism-saisi").modal();
                }
                else
                {
                    if(intrantid.id=='')
                    {
                        intrantid.id=$("#intrantid").val();
                    }

                    datas = datas+"&intrantid="+intrantid.id;

                    var modeapplication=document.getElementById('modeapplication').value;

                    if(modeapplication=='Plantation Mecanique' || modeapplication=='Semis Mecanique')
                    {
                        var materielid=$("#materielepi").select2('data');
                        var appareilid=$("#appareil").select2('data');
                        var cdebut=$("#cdebut").val();
                        var cfin=$("#cfin").val();

                        if(materielid==undefined || appareilid==undefined || materielid.name=='' || appareilid.name=='')
                        {
                            $("#modal-warning-itkism-saisi").modal();
                        }
                        else
                        {
                            if(materielid.id=='')
                            {
                                materielid.id=$("#materielidepi").val();
                            }
                            if(appareilid.id=='')
                            {
                                appareilid.id=$("#appareilid").val();
                            }
                            datas = datas+"&type=mecanique"+"&materielid="+materielid.id+"&appareilid="+appareilid.id+"&cdebut="+cdebut+"&cfin="+cfin;

                        }
                    }
                    else
                    {
                        var effectif = $("#effectif").val();
                        if(effectif=='')
                        {
                            $("#modal-warning-itkism-saisi").modal();
                        }
                        else
                        {
                            datas = datas + "&effectif=" + effectif;
                        }
                    }

                    //var heuredebut = document.getElementById('timepickerdebut').value;
                    //heuredebut = convertTo24Hour(heuredebut.toLowerCase());
                    //var heurefin = document.getElementById('timepickerfin').value;
                    //heurefin = convertTo24Hour(heurefin.toLowerCase());
                    var heuredebut = document.getElementById('hdebut').value;
                    var heurefin = document.getElementById('hfin').value;
                    datas=datas+"&heuredebut="+heuredebut+"&heurefin="+heurefin;
                    $.ajax
                    ({
                        type: "POST",
                        url: Routing.generate('updateitkismr'),
                        data: datas,
                        dataType: "html",
                        success: function ()
                        {
                            $("#modal-success-itkismr").modal();
                            location.href = Routing.generate('instructionsemis');
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown)
                        {
                            alert("Error itkismr: " + errorThrown);
                        }
                    });
                }
            }
        }
    });
}

$("#finishitkism").click(function() {
    if (this.innerText === "TERMINER" ) {
        updateitkism();
    }
    else if (this.innerText === "FIN" ) {
        location.href = Routing.generate('instructionsemis');
    }
});

$("#finishitkismr").click(function() {
    if (this.innerText === "TERMINER" ) {
        realiseritkismr();
    }
});

function updateitkism()
{

    bootbox.confirm({
        message: "Voulez vous enregistrer ces données ?",
        buttons: {
            confirm: {
                label: 'Ok',
                className: 'btn-primary'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result) {
            if (result == true) {
                var id = $("#itkismid").val();
                var tmvegetal = $("#agrobundle_itkism_tYPEMVEGETAL").val();
                var prescripteurid = $("#agrobundle_itkism_pRESCRIPTEURID").val();
                var modeapplication = $("#agrobundle_itkism_mODEAPPLICATION").val();
                var datas = "id="+id+"&prescripteurid="+prescripteurid+"&tmvegetal="+tmvegetal+"&modeapplication="+modeapplication;

                var surface = $("#surface").val();
                var surfacetot = $("#surfacetot").val();
                var eligne = $("#agrobundle_itkism_eLIGNE").val();
                var esemis = $("#agrobundle_itkism_eSEMIS").val();
                var rang = parseInt($("#agrobundle_itkism_rANGNB").val())+1;
                var qtesem = $("#agrobundle_itkism_qTESEM").val();
                var qtesemtot = $("#agrobundle_itkism_qTESEMTOT").val();
                var datep = $("#agrobundle_itkism_dATEP").val();
                var duree = $("#agrobundle_itkism_dUREE").val();


                var cv = $("#cv").select2('data');
                var cvid='';
                if(cv==undefined)
                {
                    cvid = $("#cvid").val();
                }
                else
                {
                    cvid = cv.id;
                }

                datas = datas + "&cvid="+cvid+"&surface="+surface+"&surfacetot="+surfacetot+"&eligne="+eligne+"&esemis="+esemis+"&rang="+rang+"&qtesem="+qtesem+"&qtesemtot="+qtesemtot+"&datep="+datep+"&duree="+duree;

                var parcelles = document.getElementById('parcellesitkism').value;
                parcelles = parcelles.split('[')[1];
                parcelles = parcelles.split(']')[0];
                parcelles = parcelles.split(',');


                var num = parcelles.length;
                var datasparcelle = "num="+num+"&itkismid="+id;

                for (var i = 0; i < parcelles.length; i++) {
                    var surfaceparcelle = document.getElementById("surface" + parcelles[i]).value;
                    var checked = document.getElementById("checked" + parcelles[i]).checked;
                    datasparcelle = datasparcelle + "&id" + i + "=" + parcelles[i] + "&surface" + parcelles[i] + "=" + surfaceparcelle + "&checked" + parcelles[i] + "=" + checked ;
                }


                var ls=$("#ls").select2('data');
                var quantite=$("#quantite").val();
                var unite=$("#unite").val();
                var cvsem=$("#cvsem").val();
                var libelle=$("#libelle").val();
                var semencelotid=$("#semencelotid").val();
                var dataslotsemence="itkismid="+id+"&ls="+ls.id+"&quantite="+quantite+"&unite="+unite+"&LOTNO="+cvsem+"&libelle="+libelle+"&semencelotid="+semencelotid;


                var dose=$("#dose").val();
                var prixunit=$("#prixunit").val();
                var qtetot=$("#qtetot").val();
                var prixtot=$("#prixtot").val();
                var uniteint=$("#uniteint").val();
                var commentaire =$("#comment").val();
                var intrantid=$('#intrant').select2('data');
                var datasintrant = "itkismid="+id+"&surface="+surface+"&intrantid="+intrantid.id+"&uniteint="+uniteint+"&dose="+dose+"&prixunit="+prixunit+"&qtetot="+qtetot+"&prixtot="+prixtot+"&commentaire="+commentaire;

                if(id=='' || tmvegetal=='' || eligne =='' || esemis=='' || datep=='' || duree=='' || quantite=='' || intrantid==undefined || intrantid.name=='' || prescripteurid == undefined || ls ==undefined || ls.name=='' ||  dose=='' ||modeapplication=='')
                {
                    $("#modal-warning-itkism-saisi").modal();
                }
                else
                {

                    $.ajax
                    ({
                        type: "POST",
                        url: Routing.generate('updateparcelles'),
                        data: datasparcelle,
                        dataType: "html",
                        success: function (){},
                        error: function (XMLHttpRequest, textStatus, errorThrown)
                        {
                            alert("Error parcelles: " + errorThrown);

                        }
                    });

                    if(intrantid.id=='')
                    {
                        intrantid.id=$("#intrantid").val();
                    }

                    $.ajax
                    ({
                        type: "POST",
                        url: Routing.generate('updatelotsemence'),
                        data: dataslotsemence,
                        dataType: "html",
                        success: function (){},
                        error: function (XMLHttpRequest, textStatus, errorThrown)
                        {
                            alert("Error lot semence: " + errorThrown);
                        }
                    });

                    $.ajax
                    ({
                        type: "POST",
                        url: Routing.generate('updateintrant'),
                        data: datasintrant,
                        dataType: "html",
                        success: function (){},
                        error: function (XMLHttpRequest, textStatus, errorThrown)
                        {
                            alert("Error Intrants: " + errorThrown);
                        }
                    });

                    if(modeapplication=='Plantation Mecanique' || modeapplication=='Semis Mecanique')
                    {
                        var materielid=$("#materielepi").select2('data');
                        var appareilid=$("#appareil").select2('data');
                        var applicateurid=$("#agrobundle_itkism_aPPLICATEURID").val();
                        var datasmateriel = "itkismid="+id;
                        if(materielid==undefined || appareilid==undefined || materielid.name=='' || appareilid.name=='' || applicateurid=='')
                        {
                            $("#modal-warning-itkism-saisi").modal();
                        }
                        else
                        {
                            if(materielid.id=='')
                            {
                                materielid.id=$("#materielidepi").val();
                            }
                            if(appareilid.id=='')
                            {
                                appareilid.id=$("#appareilid").val();
                            }
                            datasmateriel = datasmateriel+"&type=mecanique"+"&materielid="+materielid.id+"&appareilid="+appareilid.id;
                            datas = datas + "&applicateurid=" + applicateurid;

                            $.ajax
                            ({
                                type: "POST",
                                url: Routing.generate('updateepi'),
                                data: datasmateriel,
                                dataType: "html",
                                success: function ()
                                {
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown)
                                {
                                    alert("Error Materiel: " + errorThrown);
                                }
                            });

                            $.ajax
                            ({
                                type: "POST",
                                url: Routing.generate('updateitkism'),
                                data: datas,
                                dataType: "html",
                                success: function ()
                                {
                                    $("#modal-success-itkism").modal();
                                    location.href = Routing.generate('instructionsemis');
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown)
                                {
                                    alert("Error itkism: " + errorThrown);
                                }
                            });
                        }
                    }
                    else
                    {
                        var effectif = $("#effectif").val();
                        if(effectif=='')
                        {
                            $("#modal-warning-itkism-saisi").modal();
                        }
                        else
                        {
                            datas = datas + "&effectif=" + effectif;

                            $.ajax
                            ({
                                type: "POST",
                                url: Routing.generate('updateitkism'),
                                data: datas,
                                dataType: "html",
                                success: function ()
                                {
                                    $("#modal-success-itkism").modal();
                                    location.href = Routing.generate('instructionsemis');
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown)
                                {
                                    alert("Error itkism: " + errorThrown);
                                }
                            });
                        }
                    }

                }
            }
        }
    });
}

function updateepir(str)
{
    var epiid=$("#epi");
    var datas="itkismrid="+str;
    if($(epiid).select2('data').id==undefined)
    {
        $("#modal-warning-itkism").modal();
    }
    else
    {
        var epi = $(epiid).select2('data').name;
        datas = datas+"&type=manuel"+"&epiid="+$(epiid).select2('data').id;
        $.ajax
        ({
            type: "POST",
            url: Routing.generate('updateepir'),
            data: datas,
            dataType: "html",
            success: function ()
            {
                var elem = document.getElementById('tableepir');
                elem.innerHTML=elem.innerHTML+ '<tr id='+$(epiid).select2('data').id+'>'+' <td>'+epi+'</td> <td><button type="button"' +
                    ' class="btn btn-danger btn-sm"  onclick=" supprimerepir('+$(epiid).select2('data').id+','+str+ ');'+'"'+' >'+'<i' +
                    ' class="fa fa-trash"'+'></i>'+'Supprimer'+'</button></td></tr>';
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                $("#modal-danger-epi").modal();
            }
        });
    }
}

function checkdas()
{

    var duree = $("#agrobundle_itkism_dUREE");
    var datep =$("#agrobundle_itkism_dATEP");
    var datepfin =$("#agrobundle_itkism_dATEPFIN");
    //var datepfin =$("#datefinsemis");
    var dated=datep.val();
    var b = dated.split('  ');
    var dmy = b[0].split('-');
    var hms = b[1].split(':');
    var data=dmy[2]+'-'+dmy[1]+'-'+dmy[0];
    var date = new Date(data);

    date.setHours(hms[0],hms[1], hms[2]);

    var datedas=$("#das").val();

    if(datedas!='' )
    {
        b = datedas.split(' ');
        dmy = b[0].split('-');
        hms = b[1].split(':');
        data=dmy[2]+'-'+dmy[1]+'-'+dmy[0];
        var date1 = new Date(data);
        date1.setHours(hms[0],hms[1], hms[2]);

        var timedas = date1.getTime();

        var timesemis = date.getTime();

        if(timedas>timesemis)
        {
            $("#modal-dangerdas").modal();
            duree.val('');
            datepfin.val('');
            datep.val('');
        }
    }
}

function checkdasr()
{

    var duree = $("#agrobundle_itkismr_dUREE");
    var datep =$("#agrobundle_itkismr_dATEP");
    var datepfin =$("#agrobundle_itkismr_dATEPFIN");
    //var datepfin =$("#datefinsemis");
    var dated=datep.val();
    var b = dated.split('  ');
    var dmy = b[0].split('-');
    var hms = b[1].split(':');
    var data=dmy[2]+'-'+dmy[1]+'-'+dmy[0];
    var date = new Date(data);

    date.setHours(hms[0],hms[1], hms[2]);

    var datedas=$("#das").val();

    if(datedas!='' )
    {
        b = datedas.split(' ');
        dmy = b[0].split('-');
        hms = b[1].split(':');
        data=dmy[2]+'-'+dmy[1]+'-'+dmy[0];
        var date1 = new Date(data);
        date1.setHours(hms[0],hms[1], hms[2]);

        var timedas = date1.getTime();

        var timesemis = date.getTime();

        if(timedas>timesemis)
        {
            $("#modal-dangerdas").modal();
            duree.val('');
            datepfin.val('');
            datep.val('');
        }
    }
}

$(document).ready(function () {
    var duree = $("#agrobundle_itkism_dUREE");
    var datep =$("#agrobundle_itkism_dATEP");
    var datepp = document.getElementById("agrobundle_itkism_dATEP");
    var datepfin =$("#agrobundle_itkism_dATEPFIN");
    $(duree).change(function() {
        var dated=datep.val();
        var b = dated.split('  ');
        var dmy = b[0].split('-');
        var hms = b[1].split(':');
        var data=dmy[2]+'-'+dmy[1]+'-'+dmy[0];
        var date = new Date(data);

        var timedatep = date.getTime();
        var today = new Date();
        var timetoday = today.getTime();

        if(timedatep < timetoday)
        {
            $("#modal-warning-date").modal();
        }
        else
        {
            date.setHours(hms[0],hms[1], hms[2]);
            //var datedebut=datep.val();
            date.setDate(date.getDate()+(parseInt(duree.val())-1));
            var day =date.getDate();
            var month = date.getMonth();
            var hours = hms[0];
            var minutes = hms[1];
            var seconds = hms[2];
            if(day<10)
                day='0'+day;
            if(month<10)
            {
                month = parseInt(month) + 1;
                month = '0' + month;
            }

            datepfin.val(day+'-'+month+'-'+date.getFullYear()+'   '+hours+':'+minutes+':'+seconds);
            checkdas();
        }
    });
});

$(document).ready(function () {
    var duree = $("#agrobundle_itkismr_dUREE");
    var datep =$("#agrobundle_itkismr_dATEP");
    var datepp = document.getElementById("agrobundle_itkismr_dATEP");
    var datepfin =$("#agrobundle_itkismr_dATEPFIN");
    $(duree).change(function() {
        var dated=datep.val();
        var b = dated.split('  ');
        var dmy = b[0].split('-');
        var hms = b[1].split(':');
        var data=dmy[2]+'-'+dmy[1]+'-'+dmy[0];
        var date = new Date(data);

        date.setHours(hms[0],hms[1], hms[2]);

        //var datedebut=datep.val();
        date.setDate(date.getDate()+(parseInt(duree.val())-1));
        var day =date.getDate();
        var month = date.getMonth();
        var hours = hms[0];
        var minutes = hms[1];
        var seconds = hms[2];
        if(day<10)
            day='0'+day;
        if(month<10)
        {
            month = parseInt(month) + 1;
            month = '0' + month;
        }

        datepfin.val(day+'-'+month+'-'+date.getFullYear()+'   '+hours+':'+minutes+':'+seconds);
        checkdasr();
    });
});

function supprimerepir(epiid, itkismrid)
{
    var datas="itkismrid="+itkismrid+"&epiid="+epiid;
    $.ajax
    ({
        type: "POST",
        url: Routing.generate('supprimerepir'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            var elem = document.getElementById(epiid);
            elem.remove();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error: " + errorThrown);
        }
    });
}

function annuleritkism(itkismid)
{
    bootbox.confirm({
        message: "Etes vous sûr de vouloir annuler cette instruction ?",
        buttons: {
            confirm: {
                label: 'Ok',
                className: 'btn-primary'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result) {
            if (result === true)
            {
                var datas="id="+itkismid;
                $.ajax
                ({
                    type: "POST",
                    url: Routing.generate('annuleritkism'),
                    data: datas,
                    dataType: "html",
                    success: function ()
                    {
                        $("#modal-success-cancel-itkism").modal();
                        location.href = Routing.generate('instructionsemis', {
                            'id': itkismid
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown)
                    {
                        alert("Error: " + errorThrown);
                    }
                });
            }
        }
    });
}

function diffuseritkism(itkismid)
{
    bootbox.confirm({
        message: "Etes vous sûr de vouloir diffuser cette instruction ?",
        buttons: {
            confirm: {
                label: 'Ok',
                className: 'btn-primary'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result) {
            if (result === true)
            {
                var datas="id="+itkismid;
                var diffusion = $("#diffusion").val();
                if(diffusion===0)
                {
                    $("#modal-danger-diffusion").modal();
                }
                else
                {
                    $.ajax
                    ({
                        type: "POST",
                        url: Routing.generate('diffuseritkism'),
                        data: datas,
                        dataType: "html",
                        success: function ()
                        {
                            $("#modal-success-diffusion").modal();
                            location.href = Routing.generate('viewitkism', {
                                'id': itkismid
                            });
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown)
                        {
                            alert("Error: " + errorThrown);
                        }
                    });
                }
            }
        }
    });
}


function clotureritkismr(id)
{
    var heuredebut = document.getElementById('timepickerdebut').value;
    heuredebut = convertTo24Hour(heuredebut.toLowerCase());
    var heurefin = document.getElementById('timepickerfin').value;
    heurefin = convertTo24Hour(heurefin.toLowerCase());
    var datedebutsemis = $("#agrobundle_itkism_dATEP").val();
    var opid = $("#opid").val();
    var datas="id="+id+"&heuredebut="+heuredebut+"&heurefin="+heurefin+"&datedebutsemis="+datedebutsemis+"&opid="+opid;
    $.ajax
    ({
        type: "POST",
        url: Routing.generate('clotureritkismr'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            $("#modal-itkismr-cloture").modal();
            location.href = Routing.generate('instructionsemis', {
                'id': id
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error: " + errorThrown);
        }
    });
}

function convertTo24Hour(time) {
    var hours = parseInt(time.substr(0, 2));
    if(time.indexOf('am') != -1 && hours == 12) {
        time = time.replace('12', '0');
    }
    if(time.indexOf('pm')  != -1 && hours < 12) {
        time = time.replace(hours, (hours + 12));
    }
    return time.replace(/(am|pm)/, '');
}