/**
 * Created by akdaho on 09/01/2017.
*/

function getarticles(str)
{
    /* if (str1=="") {
     document.getElementById("txtHint").innerHTML="";
     return;
     }*/
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("articles").innerHTML=this.responseText;
        }
    };
    xmlhttp.open("GET",Routing.generate('getarticles')+"?idmaintenance="+str,true);
    xmlhttp.send();
    document.getElementById("diarticle").style.display = "block";
}

function addRowHandlers() {
    var table = document.getElementById("tableId");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler =
            function(row)
            {
                return function() {
                    var cell = row.getElementsByTagName("td")[0];
                    var id = cell.innerHTML;
                    getarticles(id);
                    //getarticlesinterventions(id);
                };
            };

        currentRow.onclick = createClickHandler(currentRow);
    }
}
window.onload = addRowHandlers();


$(document).ready(function(){
    $("tr").click(function() {
        $(this).closest("tr").siblings().removeClass("highlighted");
        $(this).toggleClass("highlighted");
    })
});