$(function()
{
    $('#datep').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY H:m:s'
    });
    $('#datepfin').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY'
    });
    $('#agrobundle_itkitl_dATEP').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY H:m:s'
    });
    $('#agrobundle_itkitlr_dATEP').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY H:m:s'
    });

    $('#WiredWizard').wizard();
});

$(document).ready(function () {
    var duree = $("#agrobundle_itkitl_dUREE");
    var datep =$("#agrobundle_itkitl_dATEP");
    var datepfin =$("#agrobundle_itkitl_dATEPFIN");
    $(duree).change(function() {
        var dated=datep.val();
        var b = dated.split('  ');
        var dmy = b[0].split('-');
        var hms = b[1].split(':');
        var data=dmy[2]+'-'+dmy[1]+'-'+dmy[0];
        var date = new Date(data);

        var timedatep = date.getTime();
        var today = new Date();
        var timetoday = today.getTime();

        if(timedatep < timetoday)
        {
            $("#modal-warning-date").modal();
        }
        else
        {
            date.setHours(hms[0],hms[1], hms[2]);
            date.setDate(date.getDate()+(parseInt(duree.val())-1));
            var day =date.getDate();
            var month = date.getMonth();
            var hours = hms[0];
            var minutes = hms[1];
            var seconds = hms[2];
            if(parseInt(day)<10) {
                day = '0' + day;
            }
            if((parseInt(month)+1)<10)
            {
                month = parseInt(month) + 1;
                month = '0' + month;
            }
            else
            {
                month = parseInt(month) + 1;
            }

            console.log(month);
            console.log(day);
            datepfin.val(day+'-'+month+'-'+date.getFullYear()+'   '+hours+':'+minutes+':'+seconds);
        }
    });
});

$(document).ready(function () {
    var duree = $("#agrobundle_itkitlr_dUREE");
    var datep =$("#agrobundle_itkitlr_dATEP");
    var datepfin =$("#agrobundle_itkitlr_dATEPFIN");
    $(duree).change(function() {
        var dated=datep.val();
        var b = dated.split('  ');
        var dmy = b[0].split('-');
        var hms = b[1].split(':');
        var data=dmy[2]+'-'+dmy[1]+'-'+dmy[0];
        var date = new Date(data);

        date.setHours(hms[0],hms[1], hms[2]);
        date.setDate(date.getDate()+(parseInt(duree.val())-1));

        var day =date.getDate();
        var month = date.getMonth();
        var hours = hms[0];
        var minutes = hms[1];
        var seconds = hms[2];
        if(day<10)
            day='0'+day;
        if(month<10)
        {
            month = parseInt(month) + 1;
            month = '0' + month;
        }

        datepfin.val(day+'-'+month+'-'+date.getFullYear()+'   '+hours+':'+minutes+':'+seconds);
    });
});

$("#bouillieutl").change(function()
{
    var fermeid=$("#agrobundle_itkitl_fERMEID").val();
    var duree = $("#agrobundle_itkitl_dUREE").val();
    var datep =$("#agrobundle_itkitl_dATEP").val();
    var datepfin =$("#agrobundle_itkitl_dATEPFIN").val();
    var qteutl =$("#qteutl").val();
    var bouillieutl =$("#bouillieutl");
    var utlid = $("#newutl").val();
    var uniteutl = $("#uniteutl").val();

    if(fermeid === '' || duree === '' || datep === '' || datepfin === '' || qteutl === '' || bouillieutl.val() === '' || utlid === '')
    {
        $("#modal-warning-itkitl-saisi").modal();
        bouillieutl.val('');
    }
    else
    {
        var datas = 'fermeid='+fermeid+'&duree='+duree+'&datep='+datep+'&datepfin='+datepfin+'&qteutl='+qteutl+'&bouillieutl='+bouillieutl.val()+'&utlid='+$('#newutl').select2('data').id+'&uniteutl='+uniteutl;

        $.ajax
        ({
            type: "POST",
            url: Routing.generate('creeritkitl'),
            data: datas,
            //dataType: "html",
            success: function (data)
            {
                if(data.itkitlid === 0)
                {
                    $("#modal-danger-itkitl").modal();
                }
                else
                {
                    window.location.href = Routing.generate('modifieritkitl', {id:data.itkitlid});
                }
                //window.location.href = Routing.generate('modifieritkitl', {fermeid:fermeid,opid:opid})
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                alert("Error: " + errorThrown);
            }
        });
    }
});

function updateepiitkitl(str)
{
    var datas="itkitlid="+str;
    var epiid=$("#epi");

    if($(epiid).select2('data').id===undefined)
    {
        $("#modal-warning-itkitl").modal();
    }
    else
    {
        var epi = $(epiid).select2('data').name;
        datas = datas+"&type=manuel"+"&epiid="+$(epiid).select2('data').id;
        $.ajax
        ({
            type: "POST",
            url: Routing.generate('updateepiitkitl'),
            data: datas,
            success: function (data)
            {
                if(data.exist === 'true')
                {
                    $("#modal-danger-epi").modal();
                }
                else
                {
                    var elem = document.getElementById('tableepi');
                    elem.innerHTML=elem.innerHTML+ '<tr id='+$(epiid).select2('data').id+'>'+' <td>'+epi+'</td> <td><button type="button"' +
                        ' class="btn btn-danger btn-sm"  onclick=" supprimerepiitkitl('+$(epiid).select2('data').id+','+str+ ');'+'"'+' >'+'<i' +
                        ' class="fa fa-trash"'+'></i>'+'Supprimer'+'</button></td></tr>';
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                alert('Error: '+errorThrown);
            }
        });
    }
}

function supprimerepiitkitl(epiid, itkitlid)
{
    var datas = "itkitlid="+itkitlid+"&epiid="+epiid;

    $.ajax
    ({
        type: "POST",
        url: Routing.generate('supprimerepiitkitl'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            var elem = document.getElementById(epiid);
            elem.remove();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error: " + errorThrown);
        }
    });
}

function updatecibleitkitl(str)
{
    var datas="itkitlid="+str;
    var cibleid=$("#cible");

    if($(cibleid).select2('data').id===undefined)
    {
        $("#modal-warning-itkitl").modal();
    }
    else
    {
        var cible = $(cibleid).select2('data').name;
        datas = datas+"&cibleid="+$(cibleid).select2('data').id;
        $.ajax
        ({
            type: "POST",
            url: Routing.generate('updatecibleitkitl'),
            data: datas,
            success: function (data)
            {
                if(data.exist === 'true')
                {
                    $("#modal-danger-cible").modal();
                }
                else
                {
                    var elem = document.getElementById('tablecible');
                    elem.innerHTML=elem.innerHTML+ '<tr id='+$(cibleid).select2('data').id+'>'+' <td>'+cible+'</td> <td><button type="button"' +
                        ' class="btn btn-danger btn-sm"  onclick=" supprimercibleitkitl('+$(cibleid).select2('data').id+','+str+ ');'+'"'+' >'+'<i' +
                        ' class="fa fa-trash"'+'></i>'+'Supprimer'+'</button></td></tr>';
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                alert('Error: '+errorThrown);
            }
        });
    }
}

function supprimercibleitkitl(cibleid, itkitlid)
{
    var datas = "itkitlid="+itkitlid+"&cibleid="+cibleid;

    $.ajax
    ({
        type: "POST",
        url: Routing.generate('supprimercibleitkitl'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            var elem = document.getElementById(cibleid);
            elem.remove();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error: " + errorThrown);
        }
    });
}

$("#finishitkitl").click(function() {
    if (this.innerText === "TERMINER" ) {
        updateitkitl();
    }
    else if (this.innerText === "FIN" ) {
        location.href = Routing.generate('instructionlocalise');
    }
});

$("#finishitkitlr").click(function() {
    if (this.innerText === "TERMINER" ) {
        updateitkitlr();
    }
    else if (this.innerText === "FIN" ) {
        location.href = Routing.generate('instructionlocalise');
    }
});

//$("#finishitkistl").click(function() {
//    if (this.innerText === "TERMINER" ) {
//        realiseritkitlr();
//    }
//});

function updateitkitl()
{

    bootbox.confirm({
        message: "Voulez vous enregistrer ces données ?",
        buttons: {
            confirm: {
                label: 'Ok',
                className: 'btn-primary'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result) {
            if (result === true) {
                var id = $("#itkitlid").val();
                var prescripteurid = $("#agrobundle_itkitl_pRESCRIPTEURID").val();
                var applicateurid = $("#agrobundle_itkitl_aPPLICATEURID").val();
                var chauffeurid = $("#agrobundle_itkitl_cHAUFFEURID").val();
                var modeapplication = $("#agrobundle_itkitl_mODEAPPLICATION").val();

                var datas = "id="+id+"&prescripteurid="+prescripteurid+"&applicateurid="+applicateurid+"&chauffeurid="+chauffeurid+"&modeapplication="+modeapplication;

                var bouillie = $("#bouillieutl").val();
                var quantite = $("#qteutl").val();
                var datep = $("#agrobundle_itkitl_dATEP").val();
                var duree = $("#agrobundle_itkitl_dUREE").val();

                datas = datas + "&bouillie=" + bouillie +"&quantite=" + quantite + "&datep=" + datep + "&duree=" + duree;

                var materielid=$("#materielepi").select2('data');
                var appareilid=$("#appareil").select2('data');

                if(id==='' || bouillie ==='' || quantite==='' || datep==='' || duree==='' || materielid===undefined || materielid.name==='' || appareilid===undefined || appareilid.name===''  || applicateurid === undefined  || chauffeurid === undefined ||modeapplication==='')
                {
                    $("#modal-warning-itkitl").modal();
                }
                else
                {
                    if(materielid.id==='')
                    {
                        materielid.id=$("#materielidepi").val();
                    }
                    if(appareilid.id==='')
                    {
                        appareilid.id=$("#appareilid").val();
                    }

                    datas = datas +'&materielid='+materielid.id + '&appareilid=' + appareilid.id;

                    $.ajax
                    ({
                        type: "POST",
                        url: Routing.generate('updateitkitl'),
                        data: datas,
                        dataType: "html",
                        success: function ()
                        {
                            $("#modal-success-itkitl").modal();
                            location.href = Routing.generate('instructionlocalise');
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown)
                        {
                            alert("Error itkitl: " + errorThrown);
                        }
                    });
                }
            }
        }
    });
}

$("#doseutl").change(function() {
    console.log('dose');
    var doseutl = document.getElementById('doseutl').value ;
    var doseminihl = document.getElementById('doseminihl').value ;
    var dosemaxihl = document.getElementById('dosemaxihl').value;
    var dosemini = document.getElementById('dosemini').value;
    var dosemaxi = document.getElementById('dosemaxi').value;
    console.log(doseminihl+ "*"+ dosemini);
    if(doseminihl !== "-" && doseminihl !== "null" && doseminihl !== "")
    {
        if(parseFloat(doseutl) < parseFloat(doseminihl))
        {
            $("#modal-intrant-utl").modal('hide');
            $("#doseutl").val(doseminihl)
            $("#modal-doseminihl-itkitl").modal();

            $("#modal-intrant-utl").modal();
        }
        else if(parseFloat(doseutl) > parseFloat(dosemaxihl))
        {
            $("#modal-intrant-utl").modal('hide');
            $("#doseutl").val(dosemaxihl);
            $("#modal-dosemaxihl-itkitl").modal();
            $("#modal-intrant-utl").modal();
        }
    }
    else if(dosemini !== "-" && dosemini !== "null" && dosemini !== "")
    {
        if(parseFloat(doseutl) < parseFloat(dosemini))
        {
            $("#modal-intrant-utl").modal('hide');
            $("#modal-dosemini-itkitl").modal();
            $("#doseutl").val(dosemini)
            $("#modal-intrant-utl").modal();
        }
        else if(parseFloat(doseutl) > parseFloat(dosemaxi))
        {
            $("#modal-intrant-utl").modal('hide');
            $("#modal-dosemaxi-itkitl").modal();
            $("#doseutl").val(dosemaxi);
            $("#modal-intrant-utl").modal();
        }
    }
    // var unite = $("unite").val();
    // unite = unite.replace("/", "%2F");
    var datas = "unite=" +  $("#unite").val() + "&dose=" + doseutl + "&bouillie=" + $("#bouillie").val();

    $.ajax({
        url:  Routing.generate('qtetotutl'),
        type: "GET",
        data: datas,
        context: this,
        error: function () {},
        dataType: 'json',
        success : function (data) {
            $("#qtetot").val(data[0]["RESULT"]);
        }
    });
});

function openmodalintrant()
{
    $("#modal-intrant-utl").modal();
}

function ajouterintrantutl(id)
{
    var rang = $("#rang").val();
    var intrantid = $("#intrantutl");
    var dose = $("#doseutl").val();
    var comment = $("#comment").val();
    var unite = $("#unite").val();
    var bouillie = $("#bouillie").val();
    var qtetot = $("#qtetot").val();
    var type = $("#type").val();

    if($(intrantid).select2('data').id===undefined || dose === "")
    {
        $("#modal-warning1-itkitl").modal();
    }
    else
    {
        var datas = "id=" + id + "&rang=" + rang +"&intrantid=" + intrantid.select2('data').id + "&dose=" + dose + "&comment=" + comment + "&unite=" + unite + "&bouillie=" + bouillie+ "&qtetot=" + qtetot;
        console.log(datas);

        if( type === "itkitl")
        {
            $.ajax
            ({
                type: "POST",
                url: Routing.generate('ajoutintrantitkitl'),
                data: datas,
                success: function ( data)
                {
                    var itkitlaid = data.itkitlaid;
                    console.log(itkitlaid);
                    if(itkitlaid === 0)
                    {
                        $("#modal-exist-intrant-itkitl").modal();
                    }
                    else
                    {
                        $("#modal-intrant-utl").modal('hide');
                        $("#modal-success-intrant-itkitl").modal();
                        var elem = document.getElementById('tableitkitla');
                        elem.innerHTML=elem.innerHTML+ '<tr id='+$(intrantid).select2('data').id+'>'+' <td>'+rang+'</td>' +
                            ' <td>'+$(intrantid).select2('data').name+'</td> <td>'+dose+'</td> <td>'+unite+'</td> <td>'+bouillie+'</td> <td>'+qtetot+'</td> <td>'+comment+'</td>' +
                            ' <td><button type="button"' +
                            ' class="btn btn-danger btn-sm"  onclick=" supprimeritkitla('+itkitlaid+','+id+ ');'+'"'+' >'+'<i' +
                            ' class="fa fa-trash"'+'></i>'+'Supprimer'+'</button></td></tr>';
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown)
                {
                    alert("Error itkitla: " + errorThrown);
                }
            });
        }
        else if( type === "itkitlr")
        {
            $.ajax
            ({
                type: "POST",
                url: Routing.generate('ajoutintrantitkitlr'),
                data: datas,
                success: function ( data)
                {
                    var itkitlraid = data.itkitlraid;
                    console.log(itkitlraid);
                    if(itkitlraid === 0)
                    {
                        $("#modal-exist-intrant-itkitlr").modal();
                    }
                    else
                    {
                        $("#modal-intrant-utl").modal('hide');
                        $("#modal-success-intrant-itkitlr").modal();
                        var elem = document.getElementById('tableitkitlra');
                        elem.innerHTML=elem.innerHTML+ '<tr id='+$(intrantid).select2('data').id+'>'+' <td>'+rang+'</td>' +
                            ' <td>'+$(intrantid).select2('data').name+'</td> <td>'+dose+'</td> <td>'+unite+'</td> <td>'+bouillie+'</td> <td>'+qtetot+'</td> <td>'+comment+'</td>' +
                            ' <td><button type="button"' +
                            ' class="btn btn-danger btn-sm"  onclick=" supprimeritkitlra('+itkitlraid+','+id+ ');'+'"'+' >'+'<i' +
                            ' class="fa fa-trash"'+'></i>'+'Supprimer'+'</button></td></tr>';
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown)
                {
                    alert("Error itkitlra: " + errorThrown);
                }
            });
        }
    }
}

function supprimeritkitla(itkitlaid)
{
    var datas="itkitlaid="+itkitlaid;
    $.ajax
    ({
        type: "POST",
        url: Routing.generate('supprimeritkitla'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            var elem = document.getElementById(itkitlaid);
            elem.remove();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error: " + errorThrown);
        }
    });
}

function supprimeritkitlra(itkitlraid)
{
    var datas="itkitlraid="+itkitlraid;
    $.ajax
    ({
        type: "POST",
        url: Routing.generate('supprimeritkitlra'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            var elem = document.getElementById(itkitlraid);
            elem.remove();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error: " + errorThrown);
        }
    });
}

function annuleritkitl(itkitlid)
{
    bootbox.confirm({
        message: "Etes vous sûr de vouloir annuler cette instruction ?",
        buttons: {
            confirm: {
                label: 'Ok',
                className: 'btn-primary'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result) {
            if (result === true)
            {
                var datas="id="+itkitlid;
                $.ajax
                ({
                    type: "POST",
                    url: Routing.generate('annuleritkitl'),
                    data: datas,
                    dataType: "html",
                    success: function ()
                    {
                        $("#modal-success-cancel-itkitl").modal();
                        location.href = Routing.generate('instructionlocalise', {
                            'id': itkitlid
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown)
                    {
                        alert("Error: " + errorThrown);
                    }
                });
            }
        }
    });
}

function diffuseritkitl(itkitlid)
{
    bootbox.confirm({
        message: "Etes vous sûr de vouloir diffuser cette instruction ?",
        buttons: {
            confirm: {
                label: 'Ok',
                className: 'btn-primary'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result) {
            if (result === true)
            {
                var datas="id="+itkitlid;
                var diffusion = $("#diffusion").val();
                if(diffusion===0)
                {
                    $("#modal-danger-diffusion").modal();
                }
                else
                {
                    $.ajax
                    ({
                        type: "POST",
                        url: Routing.generate('diffuseritkitl'),
                        data: datas,
                        dataType: "html",
                        success: function ()
                        {
                            $("#modal-success-diffusion").modal();
                            location.href = Routing.generate('viewitkitl', {
                                'id': itkitlid
                            });
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown)
                        {
                            alert("Error: " + errorThrown);
                        }
                    });
                }
            }
        }
    });
}

function genererITKITLR()
{

    var elem = $('#instructionutlid').select2('data');
    if(elem!==null)
    {
        bootbox.confirm({
            message: "Etes vous sûr de vouloir réaliser cette instruction ?",
            buttons: {
                confirm: {
                    label: 'Ok',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'Annuler',
                    className: 'btn-default'
                }
            },
            callback: function (result)
            {
                if (result === true) {
                    var datas = "idinstruction="+elem.id;
                    $.ajax({
                        type: "POST",
                        url: Routing.generate('generer'),
                        data: datas,
                        dataType: "html"
                    }).done(function (msg) {
                        window.location.href = Routing.generate('modifier_itkitlr', {id:elem.id})
                    }).fail(function () {
                        alert("error");
                    });
                }
            }
        });
    }
}

function updateitkitlr()
{

    bootbox.confirm({
        message: "Voulez vous enregistrer ces données ?",
        buttons: {
            confirm: {
                label: 'Ok',
                className: 'btn-primary'
            },
            cancel: {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result) {
            if (result === true) {
                var id = $("#itkitlrid").val();
                var prescripteurid = $("#agrobundle_itkitlr_pRESCRIPTEURID").val();
                var applicateurid = $("#agrobundle_itkitlr_aPPLICATEURID").val();
                var chauffeurid = $("#agrobundle_itkitlr_cHAUFFEURID").val();
                var modeapplication = $("#agrobundle_itkitlr_mODEAPPLICATION").val();

                var datas = "id="+id+"&prescripteurid="+prescripteurid+"&applicateurid="+applicateurid+"&chauffeurid="+chauffeurid+"&modeapplication="+modeapplication;

                var bouillie = $("#bouillieutl").val();
                var quantite = $("#qteutl").val();
                var datep = $("#agrobundle_itkitlr_dATEP").val();
                var duree = $("#agrobundle_itkitlr_dUREE").val();

                var vent = $("#vent").val();
                var temperature = $("#temperature").val();
                var humidite = $("#humidite").val();
                var pluie = $("#pluie").val();

                datas = datas + "&bouillie=" + bouillie +"&quantite=" + quantite + "&datep=" + datep + "&duree=" + duree;

                datas = datas + "&vent=" + vent + "&temperature=" + temperature + "&humidite=" + humidite + "&pluie=" + pluie;

                var materielid=$("#materielepi").select2('data');
                var appareilid=$("#appareil").select2('data');
                console.log(datas);

                if(id==='' || bouillie ==='' || quantite==='' || datep==='' || duree==='' || materielid===undefined || materielid.name==='' || appareilid===undefined || appareilid.name===''  || applicateurid === undefined  || chauffeurid === undefined || modeapplication === '' || vent === "" || temperature === "" || humidite === "" || pluie === "")
                {
                    $("#modal-warning-itkitlr").modal();
                }
                else
                {
                    if(materielid.id==='')
                    {
                        materielid.id=$("#materielidepi").val();
                    }
                    if(appareilid.id==='')
                    {
                        appareilid.id=$("#appareilid").val();
                    }

                    datas = datas +'&materielid='+materielid.id + '&appareilid=' + appareilid.id;

                    var hdebut = $("#hdebut").val();
                    var hfin = $("#hfin").val();

                    datas = datas + "&hdebut=" + hdebut + "&hfin=" + hfin;

                    $.ajax
                    ({
                        type: "POST",
                        url: Routing.generate('updateitkitlr'),
                        data: datas,
                        dataType: "html",
                        success: function ()
                        {
                            $("#modal-success-itkitlr").modal();
                            location.href = Routing.generate('instructionlocalise');
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown)
                        {
                            alert("Error itkitlr: " + errorThrown);
                        }
                    });
                }
            }
        }
    });
}

function updateepiitkitlr(str)
{
    var datas="itkitlrid="+str;
    var epiid=$("#epi");

    if($(epiid).select2('data').id===undefined)
    {
        $("#modal-warning-itkitlr").modal();
    }
    else
    {
        var epi = $(epiid).select2('data').name;
        datas = datas+"&epiid="+$(epiid).select2('data').id;
        $.ajax
        ({
            type: "POST",
            url: Routing.generate('updateepiitkitlr'),
            data: datas,
            success: function (data)
            {
                if(data.exist === 'true')
                {
                    $("#modal-danger-epir").modal();
                }
                else
                {
                    var elem = document.getElementById('tableepir');
                    elem.innerHTML=elem.innerHTML+ '<tr id='+$(epiid).select2('data').id+'>'+' <td>'+epi+'</td> <td><button type="button"' +
                        ' class="btn btn-danger btn-sm"  onclick=" supprimerepiitkitlr('+$(epiid).select2('data').id+','+str+ ');'+'"'+' >'+'<i' +
                        ' class="fa fa-trash"'+'></i>'+'Supprimer'+'</button></td></tr>';
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                alert('Error itkitlrepi: '+errorThrown);
            }
        });
    }
}

function supprimerepiitkitlr(epiid, itkitlrid)
{
    var datas = "itkitlrid="+itkitlrid+"&epiid="+epiid;

    $.ajax
    ({
        type: "POST",
        url: Routing.generate('supprimerepiitkitlr'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            var elem = document.getElementById(epiid);
            elem.remove();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error itkitlrepi: " + errorThrown);
        }
    });
}

function updatecibleitkitlr(str)
{
    var datas="itkitlrid="+str;
    var cibleid=$("#cible");

    if($(cibleid).select2('data').id===undefined)
    {
        $("#modal-warning-itkitlr").modal();
    }
    else
    {
        var cible = $(cibleid).select2('data').name;
        datas = datas+"&cibleid="+$(cibleid).select2('data').id;
        $.ajax
        ({
            type: "POST",
            url: Routing.generate('updatecibleitkitlr'),
            data: datas,
            success: function (data)
            {
                if(data.exist === 'true')
                {
                    $("#modal-danger-cibler").modal();
                }
                else
                {
                    var elem = document.getElementById('tablecibler');
                    elem.innerHTML=elem.innerHTML+ '<tr id='+$(cibleid).select2('data').id+'>'+' <td>'+cible+'</td> <td><button type="button"' +
                        ' class="btn btn-danger btn-sm"  onclick=" supprimercibleitkitlr('+$(cibleid).select2('data').id+','+str+ ');'+'"'+' >'+'<i' +
                        ' class="fa fa-trash"'+'></i>'+'Supprimer'+'</button></td></tr>';
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                alert('Error itkitlcibler: '+errorThrown);
            }
        });
    }
}

function supprimercibleitkitlr(cibleid, itkitlrid)
{
    var datas = "itkitlrid="+itkitlrid+"&cibleid="+cibleid;

    $.ajax
    ({
        type: "POST",
        url: Routing.generate('supprimercibleitkitlr'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            var elem = document.getElementById(cibleid);
            elem.remove();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error itkitlcibler: " + errorThrown);
        }
    });
}
