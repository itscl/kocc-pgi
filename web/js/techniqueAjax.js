/**
 * Created by akdaho on 13/01/2017.
 */

function updatefamille(str)
{
    var id = str;
    var code = $("#code"+str).val();
    var libelle = $("#libelle"+str).val();
    var dateinvalide = $("#dateinvalide"+str).val();

    var datas = "code="+code+"&libelle="+libelle+ "&dateinvalide="+dateinvalide+ "&idfamillemat="+str;
    $.ajax({
        type: "POST",
        url: Routing.generate('modifierfamille'),
        data: datas,
        dataType: "html"
    }).done(function (msg) {
        location.reload(true);
    }).fail(function () {
        alert("error");
    });

}

function deletefamille(str)
{
    var id = str;
    var datas = "idfamillemat="+str;
    if (confirm('Vous \u00eates s\u00fbr de vouloir supprimer cette cat\351gorie ??? ')) {
        $.ajax({
            type: "POST",
            url: Routing.generate('suprimerfamille'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            location.reload(true);
        }).fail(function () {
            alert("error");
        });
    }
}

function updatesousfamille(str)
{
    var id = str;
    var code = $("#code"+str).val();
    var libelle = $("#libelle"+str).val();
    var dateinvalide = $("#dateinvalide"+str).val();
    var famillesousf = $("#famillesousf"+str).val();

    var datas = "code="+code+"&libelle="+libelle+ "&dateinvalide="+dateinvalide+"&famillesousf="+famillesousf+ "&idsfamillemat="+str;
    $.ajax({
        type: "POST",
        url: Routing.generate('modifiersousfamille'),
        data: datas,
        dataType: "html"
    }).done(function (msg) {
        location.reload(true);
    }).fail(function () {
        alert("error");
    });

}

function deletesfamille(str)
{
    var id = str;
    var datas = "idsfamillemat="+str;
    if (confirm('Vous \u00eates s\u00fbr de vouloir supprimer ce genre ??? ')) {
        $.ajax({
            type: "POST",
            url: Routing.generate('suprimersfamille'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            location.reload(true);
        }).fail(function () {
            alert("error");
        });
    }
}

function updateaction(str)
{
    var id = str;
    var libelle = $("#libelle"+str).val();

    var datas = "libelle="+libelle+ "&idaction="+str;
    $.ajax({
        type: "POST",
        url: Routing.generate('modifieraction'),
        data: datas,
        dataType: "html"
    }).done(function (msg) {
        location.reload(true);
    }).fail(function () {
        alert("error");
    });

}

function deleteaction(str)
{
    var id = str;
    var datas = "idaction="+str;
    if (confirm('Vous \u00eates s\u00fbr de vouloir supprimer cette action ??? ')) {
        $.ajax({
            type: "POST",
            url: Routing.generate('supprimeraction'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            location.reload(true);
        }).fail(function () {
            alert("error");
        });
    }
}

function updatemarque(str)
{
    var id = str;
    var libelle = $("#libelle"+str).val();
    var code = $("#code"+str).val();
    var dateinvalide = $("#dateinvalide"+str).val();

    var datas = "code="+code+"&libelle="+libelle+ "&dateinvalide="+dateinvalide+ "&idmarque="+str;
    $.ajax({
        type: "POST",
        url: Routing.generate('modifiermarque'),
        data: datas,
        dataType: "html"
    }).done(function (msg) {
        location.reload(true);
    }).fail(function () {
        alert("error");
    });

}

function deletemarque(str)
{
    var id = str;
    var datas = "idmarque="+str;
    if (confirm('Vous \u00eates s\u00fbr de vouloir supprimer cette marque??? ')) {
        $.ajax({
            type: "POST",
            url: Routing.generate('supprimermarque'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            location.reload(true);
        }).fail(function () {
            alert("error");
        });
    }
}

function deletearticlemaintenance(str, str2)
{
    var id = str;
    var idmaintenance = str2;

    var datas = "id="+str +"&idmaintenance="+str2;

    bootbox.confirm({
        message: "Vous êtes sûr de vouloir supprimer cette pièce de rechange ?",
        buttons:
        {
            confirm:
            {
                label: 'OK',
                className: 'btn-primary'
            },
            cancel:
            {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result)
        {
            if(result==true)
            {

                $.ajax({
                    type: "GET",
                    url: Routing.generate('supprimermaintenancearticle'),
                    data: datas,
                    dataType: "html"
                }).done(function (msg) {
                    location.reload(true);
                }).fail(function () {
                    alert("Erreur")
                });
            }
        }
    });
}

function dupliquermodele(str)
{
    var id = str;
    var libelle = $("#libelle").val();
     /*var libelle = $("#libelle"+str).val();
     var dateinvalide = $("#dateinvalide"+str).val();
     */
    var datas = "id="+str +"&libelle="+libelle;

    if(libelle != '')
    {
        $.ajax({
            type: "POST",
            url: Routing.generate('dupliquermodelemateriel'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            $("#myModal").modal('hide');
            $("#modal-success-modele").modal();

        }).fail(function () {
            $("#myModal").modal('hide');
            $("#modal-warning1").modal();
        });

    }
    else
    {
        $("#modal-danger").modal();
    }
}

function dupliquerplanmaintenance(str)
{
    var id = str;
    var libelle = $("#libelle1").val();

    var datas = "id="+str +"&libelle1="+libelle;

    //alert("dgdsggff")

    if(libelle != '')
    {
        $.ajax({
            type: "POST",
            url: Routing.generate('dupliquerplandemaintenance'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            $("#myModal").modal('hide');
            window.location.href = Routing.generate('redirectmodelepmd', {libelle:libelle })
        }).fail(function () {
            alert("Erreur");
           // $("#myModal").modal('hide');
           // $("#modal-warning1").modal();
        });

    }
    else
    {
        $("#modal-danger").modal();
    }
}

function ajoutepi(str)
{
    var id = str;

    var datas = $('.checkrole:checked').serialize()+ "&id="+str;

    $.ajax({
        url: Routing.generate('ajouterepi'),
        type: "POST",
        data: datas,
        success: function (data) {
            location.reload(true);
        }
    });
}

function supprepi(str) {
    var id = str;
    var datas = $('.checkrole:checked').serialize()+ "&id="+str;

    $.ajax({
        url: Routing.generate('supprimerepi'),
        type: "POST",
        data: datas,
        success: function (data) {
            location.reload(true);
        }
    });
}

function deletearticle(str) {
    var id = str;
    var datas = "idarticle="+str;
    if (confirm('Vous \u00eates s\u00fbr de vouloir supprimer cet intrant ? ')) {
        $.ajax({
            url: Routing.generate('supprimerarticle'),
            type: "POST",
            data: datas,
            success: function (data) {
                location.reload(true);
            }
        });
    }
}

function deletemateriel(str)
{
    var id = str;
    var datas = "id="+str;
    if (confirm('Vous \u00eates s\u00fbr de vouloir supprimer cet materiel ??? ')) {
        $.ajax({
            type: "POST",
            url: Routing.generate('delete_materiel'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            // location.reload(true);
            window.location.href = Routing.generate('technique_marque');
        }).fail(function () {
            alert("Cet materiel est utilis�, elle ne peut donc pas etre supprimer");
        });
    }
}


$(function(){
    $("#ajoutmaintenance").click(function(){
        var datemaintenance = $("#datemaintenance").val();
        var materielid = $("#materielid").val();
        var compteur = $("#compteur").val();
        var maintenancepro = $("#maintenancepro").val();
        var mecanicien = $("#mecanicien").val();
        var dateimmo = $("#dateimmo").val();
        
        var datas = $('.checkmaintenance:checked').serialize()+
            "&datemaintenance="+datemaintenance+
            "&materielid="+materielid+ "&compteur="+compteur +
            "&maintenancepro="+maintenancepro
            +"&mecanicien="+mecanicien + "&dateimmo="+dateimmo;
        if(datemaintenance != '' && materielid != '' && compteur !='' && maintenancepro !='' && mecanicien !='' && dateimmo !='')
        {
            $(this).attr('disabled','disabled');
            $(this).val('Patientez...');
            $.ajax({
                type: "POST",
                url: Routing.generate('ajaxpersitmaintenance'),
                data: datas,
                datatype: "html",
                success : function (response) {

                    window.location.href = Routing.generate('ajoutarticlemaintenance', {materielid:materielid,compteur:compteur,date:datemaintenance });
                },
                failure: function (response) {
                    alert("Erreur lors de l'ajout. Un ou plusieurs champs sont vides");
                }
            });
        }
        else
        {
            $("#modal-danger").modal();
        }

    });
});

$(function(){
    $("#modifiermaintenance").click(function(){
        var id = $("#idmaintenance").val();
        var compteur = $("#compteurmodif").val();
        var maintenancepro = $("#maintenancepromodif").val();
        var mecanicien = $("#mecanicienmodif").val();
        var dateimmo = $("#dateimmomodif").val();

        var datas =  $('.checkmaintenance:checked').serialize()+"&idmaintenance="+id+ "&compteur="+compteur +"&maintenancepro="+maintenancepro +"&mecanicien=" +mecanicien+ "&dateimmo="+dateimmo;

        $.ajax({
            type: "POST",
            url: Routing.generate('ajaxmodifiermaintenance'),
            data: datas,
            dataType: "html"
        }).done(function (msg) {
            location.reload(true);
        }).fail(function () {
            alert("error");
        });

    });
});

$(function(){
    $("#realisemaintenance").click(function(){
        var id = $("#idmaintenance").val();
        var compteur = $("#compteurmodif").val();
        var maintenancepro = $("#maintenancepromodif").val();
        var mecanicien = $("#mecanicienmodif").val();
        var autremaintenace = $("#autremaintenace").val();
        var daterealisation = $("#daterealisation").val();
        var commentaire = $("#commentaire").val();

        var datas =  $('.checkmaintenance:checked').serialize()+"&idmaintenance="+id+ "&compteur="+compteur +"&maintenancepro="+maintenancepro +"&mecanicien=" +mecanicien +"&autremaintenace="+autremaintenace +"&daterealisation=" +daterealisation+ "&commentaire="+commentaire;

        if(compteur != '' && maintenancepro != '' && mecanicien !='' && maintenancepro !='')
        {


            $.ajax({
                type: "POST",
                url: Routing.generate('ajaxrealisemaintenance'),
                data: datas,
                dataType: "html"
            }).done(function (msg) {
                window.location.href = Routing.generate('realisemaintenance', {id:("#idmaintenance").val()})
            }).fail(function () {
                alert("error");
            });
        }
        else
        {
            $("#modal-danger").modal();
        }
    });

});

$(function(){
    $("#realisemaintenancemodif").click(function(){
        var id = $("#idmaintenance").val();
        var compteur = $("#compteurmodif").val();
        var maintenancepro = $("#maintenancepromodif").val();
        var mecanicien = $("#mecanicienmodif").val();
        var autremaintenace = $("#autremaintenace").val();
        var dateimmo = $("#dateimmomodif").val();
        var temitervent = $("#temiterventmodif").val();
        var datesortie = $("#datesortie").val();
        var commentaire = $("#commentaire").val();
        var datas =  $('.checkmaintenance:checked').serialize()+"&idmaintenance="+id+
            "&compteur="+compteur +"&maintenancepro="+maintenancepro +"&mecanicien=" +
            mecanicien +"&autremaintenace="+autremaintenace+
            "&dateimmo=" + dateimmo+ "&temitervent=" +temitervent+ "&datesortie=" +datesortie+ "&commentaire="+commentaire;

            $.ajax({
                type: "POST",
                url: Routing.generate('ajaxrealisemaintenance'),
                data: datas,
                dataType: "html"
            }).done(function (msg) {
                window.location.href = Routing.generate('realisemaintenance', {id:("#id").val()})
            }).fail(function () {
                alert("error");
            });
    });
});

$(function(){
    $("#curativemaintenance").click(function(){
        var idmaintenance = $("#idmaintenance").val();

        var datas =  $('.checkmaintenance:checked').serialize()+"&idmaintenance="+idmaintenance;
        $.ajax({
            type: "POST",
            url: Routing.generate('ajaxcurativemaintenance'),
            data: datas,
            datatype: "html"
        }).done(function(msg){
            window.location.href = Routing.generate('articlecurative',{idmaintenance:idmaintenance});
        }).fail(function (){
            alert("Erreur lors de l'ajout");
        })

    });
});

$(function(){

    var indexdebut = $('#techniquebundle_magasinconso_iNDEXDEBUT');
    var indexfin =$('#techniquebundle_magasinconso_iNDEXFIN');
    var jaugefin =$('#techniquebundle_magasinconso_jAUGEFIN');
    var jaugedebut =$('#techniquebundle_magasinconso_jAUGEDEBUT');
    var consoindex1 =  $("#consoindex1");
    var consojauge1 = $("#consojauge1");
    var controle1 = $("#controle1");

    $(consoindex1).val(parseInt(indexfin.val()) - parseInt(indexdebut.val()));

    $(consojauge1).val(parseInt(jaugedebut.val()) - parseInt(jaugefin.val()));
    $(controle1).val(parseInt(consoindex1.val()) - parseInt(consojauge1.val()));

});

$(function(){

    var indexdebut = $('#techniquebundle_magasinconso_iNDEXDEBUT');
    var indexfin =$('#techniquebundle_magasinconso_iNDEXFIN');
    var jaugefin =$('#techniquebundle_magasinconso_jAUGEFIN');
    var jaugedebut =$('#techniquebundle_magasinconso_jAUGEDEBUT');
    var consoindex =  $("#consoindex");
    var consojauge = $("#consojauge");
    var controle = $("#controle");

    $(indexfin).change(function(){
        $(consoindex).val(parseInt(indexfin.val()) - parseInt(indexdebut.val()))
        $(controle).val(parseInt(consoindex.val()) - parseInt(consojauge.val()))

    });

    $(jaugefin).change(function(){
        $(consojauge).val(parseInt(jaugedebut.val()) - parseInt(jaugefin.val()))
        $(controle).val(parseInt(consoindex.val()) - parseInt(consojauge.val()))
    });
});

function getlastindexjauge(str)
{
    var id = str;
    var datas = "idmagasin="+str;
    $.ajax({
        url:  Routing.generate('getlastindexjauge'),
        type: "GET",
        data: datas,
        context: this,
        error: function () {},
        dataType: 'text',
        success : function (data) {
            var json_obj = $.parseJSON(data);
            console.log(data);
            console.log(json_obj);
            if(json_obj.length === 0)
            {
                $('#techniquebundle_magasinconso_iNDEXDEBUT').val(0);
                $('#techniquebundle_magasinconso_jAUGEDEBUT').val(0);
            }
            else
            {
                $('#techniquebundle_magasinconso_iNDEXDEBUT').val(json_obj[0].INDEXFIN);
                $('#techniquebundle_magasinconso_jAUGEDEBUT').val(json_obj[0].JAUGEFIN);

                alert(json_obj[0].DATECONSO);
            }

        }
    });
}

$(function(){

    var magasinid = $('#techniquebundle_magasinconso_mAGASINID').val();
    var indexfin = $('#techniquebundle_magasinconso_iNDEXFIN');
    var indexdebut = $('#techniquebundle_magasinconso_iNDEXDEBUT');

    getlastindexjauge(magasinid);
    $('#techniquebundle_magasinconso_mAGASINID').change(function(){
        var magasinid = $('#techniquebundle_magasinconso_mAGASINID').val();
        getlastindexjauge(magasinid);
    });

    $(indexfin).change(function(){
        if(indexfin.val() < indexdebut.val())
        {
            alert("l'index fin doit etre superieur ou egal a l'index debut ");
        }
    });

});

$(function(){

   /* $('#techniquebundle_magasinconso_dATECONSO').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY H:m:s'
    });*/
});

$(function(){

   $('#daterealisation').datepicker({
       "dateFormat": 'yy-mm-dd',
   });
    $('#dateimmomodif').datepicker({
        "dateFormat": 'yy-mm-dd',
    });
    $('#datesortie').datepicker({
        "dateFormat": 'yy-mm-dd',
    });
    $('#dateimmo').datepicker({
        "dateFormat": 'yy-mm-dd',
    });
    $('#techniquebundle_maintenancemateriel_dATEMAINTENANCE').datepicker({
        "dateFormat": 'yy-mm-dd',
    });
    $('#techniquebundle_maintenancemateriel_dATEENTREE').datepicker({
        "dateFormat": 'dd-mm-yyyy',
    });
});


$(function(){
    $('#divmaint').hide();
});

function getlastcompteur(str)
{
    var id = str;
    var datas = "idmateriel="+str;
    var ctr =  $("#techniquebundle_materielconso_cTR");
    var qte =  $("#techniquebundle_materielconso_qTE");
    var ctrreel =  $("#compteur");
    var idmateriel =  $("#materielid").val();
    var materielconsopid =  $("#techniquebundle_materielconso_mATERIELCONSOPID");
    $.ajax({
        url:  Routing.generate('getlastcompteur'),
        type: "GET",
        data: datas,
        context: this,
        error: function () {},
        dataType: 'text',
        success : function (data) {
            var json_obj = $.parseJSON(data);//parse JSON
            console.log(data);
            $(materielconsopid).val(json_obj[0].MATERIELCONSOID)
            $('#derniercompteur').val(json_obj[0].compteur);
            $(ctr).change(function(){

                var diff = parseInt(json_obj[0].CTR) - parseInt(ctr.val());
                if(diff > 1000)
                {
                    alert("Difference avec le dernier compteur de plus de 1000");
                }

            });

            $(qte).change(function(){

                if(qte.val() > json_obj[0].CAPACITERESERVOIR)
                {
                    alert("Cette quantite depasse la capacite du reservoir");
                    $(qte).val(0);
                }

            });
            
            $(ctrreel).change(function(){

               /* if( ctrreel.val()  < $('#derniercompteur').val())
                {
                    alert("Le dernier compteur est inferieur a celle du compteur reel. Veuillez verifier la valeur saisi");
                    $(ctrreel).val($('#derniercompteur').val());
                }*/

                getintervention(idmateriel, $(ctrreel).val());

            });
        }
    });
}

$(function(){
    $("#techniquebundle_materielconso_mATERIELID").change(function(){

        var materielid =  $("#techniquebundle_materielconso_mATERIELID").val();
        getlastcompteur(materielid)
    });
});


function getdateconsomag(str)
{
    var id = str;
    var datas = "id="+str;
    var dateconso =  $("#dateconso");
    var dateconso1 =  $("#dateconso1");
    $.ajax({
        url:  Routing.generate('getdateconsomag'),
        type: "GET",
        data: datas,
        context: this,
        error: function () {},
        dataType: 'text',
        success : function (data) {
            var json_obj = $.parseJSON(data);
            console.log(data);
            console.log(json_obj);

            var date = new Date(json_obj[0].dATECONSO['date']);

            dateconso1.val(date.toLocaleDateString());

            if((date.getMonth()+1) < 10)
            {
                dateconso.val(date.getDate()+'-'+'0'+(date.getMonth()+1)+'-'+date.getFullYear());
            }
            else
            {
                dateconso.val(date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear());
            }


        }
    });
}

$(function(){
    var magconso =  $("#techniquebundle_materielconso_mAGASINCONSOID").val();
    getdateconsomag(magconso);

    $("#techniquebundle_materielconso_mAGASINCONSOID").change(function(){

        var magconso =  $("#techniquebundle_materielconso_mAGASINCONSOID").val();
        getdateconsomag(magconso)
    });

    $("#heureconso").change(function(){
        var heure = $("#heureconso").val();
        var date = $("#dateconso").val();

        var dateconso = date+' '+heure;

        $("#techniquebundle_materielconso_dATECONSO").val(dateconso);
    });
});

$(document).ready(function() {

    $("#file").fileinput({
        language: 'fr',
        showUpload: false,
    });
});

function format ( d ) {
    //console.log(d.intervention);
    console.log(d.articles);
    var trs='';
    $.each($(d.articles),function(key,value){
        trs+='<ul>' +
            '<li> '+ d.articles[key]+'</li>'+
            '</ul>';
    })
    // `d` is the original data object for the row
    var datas = "id="+ d.id;

    href = Routing.generate('modifierplanintervention',{id: d.modeleid, idintervention: d.id });
    return '<p><a class="btn btn-warning" href='+href+'>Modifier</a> ' +
        '<p>'+trs+'</p>';
}


$(document).ready(function() {
    var t = 18872;
    var datas = "id="+t;
    var table = $('#example').DataTable({
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "ajax": {
            "url": Routing.generate('getallmodele'),
            "type": "GET",
            "data": function ( d ) {
                return $.extend( {}, d, {
                    "id": $("#modeleid").val()
                } );
            }
        },
        "columns": [
            {
                "class":          'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "compteur"},
            { "data": "action"},
            { "data": "description"},
        ]
    });

    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('details');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('details');
        }
    });
});


$(document).on('ready', function () {
    $('#the-node').contextMenu({
        selector: 'li',

        items: {
            "edit": {
                name: "Modifier",
                icon: "edit",
            },
            "delete": {
                name: "Supprimer",
                icon: "delete",
                callback: function(key, options) {
                    var m = $(this).text();
                    var datas = "iddoc="+m;
                    if (confirm('Vous \u00eates s\u00fbr de vouloir supprimer cet document ??? ')) {
                        $.ajax({
                            type: "POST",
                            url: Routing.generate('supprimerdocmodel'),
                            data: datas,
                            dataType: "html"
                        }).done(function (msg) {
                             location.reload(true);
                        }).fail(function () {
                            alert("Cet materiel est utilis�, elle ne peut donc pas etre supprimer");
                        });
                    }

                }
            },
            "sep1": "---------",
            "quit": {name: "Quitter", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
});

function showmateriel(str1, str2) {
    /* if (str1=="") {
     document.getElementById("txtHint").innerHTML="";
     return;
     }*/
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("txtHint").innerHTML=this.responseText;
        }
    };
    xmlhttp.open("GET",Routing.generate('fitremateriel')+"?q1="+str1+"&q2="+str2,true);
    xmlhttp.send();
}

$(function(){

    var categoriemateriel = $("#categoriemateriel");
    var genremateriel = $("#genremateriel");

    $(categoriemateriel).change(function(){
        var categoriematerielid = $(categoriemateriel).select2('data').id;
        var genrematerielid = $(genremateriel).val();

        //alert(genrematerielid);

        showmateriel(categoriematerielid, genrematerielid);
    });

    $(genremateriel).change(function(){
        var genrematerielid = $(genremateriel).select2('data').id;
        var categoriematerielid = $(categoriemateriel).val();

        //alert(genrematerielid);

        showmateriel(categoriematerielid, genrematerielid);
    });
});

$(document).ready(function () {

    var datemaintenance = $("#datemaintenance");

    $('#techniquebundle_materiel_dATEMISESERVICE').datepicker();
    $(datemaintenance).datepicker({
        "dateFormat": 'yy-mm-dd',
        "setDate": new Date(),
        language: 'fr'
    });

    var date = new Date();

    if((date.getMonth()+1) < 10)
    {
        $(datemaintenance).val(date.getDate()+'-'+'0'+(date.getMonth()+1)+'-'+date.getFullYear());
    }
    else
    {
        $(datemaintenance).val(date.getDate()+'-'+(date.getMonth()+1)+'-'+date.getFullYear());
    }
});


function getintervention(str1, str2) {
    /* if (str1=="") {
     document.getElementById("txtHint").innerHTML="";
     return;
     }*/
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("txtHint").innerHTML=this.responseText;
        }
    }
    xmlhttp.open("GET",Routing.generate('getinterventionmodele')+"?idmateriel="+str1+"&compteur="+str2,true);
    xmlhttp.send();
}

function getinterventionpinc(str1, str2) {
    /* if (str1=="") {
     document.getElementById("txtHint").innerHTML="";
     return;
     }*/
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("interventionmc").innerHTML=this.responseText;
        }
    }
    xmlhttp.open("GET",Routing.generate('getinterventionmodele')+"?idmateriel="+str1+"&compteur="+str2,true);
    xmlhttp.send();
}


function getinterventionmodif(str1,str2) {
    /* if (str1=="") {
     document.getElementById("txtHint").innerHTML="";
     return;
     }*/
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("txtHintmodif").innerHTML=this.responseText;
        }
    }
    xmlhttp.open("GET",Routing.generate('getinterventionmodele')+"?idmateriel="+str1+"&compteur="+str2,true);
    xmlhttp.send();
}

function getquantite(str)
{
    var id = str;
    var datas = "id="+str;
    var qterecom = $("#qterecom");
    $.ajax({
        url:  Routing.generate('getquantitearticle'),
        type: "GET",
        data: datas,
        context: this,
        error: function () {},
        dataType: 'text',
        success : function (data) {
            var json_obj = $.parseJSON(data);//parse JSON
            console.log(data);

            if(json_obj[0].qUANTITE == null)
            {
                $(qterecom).val(0);
            }
            else
            {
                $(qterecom).val(json_obj[0].qUANTITE+" "+json_obj[0].uNITE);
            }
        }
    });
}


$(function(){
    var listearticle = ("#article");

    $("#techniquebundle_maintenancearticle_aRTICLEID").val($(listearticle).val());

    $(listearticle).change(function(){
        $("#techniquebundle_maintenancearticle_aRTICLEID").val($(listearticle).val());

        getquantite($(listearticle).val());
    });
});
$(function(){
    var ctr = $("#compteurmodif");
    var idmateriel = $("#materielid").val();
    var idmaintenance = $("#idmaintenance").val();
    var compteur = $("#compteur");
    var idmateriel1 = $("#idmateriel");

    getinterventionpinc($(idmateriel1).val(), $(compteur).val());

    getinterventionmodif($(idmateriel1).val(),$(ctr).val());
    $(ctr).change(function()
    {
        getinterventionmodif($(idmateriel1).val(),$(ctr).val());
    });
});


function getarticlesinterventions(str)
{
    /* if (str1=="") {
     document.getElementById("txtHint").innerHTML="";
     return;
     }*/
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("articlesinterventions").innerHTML=this.responseText;
        }
    };
    xmlhttp.open("GET",Routing.generate('getarticles')+"?compteur="+str,true);
    xmlhttp.send();
}



/*
function addRowHandlers1() {
    var table1 = document.getElementById("tablemmateriel");
    var rows1 = table1.getElementsByTagName("tr");
    for (i = 0; i < rows1.length; i++) {
        var currentRow1 = table1.rows[i];
        var createClickHandler =
            function(row)
            {
                return function() {
                    var cell = row.getElementsByTagName("td")[0];
                    var id = cell.innerHTML;
                    getarticlesinterventions(id);
                };
            };

        currentRow1.onclick = createClickHandler(currentRow1);
    }
}
window.onload = addRowHandlers1();

$(document).ready(function(){
    $("tr").click(function() {
        $(this).closest("tr").siblings().removeClass("highlighted");
        $(this).toggleClass("highlighted");
    })
});
*/



function clickme(str)
{
    var id = str;
    var variable = $("#desactiver"+str);
    $(variable).val('oui')
}

function checkAllBox() {

    var searchIDs =  $('.checkmaintenance:checked').map(function(){
        checkboxval = $(this).val();
        return checkboxval;
    }).get(); // <----
    for (var i = 0; i < searchIDs.length; i++)
    {
        searchIDs.checked = false;
    }
}

$(document).ready( function () {
    var table = $('#tablegrouping').dataTable({
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [
            [10, 20, 25, -1],
            [10, 20, 25, "All"]
        ],
        "iDisplayLength": 10,
        "language": {
            "search": "",
            "sLengthMenu": "_MENU_",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        },
        rowsGroup: [// Always the array (!) of the column-selectors in specified order to which rows groupping is applied
            0,
            4,
        ],
    })
   /* table.draw(false);
    $('#update_btn').click(function(){
        table.rowsgroup.update();
    })*/
} );

$(document).ready( function () {
    var oTable = $('#tablemaintenacec').dataTable({
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "aaSorting": [[2, 'desc']],
        "aLengthMenu": [
            [10, 20, 25, -1],
            [10, 20, 25, "All"]
        ],
        "iDisplayLength": 10,
        "language": {
            "search": "",
            "sLengthMenu": "_MENU_",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        }
    });

    $("tfoot input").keyup(function () {
        /* Filter on the column (the index) of this element */
        oTable.fnFilter(this.value, $("tfoot input").index(this));
    });
});

$(document).ready( function () {
    var oTable = $('#tablemaintenacep').dataTable({
        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
        "aaSorting": [[2, 'desc']],
        "aLengthMenu": [
            [10, 20, 25, -1],
            [10, 20, 25, "All"]
        ],
        "iDisplayLength": 10,
        "language": {
            "search": "",
            "sLengthMenu": "_MENU_",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        }
    });

    $("tfoot input").keyup(function () {
        /* Filter on the column (the index) of this element */
        oTable.fnFilter(this.value, $("tfoot input").index(this));
    });
});