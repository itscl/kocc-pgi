$(function()
{
    $('#debuttravaux').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY'
    });
    $('#fintravaux').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY'
    });
    $('#reactiv').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY'
    });

    $('#ficheinterventionbundle_ficheintervention_dATEINTERVENTION').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY HH:mm:ss'
    });

    $('#dateinterv').datetimepicker({
        locale: 'fr',
        format: 'DD-MM-YYYY  HH:mm:ss'
    });
});

$(document).ready(function() {

    var url = 'http://srv-pgiweb/kocc-pgi/fiche_intervention/planning_fiches';
    if(window.location.href == url)
    {
        var myevents=[];
        $.ajax
        ({
            type: "GET",
            url: Routing.generate('getplanningfichesattr'),
            async: false,
            success: function (data)
            {
                for(var i = 0; i < data.items.length; i++)
                {
                    var ficheid = data.items[i].FICHEINTERVENTIONID;
                    var editeur = data.items[i].EDITEUR;
                    var typeinterventionid = parseInt(data.items[i].TYPEINTERVENTIONID);
                    var typeintervention = "";
                    switch(typeinterventionid) {
                        case 1:
                            typeintervention = "MECANIQUE";
                            break;
                        case 2:
                            typeintervention = "SOUDURE";
                            break;
                        case 3:
                            typeintervention = "VULCANISATION";
                            break;
                        case 4:
                            typeintervention = "IRRIGATION";
                            break;
                        case 5:
                            typeintervention = "ELECTRICITE";
                            break;
                        case 6:
                            typeintervention = "MENUISERIE";
                            break;
                        case 7:
                            typeintervention = "TRAVAUX DENTRETIEN";
                            break;
                    }
                    var site = data.items[i].SITE;
                    var ferme = data.items[i].FERME;
                    var datestart = data.items[i].DATEINTERVENTION;
                    var datasdatestart = datestart.split(' ');
                    var ymd=datasdatestart[0].split('-');
                    var hm=datasdatestart[1].split(':');
                    var syear = ymd[0];
                    var smonth = ymd[1];
                    var sday = ymd[2];
                    var shour = hm[0];
                    var smin = hm[1];
                    var start = new Date(syear,smonth-1,sday,shour,smin);
                    var end = new Date(syear,smonth-1,sday,parseInt(shour)+1,smin);
                    var myevent = {
                        ficheid: ficheid,
                        editeur: editeur,
                        site: site,
                        ferme: ferme,
                        typeintervention: typeintervention,
                        start: start,
                        end: end
                    };
                    myevents.push(myevent);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                alert("Error: " + errorThrown);
            }
        });

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            droppable: true, // this allows things to be dropped onto the calendar
            drop: function () {
                if ($('#drop-remove').is(':checked')) {
                    $(this).remove();
                }
            },
            events: myevents,
            eventClick: function(calEvent, jsEvent, view) {
                $("#ficheid").val(calEvent.ficheid);
                $("#editeur").val(calEvent.editeur);
                $("#site").val(calEvent.site);
                $("#ferme").val(calEvent.ferme);
                $("#typeintervention").val(calEvent.typeintervention);
                $("#dateinterv").val(calEvent.start.format("DD-MM-YYYY HH:mm:ss"));
                $(this).css('border-color', 'orange');
                $("#ModalModifDateInterv").modal();
            }
        });

    }
});

function openmodaltache(str)
{
    document.getElementById('idtache').value = str;
    $('#listeArticleTache').select2("val",null);
    $("#nomarticle").val('');
    $("#unitearticle").val('');
    $("#qte").val('');
    $("#ModalTacheArticle").modal();
}

function openmodaltacheintervenants(str)
{
    document.getElementById('idtache').value = str;
    $("#ModalTacheIntervenants").modal();
}

function openmodaltachearticle(id, article, qte)
{
    document.getElementById('idtachearticle').value = id;
    $("#nomarticlemodify").val(article);
    $("#qtearticlemodify").val(qte);
    $("#ModalTacheArticleModify").modal();
}

function deletetachearticle()
{
    var id = $("#idtachearticle").val();
    var datas = "id="+id;
    $.ajax
    ({
        type: "POST",
        url: Routing.generate('deletetachearticle'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            $("#ModalTacheArticleModify").modal('hide');
            var elem = document.getElementById(id);
            elem.remove();

        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error: " + errorThrown);
        }
    });

}


function supprimerimage(imageid)
{
    var datas = "imageid=" + imageid;
    $.ajax
    ({
        type: "POST",
        url: Routing.generate('deleteimage'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            location.reload();

        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error: " + errorThrown);
        }
    });

}

function modifytachearticle()
{
    var id = $("#idtachearticle").val();
    var article = $("#nomarticlemodify").val();
    var qte = $("#qtearticlemodify").val();
    var datas = "id="+id +"&qte=" + qte;
    $.ajax
    ({
        type: "POST",
        url: Routing.generate('modifytachearticle'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            $("#ModalTacheArticleModify").modal('hide');
            var elem = document.getElementById(id);
            elem.innerHTML=article+' ( '+qte+' )'+'<button' +
                ' type="button"' + ' class="btn btn-warning btn-circle btn-xs" onclick=" openmodaltachearticle('+id+','+'\''+article+'\''+','+qte+ ');'+'"'+' >'+'<i' +
                ' class="fa fa-edit"'+'></i>'+''+'</button>';

        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error: " + errorThrown);
        }
    });
}

function ajoutertachearticle(str)
{
    var type = $("#type").val();
    var idtache = $("#idtache").val();
    var ficheinterventionid = $("#ficheinterventionid").val();
    var nomarticle = $("#nomarticle").val();
    var unitearticle = $("#unitearticle").val();
    var qte = $("#qte").val();

    var datas ="id=" + ficheinterventionid + "&type=" + type+ "&idtache=" + idtache + "&nomarticle=" + nomarticle + "&unitearticle=" + unitearticle + "&qte=" + qte;

     if(nomarticle === '' || unitearticle === '' || qte === '' )
    {
        $("#ModalTacheArticle").modal('hide');
        $("#modal-warning-article-tache").modal();
    }
    else
    {
        $("#ModalTacheArticle").modal('hide');

        $.ajax({
            type: "POST",
            url: Routing.generate('ajoutertachearticle'),
            data: datas,
            success : function(data){
                var tarid = data.id
                if(data.exist === 0)
                {
                    var elem = document.getElementById('listeArticle'+idtache);
                    if(str === 'mod')
                    {

                        elem.innerHTML = elem.innerHTML + '<ul> <li>'+nomarticle+' ( '+qte+' )'+'</li></ul>';
                    }
                    else
                    {
                        elem.innerHTML=elem.innerHTML + '<ul> <li id="'+tarid+'">'+nomarticle+' ( '+qte+' ) '+'<button' +
                            ' type="button"' + ' class="btn btn-warning btn-circle btn-xs" onclick="' +
                            ' openmodaltachearticle('+data.id+','+'\''+nomarticle+'\''+','+qte+ ');'+'"'+' >'+'<i' +
                            ' class="fa fa-edit"'+'></i>'+''+'</button></li></ul>';
                    }
                    $("#ModalTacheArticle").modal('hide');
                }
                else
                {
                    $("#ModalArticleError").modal();
                }

            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Status: " + textStatus);
            }
        });
    }

}

function modiftacheintervenants()
{
    var idtache = $("#idtache").val();
    var intervenants = $("#intervenants");
    var ficheinterventionid = $("#ficheinterventionid").val();

    var datas ="idtache=" + idtache  + "&intervenants=" + intervenants.val() + "&id=" + ficheinterventionid;

    $("#ModalTacheIntervenants").modal('hide');

    if(intervenants.val()!=='')
    {
        datas = datas +  "&intervenants=" + intervenants.val();

        $.ajax({
            type: "POST",
            url: Routing.generate('modiftacheintervenants'),
            data: datas,
            success : function(){
                $("#intervenants"+idtache).val(intervenants.val());
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Status: " + textStatus);
            }
        });
    }
    else
    {
        $("#modal-warning-article-tache").modal();
    }

}

function supprimertache(str)
{
    bootbox.confirm({
        message: "Voulez vous vraiment supprimer cette tâche ?",
        buttons:
        {
            confirm:
            {
                label: 'Ok',
                className: 'btn-primary'
            },
            cancel:
            {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result)
        {
            if(result==true)
            {
                var datas = "idtache="+str;

                $.ajax({
                    type: "POST",
                    url: Routing.generate('supprimertache'),
                    data: datas,
                    dataType: "html",
                    success : function(){
                        $("#modal-success-delete-tache").modal();
                        var elem = document.getElementById(str);
                        elem.remove();
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus); alert("Error: " + errorThrown);
                    }
                });
            }
        }
    });
}

function supprimerfiche(str)
{
    var datas = "idfiche="+str;

    bootbox.confirm({
        message: "Voulez vous vraiment supprimer cette fiche ?",
        buttons:
        {
            confirm:
            {
                label: 'Ok',
                className: 'btn-primary'
            },
            cancel:
            {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result)
        {
            if(result===true)
            {
                $.ajax({
                    type: "POST",
                    url: Routing.generate('supprimerficheintervention'),
                    data: datas,
                    dataType: "html",
                    success: function ()
                    {
                        $("#modal-success").modal();
                        // location.href = Routing.generate('listeficheintervention');
                        location.reload();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown)
                    {

                    }
                });
            }
        }
    });
}

function realisertache(str)
{
    bootbox.confirm({
        message: "Etes vous sûr de vos choix ?",
        buttons:
        {
            confirm:
            {
                label: 'Ok',
                className: 'btn-primary'
            },
            cancel:
            {
                label: 'Annuler',
                className: 'btn-default'
            }
        },
        callback: function (result)
        {
            if(result==true)
            {
                var num = str.length - 1;
                var datas = "num=" + num;
                var debuttravaux=document.getElementById('debuttravaux').value;
                var fintravaux=document.getElementById('fintravaux').value;
                var duree=document.getElementById('duree').value;
                var observations=document.getElementById('observations').value;
                console.log(duree);
                var id = 1;

                for (i = 0; i < str.length; i++)
                {
                    if (i == str.length - 1)
                    {
                        datas = datas + "&ficheinterventionid=" + str[i];
                        datas = datas + "&debuttravaux=" + debuttravaux + "&fintravaux=" + fintravaux + "&duree=" + duree + "&observations=" + observations;
                        id = str[i];
                    }
                    else
                    {
                        var intervenants = document.getElementById("intervenants" + str[i]).value;
                        var realise = document.getElementById("check" + str[i]).checked;
                        if (intervenants == '' && realise == true)
                        {
                            datas = '';
                            break;
                        }
                        else
                        {
                            datas = datas + "&id" + i + "=" + str[i] + "&intervenants" + str[i] + "=" + intervenants + "&realise" + str[i] + "=" + realise;
                        }
                    }
                }
                if (datas == '')
                {
                    $("#modal-warning").modal();
                }
                else
                {
                    var boutons = document.getElementsByTagName('button');
                    for (var i = 0; i < boutons.length; i++)
                    {
                        boutons[i].disabled = "true";
                        boutons[i].innerText = "Patientez" + "...";
                    }

                    $.ajax
                    ({
                        type: "POST",
                        url: Routing.generate('realisertaches'),
                        data: datas,
                        dataType: "html",
                        success: function ()
                        {
                            location.href = Routing.generate('realiserficheintervention', {
                                'id': id
                            });

                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown)
                        {
                            alert("Status: " + textStatus);
                            alert("Error: " + errorThrown);
                        }
                    });
                }
            }
        }
    });
}


function addtache(str, tech)
{
    var libelle=$("#ficheinterventionbundle_tache_lIBELLE").val();

    if(libelle==='')
    {
        $("#modal-warning-ficheintervention").modal();
    }
    else
    {
        var datas="ficheinterventionid="+str +'&libelle=' + libelle;

        $.ajax
        ({
            type: "POST",
            url: Routing.generate('addtache'),
            data: datas,
            success: function (data)
            {
                if(tech === "notech")
                {
                    var tacheid = data.idtache;
                    var elem = document .getElementById('tabletache');
                    elem.innerHTML=elem.innerHTML+ '<tr id='+tacheid+'>'+' <td>'+libelle+'</td> <td><button' +
                        ' type="button"' + ' class="btn btn-danger btn-sm" id="mod" onclick=" supprimertache('+tacheid+','+str+ ');'+'"'+' >'+'<i' +
                        ' class="fa fa-trash"'+'></i>'+'Supprimer'+'</button></td></tr>';

                    $("#ficheinterventionbundle_tache_lIBELLE").val('');
                    $("#ficheinterventionbundle_tache_iNTERVENANTS").val('');

                    $("#modal-success-tache").modal();
                }
                else
                {
                    location.reload();
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                $("#modal-danger-epi").modal();
            }
        });
    }
}

function addtacherea(str)
{
    var libelle=$("textarea#tache").val();
    if(libelle==='')
    {
        $("#modal-warning-article-tache").modal();
    }
    else
    {
        var datas="ficheinterventionid="+str +'&libelle=' + libelle;

        $.ajax
        ({
            type: "POST",
            url: Routing.generate('addtache'),
            data: datas,
            success: function (data)
            {
                location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                $("#modal-danger-epi").modal();
            }
        });
    }
}

function terminercrea(id)
{

    var mybutton = $('#buttonend');
    mybutton.attr('disabled','disabled');
    mybutton.html('Patientez...');
    var datas="id="+id ;

    $.ajax
    ({
        type: "POST",
        url: Routing.generate('terminercrea'),
        data: datas,
        success: function (data)
        {
            if(data.taches === 0)
            {
                mybutton.html('<i class="fa fa-check" ></i>Terminer');
                $('#buttonend').removeAttr('disabled');
                $("#modal-warning-sans-tache").modal();
            }
            else
            {
                location.href = Routing.generate('listeficheintervention');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("error : Probléme envoi de mail");
        }
    });
}

function openmodalreopenfiche( id)
{
    document.getElementById('ficheid').value = id;
    $("#comment").val('');
    $("#modal-fiche-reopen").modal();
}

function reactiverfiche()
{
    var mybutton = $('#buttonreactiv');
    mybutton.attr('disabled','disabled');
    mybutton.html('Patientez...');

    var id = $("#ficheid").val();
    var comment = $("#comment").val();

    while(comment.includes("'")==true){
        comment = comment.replace("'" ," ");
    }

    if(comment === "")
    {
        $("#modal-warning-reactiver").modal();
    }
    else
    {
        var datas = "id="+id + "&comment=" +comment;

        $("#modal-fiche-reopen").modal('hide');

        $.ajax
        ({
            type: "POST",
            url: Routing.generate('reactiverfiche'),
            data: datas,
            dataType: "html",
            success: function ()
            {
                $("#modal-success-reactiver").modal();
                location.reload();

            },
            error: function (XMLHttpRequest, textStatus, errorThrown)
            {
                alert("Error: " + errorThrown);
            }
        });
    }
}

function cloturerfiche( id)
{
    var fintravaux=document.getElementById('fintravaux').value;
    var debuttravaux=document.getElementById('debuttravaux').value;
    var duree=document.getElementById('duree').value;
    var observations=document.getElementById('observations').value;

    while(observations.includes("'")==true){
        observations = observations.replace("'" ," ");
    }

    var datas = "id="+id + "&fintravaux=" + fintravaux + "&debuttravaux=" + debuttravaux + "&duree=" + duree + "&observations=" + observations;

    $.ajax
    ({
        type: "POST",
        url: Routing.generate('cloturerfiche'),
        data: datas,
        dataType: "html",
        success: function ()
        {
            location.href = Routing.generate('listeficheintervention');
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            alert("Error: " + errorThrown);
        }
    });
}

function annulertache(id)
{
    bootbox.confirm({
        message: "Voulez vous vraiment annuler cette tâche ?",
        buttons:
            {
                confirm:
                    {
                        label: 'Ok',
                        className: 'btn-primary'
                    },
                cancel:
                    {
                        label: 'Annuler',
                        className: 'btn-default'
                    }
            },
        callback: function (result) {
            if (result === true)
            {
                var datas = "idtache="+id;

                $.ajax
                ({
                    type: "POST",
                    url: Routing.generate('annulertache'),
                    data: datas,
                    dataType: "html",
                    success: function ()
                    {
                        var elem1 = document.getElementById('ajoutarticle'+id);
                        elem1.remove();
                        var elem2 = document.getElementById('annularticle'+id);
                        elem2.remove();
                        $("#modal-success-cancel-tache").modal();

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown)
                    {
                        alert("Error: " + errorThrown);
                    }
                });
            }
        }
    });
}

$("#ficheinterventionbundle_ficheintervention_sITEID").change(function(){
    $('#fermesite').select2("val",null);
});
