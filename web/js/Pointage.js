/**
 * Created by akdaho on 24/11/2017.
 */

$(document).ready(function() {
     $("#file").fileinput({
         language: 'fr',
         showUpload: false,
     });

    $('#tcontractuel').DataTable({})
    $('#tpointage').DataTable({
        'rowsGroup': [0,1,11]
    })

 });

$(function(){
    $("#pointagebundle_pointageligne_aBSENT").change(function(){
        if (document.getElementById('pointagebundle_pointageligne_aBSENT').checked)
        {
            document.getElementById("pointagebundle_pointageligne_hEURED").disabled = true;
            document.getElementById("pointagebundle_pointageligne_hEUREFIN").disabled = true;
        }
        else
        {
            document.getElementById("pointagebundle_pointageligne_hEURED").disabled = false;
            document.getElementById("pointagebundle_pointageligne_hEUREFIN").disabled = false;
        }
    })

});


$(function(){

    var currentdate = new Date().toLocaleString();

    $("#datecreation").val(currentdate);

    $("#pointagebundle_fichepointage_dATEDEBUT").datepicker({
        "dateFormat": 'yy-mm-dd',
        "setDate": new Date(),
        language: 'fr'
    });

    $("#pointagebundle_fichepointage_dATEFIN").datepicker({
        "dateFormat": 'yy-mm-dd',
        "setDate": new Date(),
        language: 'fr'
    });

    $("#pointagebundle_pointageligne_hEURED").datetimepicker({
        locale: 'fr',
        format:'LT',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });

    /*$("#pointagebundle_pointageligne_jOUR").datetimepicker({

        locale: 'fr',
        format:'LT',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }

    })*/

    $("#pointagebundle_pointageligne_hEUREFIN").datetimepicker({
        locale: 'fr',
        format:'LT',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });

});

$(function(){
    $("#pointagebundle_pointageligne_hORAIRE").change(function(){
        var str1 = $("#pointagebundle_pointageligne_hORAIRE").val();
        var str2 = "h";
        var str3 = "-";

        if(str1.indexOf(str2) != -1 && str1.indexOf(str3) != -1)
        {
            $("#formath").hide();
            console.log("ok");
        }
        else
        {
            $("#formath").show();
            console.log(str2 + " not found");
        }
    });
});

$(document).ready(function () {

    var matricule = ("#matricule");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name+' ('+item.nom+' '+item.prenom+') ';
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(matricule).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('listcontractuel'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },


        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });

    $(matricule).change(function(){
        var id = $(matricule).select2('data').id;
        var nom = $(matricule).select2('data').nom;
        var prenom = $(matricule).select2('data').prenom;

        $("#nomcomplet").val(nom+' '+prenom)
        $("#pointagebundle_pointageligne_cONTRACTUELID").val(id)

    });
});

/* ************************************************************ Ferme ************************************ */
$(document).ready(function () {

    var ferme = $("#ferme");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(ferme).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getferme'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term,
                    siteid: $("#site").val()
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(ferme).change(function(){
        var id = $(ferme).select2('data').id;
        var fermelibelle = $(ferme).select2('data').name;

       // $("#nomcomplet").val(nom+' '+prenom)
        $("#pointagebundle_pointageligne_fERMEID").val(id)

    });
});

/* ************************************************************ Cycle culturale ************************************ */
$(document).ready(function () {

    var cyclecultural = ("#cyclecultural");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(cyclecultural).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getcyclecultural'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term, // search term
                    fermeid: $("#ferme").val(), // search term
                    page: 10 // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },
        initSelection: function (element, callback) {
            var data = {id:  $("#pointagebundle_pointageligne_oPID").val(), name:  $("#cycleculturallibelle").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });

    $(cyclecultural).change(function(){
        var id = $(cyclecultural).select2('data').id;
        var op = $(cyclecultural).select2('data').name;
        var culturelibelle = $(cyclecultural).select2('data').culture;
        var cultureid = $(cyclecultural).select2('data').cultureid;
        var ferme = $(cyclecultural).select2('data').ferme;
        var fermeid = $(cyclecultural).select2('data').fermeid;

        $("#formculture").hide();
        $("#formeferme").hide();

       // $("#nomcomplet").val(nom+' '+prenom)
        $("#pointagebundle_pointageligne_oPID").val(id);
        $("#pointagebundle_pointageligne_cULTUREID").val(cultureid);
        $("#pointagebundle_pointageligne_fERMEID").val(fermeid);
        $("#cycleculturallibelle").val(op);

        $("#formculture1").show();
        $("#culture1").val(culturelibelle);

        $("#formeferme1").show();
        $("#ferme1").val(ferme);


    });
});

/* ************************************************************ Culture ************************************ */
$(document).ready(function () {

    var culture = ("#culture");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(culture).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getculture'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(culture).change(function(){
        var id = $(culture).select2('data').id;
        var culturelibelle = $(culture).select2('data').name;

        // $("#nomcomplet").val(nom+' '+prenom)
        $("#pointagebundle_pointageligne_cULTUREID").val(id)

    });
});

$(document).ready(function () {

    var sgeste = ("#sgeste");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name+' ('+item.type+') ';
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(sgeste).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('pointageligne_getsgeste'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },


        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });

    $(sgeste).change(function(){
        var id =$(sgeste).val()
        var libelle = $(sgeste).select2('data').name;
        var code = $(sgeste).select2('data').type;

        $("#pointagebundle_pointageligne_gESTEID").val(id)

    });
});