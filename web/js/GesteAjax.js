/**
 * Created by akdaho on 13/01/2017.
 */

function updategeste(str)
{
    var id = str;
    var code = $("#code"+str).val();
    var libelle = $("#libelle"+str).val();
    var dateinvalide = $("#dateinvalide"+str).val();
    var typegeste = $("#typegeste"+str).val();

    var datas = "code="+code+"&libelle="+libelle+ "&dateinvalide="+dateinvalide+ "&typegeste="+typegeste+ "&idgeste="+str;
    $.ajax({
        type: "POST",
        url: Routing.generate('modifier'),
        data: datas,
        dataType: "html"
    }).done(function (msg) {
        location.reload(true);
    }).fail(function () {
        alert("error");
    });
}