/**
 * Created by akdaho on 08/02/2017.
 */

/* ************************************************************ Categorie materiel ************************************ */
$(document).ready(function () {

    var categorie = $("#categoriemateriel");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;

    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(categorie).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('listecategoriemateriel'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    page: page
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },

        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

});
/* ************************************************************ Fin Categorie materiel ************************************ */

/* ************************************************************ genre materiel ************************************ */
$(document).ready(function () {

    var genre = $("#genremateriel");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;

    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(genre).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('listegenremateriel'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    page: page,
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },

        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

});
/* ************************************************************ Fi genre materiel ************************************ */

/* ************************************************************ materiel ************************************ */
$(document).ready(function () {

    var materiel = $("#materiel");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;

    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(materiel).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('listemateriel'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    page: page,
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },

        initSelection: function (element, callback) {
            var data = {id: 1, name:  $("#materielname").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    var materielid = $(materiel).select2('data').id;
    var materielname = $(materiel).select2('data').name;
    var compteur = $("#compteur");

    getintervention(materielid, $(compteur).val());

    $(materiel).change(function(){
        var materielid = $(materiel).select2('data').id;
        var materielname = $(materiel).select2('data').name;
        var compteur = $("#compteur");

        $("#materielid").val(materielid);
        $("#techniquebundle_maintenancemateriel_mATERIELID").val(materielid);
        $("#materielname").val(materielname);
        getlastcompteur(materielid);
        getintervention(materielid, $(compteur).val());
        // $("magasinconsoselect").val(text);
    });

    $(compteur).change(function(){
        var materielid = $(materiel).select2('data').id;
        var materielname = $(materiel).select2('data').name;
        var compteur = $("#compteur");
        var derniercompteur = $("#derniercompteur");

        getintervention(materielid, $(compteur).val());

        /*if($(compteur).val() < $(derniercompteur).val())
        {
            alert("Compteur saisi ne peut pas être au dernier compteur rélévé")
            $(compteur).val($(derniercompteur).val())
        }*/

    });

});
/* ************************************************************ Fin materiel ************************************ */

/* ************************************************************ Tout les articles pieces detachés ************************************ */
$(document).ready(function () {

    var selarticle = $("#selarticle");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name+ " " +"("+ item.type+ ")";

    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(selarticle).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getarticle'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    //id: $("#modeleid").val(),
                    page: 10,
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;
                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },
            cache: true
        },

        initSelection: function (element, callback) {
            var data = {id: 1, name:  $("#techniquebundle_articleoperationint_dESARTICLE").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(selarticle).change(function(){
        var refarticle = $(selarticle).select2('data').id;
        var desarticle = $(selarticle).select2('data').name;
        var type = $(selarticle).select2('data').type;

        $("#techniquebundle_articleoperationint_rEFARTICLE").val(refarticle);
        $("#techniquebundle_articleoperationint_dESARTICLE").val(desarticle);

        $("#techniquebundle_maintenancearticler_rEFARTICLE").val(refarticle);
        $("#techniquebundle_maintenancearticler_dESARTICLE").val(desarticle);

        if(type == "LUBRIFIANTS")
        {
            $("#techniquebundle_articleoperationint_uNITE").val("LITRE");
        }
        else
        {
            $("#techniquebundle_articleoperationint_uNITE").val("PIECE");
        }


        $("#techniquebundle_maintenancearticle_rEFARTICLE").val(refarticle);
        $("#techniquebundle_maintenancearticle_dESARTICLE").val(desarticle);

        /*$("#techniquebundle_maintenancearticler_rEFARTICLE").val(refarticle);
        $("#techniquebundle_maintenancearticler_dESARTICLE").val(desarticle);*/
        
    });

});
/* ************************************************************ Fin Tout les articles pieces detachés ************************************ */

/* ************************************************************ Tout les articles maintenance ************************************ */
/*$(document).ready(function () {

    var articler = $("#articler");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name+ " " +"("+ item.id+ ")";

    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(articler).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('listearticle'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    materielid: $("#idmateriel").val(),
                    page: page,
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },

        /!*initSelection: function (element, callback) {
            var data = {id: 1, name:  $("#techniquebundle_interventionarticle_dESARTICLE").val()};
            callback(data);
        },*!/
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(articler).change(function(){
        var refarticle = $(articler).select2('data').id;
        var desarticle = $(articler).select2('data').name;

        $("#techniquebundle_maintenancearticle_rEFARTICLE").val(refarticle);
        $("#techniquebundle_maintenancearticle_dESARTICLE").val(desarticle);

        $("#techniquebundle_maintenancearticler_rEFARTICLE").val(refarticle);
        $("#techniquebundle_maintenancearticler_dESARTICLE").val(desarticle);
        
    });

});*/
/* ************************************************************ fin Tout les articles maintenance ************************************ */

/* ************************************************************ Culture ************************************ */
$(document).ready(function () {

    var culturevariete = ("#culturevariete");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(culturevariete).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getculturevariete'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        initSelection: function (element, callback) {
            var data = {id: $("#culturebundle_culture_cULTUREVARIETEID").val(), name:  $("#nameculturevariete").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });
});
/* ************************************************************ FIn Culture ************************************ */

/* ************************************************************ Culture variete ligne ************************************ */
$(document).ready(function () {

    var varieteligne = ("#varieteligne");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(varieteligne).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getvarieteligne'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term, // search term
                    culturevarieteid: $("#culturebundle_culture_cULTUREVARIETEID").val(),
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        initSelection: function (element, callback) {
            var data = {id: $("#culturebundle_culture_cULTUREVARIETELIGNEID").val(), name:  $("#namevarieteligne").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });
});
/* ************************************************************ fin Culture variete ligne ************************************ */

/* ************************************************************ Famille culture ************************************ */
$(document).ready(function () {

    var familleculture = ("#familleculture");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(familleculture).select2({
        placeholder: "",
        language: "fr",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getfamilleculture'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,

                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        initSelection: function (element, callback) {
            var data = {id: $("#culturebundle_culture_fAMILLECULTUREID").val(), name:  $("#namefamilleculture").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results

    });

    /* $(culture).change(function(){
     var id = $(culture).select2('data').id;
     var famillec = $(culture).select2('data').type;

     $("#agrobundle_itkismr_cULTUREID").val(id);

     $("#familleculture").val(famillec);

     $("#modal-warning1").modal();

     });*/

});
/* ************************************************************ fin Famille cultur ************************************ */

//coumba
function select2CultureVariete(culturevariete)
{
    var cv = $("#cv");
    var cvfield = $("#agrobundle_itkism_cULTUREID");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name+ " " +"("+ item.codevariete+ ")";
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(cv).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getallculturevariete'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    page: page
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        //initSelection: function (element, callback) {
        //    var data = {id: 1, name: culturevariete };
        //    callback(data);
        //},
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
}

//coumba
//Liste des OP
$(document).ready(function () {

    var newop = $("#newop");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name+ " " +"("+ item.ilot+ ")" +" " + "("+ item.culture+ ")";
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(newop).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getallop'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    fermeid: $("#agrobundle_itkism_fERMEID").val(),
                    page: page
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        //initSelection: function (element, callback) {
        //    var data = {id:  $("#agrobundle_itkism_cYCLECULTURALID").val(), name:  $("#newop1").val()};
        //    callback(data);
        //},
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(newop).change(function()
    {
        var fermeid = $("#agrobundle_itkism_fERMEID").val();
        var opid = $(newop).select2('data').id;
        getinstructiondiffuseop(opid);
        window.location.href = Routing.generate('modifieritkism', {fermeid:fermeid,opid:opid})
    });
});

//coumba
//Liste des Cultures varietes
$(document).ready(function () {

    var cv = $("#cv");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name+ " " +"("+ item.codevariete+ ")";
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(cv).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getallculturevariete'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    page: page
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        //initSelection: function (element, callback) {
        //    var data = {id:  $("#agrobundle_itkism_cULTUREID").val(), name:  $("#cv1").val()};
        //    callback(data);
        //},
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(cv).change(function()
    {
        var id = $(cv).select2('data').id;
        getfamilleculture(id);
        $("#modal-warning1").modal();
    });
});

//coumba
function select2lotsemence()
{
    var ls = $("#ls");
    var array= [];

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(ls).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getalllotsemence'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                array = data.items;
                return { results: data.items };
            },
            cache: true
        },
        //initSelection: function (element, callback) {
        //    var data = {id:  $("#ls").val(), name:  $("#ls1").val()};
        //    callback(data);
        //},
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(ls).change(function()
    {
        var cv='';
        var unite='';
        var libelle='';
        for(var i=0; i<array.length; i++)
        {
            if(array[i]['id']==ls.val())
            {
                $("#cvsem").val(array[i]['CULTUREVARIETE']);
                $("#unite").val(array[i]['UNITE']);
                $("#libelle").val(array[i]['name']);
                $("#semencelotid").val(array[i]['id']);
            }
        }
    });
}

//coumba
function select2lotsemenceimporte()
{
    var ls = $("#ls");
    var array= [];

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name+ " " +"  --  ("+ item.site+ ")";
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(ls).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getalllotsemenceimporte'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                array = data.items;
                return { results: data.items };
            },
            cache: true
        },
        //initSelection: function (element, callback) {
        //    var data = {id:  $("#agrobundle_itkism_cYCLECULTURALID").val(), name:  $("#op1").val()};
        //    callback(data);
        //},
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(ls).change(function()
    {
        var cv='';
        var unite='';
        var libelle='';
        for(var i=0; i<array.length; i++)
        {
            if(array[i]['id']==ls.val())
            {
                $("#cvsem").val(array[i]['code']);
                $("#unite").val(array[i]['U_Intitule']);
                $("#libelle").val(array[i]['name']);
                $("#semencelotid").val(array[i]['id']);
            }
        }
    });
}
//coumba
//Liste des lot de semence
$(document).ready(function () {

    var ls = $("#ls");
    var array= [];
    //var checked =document.getElementById('checkedlot').checked;

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(ls).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getalllotsemence'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                array = data.items;
                return { results: data.items };
            },
            cache: true
        },
        initSelection: function (element, callback) {
            var data = {id:  $("#ls2").val(), name:  $("#ls1").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(ls).change(function()
    {
        var cv='';
        var unite='';
        var libelle='';
        for(var i=0; i<array.length; i++)
        {
            if(array[i]['id']==ls.val())
            {
                $("#cvsem").val(array[i]['CULTUREVARIETE']);
                $("#unite").val(array[i]['UNITE']);
                $("#libelle").val(array[i]['name']);
                $("#semencelotid").val(array[i]['id']);
            }
        }
    });
});

//coumba
//Liste des lot de semence importes
$(document).ready(function () {

    var lsr = $("#lsr");
    var array= [];
    //var checked =document.getElementById('checkedlot').checked;

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(lsr).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getalllotsemenceimporte'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                array = data.items;
                return { results: data.items };
            },
            cache: true
        },
        initSelection: function (element, callback) {
            var data = {id:  $("#lsr2").val(), name:  $("#lsr1").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(lsr).change(function()
    {
        var cv='';
        var unite='';
        var libelle='';
        for(var i=0; i<array.length; i++)
        {
            if(array[i]['id']==lsr.val())
            {
                $("#cvsem").val(array[i]['CULTUREVARIETE']);
                $("#unite").val(array[i]['UNITE']);
                $("#libelle").val(array[i]['name']);
                $("#semencelotid").val(array[i]['id']);
            }
        }
    });
});

//coumba
//Liste des intrants
$(document).ready(function () {

    var intrant = $("#intrant");
    var array =[];

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(intrant).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getallintrant'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    page: page
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                array = data.items;
                return { results: data.items };
            },
            cache: true
        },
        initSelection: function (element, callback) {
            var data = {id:  $("#intrant2").val(), name:  $("#intrant1").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(intrant).change(function()
    {
        for(var i=0; i<array.length; i++)
        {
            if(array[i]['id']==intrant.val())
            {
                $("#uniteint").val(array[i]['UNITE']);
                $("#prixunit").val(array[i]['PRIXUNITAIRE']);

            }
        }
    });
});

//coumba
//Liste des materiels
$(document).ready(function () {

    var materielepi = $("#materielepi");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name+ " - "+item.CODE;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(materielepi).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getallmateriel'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    page: page
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        initSelection: function (element, callback) {
            var data = {id:  $("#materiel2").val(), name:  $("#materiel1").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});


//coumba
//Liste des appareils
$(document).ready(function () {

    var appareil = $("#appareil");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name+ " - "+item.CODE;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(appareil).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getallappareil'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    page: page
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        initSelection: function (element, callback) {
            var data = {id:  $("#appareil2").val(), name:  $("#appareil1").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});


//coumba
//Liste des epi
$(document).ready(function () {

    var epi = $("#epi");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(epi).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getallepi'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        //initSelection: function (element, callback) {
        //    var data = {id:  $("#epi2").val(), name:  $("#epi1").val()};
        //    callback(data);
        //},
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});

//coumba
//Liste des UTL
$(document).ready(function () {

    var newutl = $("#newutl");
    var array = [];

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(newutl).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getallutl'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term, // search term
                    fermeid: $("#agrobundle_itkitl_fERMEID").val()
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                array = data.items;
                return { results: data.items };
            },
            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(newutl).change(function()
    {
        for(var i=0; i<array.length; i++)
        {
            // console.log(newutl.val());
            // console.log(array[i]['id']);
            if(parseInt(array[i]['id']) === parseInt(newutl.val()))
            {
                console.log(newutl.val());
                $("#uniteutl").val(array[i]['UNITE']);
                $("#cultureutl").val(array[i]['CULTURE']);
            }
        }
    });
});

//coumba
//Liste des cibles
$(document).ready(function () {

    var cible = $("#cible");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(cible).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getallcible'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term
                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                return { results: data.items };
            },
            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });
});

//coumba
//Liste des intrants itkitl
$(document).ready(function () {

    var intrant = $("#intrantutl");
    var array =[];

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(intrant).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getallintrantitkitl'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    motcle: term, // search term
                    cultureid: $("#cultureid").val(),
                    page: page
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                array = data.items;
                page = page || 1;
                return {
                    results: data.items,
                    more: (page * 10) < data.count
                };
            },
            cache: true
        },
        // initSelection: function (element, callback) {
        //     var data = {id:  $("#intrant2").val(), name:  $("#intrant1").val()};
        //     callback(data);
        // },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(intrant).change(function()
    {
        for(var i=0; i<array.length; i++)
        {
            if(array[i]['id']===intrant.val())
            {
                console.log(array[i]);
                if(array[i]['TABLEREF']=== "HEURE")
                {
                    document.getElementById('nbapp').style.visibility = 'hidden';
                    document.getElementById('matact').style.visibility = 'hidden';
                    document.getElementById('unite').value = 'h/hl';
                    document.getElementById('uniteprix').value = 'heure';
                }
                else if(array[i]['TABLEREF']=== "PHYTO")
                {
                    document.getElementById('nbapp').style.visibility = 'visible';
                    document.getElementById('nbapplimaxi').value = array[i]['NBAPPLIMAXI'];
                    document.getElementById('matact').style.visibility = 'visible';
                    document.getElementById('matactives').value = array[i]['MATIEREACTIVE'];
                    document.getElementById('unite').value = array[i]['UNITE'];
                    // document.getElementById('uniteprix').value = (document.getElementById('unite').value).split("/")[0];
                }

                document.getElementById('doseminihl').value = array[i]['DOSEMINIHL'];
                document.getElementById('dosemaxihl').value = array[i]['DOSEMAXIHL'];
                document.getElementById('doseutl').value = array[i]['DOSEMINIHL'];
                document.getElementById('dosemini').value = array[i]['DOSEMINI'];
                document.getElementById('dosemaxi').value = array[i]['DOSEMAXI'];

                var datas = "unite=" +  array[i]['UNITE'] + "&dose=" + array[i]['DOSEMINIHL'] + "&bouillie=" + $("#bouillie").val();

                $.ajax({
                    url:  Routing.generate('qtetotutl'),
                    type: "GET",
                    data: datas,
                    context: this,
                    error: function () {},
                    dataType: 'json',
                    success : function (data) {
                        $("#qtetot").val(data[0]["RESULT"]);
                    }
                });

                break;
            }
        }
    });
});

//coumba
//Liste des articles fiche intervention
$(document).ready(function () {

    var listeArticle = $("#listeArticleTache");
    var array =[];

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;
    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(listeArticle).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getarticlegcm'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term // search term

                };
            },
            results: function (data) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter the remote JSON data
                array = data.items;
                //page = data.count;
                //page = page || 1;

                return {
                    results: data.items
                    //more: (page * 10) < data.count
                };
            },
            cache: true
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(listeArticle).change(function()
    {
        for(var i=0; i<array.length; i++)
        {
            if(array[i]['id']===listeArticle.val())
            {
                $("#nomarticle").val(array[i]['name']);
                $("#unitearticle").val(array[i]['unite']);
            }
        }
    });
});

$(document).ready(function () {

    var instructionid = $("#instructionid");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.id+ " " +"("+ item.name+ ")";

    }

    function formatRepoSelection(item) {
        return item.id;
    }

    $(instructionid).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getinstructiondiffuse'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term, // search term
                    fermeid: $("#ferme").val()
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,
                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        /*initSelection: function (element, callback) {
         var data = {id: 1, name:  $("#techniquebundle_interventionarticle_dESARTICLE").val()};
         callback(data);
         },*/
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(instructionid).change(function(){
        var data = $(instructionid).select2('data')
        $("#instr").val(data.id+'@'+data.name);
    });
});

$(document).ready(function () {

    var instructionid = $("#instructionutlid");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.id;

    }

    function formatRepoSelection(item) {
        return item.id;
    }

    $(instructionid).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getinstructiondiffuseutl'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term, // search term
                    fermeid: $("#ferme").val()
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,
                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        /*initSelection: function (element, callback) {
         var data = {id: 1, name:  $("#techniquebundle_interventionarticle_dESARTICLE").val()};
         callback(data);
         },*/
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(instructionid).change(function(){
        var data = $(instructionid).select2('data')
        $("#instr").val(data.id+'@'+data.name);
    });
});

$(document).ready(function () {

    var fermesite = $("#fermesite");

    function formatRepo(item) {
        if (!item.id) return item.name;
        return item.name;

    }

    function formatRepoSelection(item) {
        return item.name;
    }

    $(fermesite).select2({
        placeholder: "",
        ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
            url: Routing.generate('getferme'),
            dataType: 'json',
            quietMillis: 250,
            data: function (term) {
                return {
                    motcle: term, // search term
                    siteid: $("#ficheinterventionbundle_ficheintervention_sITEID").val()
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                page = page || 1;

                return {
                    results: data.items,
                    more: (page * 10) < data.count
                };
            },

            cache: true
        },

        initSelection: function (element, callback) {
            var data = {id: $("#ficheinterventionbundle_ficheintervention_fERMEID").val(), name:  $("#fermename").val()};
            callback(data);
        },
        formatResult: formatRepo, // omitted for brevity, see the source of this page
        formatSelection: formatRepoSelection,  // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
    });

    $(fermesite).change(function(){
        var data = $(fermesite).select2('data');
        $("#ficheinterventionbundle_ficheintervention_fERMEID").val(data.id);
    });
});